<?php
/** 
* guests.php
*/
include_once('config.php');
include_once('functions.php');


function getGuestList() {
	$filter = '';
	if(isset($_POST['start']) && $_POST['start'] != '') {
		$filter = " and a.lastname like '{$_POST['start']}%' ";
		$filter .= " or a.lastname like '{$_POST['start']}%' ";
	} 
	$sql = "select distinct a.guest_id, a.lastname, a.firstname, 
			a.address as 'address', concat_ws(' ', a.mobile, a.phone, a.company_mobile) as 'contact info'
			from guests a, reservations b
			where a.guest_id=b.guest_id 
			$filter
			order by a.lastname, a.firstname
			";
	$res = mysql_query($sql) or die($sql);

	$guestlist = '<ol>';
	while(list($gid, $lastname, $firstname, $address, $contact) = mysql_fetch_row($res)) {
		$firstname = ucwords(strtolower(trim($firstname)));
		$lastname = ucwords(strtolower(trim($lastname)));
		$address = ucwords(strtolower(trim($address)));
		if ($lastname != '' && $firstname != '') { 
			$guestlist .= "<li><a href='$gid'><span>$lastname, $firstname</span>
			<span>$address</span><span>$contact</span></a></li>";
		}
	}		
	$guestlist .= '</ol>';
	return $guestlist;
}

function getGuestHistory() {
	$retval = '';
	if(isset($_POST['gid']) && $_POST['gid'] != '') {
		$gid = $_POST['gid'];
		$sql = " select date_created, reserve_date, reserve_code, 
		reserve_fee, payment_type, status, created_by
		from reservations 
		where guest_id=$gid
		order by date_created desc ";
		$res = mysql_query($sql) or die($sql);
		$retval = '<h4>Reservations made by: ' . ucwords(getGuestName($gid)) . '</h4>';
		$retval.='<dl>';
		while(list($created, $reserve, $code, $fee, $payment, $status, $userid)=mysql_fetch_row($res)) {
			$receiver = getUserName($userid);
			$retval.="<dt>$code</dt>";
			$retval.="<dd>
				<div><span>Record Created:</span> <span>$created</span></div>
				<div><span>Reservation For:</span><span> $reserve</span></div>
				<div><span>Fee Received:</span><span> $fee</span></div>
				<div><span>Payment Type:</span><span> $payment</span></div>
				<div><span>Status:</span><span> $status</span></div>
				<div><span>Received By:</span><span> $receiver</span></div>
			</dd>";
		}
		$retval.='</dl>';
		
	} else {
		$retval.='Error getting historical data. Please select a valid guest entry from the list';
	}
	echo $retval;
}

function getUserName($uid) {
	if ($uid) {
		$sql = "select fullname from users where user_id=$uid";
		$res = mysql_query($sql);
		$row = mysql_fetch_row($res);
		return $row[0];
	} else {
		return 'Website';
	}
}

function getGuestName($gid) {
	$sql = "select concat_ws(' ', firstname, lastname) from guests where guest_id=$gid";
	$res = mysql_query($sql);
	$row = mysql_fetch_row($res);
	return $row[0];
}

if(isset($_POST['hist']) && $_POST['hist'] === '1') {
	echo getGuestHistory();
} else {
	echo getGuestList();
}

?>
