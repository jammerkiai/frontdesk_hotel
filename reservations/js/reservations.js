/**
* reservations.js
*/

$(function() {
	var dates = $( "#dateStart, #dateEnd" ).datepicker({
		defaultDate: "+1d",
		changeMonth: true,
		numberOfMonths: 2,
		onSelect: function( selectedDate ) {
			var option = this.id == "dateStart" ? "minDate" : "maxDate",
				instance = $( this ).data( "datepicker" );
				date = $.datepicker.parseDate(
					instance.settings.dateFormat ||
					$.datepicker._defaults.dateFormat,
					selectedDate, instance.settings );
			dates.not( this ).datepicker( "option", option, date );
		}
	});

	$('#view')
		.button({ icons: {primary: "ui-icon-search"} })
		.click(function(e){
			e.preventDefault();
			if ( $('#dateStart').val() != '') {
				$.post('results.php', 
					{	from: $('#dateStart').val(), 
						to: $('#dateEnd').val(),
						status: $('#status').val(),
						act: 'view'
					},
					function(data){
						$('#reportData').html(data);
					});
			} else {
				alert('Please enter a start date.');
			}
		});
	$('#print')
		.button({ icons: {primary: "ui-icon-print"} })
		.click(function(e){
			e.preventDefault();
			window.print();
		});
	$('#clear')
		.button({ icons: {primary: "ui-icon-refresh"} });
	$('#guests')
		.button({ icons: {primary: "ui-icon-person"} })
		.click(function(e){
			e.preventDefault();
			document.location.href='webreports.php';
		});
	
});
