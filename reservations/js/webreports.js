$(function() {
	$("#guests")
		.button({ icons: {primary: "ui-icon-person"} })
		.click(function(e){
			e.preventDefault();
			$.post('guests.php', 
				{	
				},
				function(data){
					$('#reportData').html(data);
				});
		});
	$(".alphaButton")
		.click(function(e){
			e.preventDefault();
			$.post('guests.php',
				{
					start: $(this).html()
				},
				function(data){
					$('#reportData').html(data);
				}
			);
		});
	$("ol > li > a").live(
		'click', 
		function(e){
			e.preventDefault();
			$.post('guests.php',
				{
					hist: 1,
					gid: $(this).attr('href')
				},
				function(data){
					$('#reportData').html(data);
				}
			);
		});
	
	$('#print')
		.button({ icons: {primary: "ui-icon-print"} })
		.click(function(e){
			e.preventDefault();
			window.print();
		});
	$('#clear')
		.button({ icons: {primary: "ui-icon-refresh"} });
	$('#reservations')
		.button({ icons: {primary: "ui-icon-person"} })
		.click(function(e){
			e.preventDefault();
			document.location.href='index.php';
		});
});