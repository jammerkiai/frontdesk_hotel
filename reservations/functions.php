<?php
/**
* functions.php 
*/

class reservations {
	
	public function reservations($options = array()) {
		$this->status = 'Active';
		foreach ($options as $index => $value) {
			$this->$index = $value;
		}
		$this->getExactDates();
	}

	private function reformatDate($date) {
		$tmp = explode('/', $date);
		return $tmp[2] . '-' . $tmp[0] . '-' . $tmp[1];
	}
	private function getExactDates() {
		$this->from = $this->reformatDate($this->from);
		$fromTime = ' 00:00:01';
		$toTime = ' 23:59:59';
		if ($this->to == '') {
			$this->to = $this->from . $toTime;
		} else {
			$this->to = $this->reformatDate($this->to);
			$this->to = $this->to . $toTime;
		}
		$this->from .= $fromTime;
	}

	public function getListing() {
		$sql = "select a.reserve_code, concat_ws(' ', b.firstname, b.lastname),
				a.reserve_date, a.pax, a.reserve_fee, a.payment_type, a.notes, 
				a.status, a.pickup, a.date_created, c.fullname
				from reservations a, guests b, users c 
				where a.guest_id=b.guest_id and a.created_by=c.user_id and
				a.reserve_date between '{$this->from}' and '{$this->to}'
				and a.status='{$this->status}'
				order by a.reserve_date
				";
		$res = mysql_query($sql) or die($sql);
		$retval = "<table id='mytable'>";
		$header = "<tr class='labelHeader'>
					<td>CODE</td>
						<td>GUEST</td>
						<td>DEPOSIT</td>
						<td>PAYMENT TYPE</td>
						<td>ROOMS</td>
						<td>NOTES</td>
						<td>STATUS</td>
						<td>PICKUP INSTRUCTIONS</td>
						<td>ENTERED BY</td>
					</tr>";
		$thisdate = '';
		while(list($rcode, $guest, $date, $pax, 
					$fee, $type, $notes, $status, 
					$pickup, $datecreated, $name) = mysql_fetch_row($res)) {
			if ($thisdate != $date) {
				$thisdate = $date;
				$retval.="<tr class='dateHeader'><td colspan='2'>DATE OF ARRIVAL</td><td colspan='7'>$date</td></tr>";
				$retval.=$header;
			}
			$rooms= $this->roomReserved($rcode);
			$retval.="<tr class='dataRow'>
						<td>$rcode</td>
						<td>$guest</td>
						<td>$fee</td><td>$type</td><td>$rooms</td><td>$notes</td>
						<td>$status</td><td>$pickup</td><td>$name</td>
					</tr>";
		}
		$retval .= "</table>";
		return $retval;
	}
	
	public static function roomReserved($code) {
		$sql = "select b.door_name, a.checkin, a.checkout, a.deposit, a.status
			from reserve_rooms a, rooms b
			where a.room_id=b.room_id and a.reserve_code='$code'
		";
		$res = mysql_query($sql);
		$table = "<table class='myroom'>";
		while(list($door, $in, $out, $dep, $status)=mysql_fetch_row($res)) {
			$table.= "<tr><td>$door</td><td>$in</td><td>$out</td><td>$dep</td><td>$status</td></tr>";
		}
		$table.= "</table>";
		return $table;
	}
		
}
?>