<?php
/**
* index.php
*/
include_once('config.php');
include_once('functions.php');
function alphaLinks() {
	$src = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$retval='';
	for($i=0; $i < strlen($src); $i++) {
		$retval.="<button class='alphaButton'>{$src[$i]}</button>";
	}
	return $retval;
}
?>
<html>
<head>
<link rel="stylesheet" href="css/ui-lightness/jquery-ui-1.8.7.custom.css" type="text/css" media="all" />
<script src="js/jquery-1.4.4.min.js" type="text/javascript"></script>
<script src="js/jquery-ui-1.8.7.custom.min.js" type="text/javascript"></script>
<script src="js/webreports.js" type="text/javascript"></script>
<style>
body, #mytable, .myroom, input, button, h1, h2, h3, select {font-size: small; font-family: arial;}
#toolbar {padding:10px 4px; font-size: small;}
#toolbar > button {font-size: 0.9em;}

ol {
	list-style-position: outside;
	padding: 10px;
	margin-left: 30px;
}
ol li{
	padding: 2px;
	border-bottom: 1px solid #aac;
	background-color: #fef;
}

ol li:hover {
	background-color: #efe;
}

ol li span {
	display: inline-block;
	min-width: 260px;
	padding:2px;
	margin: 0px;
}

ol li span:first-child {
	background-color: #eee;
	padding-left:4px;
}


ol li a:hover span {
	color: #f0f;
}

dl {
	padding: 20px;
}

h4 {
	font-size: 1.4em;
	font-weight: bold;
	color: #f0f;
}

dl dt {
	font-size: 1.1em;
	font-weight: bold;
}
dl dd {
	font-size: 1em;
}
dl dd div {
	display: inline-block;
	padding: 2px;
	border-bottom: 1px dotted #eee;
	min-width: 500px;
}
dl dd div span {
	display: inline-block;
	min-width: 140px;
	font-weight: bold;
}
dl dd div span:first-child {
	font-weight: normal;
}
</style>
</head>
<body>
<form name="myform" method="post" id="myform"> 
<h1>Web Reports</h1>
<span id="toolbar" class="ui-widget-header ui-corner-all">
<button id="guests">guests</button>
<button id="print">print</button>
<button id="clear">clear</button>
<button id="reservations">reservations</button>
</span>
<hr />
<?php echo alphaLinks(); ?>
<div id="reportData"></div>
</form>
</body>
</html>