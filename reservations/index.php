<?php
/**
* index.php
*/
include_once('config.php');
include_once('functions.php');

?>
<html>
<head>
<link rel="stylesheet" href="css/ui-lightness/jquery-ui-1.8.7.custom.css" type="text/css" media="all" />
<script src="js/jquery-1.4.4.min.js" type="text/javascript"></script>
<script src="js/jquery-ui-1.8.7.custom.min.js" type="text/javascript"></script>
<script src="js/reservations.js" type="text/javascript"></script>
<style>
body, #mytable, .myroom, input, button, h1, h2, h3, select {font-size: small; font-family: arial;}
#toolbar {padding:10px 4px; font-size: small;}
#toolbar > button {font-size: 0.9em;}
#mytable {
	background-color: #eeeeee;
	border-collapse:collapse;
}
#mytable tr {
	height: 24px; 
	font-size:0.9em;
	border-bottom:1px solid #aaaaaa;
}
#mytable tr.dataRow {
	padding-left: 8px;
	text-align: center;
}
#mytable tr.dateHeader {
	background-color: #FFFFFF; 
	height: 48px;
	font-size: 1.0em;
	vertical-align: bottom;
	font-weight: bold;
}
#mytable tr.labelHeader {
	background-color: #88ffFF; 
	font-size: 0.9em;
	text-align: center;
}

.rooms {
	display: hidden;
	
}

</style>
</head>
<body>
<form name="myform" method="post" id="myform"> 
<h1>Reservations Reports</h1>
<span id="toolbar" class="ui-widget-header ui-corner-all">
<label for="dateStart">Date From:</label>
<input type="text" id="dateStart" name="dateStart" />
<label for="dateStart">Date To:</label>
<input type="text" id="dateEnd" name="dateEnd" />
<select name="status" id="status">
<option value="Active">Active</option>
<option value="Cancelled">Cancelled</option>
<option value="Claimed">Claimed</option>
</select>
<button id="view">view</button>
<button id="print">print</button>
<button id="clear">clear</button>
<button id="guests">guests</button>
</span>
<hr />
<div id="reportData"></div>
</form>
</body>
</html>