
select room_type_name, door_name, 
getavailabilitystatus(room_id, '2012-06-07')  as '07'
from room_types, rooms where room_types.room_type_id=rooms.room_type_id
order by room_types.rank;


drop function if exists getavailabilitystatus;
delimiter $$
create function getavailabilitystatus (roomid int, checkdate date) returns varchar(10)
begin
    declare reservestat varchar(20);
    
    select status 
        into reservestat
        from reserve_rooms
        where room_id=roomid 
        and checkin <= checkdate
        and checkout >= checkdate
        limit 0, 1;

    if reservestat = 'Pending' then
        return reservestat;
    else
        return (
            select statuses.status_name 
            from statuses, rooms, occupancy
            where statuses.status_id=rooms.status
            and rooms.room_id=roomid
            and rooms.room_id=occupancy.room_id
            and date(occupancy.actual_checkin) <= checkdate
            and date(occupancy.expected_checkout) >= checkdate
            and occupancy.actual_checkout = '0000-00-00 00:00:00'
        );
    end if;        
    
    
end $$
delimiter ;

select getavailabilitystatus(183, '2012-06-06');



drop procedure if exists make_intervals;
delimiter $$

CREATE PROCEDURE make_intervals(startdate timestamp, enddate timestamp, intval integer, unitval varchar(10))
BEGIN
   declare thisDate timestamp;
   declare nextDate timestamp;
   set thisDate = startdate;
   drop temporary table if exists time_intervals;
   create temporary table if not exists time_intervals
      (
      interval_start timestamp,
      interval_end timestamp
      );

   repeat
      select
         case unitval
            when 'MICROSECOND' then timestampadd(MICROSECOND, intval, thisDate)
            when 'SECOND'      then timestampadd(SECOND, intval, thisDate)
            when 'MINUTE'      then timestampadd(MINUTE, intval, thisDate)
            when 'HOUR'        then timestampadd(HOUR, intval, thisDate)
            when 'DAY'         then timestampadd(DAY, intval, thisDate)
            when 'WEEK'        then timestampadd(WEEK, intval, thisDate)
            when 'MONTH'       then timestampadd(MONTH, intval, thisDate)
            when 'QUARTER'     then timestampadd(QUARTER, intval, thisDate)
            when 'YEAR'        then timestampadd(YEAR, intval, thisDate)
         end into nextDate;

      insert into time_intervals select thisDate, timestampadd(MICROSECOND, -1, nextDate);
      set thisDate = nextDate;
   until thisDate >= enddate
   end repeat;

 END $$

delimiter ;


-- To populate all_dates table

call make_intervals('2012-01-01 00:00:00','2015-12-31 00:00:00',1,'DAY');
insert into all_dates select date(interval_start) from time_intervals;



select all_dates.dt, rooms.door_name, getavailabilitystatus(reserve_rooms.room_id, all_dates.dt)
from all_dates
left outer join reserve_rooms on all_dates.dt=reserve_rooms.checkin
left outer join rooms on reserve_rooms.room_id=rooms.room_id
where all_dates.dt between '2012-06-01' and '2012-06-05'


