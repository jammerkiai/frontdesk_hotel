Ext.BLANK_IMAGE_URL = '../ext3/resources/images/default/s.gif';

Ext.ns('Application');
	
Ext.onReady(function() {
//Ext.state.Manager.setProvider(new Ext.state.CookieProvider());
Ext.QuickTips.init();
//var keyboardplugin = new Ext.ux.plugins.VirtualKeyboard();


var userLogged = new Ext.Toolbar.TextItem({applyTo:'whoami'});
var lastRoom = new Ext.Toolbar.TextItem({applyTo:'lastRoom'});
var dateToday = new Ext.Toolbar.TextItem('Today is ' + new Date().format('l, d M Y'));
var clock = new Ext.Toolbar.TextItem('');

var viewport = new Ext.Viewport({
	layout: 'border',
	showLoading:true,
	defaults: {splitter:true,margins:'2 0 2 0'},
	items: [
		{
			region:'north',
			height:75,
			title:'Shogun Hotel Frontdesk',
			tbar:[
			{
	xtype:'buttongroup',
	title:'Frontdesk',
	items:[
		{
			xtype:'button',
			text:'Home',
			iconCls:'x-icon-house'
			,handler:function() {
				document.location.href='index.php';
			}
		}
		/*,
		{
			xtype:'splitbutton',
			text:'Floor View',
			iconCls:'x-icon-building',
			menu:[
				 {text:'2nd Floor', iconCls:'x-icon-building' ,handler: function() {Ext.getCmp('hotellayout').setActiveTab(0);} }
				,{text:'3rd Floor',  iconCls:'x-icon-building' ,handler: function() {Ext.getCmp('hotellayout').setActiveTab(1);} }
				,{text:'4th Floor',  iconCls:'x-icon-building' ,handler: function() {Ext.getCmp('hotellayout').setActiveTab(2);} }
				,{text:'5th Floor',  iconCls:'x-icon-building' ,handler: function() {Ext.getCmp('hotellayout').setActiveTab(3);} }
				,{text:'6th Floor',  iconCls:'x-icon-building' ,handler: function() {Ext.getCmp('hotellayout').setActiveTab(4);} }
			]
		}*/
	]
},
{
	xtype:'buttongroup',
	title:'Reservations',
	items:[
		{text:'New',iconCls:'x-icon-calendar-add'
		,handler:function(){
			new Ext.Window(
					{   
						layout:'form',
						title:'Reservations',
						iconCls:'x-icon-house',
						modal:true,
						maximizable:true,
						autoScroll:true,
						html:'<iframe src="../newreserve/base.php" frameborder=0 width="100%" height="100%"></iframe>'
						,id:'reservewin'
						,tbar:[
						 {text:'Back to Main',handler:function(){Ext.getCmp('reservewin').destroy();} }	
						]
					})
					.show().maximize();
		}}
        ,{text:'Monthly View',iconCls:'x-icon-calendar-view-day'
		    ,handler:function(){
			    new Ext.Window(
					{   
						layout:'form',
						title:'Reservations',
						iconCls:'x-icon-house',
						modal:true,
						maximizable:true,
						autoScroll:true,
						html:'<iframe src="../reservations/monthlyview.php" frameborder=0       width="100%" height="100%"></iframe>'
						,id:'reservewin'
						,tbar:[
						{text:'Back to Main',handler:function(){Ext.getCmp('reservewin').destroy();} }	
						]
					})
					.show().maximize();
		}}
		,{text:'Reports',iconCls:'x-icon-calendar-view-month'
		,handler:function(){
			var today = new Date().format("Y-m");
			var  tmp = today.split('-');
			var url ='../reservations/reports.php';
			new Ext.Window(
					{   
						layout:'form',
						title:'Reservation Reports',
						iconCls:'x-icon-house',
						modal:true,
						maximizable:true,
						autoScroll:true,
						html:'<iframe src="'+url+'" frameborder=0 width="100%" height="100%"></iframe>'
						,id:'reservewin'
						,tbar:[
						{text:'Back to Main',handler:function(){Ext.getCmp('reservewin').destroy();} }	
						]
					})
					.show().maximize();
		}}
	]
},
{
	xtype:'buttongroup',
	title:'Guests',
	items:[
		/*{text:'New',iconCls:'x-icon-user_add'
		,handler:function() {
				new Ext.Window(
					{   
						layout:'form',
						title:'Guests',
						iconCls:'x-icon-house',
						modal:true,
						autoScroll:true,
						items:[{xtype:'guestform',width:600}]
					})
					.show().center();
			}}
		,
		*/
		{text:'Guest List',iconCls:'x-icon-user_edit',handler:function() {
				new Ext.Window(
					{   
						title:'Guests',
						iconCls:'x-icon-house',
						modal:true,
						width: 600,
						height: 500,
						maximizable:true,
						autoScroll:true,
						id:'guestwin',
						html:'<iframe src="./ajax/guest.php" frameborder=0 width="100%" height="100%"></iframe>'
						,tbar:[
							{text:'Back to Main', handler:function(){ Ext.getCmp('guestwin').destroy();}}
						]
						//items:[{xtype:'guestform',width:600}]
					})
					.show().maximize();
			}}
		,{text:'Reports',iconCls:'x-icon-user_comment',handler:function() {
				new Ext.Window(
					{   
						layout:'form',
						title:'Guests',
						iconCls:'x-icon-house',
						modal:true,
						id:'guestwin',
						autoScroll:true,
						html:'<iframe src="./ajax/guestreports.php" frameborder=0 width="100%" height="100%"></iframe>'
						//items:[{xtype:'guestform',width:600}]
						,tbar:[
							{text:'Back to Main', handler:function(){ Ext.getCmp('guestwin').destroy();}}
						]
					})
					.show().maximize();
			}}
			,
		{text:'Receivables',iconCls:'x-icon-coins',handler:function() {
			Ext.getCmp('hotellayout').add({
							title:'Receivables',
							closable:true,
							id:'receivables',
							html:'<iframe src="./ajax/receivables.php" width="100%" height="100%" frameborder="0"></iframe>'
						});
						Ext.getCmp('hotellayout').activate('receivables');
			}
		}
	]
},

{
	xtype:'buttongroup',
	title:'Cashier',
	items:[
		{text:'Shift Start',iconCls:'x-icon-clock-play',handler:function() {
			Ext.getCmp('hotellayout').add({
							title:'Shift Start',
							closable:true,
							id:'shiftstart',
							html:'<iframe src="./ajax/shiftstart.php" width="100%" height="100%" frameborder="0"></iframe>'
						});
						Ext.getCmp('hotellayout').activate('shiftstart');
			}
		},
		{text:'Mid Shift',iconCls:'x-icon-clock-stop',handler:function() {
			Ext.getCmp('hotellayout').add({
							title:'Mid Shift',
							closable:true,
							id:'shiftmid',
							html:'<iframe src="./ajax/shiftend.php?mid=1" width="100%" height="100%" frameborder="0"></iframe>'
						});
						Ext.getCmp('hotellayout').activate('shiftmid');
			}
		}
		,
		{text:'Shift End',iconCls:'x-icon-clock-stop',handler:function() {
			Ext.getCmp('hotellayout').add({
							title:'Shift-end',
							closable:true,
							id:'shiftend',
							html:'<iframe src="./ajax/shiftend.php" width="100%" height="100%" frameborder="0"></iframe>'
						});
						Ext.getCmp('hotellayout').activate('shiftend');
			}
		}
		,	
		{text:'Safekeep',iconCls:'x-icon-coins', handler:function() {
			Ext.getCmp('hotellayout').add({
							title:'Safekeep',
							closable:true,
							id:'safekeep',
							html:'<iframe src="./ajax/safekeep.php" width="100%" height="100%" frameborder="0"></iframe>'
						});
						Ext.getCmp('hotellayout').activate('safekeep');
			}
		},
		
		{text:'Rechit',iconCls:'x-icon-coins', handler:function() {
			Ext.getCmp('hotellayout').add({
							title:'Rechit',
							closable:true,
							id:'rechit',
							html:'<iframe src="./ajax/rechitlist.php" width="100%" height="100%" frameborder="0"></iframe>'
						});
						Ext.getCmp('hotellayout').activate('rechit');
			}
		},
		
		{text:'Adjustment',iconCls:'x-icon-coins', handler:function() {
			Ext.getCmp('hotellayout').add({
							title:'Adjustment',
							closable:true,
							id:'adjust',
							html:'<iframe src="./ajax/oic_login.php" width="100%" height="100%" frameborder="0"></iframe>'
						});
						Ext.getCmp('hotellayout').activate('adjust');
			}
		},
		{text:'Registry',iconCls:'x-icon-coins', handler:function() {
			Ext.getCmp('hotellayout').add({
							title:'Registry',
							closable:true,
							id:'registry',
							html:'<iframe src="./ajax/fc_registry.php" width="100%" height="100%" frameborder="0"></iframe>'
						});
						Ext.getCmp('hotellayout').activate('registry');
			}
		},
		{text:'Special Receipt',iconCls:'x-icon-coins', handler:function() {
			Ext.getCmp('hotellayout').add({
							title:'Special Receipt',
							closable:true,
							id:'registry',
							html:'<iframe src="./ajax/finder.php" width="100%" height="100%" frameborder="0"></iframe>'
						});
						Ext.getCmp('hotellayout').activate('finder');
			}
		}
	]
}
,

/*{
	xtype:'buttongroup',
	title:'Banquet Sales',
	items:[
		{text:'Transfer'}
		,{text:'Cancel'}
		,{text:'Print'}
	]
},
*/
{
	xtype:'buttongroup',
	title:'Administrator',
	items:[
		{
			xtype:'splitbutton',
			text:'Master Tables'
			,iconCls:'x-icon-database_table'
			,menu:[
				{text:'Rooms', iconCls:'x-icon-building' ,handler: function() {
						Ext.getCmp('hotellayout').add({
							title:'Rooms Management',
							closable:true,
							id:'roomsadmin',
							html:'<iframe src="to.php?to=mod/admin/rooms/rooms.php" width="100%" height="100%" frameborder="0"></iframe>'
						});
						Ext.getCmp('hotellayout').activate('roomsadmin');
					}
				},{text:'Room Types',  iconCls:'x-icon-building' ,handler: function() {
						Ext.getCmp('hotellayout').add({
							title:'Room Type Definitions',
							closable:true,
							id:'roomtypeadmin',
							html:'<iframe src="to.php?to=mod/admin/mastertable/room_type.php" width="100%" height="100%" frameborder="0"></iframe>'
						});
						Ext.getCmp('hotellayout').activate('roomtypeadmin');
					} 
				}
				,{text:'Rates',  iconCls:'x-icon-building' ,handler: function() {
						Ext.getCmp('hotellayout').add({
								title:'Rates Definitions',
								closable:true,
								id:'ratesadmin',
								html:'<iframe src="to.php?to=mod/admin/rates/rates.php" width="100%" height="100%" frameborder="0"></iframe>'
							});
							Ext.getCmp('hotellayout').activate('ratesadmin');
					}
				}
				,{text:'Floors',  iconCls:'x-icon-building' ,handler: function() {
						Ext.getCmp('hotellayout').add({
							title:'Floor Definitions',
							closable:true,
							id:'flooradmin',
							html:'<iframe src="to.php?to=mod/admin/mastertable/floor.php" width="100%" height="100%" frameborder="0"></iframe>'
						});
						Ext.getCmp('hotellayout').activate('flooradmin');
					}
				}
				,{text:'Food & Beverages',  iconCls:'x-icon-building' ,handler: function() {
						Ext.getCmp('hotellayout').add({
							title:'Food & Beverages',
							closable:true,
							id:'fnbadmin',
							html:'<iframe src="to.php?to=mod/admin/fnb/fnb.php" width="100%" height="100%" frameborder="0"></iframe>'
						});
						Ext.getCmp('hotellayout').activate('fnbadmin');
					}
				}
				,{text:'Amenities',  iconCls:'x-icon-building' ,handler: function() {
						Ext.getCmp('hotellayout').add({
							title:'Floor Definitions',
							closable:true,
							id:'amenityadmin',
							html:'<iframe src="to.php?to=mod/admin/mastertable/amenity.php" width="100%" height="100%" frameborder="0"></iframe>'
						});
						Ext.getCmp('hotellayout').activate('amenityadmin');
					}
				}
				,{text:'Groups',  iconCls:'x-icon-building' ,handler: function() {
						Ext.getCmp('hotellayout').add({
							title:'Groups',
							closable:true,
							id:'groupadmin',
							html:'<iframe src="to.php?to=mod/admin/mastertable/group.php" width="100%" height="100%" frameborder="0"></iframe>'
						});
						Ext.getCmp('hotellayout').activate('groupadmin');
					} 
				}
				,{text:'Discounts',  iconCls:'x-icon-building' ,handler: function() {
						Ext.getCmp('hotellayout').add({
							title:'Discounts Definitions',
							closable:true,
							id:'discountadmin',
							html:'<iframe src="to.php?to=mod/admin/mastertable/discount.php" width="100%" height="100%" frameborder="0"></iframe>'
						});
						Ext.getCmp('hotellayout').activate('discountadmin');
					} 
				},{text:'Themes',  iconCls:'x-icon-building' ,handler: function() {
						Ext.getCmp('hotellayout').add({
							title:'Themes Management',
							closable:true,
							id:'themesadmin',
							html:'<iframe src="to.php?to=mod/admin/mastertable/theme.php" width="100%" height="100%" frameborder="0"></iframe>'
						});
						Ext.getCmp('hotellayout').activate('themesadmin');
					} 
				},{text:'Food Category',  iconCls:'x-icon-building' ,handler: function() {
						Ext.getCmp('hotellayout').add({
							title:'Food Category Definitions',
							closable:true,
							id:'foodcategadmin',
							html:'<iframe src="to.php?to=mod/admin/mastertable/food_category.php" width="100%" height="100%" frameborder="0"></iframe>'
						});
						Ext.getCmp('hotellayout').activate('foodcategadmin');
					} 
				}
				,{text:'Forecast',  iconCls:'x-icon-chart_curve' ,handler: function() {
						Ext.getCmp('hotellayout').add({
							title:'Forecast',
							closable:true,
							id:'forecastadmin',
							html:'<iframe src="to.php?to=mod/admin/forecast/forecast.php" width="100%" height="100%" frameborder="0"></iframe>'
						});
						Ext.getCmp('hotellayout').activate('forecastadmin');
					} 
				}
			]
		}
		,{
			xtype:'splitbutton',
			text:'Settings'
			,iconCls:'x-icon-cog'
			,menu:[
				{text:'Rate Schedules', handler: function(){
						Ext.getCmp('hotellayout').add({
							title:'Rate Schedules',
							closable:true,
							id:'rateschedule',
							html:'<iframe src="to.php?to=mod/admin/rates/rates.php" width="100%" height="100%" frameborder="0"></iframe>'
						});
						Ext.getCmp('hotellayout').activate('rateschedule');
					}
				},
				{text:'Currency Rates', handler: function(){
						Ext.getCmp('hotellayout').add({
							title:'Currency Rates',
							closable:true,
							id:'currency',
							html:'<iframe src="to.php?to=mod/admin/mastertable/currency.php" width="100%" height="100%" frameborder="0"></iframe>'
						});
						Ext.getCmp('hotellayout').activate('currency');
					}
				}
				,
				{text:'Minibar', handler: function(){
						Ext.getCmp('hotellayout').add({
							title:'Minibar',
							closable:true,
							id:'minibar',
							html:'<iframe src="to.php?to=mod/admin/minibar/minibar.php" width="100%" height="100%" frameborder="0"></iframe>'
						});
						Ext.getCmp('hotellayout').activate('minibar');
					}
				}
				,{text:'Users', handler: function(){
						Ext.getCmp('hotellayout').add({
							title:'User Management',
							closable:true,
							id:'useradmin',
							html:'<iframe src="to.php?to=mod/admin/users/users.php" width="100%" height="100%" frameborder="0"></iframe>'
						});
						Ext.getCmp('hotellayout').activate('useradmin');
					}
				}
				,{text:'Settings', handler: function(){
						Ext.getCmp('hotellayout').add({
							title:'Application Settings',
							closable:true,
							id:'appsettings',
							html:'<iframe src="to.php?to=mod/admin/mastertable/settings.php" width="100%" height="100%" frameborder="0"></iframe>'
						});
						Ext.getCmp('hotellayout').activate('appsettings');
					}
				
				}
			]
		}
		/*
		,{
			xtype:'button',
			text:'Reports',
			iconCls:'x-icon-chart_curve'
		}
		*/
	]
}
			
			]
		}
		,
		{
			region:'west',
			title: 'Information at a Glance',
			id:'westend',
			width:300,
			minWidth:100,
			collapsible:true,
			split: true,
			iconCls:'x-icon-eye',
			autoScroll:true,
			frame:false,
			border:true,
			layout:'border',
			items:[		
				{
					region:'center',
					layout:'accordion',
					layoutConfig: {
						align : 'stretch',
						pack  : 'start',
					},
					items:[
					{
						title:'Alerts', 
						iconCls:'x-icon-alert',
						height:200, 
						id:'alertpanel', 
						html:'<iframe width="100%" height="100%" frameborder="0" src="./ajax/alerts.php"></iframe>'
					}
					,
					{
						xtype:'quickstatgridpanel'
					}
					]
				}
					,
				{
					region:'south',
					collapsible:true,
					autoScroll:true,
					title:'Quick Statistics',
					iconCls:'x-icon-chart_curve',
					autoLoad:'./ajax/frontstats.php',
					height:160
				}
				]
		},
		 {
				region: 'center', // a center region is ALWAYS required for border layout
				deferredRender: false,
				border:false,
				frame:false,
				layout:'fit',
				items:[{xtype:'buildingplan', id:'hotellayout'}],
				bbar: new Ext.ux.StatusBar({
					id: 'word-status',
					items: [lastRoom,' ', userLogged, ' ', dateToday, ' ', clock, ' ',
							{text:'Lock'
							,iconCls:'x-icon-lock'
							,handler:function() {
									Ext.MessageBox.confirm('Confirm', 'Unlock the application?');
								}
							}
							,{text:'Logout', iconCls:'x-icon-doorout', handler:function() {document.location.href='logout.php'}}]
				}),
				listeners: {
					render: {
						fn: function(){
							Ext.fly(lastRoom.getEl().parent()).addClass('x-status-text-panel').createChild({cls:'spacer'});
							Ext.fly(userLogged.getEl().parent()).addClass('x-status-text-panel').createChild({cls:'spacer'});
							Ext.fly(dateToday.getEl().parent()).addClass('x-status-text-panel').createChild({cls:'spacer'});
							Ext.fly(clock.getEl().parent()).addClass('x-status-text-panel').createChild({cls:'spacer'});
							Ext.TaskMgr.start({
								run: function(){
									Ext.fly(clock.getEl()).update(new Date().format('g:i:s A'));
									if(Ext.getCmp("hotellayout").getActiveTab()==null)
									{
										Ext.getCmp("hotellayout").setActiveTab(parseInt(queryString("f")));
									}

								},
								interval: 2000
							});
							
						},
						delay: 100
					}//render
				}//listeners
			}//center region
		]//viewport items
});
	
	var hideMask = function () {
        Ext.get('loading').remove();
        Ext.fly('loading-mask').fadeOut({
            remove:true
        });

		//Ext.getCmp('hotellayout').activate(1);
    }
    hideMask.defer(250);
	Ext.getCmp('hotellayout').setActiveTab(1);
});
