/**
* jquery.approver.js
*
* Uses the $.widget() factory pattern
*
* This plugin provides a dialog UI for granting higher-level approval to forms-submission
* that includes:
* - username
* - password
* - fingerprint scanner call
* - approval button
*
* Usage:
*
*      $('#submitThatNeedsSupervisorPermission').approver();
*
*/

(function( $ ) {

    var proto =  
    {       
        options : {
            url : '',
            hidetarget : false,
            userstore : {},
            success : '',
            fpscanner : 'oicfscan.php',
            approvaltype : '',
            approvalinfo : ''
        },
        
        _create : function()
        {                       
            this.refreshUsers(this.options.url)
                .buildLoginUI();
        },
        
        init : function() 
        {
            this.myInterval = undefined;
        },
        
        refreshUsers : function(source)
        {
            var mythis = this;
            if (source.length > 0) {
                $.ajax({
                    type : 'GET',
                    url : source,
                    async : false,
                    dataType : 'json',
                    success : function(resp) {
                        mythis.setUserlist(resp.supervisors);
                    }
                });
            }
            return this;
        },
        
        setUserlist : function(data)
        {
            this.options.userstore = data;
            return this;
        },
        
        buildLoginUI : function()
        {
            var mythis = this,
                elid = this.element.attr('id') || (new Date()).getTime(),
                contClass = 'approverContainer',
                contprefix = contClass + '-' + elid,
                containerId =  '#' + contprefix;
                        
            $('<div>').attr('id', contprefix)
                .addClass(contClass)
                .append('<label>OIC</label>')
                .append(
                    this.buildUserList(this.options.userstore)   
                    ||
                    $('<input type="text" />')
                        .attr('id', 'user-' + elid)
                        .addClass('approver-oic')
                )
                .append('<label>Password</label>')
                .append(
                    $('<input type="password" />')
                        .attr('id', 'pass-' + elid)
                        .addClass('approver-pass')
                        .on('change', function(){
                            $('#proceed-' + elid).attr('disabled', false);
                            mythis.stopscan();
                        })
                )
                .append(
                    $('<input type="button" />')
                        .attr('id', 'finger-' + elid)
                        .val("Finger Print")
                        .addClass('approver-finger approverDialog-button')
                        .on('click', function() {
                            mythis.startscan();
                        }
                    )
                )
                .append(
                    $('<input type="button">')
                        .attr('id', 'proceed-' + elid)
                        .attr('name', 'proceed-' + elid)
                        .attr('disabled', 'disabled')
                        .val('Proceed')
                        .addClass('approver-proceed approverDialog-button')
                        .on('click', function() {
                        	 var self = $(this);
                            mythis.stopscan();
                            $.post('approvallogger.php',
                              {
                              	approvaltype: mythis.options.approvaltype,
                              	approvalinfo: mythis.options.approvalinfo,
                              	status: 1,
                              	supervisor: $('#oic-' + elid).val(),
                              	keycode: $('#pass-' + elid).val()
                            	},
                            	function(resp) {
									
                            		if (resp.success == true) {
                            			mythis.element.attr('disabled', false);
                            			$(containerId).dialog('close');
                            			if (mythis.options.hidetarget) mythis.element.show();
                            			if ($.isFunction(mythis.options.success)) mythis.options.success();
                            		} else {
                            			alert('Approval Denied!');
                            			self.attr('disabled', 'disabled');
                            			$('#pass-' + elid).val('')
                            		}
                            	},
                            	'json');
                        }
                    )
                )
                .insertAfter(this.element)
                .dialog({
                    modal : true,
                    title : 'Approval Required',
                    autoOpen : false
                });
                
                
            $('<input type="button">')
                .attr('id', 'launcher-' + elid)
                .attr('name', 'launcher-' + elid)
                .val('Approval Required')
                .addClass('approver-launcher')
                .on('click', function(){
                    $('#pass-' + elid).val('');
                    $('#proceed-' + elid).attr('disabled', 'disabled');
                    $(containerId).dialog('open');
                })
                .insertAfter(this.element);
                
            this.element.attr('disabled', 'disabled');
            
            if (this.options.hidetarget) this.element.hide();
            
            return this;
        },
              
        buildUserList : function(store)
        {
            var id = this.element.attr('id'),
                elementId = 'oic-' + id,
                $list = undefined;
            
            if (store.length > 0) {
                $list = $('<select>')
                            .attr('name', elementId)
                            .attr('id', elementId)
                            .addClass('approver-oic');
                $.each(store, function(index, oic){
                    $list.append('<option value='+ oic.userName +'>'+ oic.fullName +'</option>');
                });
            }

            return $list;
        },
        
        startscan : function(user)
        {                        
			var elid = this.element.attr('id');
			
            $.post("oicfscan.php",{act:'scan', user: $("#oic-" + elid).val()});
            
		    this.myinterval = setInterval(function(){
		            $.post("oicfscan.php",{act:'monitor', user: $("#oic-" + elid).val()},
	                    function(resp){
		                    if(resp.success == true) {
                                $('#pass-' + elid).val(resp.pass);
                                $('#proceed-' + elid).attr('disabled', false).focus();
		                    }
	                    },
	                    'json'
	                );    
		        }, 
		        3000
		    );
        },
        
        stopscan : function()
        {
            clearInterval(this.myinterval);
        },
        
        monitorscan : function(elid)
        {            
            var elid = this.element.attr('id');
            $.post("oicfscan.php",{act:'monitor', user: $("#oic-" + elid).val()},
	            function(resp){
		            if(resp.success=='true') {
                        $('#pass-' + elid).val(resp.pass);
                        $('#proceed-' + elid).attr('disabled', false).focus();
		            }
	            },
	            'json'
	        );
            
        },
        
        destroy: function ()
        {
            clearInterval(this.myinterval);
            $.Widget.prototype.destroy.call(this);    
        },

        _setOption: function (key, value)
        {
            $.Widget.prototype._setOption.apply(this, arguments);
        },
    };

    //Run jQuery's widget factory to create the widget
    $.widget('zno.approver', proto);
    
})( jQuery );
