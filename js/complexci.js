Application.ComplexCheckin = Ext.extend(Application.SimpleCheckin, {
	border:false
    ,initComponent:function() {

        Ext.apply(this, Ext.apply(this.initialConfig));
        Application.ComplexCheckin.superclass.initComponent.apply(this, arguments);
    } // eo function initComponent
 
    ,onRender:function() {
        Application.ComplexCheckin.superclass.onRender.apply(this, arguments);
		
		var store = new Ext.data.ArrayStore({
			fields:[
					{name: 'checkin', type: 'date', dateFormat: 'Y-m-d H:i:s'},
				   {name: 'duration'},
				   {name: 'remarks'}
					],
			data: [['2009-07-01 03:30:00','12','Room 202'],['2009-06-01 03:30:00','24','Room 606']]
		});
		var guesthistory = new Ext.grid.GridPanel({
			store: store,
			title:'Transaction History',
			layout:'fit',
			columns: [
				{header: "Check-in", width: 120, dataIndex: 'checkin', sortable: true, type: 'date', dateFormat: 'Y-m-d H:i:s'},
				{header: "Length of Stay", width:40, dataIndex: 'duration', sortable: true},
				{header: "Remarks", width: 100, dataIndex: 'remarks', sortable: true}
			],
			sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
			viewConfig: {
				forceFit: true
			},
			height:210,
			split: true,
		});

		
		var tab2 = new Ext.Panel({
		colspan:2,
		title:'Guest Information',
        labelAlign: 'top',
        bodyStyle:'padding:5px',
        width: 550,
        items: [{
            layout:'column',
            border:false,
            items:[{
                columnWidth:.5,
                layout: 'form',
                border:false,
                items: [{
                    xtype:'textfield',
                    fieldLabel: 'First Name',
                    name: 'first',
                    anchor:'95%'
                }, {
                    xtype:'textfield',
                    fieldLabel: 'Mobile',
                    name: 'mobile',
                    anchor:'95%'
                }]
            },{
                columnWidth:.5,
                layout: 'form',
                border:false,
                items: [{
                    xtype:'textfield',
                    fieldLabel: 'Last Name',
                    name: 'last',
                    anchor:'95%'
                },{
                    xtype:'textfield',
                    fieldLabel: 'Email',
                    name: 'email',
                    vtype:'email',
                    anchor:'95%'
                }]
            }]
        },{
            xtype:'tabpanel',
            plain:true,
            activeTab: 0,
            height:235,
            defaults:{bodyStyle:'padding:10px',labelWidth:140},
            items:[{
                title:'Company Information',
                layout:'form',
                defaults: {width: 230},
                defaultType: 'textfield',

                items: [{
                    fieldLabel: 'Company Name',
                    name: 'first',
                    allowBlank:false
                },{
                    fieldLabel: 'Address',
                    name: 'last'
                },{
                    fieldLabel: 'Position/Designation',
                    name: 'company'
                }, {
                    fieldLabel: 'Email',
                    name: 'email',
                    vtype:'email'
                }]
            },{
                title:'Phone Numbers',
                layout:'form',
                defaults: {width: 230},
                defaultType: 'textfield',

                items: [{
                    fieldLabel: 'Home',
                    name: 'home',
                    value: '(888) 555-1212'
                },{
                    fieldLabel: 'Business',
                    name: 'business'
                },{
                    fieldLabel: 'Mobile',
                    name: 'mobile'
                },{
                    fieldLabel: 'Fax',
                    name: 'fax'
                }]
            },{
                cls:'x-plain',
                title:'Special Requirements',
                layout:'fit',
                items: {
                    xtype:'htmleditor',
                    id:'bio2',
                    fieldLabel:'Biography'
                }
            },guesthistory
			
			]
        }]
    });

		guesthistory.show();
		
		this.add(tab2);
    } // eo function onRender
});
 
Ext.reg('complexcheckin', Application.ComplexCheckin);