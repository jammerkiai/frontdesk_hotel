Ext.BLANK_IMAGE_URL = '../ext3/resources/images/default/s.gif';
Ext.ns('Application');
Ext.onReady(function() {
    Ext.QuickTips.init();

	var win = new Ext.Window({
		 title:'Shogun Hotel Frontdesk Login',
		 id:'metaform-win',
		 width:380,
		 layout:'form',
		 closable:false,
		 items: [
			{
			xtype:'form',
			labelWidth: 75, 
			id:'loginwin',
			frame:false,
			border:false,
			bodyStyle:'padding:15px 15px 10px 10px',
			defaults: {width: 200},
			//defaultType:'alphakeypadfield',
			items: [
				{
					xtype:'alphakeypadfield',
					fieldLabel: 'Username',
					name: 'uname',
					value:'',
					allowBlank:false,
					blankText:'Enter your username'
				},{
					xtype:'alphakeypadfield',
					fieldLabel: 'Password',
					value:'',
					inputType:'password',
					name: 'pword',					
					blankText:'Enter your password'
				}
			]
		}]
		,
			buttons: [
				{
					text: 'Submit',
					iconCls:'x-icon-key',
					scale:'small',
					iconAlign:'top',
					type:'button',
					formBind: true,
					handler: function () {
						 Ext.getCmp('loginwin').getForm().submit({
							url:'ajax/loginform.php',
							method:'post',
							waitMsg: 'Authenicating. Please wait...',
							success: function(form, action) {								
								
							   if(action.result.shiftstatus==1) {								
							     //Ext.Msg.alert('Success', action.result.msg);
								 document.location.href='index.php';
							   }else{
							     //Ext.Msg.alert('Success', action.result.msg);
								 document.location.href='ajax/shiftstart.php';
							   }
							   //document.location.href='index.php';
							},
							failure:function (form,action) {
								Ext.Msg.alert('Failed', action.result.msg);
							}
						 });
					}
				}
			]
		
	});
	win.show().center();

	var hideMask = function () {
        Ext.get('loading').remove();
        Ext.fly('loading-mask').fadeOut({
            remove:true
        });
    }
    hideMask.defer(250);
});