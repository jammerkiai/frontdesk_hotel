Ext.ns('Application');
Application.AlphaKeyPassPadField = Ext.extend(Ext.form.TextField, {
    onRender:function() {
        // call parent
        Application.AlphaKeyPassPadField.superclass.onRender.apply(this, arguments);
        this.el.on({
             focus:{scope:this, fn:this.showkeypad}
        });
    } // e/o function onRender

	,showkeypad:function() {
		//alert(this.id);
		var keypad= new Ext.Panel({
			layout:'table',
			target: this.id,
			layoutConfig:{columns:10},
			id:'alphakeys',
			frame:true,
			border:false,
			region:'center',
			closable:true,
			split:true,
			width:440,
			autoScroll:true,
			defaultType:'button',
			defaults:{width:40,height:32,margins:'1 1 1 1', style:'margin:1px;',itemCls:'required',labelStyle:'color:#ff0000',handler: this.returnkeystroke},
			items:[
				{xtype:'textfield',inputType:'password',id:'keypressdisplay3', height:24, colspan:10, width:426,style:'font-size:1.1em;color:#005533;text-align:right;font-weight:bold;font-family:"lucida,tahoma, arial, helvetica"',value:this.getValue()},
				{text:'1'},{text:'2'},{text:'3'},{text:'4'},{text:'5'},{text:'6'},{text:'7'},{text:'8'},{text:'9'},{text:'0'},
				{text:'q'},{text:'w'},{text:'e'},{text:'r'},{text:'t'},{text:'y'},{text:'u'},{text:'i'},{text:'o'},{text:'p'},
				{text:'a'},{text:'s'},{text:'d'},{text:'f'},{text:'g'},{text:'h'},{text:'j'},{text:'k'},{text:'l'},{text:"'"},
				{text:'z'},{text:'x'},{text:'c'},{text:'v'},{text:'b'},{text:'n'},{text:'m'},{text:','},{text:'.'},{text:'+/-'},
				{text:'BkSp',colspan:2,width:80},{text:'Space',colspan:4,width:160},{text:'Del',colspan:2,width:80},{text:'Enter',colspan:2,width:80}
			]
		});
		keypad.show();
		
		var win = new Ext.Window({
			id: 'keypadwin3',
			title:this.fieldLabel,
			width:470,
			height:250,
			frame:true,
			border:true,
			items: keypad,
			modal:true
		});
		win.show().center();
	}
	
	
	,returnkeystroke:function(o,e) {
		var t = document.getElementById(o.ownerCt.target);
		var c = document.getElementById('keypressdisplay3');
		var init = t.value;
		if(init==0) init = '';
		if(o.text=='Enter') {
			o.ownerCt.destroy();
			Ext.getCmp('keypadwin3').destroy();
		}else if(o.text=='Space'){
			var newval = init + ' ';
			c.value=newval;
		}else if(o.text=='Del'){
			t.value = "";
			c.value = "";
		}else if(o.text=='BkSp'){
			c.value = init.substring(0, init.length - 1);
		}else if(o.text=='+/-'){
			c.value = init * (-1);
		}else{
			var newval = init + o.text;
			c.value=newval;
		}	
		t.value=c.value;
	}


}); // end of extend

// register xtype
Ext.reg('alphakeypasspadfield', Application.AlphaKeyPassPadField);