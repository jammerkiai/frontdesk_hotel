Ext.ns('Application');
Application.FloorPlan = Ext.extend(Ext.Panel, {
     border:false
	 ,layout:'absolute'
	 ,title:'Mapping'	
	 ,deferredRender:true
    ,initComponent:function() {
        var config = {

        }; 
		Ext.apply(this, Ext.apply(this.initialConfig, config));
		Ext.apply(this, {
			store : new Ext.data.JsonStore({
				autoDestroy:true,
				url: "./ajax/json.php",
				batch:true,
				method: "POST", 
				baseParams: {
					act:'floor', fid: this.floorid, owner: this.id
				}
			})
		});

        Application.FloorPlan.superclass.initComponent.apply(this, arguments);
    } 

    ,onRender:function(ct,position) {
		Application.FloorPlan.superclass.onRender.apply(this, arguments);
		this.store.load();
		this.store.on({
			'load' : {
					fn: function(r) {
						r.each(
							 function(r){
								Ext.getCmp(this.store.baseParams.owner).add(
									{
										xtype:'button',
										deferredRender:true,
										text:r.data.door_name,
										y:r.data.ui_top,
										x:r.data.ui_left,
										width:r.data.ui_width,
										height:r.data.ui_height,
										roomid: r.data.room_id,
										siteid: r.data.site_id,
										autoShow:true,
										iconCls:  'x-icon-' + r.data.status,
										handler: function() {
											var url = '';
											if( r.data.status==1 ) {
												wintitle = 'Check-in: Rm ';
												url = './ajax/checkinform.php?room=' + r.data.room_id; 
												formname = 'checkinform';
												//myform = new Ext.Panel({
												html: '<iframe width="100%" height="100%" frameborder="0" src="'+url+'"></iframe>'
												//})
												document.location.href=url;
												exit;
											}else if( r.data.status==2 ) {
												wintitle = 'Check-out: Rm ';
												url = "checkout.php";
												formname = 'checkoutform';
												var myform = new Application.CheckOutForm({ roomid: r.data.room_id, roomtype: r.data.room_type_id });
											}else if( r.data.status==3 ) {
												wintitle = 'Change Status: Rm ';
												url = './ajax/restatus.php?room=' + r.data.room_id; 
												formname = 'statusform';
												myform = new Ext.Panel({
													html: '<iframe width="100%" height="100%" frameborder="0" src="'+url+'"></iframe>'
												})
											}else if( r.data.status==4 ) {
												wintitle = 'Change Status: Rm ';
												url = './ajax/restatus.php?room=' + r.data.room_id; 
												formname = 'statusform';
												myform = new Ext.Panel({
													html: '<iframe width="100%" height="100%" frameborder="0" src="'+url+'"></iframe>'
												})
											}else if( r.data.status==5 ) {
												wintitle = 'Change Status: Rm ';
												url = './ajax/restatus.php?room=' + r.data.room_id; 
												formname = 'statusform';
												myform = new Ext.Panel({
													html: '<iframe width="100%" height="100%" frameborder="0" src="'+url+'"></iframe>'
												})
											}
											var win = new Ext.Window({
													modal:true,
													maximizable:true,
													id:'roomupdater',
													title: wintitle + r.data.door_name,
													closeAction:'destroy',
													layout:'fit',
													tbar:[
														{text:'Back to Main', iconCls:'x-icon-doorout',
														 handler:function(){ 
															Ext.getCmp('roomupdater').destroy() 
															//document.location.href='index.php?f=' + this.owner.store.baseParams.fid;
														}}
													],
													items:[myform]
													//html: '<iframe width="100%" height="100%" frameborder="0" src="'+url+'"></iframe>'
												});
												win.show().maximize();
										}
									}
								);
								Ext.getCmp(this.store.baseParams.owner).doLayout();
							}
						);
					}
					,scope:this
				}
		});
		
	}
	
});
 
Ext.reg('floorplan', Application.FloorPlan);