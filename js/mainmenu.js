var mainmenu = [{
	items:[
{
	xtype:'buttongroup',
	title:'Frontdesk',
	items:[
		{
			xtype:'splitbutton',
			text:'Walk-in',
			iconCls:'x-icon-house',
			menu:[
				{text:'Single', iconCls:'x-icon-user',handler:function() {Ext.getCmp('dashboard').add({xtype:'checkinform',closable:true})}},
				{text:'Group', iconCls:'x-icon-group'}
			]
			,handler:function() {
				new Ext.Window(
					{   
						layout:'form',
						title:'Walk-in',
						iconCls:'x-icon-house',
						modal:true,
						autoScroll:true,
						items:[{xtype:'checkinform',width:600}]
					})
					.show();
			}
		},
		{
			xtype:'splitbutton',
			text:'Floor View',
			iconCls:'x-icon-building',
			menu:[
				 {text:'2nd Floor', iconCls:'x-icon-building' ,handler: function() {Ext.getCmp('dashboard').setActiveTab(0);} }
				,{text:'3rd Floor',  iconCls:'x-icon-building' ,handler: function() {Ext.getCmp('dashboard').setActiveTab(1);} }
				,{text:'4th Floor',  iconCls:'x-icon-building' ,handler: function() {Ext.getCmp('dashboard').setActiveTab(2);} }
				,{text:'5th Floor',  iconCls:'x-icon-building' ,handler: function() {Ext.getCmp('dashboard').setActiveTab(3);} }
				,{text:'6th Floor',  iconCls:'x-icon-building' ,handler: function() {Ext.getCmp('dashboard').setActiveTab(4);} }
			]
		}
	]
},
{
	xtype:'buttongroup',
	title:'Reservations',
	items:[
		{text:'New'}
		,{text:'Group'}
		,{text:'Search'}
	]
},
{
	xtype:'buttongroup',
	title:'Guests',
	items:[
		{text:'New',iconCls:'x-icon-user_add'
		,handler:function() {
				new Ext.Window(
					{   
						layout:'form',
						title:'Guests',
						iconCls:'x-icon-house',
						modal:true,
						autoScroll:true,
						items:[{xtype:'guestform',width:600}]
					})
					.show();
			}}
		,{text:'Guest List',iconCls:'x-icon-user_edit',handler:function() {
				new Ext.Window(
					{   
						layout:'form',
						title:'Guests',
						iconCls:'x-icon-house',
						modal:true,
						autoScroll:true,
						items:[{xtype:'guestform',width:600}]
					})
					.show();
			}}
		,{text:'Surveys',iconCls:'x-icon-user_comment',handler:function() {
				new Ext.Window(
					{   
						layout:'form',
						title:'Guests',
						iconCls:'x-icon-house',
						modal:true,
						autoScroll:true,
						items:[{xtype:'guestform',width:600}]
					})
					.show();
			}}
	]
},

{
	xtype:'buttongroup',
	title:'Reports',
	items:[
		{text:'Shift-end'}
		,{text:'Cashier'}
		,{text:'Print'
			,handler: function() {

			}
		}
	]
}]
});