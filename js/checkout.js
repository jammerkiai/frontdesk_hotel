Ext.ns('Application');
Application.CheckOutForm = Ext.extend(Ext.form.FormPanel, {
	id:'checkoutform',
	autoScroll:true,
	style:'padding:1px',
	layout:'border',
    initComponent:function() {
		Ext.apply(this, Ext.apply(this.initialConfig));
        Application.CheckOutForm.superclass.initComponent.apply(this, arguments);
    }//end initComponent

    ,onRender:function(ct,position) {
		Application.CheckOutForm.superclass.onRender.apply(this, arguments);
		var orderUrl = './ajax/order.php&roomid='+this.roomid;
		
		var checkoutpanel = new Ext.Panel(
			{
			title: 'Checkout Panel',
			id:'mycheckoutpanel',
			roomid:this.roomid,
			border:true,
			autoScroll:true,
			tbar:[
				{text:'Reload',iconCls:'x-icon-refresh',handler:function(){
					document.getElementById("checkoutframe").contentDocument.location.reload(true);//='./ajax/checkout.php?roomid='+this.roomid;
				}}
			],
			html:'<iframe id="checkoutframe" frameborder=0 src="./ajax/checkout.php?roomid='+this.roomid +'" width="100%" height="100%" ></iframe>'
		});
		
		var reports = new Ext.TabPanel({
			tbar:[
				{text:'Reload',iconCls:'x-icon-refresh',handler:function(o,e){
					var loadurl = reports.getActiveTab().autoLoad;
					reports.getActiveTab().load({
						url:loadurl
					});
					
				}}
				,{text:'Print',iconCls:'x-icon-printer',handler:function(o,e){
					var loadurl = reports.getActiveTab().autoLoad;
					reports.getActiveTab().load({
						url:loadurl + "&print=1"
					});
					
				}}
			]
			,items:[
				{
					title:'ROS',
					autoScroll:true,
					autoLoad:'./ajax/ros.php?roomid=' + this.roomid
				}
				,
				{
					title:'MOS',
					autoScroll:true,
					autoLoad:'./ajax/mos.php?roomid=' + this.roomid
				}
				,
				{
					title:'FOS',
					autoScroll:true,
					autoLoad:'./ajax/FOS.php?roomid=' + this.roomid
				}
				,
				{
					title:'SOA',
					autoScroll:true,
					autoLoad:'./ajax/soa.php?roomid=' + this.roomid
				}
			]
		});
		reports.activate(0);
		this.add(
			{
				xtype:'panel',region:'west',
				width:280,layout:'fit',
				collapsible:true,
				split:true, title:'Summary Reports',items:reports
			},
			{
				xtype:'tabpanel',
				region:'center',
				margins:'2 2 2 2',
				items:[{
						title: 'Orders/Room Service',
						layout:'fit',
						tbar:[
							{text:'Reload',iconCls:'x-icon-refresh',handler:function(){
								document.getElementById("roomserviceframe").contentDocument.location.reload(true);//='./ajax/checkout.php?roomid='+this.roomid;
							}}
						],
						html:'<iframe id="roomserviceframe" src="./ajax/order.php?roomid='+ this.roomid + '" frameborder=0 width="100%" height="100%"></iframe>'
					}
					,{
						title: 'Extension',
						layout:'fit',
						tbar:[
							{text:'Reload',iconCls:'x-icon-refresh',handler:function(){
								document.getElementById("extensionframe").contentDocument.location.reload(true);//='./ajax/checkout.php?roomid='+this.roomid;
							}}
						],
						html:'<iframe id="extensionframe" src="./ajax/extension.php?roomid='+ this.roomid + '" frameborder=0 width="100%" height="100%"></iframe>'
					}
					,{
						title: 'Transfer',
						layout:'fit',
						tbar:[
							{text:'Reload',iconCls:'x-icon-refresh',handler:function(){
								document.getElementById("transferframe").contentDocument.location.reload(true);//='./ajax/checkout.php?roomid='+this.roomid;
							}}
						],
						html:'<iframe id="transferframe" src="./ajax/transfer.php?roomid='+ this.roomid + '" frameborder=0 width="100%" height="100%"></iframe>'
					},
					{
						title: 'Cancellation',
						layout:'fit',
						tbar:[
							{text:'Reload',iconCls:'x-icon-refresh',handler:function(){
								document.getElementById("cancelframe").contentDocument.location.reload(true);//='./ajax/checkout.php?roomid='+this.roomid;
							}}
						],
						html:'<iframe id="cancelframe" src="./ajax/cancellation.php?roomid='+ this.roomid + '" frameborder=0 width="100%" height="100%"></iframe>'
					}
					,
					{
						title: 'Messages/Reminders',
						layout:'fit',
						tbar:[
							{text:'Reload',iconCls:'x-icon-refresh',handler:function(){
								document.getElementById("messageframe").contentDocument.location.reload(true);//='./ajax/checkout.php?roomid='+this.roomid;
							}}
						],
						html:'<iframe id="messageframe" src="./ajax/messages.php?roomid='+ this.roomid + '" frameborder=0 width="100%" height="100%"></iframe>'
					}
					,
					{
						title: 'Make-up Room',
						layout:'fit',
						tbar:[
							{text:'Reload',iconCls:'x-icon-refresh',handler:function(){
								document.getElementById("makeupframe").contentDocument.location.reload(true);//='./ajax/checkout.php?roomid='+this.roomid;
							}}
						],
						html:'<iframe id="makeupframe" src="./ajax/makeup.php?roomid='+ this.roomid + '" frameborder=0 width="100%" height="100%"></iframe>'
					}
					,
					checkoutpanel
					
					]
			}
			);
		
	}//end onrender
});
Ext.reg('checkoutform', Application.CheckOutForm);
