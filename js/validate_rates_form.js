// JavaScript Document
function validate_rates_form(form) {

  if (form.newRate_Name.value == "") {
      alert('* Rate Name is required.');
      return false;
  }
  else if (form.newHour_Start.value == "" || form.newHour_Start.value == "0") {
      alert('* Hour Start is required.');
      return false;
  }
  else if (form.newHour_End.value == "" || form.newHour_End.value == "0") {
      alert('* Hour End is required.');
      return false;
  }
  else if (!form.dow_monday.checked && !form.dow_tuesday.checked && !form.dow_wednesday.checked && !form.dow_thursday.checked && !form.dow_friday.checked && !form.dow_saturday.checked && !form.dow_sunday.checked) {
      alert('* Day of Week is required.');
      return false;
  }
  else if (form.newDuration.value == "") {
      alert('* Duration is required.');
      return false;
  }
  else return true;

}