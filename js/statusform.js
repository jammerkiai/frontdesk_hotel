Ext.ns('Application');
Application.StatusForm = Ext.extend(Ext.form.FormPanel, {
    initComponent:function() {
        var config = {
			id:'checkinform',
			defaultType:'textfield',
			items:[
				{fieldLabel: 'Remarks',name:'remarks'}
			]
        }; 
		Ext.apply(this, Ext.apply(this.initialConfig, config));

        Application.StatusForm.superclass.initComponent.apply(this, arguments);
    } 

    ,onRender:function(ct,position) {
		Application.StatusForm.superclass.onRender.apply(this, arguments);
		
	}
});
Ext.reg('statusform', Application.StatusForm);