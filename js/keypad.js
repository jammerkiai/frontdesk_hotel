Ext.ns('Application');
Application.KeyPadField = Ext.extend(Ext.form.NumberField, {
    onRender:function() {
        // call parent
        Application.KeyPadField.superclass.onRender.apply(this, arguments);
        this.el.on({
             focus:{scope:this, fn:this.showkeypad}
        });
		this.el.value=this.getValue();
    } // e/o function onRender

	,showkeypad:function() {
		var keypad= new Ext.Panel({
			layout:'table',
			target: this.id,
			layoutConfig:{columns:3},
			id:'alphakeys',
			frame:true,
			border:true,
			closable:true,
			split:true,
			width:144,
			height:208,
			autoScroll:true,
			defaultType:'button',
			defaults:{width:40,height:32,margins:'1 1 1 1', style:'margin:1px;',itemCls:'required',labelStyle:'color:#ff0000',handler: this.returnkeystroke},
			items:[
				{xtype:'textfield',id:'keypressdisplay', height:24, colspan:3, width:130,style:'font-size:1.1em;color:#005533;text-align:right;font-weight:bold;font-family:"arial, helvetica,tahoma"', value:this.getValue()},
				{text:'BkSp'},{text:'Del'},{text:'Ok'},
				{text:'1'},{text:'2'},{text:'3'},
				{text:'4'},{text:'5'},{text:'6'},
				{text:'7'},{text:'8'},{text:'9'},
				{text:'+/-'},{text:'0'},{text:'.'}
			]
		});
		
		var win = new Ext.Window({
			id: 'keypadwin1',
			title:this.fieldLabel,
			items: keypad,
			width:152,
			height:236,
			modal:true
		});
		win.show().center();
	}
	
	,returnkeystroke:function(o,e) {
		var t = document.getElementById(o.ownerCt.target);
		var c = document.getElementById('keypressdisplay');
		c.value =t.value;
		var init = t.value;
		if(init==0) init = '';
		if(o.text=='Ok') {
			c.value=t.value;
			o.ownerCt.destroy();
			Ext.getCmp('keypadwin1').destroy();
		}else if(o.text=='Del'){
			t.value = "";
			c.value=t.value;
		}else if(o.text=='BkSp'){
			t.value = init.substring(0, init.length - 1);
			c.value=t.value;
		}else if(o.text=='+/-'){
			t.value = init * (-1);
			c.value=t.value;
		}else{
			var newval = init + o.text;
			t.value=newval;
			c.value=t.value;
		}	
		
	}


}); // end of extend

// register xtype
Ext.reg('keypadfield', Application.KeyPadField);