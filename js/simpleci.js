Ext.ns('Application');
Application.SimpleCheckin = Ext.extend(Ext.form.FormPanel, {
	frame:false,
	border:false,
	id:'checkinform',
	autoScroll:true,
	//layout:'border',
    //layoutConfig: {columns:2},
	style:'padding:2px',
	defaults:{margins:'2 2 2 2',style:'padding:2px;valign:top'},
    initComponent:function() {
		Ext.apply(this, Ext.apply(this.initialConfig));
        Application.SimpleCheckin.superclass.initComponent.apply(this, arguments);

    } 

    ,onRender:function(ct,position) {
		Application.SimpleCheckin.superclass.onRender.apply(this, arguments);
		
		var ratespanel = new Ext.Panel({
			//title:'Room Rate',
			frame:true,
			border:true,
			id:'ratespanel',
			layout:'hbox',
			//width:240,
			layoutConfig: {flex:1},
			defaults:{width:50,height:50,margins:'2 2 2 2',}
		});
		
		var discountpanel = new Ext.Panel({
			//title:'Discount',
			frame:true,
			border:true,
			//width:240,
			layout:'hbox',
			layoutConfig: {flex:1},
			defaults:{width:50,height:50,margins:'2 2 2 2',disabled:true}
		});
		
		var ratestore = new Ext.data.JsonStore({
				autoDestroy:true,
				url: "./ajax/json.php",
				batch:true,
				method: "POST", 
				baseParams: {
					act:'roomrates', owner: this.id, roomtype: this.roomtype, roomid:this.roomid
				},
				autoLoad:true,
				listeners: {
					load: {
						fn: function(r){	
							r.each(function(r,o){
								ratespanel.add({
									xtype:'button',
									margins:'2 2 2 2',
									text:r.data.rate_name,
									rate: r.data.amount,
									toggleGroup:'rategroup',
									rateid: r.data.rate_id,
									roomtype: r.data.room_type_id,
									ot_amount: r.data.ot_amount,
									duration:r.data.duration,
									roomid: o.roomid,
									listeners: {
										click: {
											fn: function(b,e) {

												for(i=0; i < discountpanel.items.length; i++){
													var tmp = discountpanel.get(i);
													tmp.toggle(false);
													tmp.setDisabled(tmp.rateid!=b.rateid);
												}
												
												document.getElementById('roomcharge').value=b.rate;
												document.getElementById('discount').value=0;
												document.getElementById('duration').value=b.duration;
												document.getElementById('rateid').value=b.rateid;
												document.getElementById('roomtype').value=b.roomtype;
												document.getElementById('hiddenhourextend').value=b.ot_amount;
												if(b.duration < 24) {
													Ext.getCmp('newcheckin').disable();
													Ext.getCmp('newcheckout').disable();
												}else{
													Ext.getCmp('newcheckin').enable();
													Ext.getCmp('newcheckout').enable();
												}
												recompute();
											}
										}
									}
								});
								ratespanel.doLayout();
							});
						}
					}
					
				}
			});
			
		
		var discountstore = new Ext.data.JsonStore({
			autoDestroy:true,
			url: "./ajax/json.php",
			batch:true,
			method: "POST", 
			baseParams: {
				act:'roomdiscounts', owner: this.id, roomtype: this.roomtype
			},
			autoLoad: true,
			listeners: {
				load: {
						fn: function(r){	
							r.each(function(r){
								discountpanel.add({
									xtype:'button',
									toggleGroup:'discountgroup',
									text:r.data.discount_label,
									percent: r.data.discount_percent,
									rateid: r.data.rate_id,
									listeners: {
										click: {
											fn: function(b,e) {
												var rc = document.getElementById('roomcharge').value;
												document.getElementById('discount').value= rc * b.percent/100;
												recompute();
											}
										}
									}
								});
								discountpanel.doLayout();
							});
						}
					}
			}
		});//discountstore 
		
		var extraitems=new Ext.Panel({
			//title:'Additional Items',
			frame:false,
			border:false,
			layout:'form',
			height:120,
			defaultType:'keypadfield',
			style:'padding:10 10 10 10;',
			items:[
				{
					 xtype:'xdatetime'
					,id:'newcheckin'
					,name:'newcheckin'
					,disabled:true
					,fieldLabel:'Check In'
					,timeFormat:'H:i:s'
					,emptyToNow:true
					,timeWidth:100
					,timeConfig: {
						 altFormats:'H:i:s'
						,allowBlank:true
						,value: new Date()
					}
					,dateFormat:'Y-m-d'
					,dateConfig: {
						 altFormats:'Y-m-d|Y-n-d'
						,allowBlank:true   
						,value: new Date()					
					}
				},
				{
					 xtype:'xdatetime'
					,id:'newcheckout'
					,name:'newcheckout'
					,disabled:true
					,fieldLabel:'Check Out'
					,timeFormat:'H:i:s'
					,emptyToNow:true
					,timeWidth:100
					,timeConfig: {
						 altFormats:'H:i:s'
						,allowBlank:true
						,value: new Date()
					}
					,dateFormat:'Y-m-d'
					,dateConfig: {
						 altFormats:'Y-m-d|Y-n-d'
						,allowBlank:true   
						,value: new Date()					
					}
				},
				{
					fieldLabel:'Extension (Hours)',
					labelWidth:200,
					id:'hoursextend',
					name:'hoursextend',
					decimalPrecision:2,
					value:0,
					style:'text-align:right;margin-top:4px;',
					width:40,
					listeners:{
						change: {
							fn: function(b,e) {
								document.getElementById('extracharge').value= document.getElementById('hiddenhourextend').value * b.value;
								recompute();
							}
						}
					}
				},
				{
					xtype:'textfield',
					inputType:'hidden',
					id:'hiddenhourextend',
					name: 'newhiddenhourextend',
					value:0
				},
				{
					xtype:'textfield',
					inputType:'hidden',
					id:'rateid',
					name: 'newrateid',
					value:0
				},
				{
					xtype:'textfield',
					inputType:'hidden',
					id:'roomtype',
					name: 'newroomtypeid',
					value:0
				},
				{
					xtype:'textfield',
					inputType:'hidden',
					id:'duration',
					name: 'newduration',
					value:0
				}
			]
		});
		
		var adjustment=new Ext.Panel({
			//title:'Adjustment',
			frame:false,
			border:false,
			layout:'form',
			defaultType:'keypadfield',
			height:120,
			style:'padding:10 10 10 10;',
			defaults:{
				style:'text-align:right',
				width:180,
				anchor:'90%'
			},
			items:[
				{
					xtype:'alphakeypadfield',
					fieldLabel:'OIC Password',
					inputType:'password',
					name:'oicpass',
					id:'oicpass'
				},
				{
					fieldLabel:'Discount',
					name:'newadjustment',
					value:0,
					decimalPrecision:2,
					id:'adjusted_discount',
					listeners:{
						change: {
							fn: function(b,e) {
								document.getElementById('discount').value= document.getElementById('roomcharge').value * b.value/100;
								recompute();
							}
						}
					}
				},
				{
					xtype:'alphakeypadfield',
					fieldLabel:'Remarks',
					name:'newremarks',
					id:'newremarks'
				}
			]
		});
		
		

		var summary = new Ext.Panel({
			//title:'Summary of Charges',
			layout:'form',
			frame:false,
			border:false,
			height:220,
			style:'padding:10 10 10 10;',
			defaults:{
				style:'text-align:right',
				width:200,
				labelWidth:180
			},
			defaultType:'numberfield',
			items:[
				{
					fieldLabel:'Room Charge',
					labelStyle:'margin-top:4px;',
					name:'newroomcharge',
					value:0,
					id:'roomcharge'
				},
				{
					fieldLabel:'Extra Charges',
					name:'newextracharge',
					value:0,
					id:'extracharge'
				},
				{
					fieldLabel:'Discount',
					name:'newdiscount',
					value:0,
					labelStyle:'color:#ff0000',
					id:'discount'
				},
				{
					fieldLabel:'Total Charge',
					name:'newtotalcharge',
					value:0,
					labelStyle:'color:#0000ff;font-weight:bold;',
					id:'totalcharge'
				}
				,
				{
					fieldLabel:'Amount Paid',
					name:'newamountpaid',
					value:0,
					labelStyle:'color:#ff00ff',
					id:'amountpaid'
				}
				,
				{
					fieldLabel:'Change',
					name:'change',
					value:0,
					id:'change'
				},
				{
					fieldLabel:'Roomid',
					name:'newroomid',
					inputType:'hidden',
					value:this.roomid,
					id:'myroom_id'
				}
			],
			buttons:[
				{
					text:'Recompute',
					iconCls:'x-icon-calculator',
					handler: recompute
				}
				,
				{text:'Check-in Now', iconCls:'x-icon-dooropen',handler: function() {
						alert('Are you sure?');
						recompute();
						Ext.getCmp('checkinform').getForm().submit({
							url:'./ajax/checkin.php',
							params:{act:'newcheckin'},
							success:function() {
								document.location.href='index.php'
							}
						});
					}
				}
			]
		});
		var payment = new Ext.Panel({
			//title:'Payment Details',
			layout:'form',
			frame:false,
			border:false,
			height:220,
			style:'padding:10 10 10 10',
			defaults:{
				style:'text-align:right;',
				width:200,
				labelWidth:180
			},
			defaultType:'keypadfield',
			items:[
				{
					fieldLabel:'Cash Payment',
					name:'cash',
					value:0,
					id:'cash',
					listeners:{
						change: {
							fn: function(b,e) {
								document.getElementById('amountpaid').value= document.getElementById('card').value + 1*b.value;
								recompute();
							}
						}
					}
				},
				{
					fieldLabel:'Card Payment',
					name:'card',
					value:0,
					id:'card',
					listeners:{
						change: {
							fn: function(b,e) {
								document.getElementById('amountpaid').value= document.getElementById('cash').value + 1*b.value;
								recompute();
							}
						}
					}
				},
				{
					xtype: 'checkboxgroup',
					style:'text-align:left',
					fieldLabel:'CC Type',
					columns: 1,
					items: [
						{boxLabel: 'Visa', name: 'ccard-1',value:'visa'},
						{boxLabel: 'MasterCard', name: 'ccard-2', value:'mastercard'}
					]

				},
				{
					fieldLabel:'Batch Number',
					name:'batch',
					id:'batch'
				},
				{
					fieldLabel:'Trans ID',
					name:'trxnid',
					id:'trxnid'
				}
			]
		});
		//this.add(ratespanel,discountpanel,extraitems,adjustment,payment,summary);
		
		 var tools = [{
				id:'gear',
				handler: function(){
					Ext.Msg.alert('Message', 'The Settings tool was clicked.');
				}
			},{
				id:'close',
				handler: function(e, target, panel){
					panel.ownerCt.remove(panel, true);
				}
			}];

		this.add({
            xtype:'portal',
			width:1000,
            margins:'35 5 5 0',
            items:[{
                columnWidth:.5,
                style:'padding:10px 10px 10px 10px',
                items:[{
                    title: 'Room Charge',
                    tools: tools,
                    items: ratespanel
                }
				,{
                    title: 'Discount',
                    tools: tools,
                    items: discountpanel
                }
				,{
                    title: 'Length of Stay Adjustments',
                    tools: tools,
                    items: extraitems
                }
				,{
                    title: 'Rate Adjustments',
                    tools: tools,
                    items: adjustment
                }
				]
            },{
                columnWidth:.5,
                style:'padding:10px 10px 10px 10px',
				frame:true,
				border:true,
                items:[{
                    title: 'Deposit / Payments',
                    tools: tools,
                    items: payment
                },
				{
                    title: 'Summary of Charges',
                    tools: tools,
					tbar:[{
							xtype:'button',
							text:'Recompute',
							iconCls:'x-icon-calculator',
							handler:recompute
						}],
                    items: summary
                }
				
				
				]
            }]
			});
			
		function recompute() {
			document.getElementById('extracharge').value= document.getElementById('hiddenhourextend').value * document.getElementById('hoursextend').value;
			var roomcharge=document.getElementById('roomcharge').value;
			var extracharge=document.getElementById('extracharge').value;
			var discount=document.getElementById('discount').value;
			var cash=document.getElementById('cash').value;
			var card=document.getElementById('card').value;
			
			document.getElementById('totalcharge').value = 1*roomcharge +  1*extracharge - discount;
			document.getElementById('amountpaid').value = 1*cash + 1*card;
			document.getElementById('change').value =   1*document.getElementById('amountpaid').value - 1*document.getElementById('totalcharge').value;
		}
	}//end onrender
});
Ext.reg('simplecheckin', Application.SimpleCheckin);
