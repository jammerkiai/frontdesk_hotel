Shogun Hotel Front Desk System (FDS)

Web-based Hotel Management System using PHP 5 and MySQL

This project also makes use of the following tools/frameworks:

- ExtJs for the main UI
- JQuery for ajax and main module UI
- JqEasy for reservation module UI
- Digital Persona fingerprint scanner for security authentication
