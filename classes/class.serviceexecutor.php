<?php
/**
* @Filename: class.serviceexecutor.php
* @Desc: caller class will pass parameters to webservice via cUrl
* @Author: jammer
*
*/

class ServiceExecutor 
{
	protected $_url = '';
	protected $_handle;
	protected $_data;
	protected $_preparedData = '';
	protected $_result;
	
	/**
	* @param array $data, usually $_POST
	*/
	public function __construct($url = '', $data)
	{
		$this->_url = $url;
		$this->_data = $data;
		$this->_handle = curl_init($this->_url);
		$this->_prepare()->post();
	}
	
	/**
	* prepare data for sending to webservice
	*/
	protected function _prepare()
	{
		foreach ($this->_data as $key => $value) {
			$this->_preparedData .= "&$key=$value";
		}
		return $this;
	}
	
	/**
	* send data to the webservice
	*/
	public function post()
	{
		$options = array(
			CURLOPT_RETURNTRANSFER => true,         // return web page
			CURLOPT_CONNECTTIMEOUT => 120,          // timeout on connect
			CURLOPT_TIMEOUT        => 120,          // timeout on response
			CURLOPT_MAXREDIRS      => 10,           // stop after 10 redirects
			CURLOPT_POST            => 1,            // i am sending post data
			CURLOPT_POSTFIELDS     => $this->_preparedData,    // this are my post vars
			CURLOPT_SSL_VERIFYHOST => 0,            // don't verify ssl
			CURLOPT_SSL_VERIFYPEER => false,        //
		); 
		curl_setopt_array($this->_handle, $options);
		$this->_result = curl_exec($this->_handle);
	}
	
	public function showResults()
	{
		return $this->_result;
	}
}


