<?php
/**
* @filename: glcodepairs
* 
* array of glcodepairs for particular transactions
*
* 
*/

	    $GL_CODE_PAIR = array(

            // FOR CHECK-IN			
            // CO => CHECK OUT SALES												 		
            
            'CI_NORMAL' => array('1035','3370'), // and NORMAL_RSVN deposit received by cashier as CIT	
            'CO_CASH' => array('1020','1035'),	// and NORMAL RESERVATION CHECK OUT CASH		

            'CO_RSH' => array('3370','5010'), // CHECK OUT SALES ROOM HOTEL						
            'CO_RSW' => array('3370','5020'), // CHECK OUT SALES ROOM WALK-UP 						
             						
            'CO_FSH' => array('3370','5030'), // CHECK OUT SALES FOOD HOTEL  						
            'CO_FSW' => array('3370','5031'), // CHECK OUT SALES FOOD WALK-UP						
            'CO_FSR' => array('3370','5032'), // CHECK OUT SALES FOOD RESTAU						
            'CO_FSG' => array('3370','5033'), // CHECK OUT SALES FOOD THE GARDEN 						
            'CO_FSB' => array('3370','5034'), // CHECK OUT SALES FOOD THE BANQUET						
            'CO_FSL' => array('3370','5035'), // CHECK OUT SALES FOOD LOBBY						
            'CO_FSI' => array('3370','5036'), // CHECK OUT SALES FOOD IN-HOUSE						
            'CO_FSF' => array('3370','5037'), // CHECK OUT SALES FOOD FREE BREAKFAST						
            						
            'CO_BSH' => array('3370','5040'), // CHECK OUT SALES BEVERAGE HOTEL						
            'CO_BSW' => array('3370','5041'), // CHECK OUT SALES BEVERAGE WALK-UP						
            'CO_BSR' => array('3370','5042'), // CHECK OUT SALES BEVERAGE RESTAU						
            'CO_BSG' => array('3370','5043'), // CHECK OUT SALES BEVERAGE THE GARDEN						
            'CO_BSB' => array('3370','5044'), // CHECK OUT SALES BEVERAGE THE BANQUET						
            'CO_BSL' => array('3370','5045'), // CHECK OUT SALES BEVERAGE LOBBY						
            'CO_BSI' => array('3370','5046'), // CHECK OUT SALES BEVERAGE IN-HOUSE						
            						
            'CO_BRH' => array('3370','5050'), // CHECK OUT SALES BEER HOTEL						
            'CO_BRW' => array('3370','5051'), // CHECK OUT SALES BEER WALK-UP						
            'CO_BRR' => array('3370','5052'), // CHECK OUT SALES BEER RESTAU						
            'CO_BRG' => array('3370','5053'), // CHECK OUT SALES BEER THE GARDEN						
            'CO_BRB' => array('3370','5054'), // CHECK OUT SALES BEER THE BANQUET 						
            'CO_BRL' => array('3370','5055'), // CHECK OUT SALES BEER LOBBY						
            'CO_BRI' => array('3370','5056'), // CHECK OUT SALES BEER IN-HOUSE						
            						
            'CO_MSH' => array('3370','5060'), // CHECK OUT SALES MISCE HOTEL						
            'CO_MSW' => array('3370','5061'), // CHECK OUT SALES MISCE WALK-UP						
            							
        // MANAGEMENT ENDORSEMENT	
        // MECI => MANAGEMNET ENDORSE CHECK-IN
        // MECO => MANAGEMENT ENDORSE CHECK-OUT
            	
            'MECI' => array('1370','3375'),      // MANAGEMNET ENDORSE CHECK-IN	
            'MECO_FREE' => array('3375','1370'), // MANAGEMENT ENDORSE CHECK-OUT FREE	
            'MECO_PAID' => array('1030','1370'), // MANAGEMENT ENDORSE CHECK-OUT PAID	

        // FOR SECURITY DEPOSIT					
            			
            'SECURITY_DEPOSIT' => array('1360','3375'),			

        // FOR UNPAID					
        // COU => CHECK-OUT UNPAID
        // SDPS => SECURITY DEPOSIT PAID
        // MECOP => MANAGEMENT ENDORSE CHECK-OUT PAID
            					
            'UNPAID' => array('1350','3375'),					
            'COU_CASH' => array('1020','1350'),

            'RSH' => array('3375','5010'), // SDPS & MECOP CHECK-OUT UNPAID SALES ROOM HOTEL						
            'RSW' => array('3375','5020'),  // SDPS & MECOP CHECK-OUT UNPAID SALES ROOM WALK-UP						
            						
            'FSH' => array('3375','5030'), // SDPS & MECOP CHECK-OUT UNPAID SALES FOOD HOTEL						
            'FSW' => array('3375','5031'), // SDPS & MECOP CHECK-OUT UNPAID SALES FOOD WALK-UP						
            'FSR' => array('3375','5032'), // SDPS & MECOP CHECK-OUT UNPAID SALES FOOD RESTAURANT						
            'FSG' => array('3375','5033'), //  SDPS & MECOPCHECK-OUT UNPAID SALES FOOD GARDEN						
            'FSB' => array('3375','5034'), // SDPS & MECOPCHECK-OUT UNPAID SALES FOOD BANQUET						
            'FSL' => array('3375','5035'), // SDPS & MECOP CHECK-OUT UNPAID SALES FOOD LOBBY						
            'FSI' => array('3375','5036'), // SDPS & MECOP CHECK-OUT UNPAID SALES FOOD INHOUSE						
            'FSF' => array('3375','5037'), // SDPS & MECOPCHECK-OUT UNPAID SALES FOOD FREE						
            						
            'BSH' => array('3375','5040'), // SDPS & MECOPCHECK-OUT SALES BEVERAGE UNPAID HOTEL						
            'BSW' => array('3375','5041'), // SDPS & MECOP CHECK-OUT SALES BEVERAGE UNPAID WALK-UP						
            'BSR' => array('3375','5042'), // SDPS & MECOP CHECK-OUT SALES BEVERAGE UNPAID RESTAURANT						
            'BSG' => array('3375','5043'), // SDPS & MECOP CHECK-OUT SALES BEVERAGE UNPAID GARDEN						
            'BSB' => array('3375','5044'), // SDPS & MECOP CHECK-OUT SALES BEVERAGE UNPAID BANQUET						
            'BSL' => array('3375','5045'), // SDPS & MECOP CHECK-OUT SALES BEVERAGE UNPAID LOBBY						
            'BSI' => array('3375','5046'), // SDPS & MECOP CHECK-OUT SALES BEVERAGE UNPAID INHOUSE						
             						
            'BRH' => array('3375','5050'), // SDPS & MECOPCHECK-OUT UNPAID SALES BEER HOTEL						
            'BRW' => array('3375','5051'), // SDPS & MECOPCHECK-OUT UNPAID SALES BEER WALK-UP						
            'BRR' => array('3375','5052'), // SDPS & MECOPCHECK-OUT UNPAID SALES BEER RESTAURANT						
            'BRG' => array('3375','5053'), // SDPS & MECOPCHECK-OUT UNPAID SALES BEER GARDEN						
            'BRB' => array('3375','5054'), // SDPS & MECOPCHECK-OUT UNPAID SALE BEER BANQUET						
            'BRL' => array('3375','5055'), // SDPS & MECOPCHECK-OUT UNPAID SALES BEER LOBBY						
            'BRI' => array('3375','5056'), // SDPS & MECOPCHECK-OUT UNPAID SALES BEER INHOUSE						
            						
            'MSH' => array('3375','5060'), // SDPS & MECOP CHECK-OUT UNPAID SALES MISC HOTEL						
            'MSW' => array('3375','5061'), // SDPS & MECOPCHECK-OUT UNPAID SALES MISC WALK-UP						
	


        // NORMAL RESERVATION						
        // NRCI => NORMAL RESERVATION CHECK-IN											
            							  							
            'NRCI_DEPCLAIM' => array('3370','3380'), // NORMAL RESERVATION CHECK-IN claim deposit							
          					
        // BANK RESERVATION						
        // BRCI => BANK RESERVATION CHECK-IN					
        // BRCO => BANK RESERVATION CHECK-OUT
        // RCO => RESERVATION CHECK-OUT	
        // RESERVATION CHECK OUT	
            						
            'BANK_RSVN' => array('1030','3380'),						
            'BRCI_CASH' => array('1035','1030'), // BANK RESERVATION CHECK-IN SALES CASH						
            'BRCO_CASH' => array('1030','1035'), // BANK RESERVATION CHECK-OUT SALES CASH						

            'RCO_RSH' => array('3380','5010'), // BANK RESERVATION CHECK-OUT SALES ROOM HOTEL and NORMAL RESERVATION CHECK OUT SALES			
            'RCO_RSW' => array('3380','5020'), // BANK RESERVATION CHECK-OUT SALES ROOM WALKUP and NORMAL RESERVATION CHECK OUT SALES			
            								    			
            'RCO_FSH' => array('3380','5030'), // BANK RESERVATION CHECK-OUT SALES FOOD HOTEL and NORMAL RESERVATION CHECK OUT SALES    			
            'RCO_FSW' => array('3380','5031'), // BANK RESERVATION CHECK-OUT SALES FOOD WALK-UP	and NORMAL RESERVATION CHECK OUT SALES			
            'RCO_FSR' => array('3380','5032'), // BANK RESERVATION CHECK-OUT SALES FOOD RESTAURANT and NORMAL RESERVATION CHECK OUT SALES			
            'RCO_FSG' => array('3380','5033'), // BANK RESERVATION CHECK-OUT SALES FOOD GARDEN and	NORMAL RESERVATION CHECK OUT SALES			
            'RCO_FSB' => array('3380','5034'), // BANK RESERVATION CHECK-OUT SALES FOOD BANQUET and NORMAL RESERVATION CHECK OUT SALES			
            'RCO_FSL' => array('3380','5035'), // BANK RESERVATION CHECK-OUT SALES FOOD LOBBY and NORMAL RESERVATION CHECK OUT SALES			
            'RCO_FSI' => array('3380','5036'), // BANK RESERVATION CHECK-OUT SALES FOOD INHOUSE and	NORMAL RESERVATION CHECK OUT SALES			
            'RCO_FSF' => array('3380','5037'), // BANK RESERVATION CHECK-OUT SALES FOOD FREE and NORMAL RESERVATION CHECK OUT SALES			
            								    			
            'RCO_BSH' => array('3380','5040'), // BANK RESERVATION CHECK-OUT SALES BEVERAGE HOTEL and NORMAL RESERVATION CHECK OUT SALES			
            'RCO_BSW' => array('3380','5041'), // BANK RESERVATION CHECK-OUT SALES BEVERAGE WAKLUP and NORMAL RESERVATION CHECK OUT SALES			
            'RCO_BSR' => array('3380','5042'), // BANK RESERVATION CHECK-OUT SALES BEVERAGE RESTAURANT and	NORMAL RESERVATION CHECK OUT SALES			
            'RCO_BSG' => array('3380','5043'), // BANK RESERVATION CHECK-OUT SALES BEVERAGE GARDEN and NORMAL RESERVATION CHECK OUT SALES			
            'RCO_BSB' => array('3380','5044'), // BANK RESERVATION CHECK-OUT SALES BEVERAGE BANQUET and NORMAL RESERVATION CHECK OUT SALES			
            'RCO_BSL' => array('3380','5045'), // BANK RESERVATION CHECK-OUT SALES BEVERAGE LOBBY and NORMAL RESERVATION CHECK OUT SALES			
            'RCO_BSI' => array('3380','5046'), // BANK RESERVATION CHECK-OUT SALES BEVERAGE INHOUSE and	NORMAL RESERVATION CHECK OUT SALES			
            								   			
            'RCO_BRH' => array('3380','5050'), // BANK RESERVATION CHECK-OUT SALES BEER HOTEL and	NORMAL RESERVATION CHECK OUT SALES			
            'RCO_BRW' => array('3380','5051'), // BANK RESERVATION CHECK-OUT SALES BEER WALKUP and NORMAL RESERVATION CHECK OUT SALES			
            'RCO_BRR' => array('3380','5052'), // BANK RESERVATION CHECK-OUT SALES BEER RESTAURANT and NORMAL RESERVATION CHECK OUT SALES			
            'RCO_BRG' => array('3380','5053'), // BANK RESERVATION CHECK-OUT SALES BEER GARDEN and NORMAL RESERVATION CHECK OUT SALES			
            'RCO_BRB' => array('3380','5054'), // BANK RESERVATION CHECK-OUT SALES BEER BANQUET and NORMAL RESERVATION CHECK OUT SALES			
            'RCO_BRL' => array('3380','5055'), // BANK RESERVATION CHECK-OUT SALES BEER LOBBY and NORMAL RESERVATION CHECK OUT SALES			
            'RCO_BRI' => array('3380','5056'), // BANK RESERVATION CHECK-OUT SALES BEER INHOUSE and NORMAL RESERVATION CHECK OUT SALES			
            								    			
            'RCO_MSH' => array('3380','5060'), // BANK RESERVATION CHECK-OUT SALES MISC HOTEL and	NORMAL RESERVATION CHECK OUT SALES			
            'RCO_MSW' => array('3380','5061'),  // BANK RESERVATION CHECK-OUT SALES MISC WALKUP	and NORMAL RESERVATION CHECK OUT SALES			
	
	        //FOOD ORDER
	
	        'FOUP' => array('1351','3376'), // FOOD ORDER UNPAID
            'FOP_CASH' => array('1020','1351'),	// FOOD ORDER PAID CASH	
	        'FOP_FSH' => array('3376','5030'), // FOOD ORDER PAID SALES  HOTEL  						
            'FOP_FSW' => array('3376','5031'), // FOOD ORDER PAID SALES  WALK-UP						
            'FOP_FSR' => array('3376','5032'), // FOOD ORDER PAID SALES  RESTAU						
            'FOP_FSG' => array('3376','5033'), // FOOD ORDER PAID SALES THE GARDEN 						
            'FOP_FSB' => array('3376','5034'), // FOOD ORDER PAID SALES THE BANQUET						
            'FOP_FSL' => array('3376','5035'), // FOOD ORDER PAIDSALES LOBBY						
            'FOP_FSI' => array('3376','5036'), // FOOD ORDER PAID SALES IN-HOUSE						
            'FO_FSF' => array('3376','5037'), // FOOD ORDER  SALES FREE BREAKFAST	

	        //MISC ORDER
	
	        'MOUP' => array('1352','3377'), // MISC ORDER UNPAID
            'MOP_CASH' => array('1020','1352'),	// MISC ORDER PAID CASH	
            'MOP_MSH' => array('3377','5060'),	// MISC ORDER PAID CASH	
	
            //COMMISSION			
            			
            'COMMISSION_OP' => array('6135','5010'),			
            'NORMDISC_RS' => array('6840','5010'),			
            'FREEFOOD' => array('6840','5345'),			

        );


?>
