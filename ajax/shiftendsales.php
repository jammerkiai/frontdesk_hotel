<?php
session_start();
include_once('config/config.inc.php');
include_once("class.shiftendsales.php");

if (isset($_POST)) {
	$shiftid = $_POST["rblshifts"];
	$sql = "select `shift-transaction_id`, datetime,user_id 
            from `shift-transactions` 
            where `shift-transaction_id` >= '$shiftid' 
            order by datetime asc limit 0, 2";
} else {
    $sql = "select `shift-transaction_id`, datetime,user_id 
            from `shift-transactions` 
            where shift = 'end' order by datetime desc limit 0, 2";
}

$res = mysql_query($sql);

$shiftid = '';

while (list ($id, $sdatetime, $suser_id) = mysql_fetch_row($res)) {
    if ($shiftid === '') {
        $shiftid = $id;
        $startshift = $sdatetime;
    } else {
        $end = $sdatetime;
    }
}

$ses = new shiftendsales($startshift, $end);
$occs = $ses->prepareData($startshift, $end)->listOccupancies();

?>
<html>
<head>
<title>Shiftend Summary for SL Posting</title>
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="../css/start/jquery-ui.css" />
<style>
body {font-family:helvetica;margin:0px;}
table {font-size:12px;font-family:helvetica;border-collapse: collapse;min-width: 500px;}
th {width:auto; border-bottom:1px solid #cccccc;}
.totals {color:#00F;font-weight:bold;}
td {border-bottom:1px solid #cccccc; text-align:left; min-width: 100px; }
.amount {text-align:right}
h3 {font-size:12px;}
.menubar {background-color:#eeeeee;font-size:11px;padding:4px;border-bottom:1px solid #cccccc;}
.content {padding:10px; font-size:11px}
.message {display:inline; color:#ff0000;}
.targets {color: orange;}
.actuals {color: green;}
.details {display: none}

</style>

</head>
<body>
<form method="post">
<div class="menubar">
    <div style="display:inline">
    Select Shift:  <? echo $ses->getLatestShifts($shiftid); ?>
    <input type="submit" name="submit" value="GO" />
    <input type="submit" name="submit" value="POST" />
    </div>
<div class="message"><?php echo $result ?></div>
</div>

<div class="content">
<h4>Cash transactions</h4>
<div class='details'>
<h5>Room</h5>
<?php echo $ses->getRoomCashSales(3) ?>
<h5>Food</h5>
<?php echo $ses->getFnbCashSales(1) ?>
<h5>Beer</h5>
<?php echo $ses->getFnbCashSales(2) ?>
<h5>Beverage</h5>
<?php echo $ses->getFnbCashSales(3) ?>
<h5>Misc</h5>
<?php echo $ses->getRoomCashSales(1) ?>
</div>

<table>
<tr>
	<td>Cash in Bank</td>
	<td><?php echo number_format($ses->total_cash, 2) ?></td>
	<td></td>
	<td></td>
</tr>
<tr>
	<td></td>
	<td></td>
	<td>Room</td>
	<td class="amount"><?php echo number_format($ses->roomsales_cash/$ses::VAT_DIV, 2) ?></td>
</tr>
<tr>
	<td></td>
	<td></td>
	<td>Food</td>
	<td class="amount"><?php echo number_format($ses->food_cash/$ses::VAT_DIV, 2) ?></td>
</tr>
<tr>
	<td></td>
	<td></td>
	<td>Beer</td>
	<td class="amount"><?php echo number_format($ses->beer_cash/$ses::VAT_DIV, 2) ?></td>
</tr>
<tr>
	<td></td>
	<td></td>
	<td>Beverage</td>
	<td class="amount"><?php echo number_format($ses->beverage_cash/$ses::VAT_DIV, 2) ?></td>
</tr>
<tr>
	<td></td>
	<td></td>
	<td>Miscellaneous</td>
	<td class="amount"><?php echo number_format($ses->miscsales_cash/$ses::VAT_DIV, 2) ?></td>
</tr>
<tr>
	<td></td>
	<td></td>
	<td>VAT</td>
	<td class="amount"><?php echo number_format($ses->vat_cash, 2) ?></td>
</tr>
</table>



<h4>Credit Card transactions</h4>

<div class="details">
<h5>Room</h5>
<?php echo $ses->getRoomCardSales(3, 0) ?>
<h5>Food</h5>
<?php echo $ses->getFnbCardSales(1) ?>

<h5>Beer</h5>
<?php echo $ses->getFnbCardSales(2) ?>
<h5>Beverage</h5>
<?php echo $ses->getFnbCardSales(3) ?>
<h5>Misc</h5>
<?php echo $ses->getRoomCardSales(1, 0) ?>
<h5>VAT</h5>
</div>

<table>
<tr>
	<td>Accounts Receivable - Credit Card</td>
	<td class="amount"><?echo number_format($ses->total_credit, 2) ?></td>
	<td>&nbsp;</td>
	<td class="amount">&nbsp;</td>
</tr>
<tr>
	<td></td>
	<td class="amount"></td>
	<td>Room</td>
	<td class="amount"><?php echo number_format($ses->roomsales_credit/$ses::VAT_DIV, 2) ?></td>
</tr>
<tr>
	<td></td>
	<td></td>
	<td>Food</td>
	<td class="amount"><?php echo number_format($ses->food_credit/$ses::VAT_DIV, 2) ?></td>
</tr>
<tr>
	<td></td>
	<td></td>
	<td>Beer</td>
	<td class="amount"><?php echo number_format($ses->beer_credit/$ses::VAT_DIV, 2) ?></td>
</tr>
<tr>
	<td></td>
	<td></td>
	<td>Beverage</td>
	<td class="amount"><?php echo number_format($ses->beverage_credit/$ses::VAT_DIV, 2) ?></td>
</tr>
<tr>
	<td></td>
	<td></td>
	<td>Miscellaneous</td>
	<td class="amount"><?php echo number_format($ses->miscsales_credit/$ses::VAT_DIV, 2) ?></td>
</tr>
<tr>
	<td></td>
	<td></td>
	<td>VAT</td>
	<td class="amount"><?php echo number_format($ses->vat_credit, 2) ?></td>
</tr>
</table>



<h4>Debit Card Transactions</h4>
<div class="details">
<h5>Room</h5>
<?php echo $ses->getRoomCardSales(3, 1) ?>
<h5>Food</h5>
<?php echo $ses->getFnbCardSales(1) ?>
<h5>Beer</h5>
<?php echo $ses->getFnbCardSales(2) ?>
<h5>Beverage</h5>
<?php echo $ses->getFnbCardSales(3) ?>
<h5>Misc</h5>
<?php echo $ses->getRoomCardSales(1, 1) ?>
<h5>VAT</h5>
</div>
<table>
<tr>
	<td>Accounts Receivable - Debit Card</td>
	<td class="amount"><?echo number_format($ses->total_debit/$ses::VAT_DIV, 2) ?></td>
	<td></td>
	<td></td>
</tr>
<tr>
	<td></td>
	<td></td>
	<td>Room</td>
	<td class="amount"><?php echo number_format($ses->roomsales_debit/$ses::VAT_DIV, 2) ?></td>
</tr>
<tr>
	<td></td>
	<td></td>
	<td>Food</td>
	<td class="amount"><?php echo number_format($ses->food_debit/$ses::VAT_DIV, 2) ?></td>
</tr>
<tr>
	<td></td>
	<td></td>
	<td>Beer</td>
	<td class="amount"><?php echo number_format($ses->beer_debit/$ses::VAT_DIV, 2) ?></td>
</tr>
<tr>
	<td></td>
	<td></td>
	<td>Beverage</td>
	<td class="amount"><?php echo number_format($ses->beverage_debit/$ses::VAT_DIV, 2) ?></td>
</tr>
<tr>
	<td></td>
	<td></td>
	<td>Miscellaneous</td>
	<td class="amount"><?php echo number_format($ses->miscsales_debit/$ses::VAT_DIV, 2) ?></td>
</tr>
<tr>
	<td></td>
	<td></td>
	<td>VAT</td>
	<td class="amount"><?php echo number_format($ses->vat_debit, 2) ?></td>
</tr>
</table>


</div>


</form>
<script>

$(document).ready(function(){
    $('#fcdate').datepicker({dateFormat:'yy-mm-dd'});    
});
</script>
<iframe src="" id="mf" title="forecastsummary" style="border: 0; width: 0; height: 0;" />
</body>
</html>

