<?php
class room 
{
	public function room($roomid) 
	{
		$this->roomid=$roomid;
		$this->getroomdetails();
	}
	
	public function getroomdetails() 
	{
		$sql2 = "
			select a.door_name, a.room_type_id, b.room_type_name, a.status, a.ui_top, a.ui_left, a.ui_width, a.ui_height, a.site_id,a.floor_id
			from rooms a, room_types b
			where a.room_type_id=b.room_type_id
			and a.room_id='". $this->roomid."'";
		$res = mysql_query($sql2) or die(mysql_error().$sql2);
		list($this->door_name,$this->room_type_id, $this->room_type_name, $this->status, $this->ui_top,$this->ui_left,$this->ui_width,$this->ui_height,$this->site_id,$this->floor_id) = mysql_fetch_row($res);
	}
	
	public function getroomrates() 
	{
		$sql2 = "SELECT room_type_rates.rtr_id, room_type_rates.amount, rates.rate_name, rates.rate_id, 
		    room_type_rates.room_type_id, room_type_rates.ot_amount,rates.duration, room_type_rates.publishrate
			FROM room_type_rates
			LEFT JOIN rates ON room_type_rates.rate_id = rates.rate_id
			WHERE   room_type_rates.active=1   and room_type_rates.room_type_id =" . $this->room_type_id;
		$res  = mysql_query($sql2) or die(mysql_error().$sql2);
		$this->rates=array();
		while($row=mysql_fetch_array($res)) {
			array_push($this->rates, $row);
		}
	}
	
	public function getroomdiscounts() 
	{
		$sql2 = "SELECT discounts.discount_id, discounts.discount_label, discounts.discount_percent,room_type_discounts.rate_id
		FROM room_type_discounts
		LEFT JOIN discounts ON room_type_discounts.discount_id = discounts.discount_id
		WHERE room_type_discounts.room_type_id =" . $this->room_type_id;
		
		$res  = mysql_query($sql2) or die(mysql_error());
		$this->discounts=array();
		while($row=mysql_fetch_array($res)) {
			array_push($this->discounts, $row);
		}
	}
	
	public function updatestatus($args) {
		$roomid = ($args["roomid"]) ? $args["roomid"] : 0;
		$status = ($args["status_id"]) ? $args["status_id"] : 0;
		$reason = ($args["newremarks"]) ? $args["newremarks"] : '';
		$user = $_SESSION["hotel"]["userid"];
		
		$now = date("Y-m-d H:i:s");
		$sql = "insert into room_log values (null, '$roomid','$status','$now','$user','$reason') ";

		mysql_query($sql) or die('{success:false,error:"Unable to log room status."}');
		$sql = "update rooms set status='$status', last_update='$now', update_by='$user' where room_id='$roomid' ";

		mysql_query($sql) or die('{success:false,error:"Unable to update room status.$sql"}');
	}	

	public function getoccupancy() {
		$sql = " select a.occupancy_id, a.actual_checkin, a.expected_checkout,  c.rate_name, a.shift_checkin, d.fullname, a.rate_id
				from occupancy a, rooms b, rates c, users d
				where a.room_id=b.room_id and a.rate_id=c.rate_id and a.update_by=d.user_id 
				and a.room_id={$this->roomid} and actual_checkout='0000-00-00 00:00:00'
				order by a.actual_checkin desc";
		$res = mysql_query($sql);
		list($this->occupancy_id, $this->actual_checkin,$this->expected_checkout, $this->rate_name,$this->shift_checkin,$this->guestname,$this->rate_id)=mysql_fetch_row($res);
	}
	
	public function getRateGroup() {
		return preg_replace('/[0-9*]/', '', $this->rate_name);
	}
	public function getotamount() {
		$sql = " select ot_amount from room_type_rates where room_type_id='$this->room_type_id' and rate_id='$this->rate_id' ";
		$res = mysql_query($sql) or die(mysql_error());
		list($this->ot_amount)=mysql_fetch_row($res);
		return $this->ot_amount;
	}

	public function getExt12Amount(){
		$phrase = $this->getRateGroup();
		$sql = "select rate_id from rates where rate_name like '%{$phrase}%' and duration=12";
		$res =mysql_query($sql) or die($sql);
		list($rate_id)=mysql_fetch_row($res);
		$sql = "select amount from room_type_rates where rate_id = '$rate_id' and active = 1 and room_type_id = ".$this->room_type_id;
		$res = mysql_query($sql);
		list($this->ot_12_amount)=mysql_fetch_row($res);
		return $this->ot_12_amount;
	}

	public function getExtDayAmount(){
		$phrase = $this->getRateGroup();
		$sql = "select rate_id from rates where rate_name like '%{$phrase}%' and duration=24";
		$res =mysql_query($sql) or die($sql);
		list($rate_id)=mysql_fetch_row($res);
		$sql = "select amount from room_type_rates where rate_id = '$rate_id' and active = 1 and room_type_id = ".$this->room_type_id;
		$res = mysql_query($sql);
		list($this->ot_day_amount)=mysql_fetch_row($res);
		return $this->ot_day_amount;
	}


	public function getovertimehours() {
		//prepare data 
		$this->getoccupancy();
		$this->getotamount();
		//compare expected_checkout date from date now
		$now = new DateTime();
		$eco = new DateTime($this->expected_checkout);

		if($now < $eco) {
			$this->overtime_hours=0;
		}else{
		
			$diff = strtotime(date("Y-m-d H:i:s")) - strtotime($this->expected_checkout) + 1;
			

			$oDTdiff = $now->diff($eco);
			$this->overtime_hours = $oDTdiff->h;
			$ottime = $oDTdiff->h+1;			
			//echo $ottime."x".abs($diff)."x".(1800*($ottime));
			
			if(abs($diff) >= 1800+(3600*$this->overtime_hours))
			{
				$this->overtime_hours = $this->overtime_hours + 1;	
			}
			
		}
	}

	public function getStaySummary()
	{
		$this->getoccupancy();
		$sql  ="select roomsales_id, sales_date, unit_cost*qty, status as total_cost from room_sales where category_id=3 and occupancy_id=$this->occupancy_id";
		$res = mysql_query($sql);
		$occ = $this->occupancy_id;
		$roomid=$this->roomid;
		$ret='<table class="staytable">
		<tr><th>Date</th><th>Status</th><th>Cost of Stay</th></tr>';
		while(list($rid, $date,$cost,$status)=mysql_fetch_row($res)) {
			if($status=='Draft') {
				$ret.="<tr><td>$date</td><td style='text-align:center'>$status</td><td style='text-align:right'>$cost</td>
				<td><a href='?roomid=$roomid&act=del&rid=$rid&date=$date&occ=$occ'>Delete</a></td>
				</tr>";
			}else{
				$ret.="<tr><td>$date</td><td style='text-align:center'>$status</td><td style='text-align:right'>$cost</td></tr>";
			}
		}	
		$ret.='</table>';
		echo $ret;
	}
}
?>
