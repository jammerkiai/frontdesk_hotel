<?php
/**
* checkinform.php
*/
session_start();
include_once("./config/config.inc.php");
include_once("./class.room.php");
include_once("./reserve.functions.php");

function getRoomMinibarQty($room,$mb)
{
	$sql = "select qty from room_minibar where room_id='$room' and minibar_id='$mb' ";
	$res = mysql_query($sql) or die($sql);

	$num = mysql_num_rows($res);
	if($num) {
		$row = mysql_fetch_row($res);
		return $row[0];
	}else{
		return 0;
	}
}

$room = new room( $_GET["room"] );
$reservecode = $_GET["res"] ? $_GET["res"] : 0;
$reservedeposit = $_GET["dep"] ? $_GET["dep"] : 0;
$reserveroomid = $_GET["rr"] ? $_GET["rr"] : 0;
$reserveclose = $_GET["close"] ? $_GET["close"] : 0;
$now = date("Y-m-d H:i:s");
$displaydate = date("l, d F Y");
$room->getroomrates();
$room->getroomdiscounts();

$_SESSION["lastroom"]=$room->room_id;
$_SESSION["lasttab"]=$room->floor_id - 1;


?>
<style>
	.availability {border: 1px solid #ccc; padding:4px; font-size: 11px; margin: 1px; float:left;}
	.reserved {background-color: #f33; color: #fff;cursor:pointer;float:left;}
	.owner {background-color: blue;}
</style>
<link rel="stylesheet" type="text/css" href="../css/ajaxphp.css" />
<link rel="stylesheet" type="text/css" href="../css/jquery.keypad.css" />
<link rel="stylesheet" type="text/css" href="../css/start/jquery-ui.css" />
<link rel="stylesheet" type="text/css" href="../css/jquery.approver.css" />
<form method="post" name="checkinform" id="checkinform" action="checkin.php">
<div  style="border-bottom:1px solid #cccccc;padding-bottom:2px;">
<input type="button"  value="Back to Main"  onclick="javascript:document.location.href='../index.php?f=<?php echo $room->floor_id -1 ?>'"/>
<input type="button"  value="Reload"  onclick="javascript:document.location.href=''<?=$_SELF?>"/>
<div style="float:right;padding:2px;background-color:green;color:#ff9933;font-size:20px;padding-left:4px;padding-right:4px;text-shadow:#111111 1px 1px 1px">
<img src="../img/house.png" />
<span style="color:#FF0000">Room <?=$room->door_name ?> </span>
<?=$displaydate?> <span id="clock"></span>
</div>
<div style="clear:both"></div>
</div>
<table width="970" border=0 height='690'>
<tr>
<td width="330px" valign='top'>
<fieldset id="availability">
<legend>Availability</legend>
<?php
	$guest = '';
	if (isset($_GET['fn']) && isset($_GET['ln'])) {
		$guest = $_GET['fn'] . ' ' . $_GET['ln'];
	}
	echo getRoomAvailabilityForeCast($room->roomid, 27, $guest);
?>
</fieldset>
<fieldset id="rates">
<legend>Rates</legend>
<?php
for ($x=0; $x < count($room->rates);$x++) {
	$amt = $room->rates[$x]['amount'];
	$label = $room->rates[$x]['rate_name'];
	$publishrate = $room->rates[$x]['publishrate'];
	$id = "rate_" .  $room->rates[$x]['rate_id'];
	echo "<input type='button' id='$id' alt='$amt' data-publish='$publishrate'  value='$label' class='ratebuttons' />";
	if ($room->rates[$x]['duration']==12) {
		echo "<input type='hidden' id='new_12HRS_$label' value='$amt' />";
	}
}
?>
</fieldset>

<fieldset id="discounts">
<legend>Discount</legend>
<?php
foreach( $room->discounts as $discount ) {
	$percent = $discount['discount_percent'];
	$rateclass = "rate_" . $discount["rate_id"];
	$name = $discount["discount_label"];
	echo "<input type='button' class='discountbuttons $rateclass'  value='$name' alt='$percent' />";
}
?>
<input type='button' id="otherdisc" class='otherdisc'  value='Other'  />

<fieldset id="adjform"><legend>Please specify reason and enter the new discount amount</legend>
<table class="formtable">

<tr>
<td>Reason</td>
<td><?php
$sql  ="select remark_text from remarks where remark_classification=1" ;
$res= mysql_query($sql) or die(mysql_error());
$ret ="<span style='font-size:.7em;'></span><select name='discount_reason' id='discount_reason'>";
$ret .= "<option></option>";
while(list($text)=mysql_fetch_row($res)) {
	$ret .= "<option value='$text'>$text</option>";
}
$ret.="</select>";
echo $ret;
?></td>
</tr>
<!-- <tr>
<td>OIC Password</td>
<td><input type="password"  id="new_password" name="new_password"  class="textfield" /></td>
</tr> -->
<tr id="adjrow">
<td>Adjustment</td>
<td><input type="text"  id="new_adjustment" name="new_adjustment"  class="money"  /></td>
</tr>
<tr>
<td>Amount</td>
<td><input type="text" name="new_adjustment_amount" id="new_adjustment_amount" class="adjustelement money" /></td>
</tr>
<tr>
<td valign="top">Supervisor/OIC:</td>
<td>

<?php
/*
Username:<br>
$sql = "select username, fullname from users where group_id=4";
$res = mysql_query($sql);
echo "<select name='new_adj_oic' id='new_adj_oic'>";
echo "<option ></option>";
while(list($usr,$full)=mysql_fetch_row($res))
{
	echo "<option value='$usr'>$full</option>";
}
echo "</select>";


<input type="button" name="cmdFingerScan" id="cmdFingerScan" value="Finger Scan" /><br>
Password:<br>
<input type="password" name="new_adj_pwd" id="new_adj_pwd"  />
*/
?>
<input type="button" value="Approve" name="cmdApprove" id="cmdApprove" />
</td>
</tr>
 
</tr>
</table>
</fieldset>
</fieldset>

<fieldset id="discounts">
<legend>Length of Stay (Additional days or hours)</legend>
<table class="formtable">
<!--tr>
<td>Checkin</td>
<td><input type="text"  id="new_checkin" name="new_checkin"  class="dateinput" value="<?=$now?>" /></td>
</tr-->
<tr>
<td>OT Hours</td>
<td><input type="text" value='0' id='new_extension' name="new_extension"  class="numeric"  />
<input type='button' value='-' class='incbutton' alt="#new_extension" />
<input type='button' value='+' class='incbutton' alt="#new_extension"  /></td>
</tr>
<tr><td colspan=2><hr /></td></tr>
<tr id="halfdaysrow">
<td>12-Hour</td>
<td><input type="text" value='0' id='new_halfduration' name="new_halfduration"  class="numeric"  />
<input type='button' value='-' class='incbutton' alt="#new_halfduration" />
<input type='button' value='+' class='incbutton' alt="#new_halfduration"  /></td>
</tr>

<tr id="daysrow">
<td>Days</td>
<td><input type="text" value='0' id='new_duration' name="new_duration"  class="numeric"  />
<input type='button' value='-' class='incbutton' alt="#new_duration" />
<input type='button' value='+' class='incbutton' alt="#new_duration"  /></td>
</tr>
</table>
</fieldset>
<fieldset id="extras">
<legend>Extras</legend>
<table class="formtable">
<?php
$sql =" select sas_id, sas_description,sas_amount from sales_and_services where sas_cat_id=1 ";
$res = mysql_query($sql);
while(list($fnbid,$fnbname, $amount)=mysql_fetch_row($res)) {
	$xtra = "xtra_$fnbid";
	echo "<tr>";
	echo "<td>$fnbname</td>";
	echo "<td><input type='text' name='$xtra' id='$xtra' alt='$amount' value='0' class='numeric extras '></td>";
	echo "<td nowrap=true>
	<input type='button' value='-' class='incbutton' alt='#$xtra' />
<input type='button' value='+' class='incbutton' alt='#$xtra'  />
	</td>";
	echo "</tr>";
}
?>
</table>
</fieldset>
</td>
<?php if ($room->site_id == 2) { ?>
<!--------------------------------------   middle  -------------------------------------->
<td width="330px" valign='top'>
<fieldset id="guestinfo">
<legend>Guest Information</legend>
<input type="hidden" name="new_guestid" id="new_guestid" value="<?php echo $_GET['gid'] ?>" />
<table class="formtable">
<tr>
<td  width="80">First Name</td>
<td><input type="text"  id="new_firstname" name="new_firstname" value="<?php echo $_GET['fn'] ?>"  class="textfield" /></td>
</tr>
<tr>
<td>Last Name</td>
<td><input type="text"  id="new_lastname" name="new_lastname" value="<?php echo $_GET['ln'] ?>"  class="textfield" /></td>
</tr>
<tr>
<td>Company Name</td>
<td><input type="text" value='' id='new_company' name="new_company" value="<?php echo $_GET['cm'] ?>"   class="textfield"  />
</td>
</tr>
<tr>
<td>Remarks</td>
<td><input type="text" value='' id='new_remarks' name="new_remarks"  class="textfield"  />
</td>
</tr>
</table>
</fieldset>
<fieldset id="minibar">
<legend>Minibar</legend>
<table  class="formtable">
<?php

$sql =" select a.minibar_id, a.fnb_id, a.amount,b.fnb_name from minibar a, fnb b where a.fnb_id=b.fnb_id ";
$res = mysql_query($sql);
while(list($mbid, $fnbid, $amount,$fnbname)=mysql_fetch_row($res)) {
	$barname = "bar_$mbid";
	$qty = getRoomMinibarQty($_GET["room"], $mbid);
	
	echo "<tr>";
	echo "<td>$fnbname</td>";
	echo "<td><input type='text' name='$barname' id='$barname' alt='$amount' value='$qty' class='minibar numeric'></td>";
	echo "<td nowrap=true>
	<input type='button' value='-' class='incbutton' alt='#$barname' />
<input type='button' value='+' class='incbutton' alt='#$barname'  />
	</td>";
	echo "</tr>";
}
?>
</table>
</fieldset>
</td>
<!--------------------------------------   middle  -------------------------------------->
<?php } ?>
<td width="330px" valign='top'>
<fieldset id="summary">
<legend>Summary</legend>

<table class="formtable">
<tr>
<td>&nbsp;</td>
<td  style="background-color:#ffcc99;">Converter</td>
<td  style="background-color:#ffcc99;">
<select name="currency" id="currency" class="money"  style="text-align:left">
<option value="1">Peso</option>
<?php
$sql = " select currency_name, currency_value from currency order by currency_name ";
$res =  mysql_query($sql) or die(mysql_error());
while(list($cname, $cvalue)=mysql_fetch_row($res)) {
	echo "<option value='$cvalue'>$cname [Php $cvalue]</option>";
}
?>
</select>
</td>
</tr>
<?php if($reservedeposit) : ?>
<tr>
<td  width="80">Reserve Fee</td>
<td style='text-align:right;'><input type="text" class="money" name="new_roomdeposit" id="new_roomdeposit"
value="<?php echo $reservedeposit ?>">
</td>
<td></td>
</tr>
<?php endif; ?>
<tr>
<td  width="80">Publish Rate</td>
<td><input type="text"   class="money"  id="new_publishrate" name="new_publishrate" value="0" /></td>
<td><input type="hidden"   class="money"  id="new_publishrate_alt" name="new_publishrate_alt" value="0" /></td>
</tr>
<tr>
<td  width="80">Standard Rate</td>
<td><input type="text"   class="money"  id="new_rate" name="new_rate" value="0" /></td>
<td><input type="hidden"   class="money"  id="new_rate_alt" name="new_rate_alt" value="0" /></td>
</tr>
<tr>
<td>Discount</td>
<td><input type="text"  class="money" id="new_discount" name="new_discount" value="0" /></td>
<td><input type="hidden"   class="money" id="new_discount_alt" name="new_discount_alt" value="0" /></td>
</tr>
<tr>
<td>Extras</td>
<td><input type="text"  class="money" id="new_extracost" name="new_extracost" value="0" /></td>
<td><input type="hidden"   class="money" id="new_extracost_alt" name="new_extracost_alt" value="0" /></td>
</tr>
<tr>
<td>Overtime Cost</td>
<td><input type="text"  class="money" id="new_extensioncost" name="new_extensioncost" value="0"/></td>
<td><input type="hidden"  class="money" id="new_extensioncost_alt" name="new_extensioncost_alt" value="0"/></td>
</tr>
<tr>
<td>Amount Due</td>
<td><input type="text"  class="money" id="new_amountdue" name="new_amountdue" value="0" /></td>
<td><input type="text"  class="money" id="new_amountdue_alt" name="new_amountdue_alt" value="0" /></td>
</tr>
<tr style="background-color:#333333;color:#ffffff;">
<td>Change Due</td>
<td><input type="text"  class="money" id="new_changedue" name="new_changedue" value="0" /></td>
<td><input type="hidden"  class="money" id="new_changedue_alt" name="new_changedue_alt" value="0" />
<input type="checkbox" value="1" name="new_alreadypayed" id="new_alreadypayed" /><label for="new_alreadypayed"> Change Returned </label>
</td>
</tr>
<tr>
<td colspan=3>
<input type="checkbox" id='management_endorsement' name='management_endorsement' /> <label for='management_endorsement'>Management Endorsement</label>
</td>
</tr>
</table>
</fieldset>

<fieldset><legend>Payment</legend>
<table class="formtable" border=0 width="290">
<tr>
<td>Cash</td>
<td>
<input type="text"  class="money tender" id="new_cash_tendered" name="new_cash_tendered" value="0" />
</td>
<td>
<input type="text"  class="money tender" id="new_cash_tendered_alt" name="new_cash_tendered_alt" value="0" />
</td>
</tr>
<tr>
<td  width="80">Card</td>
<td>
<input type="text"  class="money tender" id="new_card_tendered" name="new_card_tendered" value="0" />
</td>
<td>
<input type="hidden"  class="money tender" id="new_card_tendered_alt" name="new_card_tendered_alt" value="0" />
</td>
</tr>

<tr>
<td colspan=3>
<fieldset id="carddetails">
<legend  style="font-size:1em">Card Details</legend>
<table class="formtable"  style="font-size:1em">
<tr>
<td>
Card Type</td>
<td>
<input type="radio" name="new_card_type" value="AMEX"  id="ct_4" /> <label for="ct_4"><span width="80px">AMEX</span></label>
<input type="radio" name="new_card_type" value="JCB"  id="ct_3" /> <label for="ct_3">JCB</label><br />
<input type="radio" name="new_card_type" value="Mastercard"  id="ct_2" /> <label for="ct_2">Mastercard</label>
<input type="radio" name="new_card_type" value="Visa"  id="ct_1" /> <label for="ct_1">Visa</label>
<br />
<input type="radio" name="new_card_type" value="BDO Card"  id="ct_6" /> <label for="ct_5"><span width="80px">BDO Card</span></label>
<br />
<input type="radio" name="new_card_type" value="ExpressNet"  id="ct_6" /> <label for="ct_6">ExpressNet</label>
<br />
<input type="radio" name="new_card_type" value="Megalink"  id="ct_7" /> <label for="ct_7">Megalink</label>
<br />
<input type="radio" name="new_card_type" value="BancNet"  id="ct_8" /> <label for="ct_8">BancNet</label>
</td></tr>
<tr>
<td>
Card Suffix</td>
<td>
<input type="text" name="new_card_suffix" value=""  id="new_card_suffix"  class="money full"/> 
</td>
</tr>
<tr>
<td>
Approval Code</td>
<td>
<input type="text" name="new_approval_code" value=""  id="new_approval_code"  class="money full"/> 
</td>
</tr>
<tr>
<td>
Batch #</td>
<td><input type="text" name="new_batch_number" value=""  id="new_batch_number"  class="money full"/> 
</td>
</tr>
<tr>
<td align=right><input type='checkbox' name='newisdebit' value='0'  id='newisdebit' class='' /></td>
<td><label for='newisdebit'>Debit / ATM</label></td>
</tr>
</table>
</fieldset>
</td>
</tr>


<tr>
<td colspan=3 style="background-color:#EFE0D1" align=middle>
<input type="button" class="denomination" value="1" />
<input type="button" class="denomination" value="5" />
<input type="button" class="denomination" value="10" />
<input type="button" class="denomination" value="20" />
<input type="button" class="denomination" value="50" />
<input type="button" class="denomination" value="100" />
<input type="button" class="denomination" value="500" />
<input type="button" class="denomination" value="1000" />
<input type="button" class="denomination half" value="Clear" />
<input type="button" class="denomination" value="0.1" />
<input type="button" class="denomination" value="0.01" />
</td>
</tr>
</table>
<input type="button" class="denomination full" value="Check In" id="checkinsubmit"/>
</fieldset>
</td>
</tr>
</table>
<input type="hidden" id="hidden_ot_amount"  name="hidden_ot_amount"  value="0" />
<input type="hidden" id="hidden_duration"  name="hidden_duration"  value="0" />
<input type="hidden" id="newroomid"  name="newroomid"  value="<?=$_GET["room"]?>" />
<input type="hidden" id="newrateid"  name="newrateid"  value="" />
<input type="hidden" id="reservedeposit"  name="reservedeposit"  value="<?php echo $reservedeposit ?>" />
<input type="hidden" id="reservecode"  name="reservecode"  value="<?php echo $reservecode ?>" />
<input type="hidden" id="reserveroomid"  name="reserveroomid"  value="<?php echo $reserveroomid ?>" />
<input type="hidden" id="reserveclose"  name="reserveclose"  value="<?php echo $reserveclose ?>" />
<input type="hidden" id="act"  name="act"  />
<input type="hidden" id="new_12HRS"  name="new_12HRS" value="0" />
<div id="otstore"></div><div id="durationstore"></div>
</form>
<link rel="stylesheet" type="text/css" href="../css/jquery.keypad.css" />
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/jquery-ui.js"></script>
<script type="text/javascript" src="../js/jquery.keypad.pack.js"></script>
<script type="text/javascript" src="../js/jclock.js"></script>
<script type="text/javascript" src="../js/jquery.approver.js"></script>
<script lang="javascript">

function recompute(){
	//compute extras
	var extracost = 0;
	$.each( $(".extras"), function(o, v) {
		//alert(v.alt);
		extracost =extracost + (  v.value * v.alt);
	});
	
	var currencyconvert = $("#currency").val();
	var rate = $("#new_rate").val()  - 0;
	var disc = $("#new_discount").val()  - 0;
	var ot_amt = $("#hidden_ot_amount").val() - 0;
	var ext = $("#new_extension").val() - 0;
	var ot = ext * ot_amt;
	var reserve = ($("#new_roomdeposit").val()!= undefined) ? $("#new_roomdeposit").val() - 0 : 0;
	var cost12hrs = $("#new_12HRS").val() - 0;
	$("#new_extracost").val( extracost );
	$("#new_extensioncost").val(ot);
	var days = 1 * $("#new_duration").val() + 1;
	var halfdays_amt = $("#new_halfduration").val() * cost12hrs ;
	var amountdue = rate + extracost - disc;
	amountdue=amountdue * (days);
	amountdue = amountdue + halfdays_amt;
	//with reservedeposit
	amountdue=amountdue - reserve + ot;
	$("#new_amountdue").val(amountdue);
	//tendered
	var tender = $("#new_cash_tendered").val()*1 + $("#new_card_tendered").val()*1;
	$("#new_changedue").val(  tender - $("#new_amountdue").val());
	//converters 
	$("#new_rate_alt").val( (rate/currencyconvert).toFixed(2));
	$("#new_discount_alt").val( (disc/currencyconvert).toFixed(2) );
	$("#new_extracost_alt").val( (extracost  / currencyconvert).toFixed(2) );
	$("#new_extensioncost_alt").val( ($("#new_extensioncost").val()  / currencyconvert).toFixed(2)  );
	$("#new_amountdue_alt").val( ($("#new_amountdue").val() / currencyconvert).toFixed(2));
	$("#new_changedue_alt").val( ($("#new_changedue").val() / currencyconvert).toFixed(2));
	$("#new_cash_tendered_alt").val( ($("#new_cash_tendered").val() / currencyconvert).toFixed(2) );
	$("#new_card_tendered_alt").val( ($("#new_card_tendered").val() / currencyconvert).toFixed(2));
}
var myinterval='';
$(document).ready(function(){
	$('.availability.reserved').click(function(){
			var mesg = 'Reserve Code: ' + $(this).attr('data-code');
			mesg += '\nGuest: ' + $(this).attr('data-guest');
			mesg += '\nPartner: ' + $(this).attr('data-partner');
			mesg += '\nReserve Fee: ' + $(this).attr('data-fee');
			alert(mesg);
		});


	$('#new_approval_code').keypad({keypadOnly: false, 
    layout: $.keypad.qwertyLayout});
	$('#new_batch_number').keypad({keypadOnly: false, 
    layout: $.keypad.qwertyLayout});
	$("#daysrow").show();
	$("#new_amountdue_alt").hide();
	$("#new_cash_tendered_alt").hide();
	$("#carddetails").hide();
	$("#clock").jclock({foreground:'yellow',background:'green',fontSize:'20px',timeNotation:'24h'});
	$("#adjform").hide();
	$("#adjrow").hide();
	var focusme="new_cash_tendered";
	<?php
		foreach($room->rates as $rate) {
			$name="rate_" . $rate["rate_id"];
			$value = $rate["ot_amount"];
			$duration = $rate["duration"];
			echo '$("#otstore").data("' . $name .'", ' . $value .');' . "\n";
			echo '$("#durationstore").data("' . $name .'", ' . $duration .');' . "\n";
		}
	?>
	
	$("#currency").change(function(){
		if($(this).val()==1) {
			$("#new_amountdue_alt").hide();
			$("#new_cash_tendered_alt").hide();
		}else{
			$("#new_amountdue_alt").show();	
			$("#new_cash_tendered_alt").show();
		}
		recompute(); });
	$("#otherdisc").click(function() { 
		if( $(this).hasClass("pressed") ) {
			$(this).removeClass("pressed");
			$("#adjform").hide();
			$("#adjrow").hide();
			$("#new_adjustment").val(0);
		}else{
			$(this).addClass("pressed");
			$("#adjform").show();
		}		
	} );
	$("#new_adjustment").change(function(){
		$("#new_discount").val( $(this).val() );
		recompute();
	});
	$("#new_password").keypad();
	$("#new_adjustment").keypad();
	//$("#new_approval_code").keypad();
	//$("#new_batch_number").keypad();
	//$("#new_batch_number").keypad({keypadOnly:false, layout: $.keypad.qwertyLayout});	
	//$("#new_approval_code").keypad({keypadOnly:false, layout: $.keypad.qwertyLayout});

	$("#new_password").change(function(){
		$.post("passcheck.php", {act:'verify' , pwd: $(this).val()}, function(resp) {
			if(resp.success==true) {
				$("#adjrow").show();
			}
		} ,  'json');
	});
	$(".discountbuttons").click(function() {
		var disc = $(this).attr("alt") - 0;
		var rate = $("#new_rate").val() - 0;
		$("#new_discount").val(   (rate * disc/100)  );
		if($("#otherdisc").hasClass("pressed")) $("#otherdisc").trigger("click");
		recompute();
	});
	
	$(".tender").click(function(){
		$(".tender").removeClass("hilite");
		focusme = $(this).attr("id");
		$(this).addClass("hilite");
	});
	
	$(".ratebuttons").click(function(){
		var myid = $(this).attr("id");
		var myval =  $(this).val();
		var new12 = $("#new_12HRS_" + myval.replace('24','12')).val();
		$("#new_12HRS").val(new12);
		$("#new_discount").val(0);
		$(".ratebuttons").removeClass("pressed");
		$(this).addClass("pressed");
		$("#new_rate").val($(this).attr("alt"));
		$("#new_publishrate").val($(this).attr("data-publish"));
		$("#hidden_ot_amount").val($( "#otstore").data( myid )  );
		$("#hidden_duration").val($( "#durationstore").data( myid )  );
		$(".discountbuttons").hide();
		$("." + myid).show();
		var tmp = myid.split("_");

		$("#newrateid").val(tmp[1]);
		if( $("#hidden_duration").val() < 24 ) {
			$("#daysrow").hide();
			$("#halfdaysrow").hide();
		}else{
			$("#daysrow").show();
			$("#halfdaysrow").show();
		}
		
		recompute();
	});
	
	$(".incbutton").click(function(){
		var target = $(this).attr("alt");
		var unitcost = $(target).attr("alt");

		if($(this).val()=="+") 
		{
			var newval = $(target).val()*1 + 1;
			$(target).val(  ( newval > 100 ) ? 100 : newval  );
		}
		else if($(this).val()=="-") 
		{
			var newval = $(target).val()*1 - 1;
			$(target).val(   (newval <= 0) ? 0 : newval );
		}
		recompute();
		
		$(target).trigger('change');
	});
	
	$(".dateinput").datepicker({dateFormat:'yy-mm-dd'});
	
	$(".denomination").click(function() {
		if($(this).val()=="Clear") {
			$("#" + focusme).val( 0);
			if(focusme=="new_cash_tendered_alt") {
				$("#new_cash_tendered").val(  $("#" + focusme).val()  * $("#currency").val()  );
			}	
		}else if($(this).val()=="Check In") {	
			
			if($("#newrateid").val()=="") {
				alert("Please select a valid rate.");
			}else{
				
				if($("#new_changedue").val()< 0)
				{
					if(confirm('Lack of Payment. Do you still want to continue?')){
					$("#act").val("newcheckin");
					$("#checkinform").submit();	
					}
				}else{
					$("#act").val("newcheckin");
					$("#checkinform").submit();	
				}
				
			}
			return false;
		}else{
			var curval = $("#" + focusme).val() * 1;
			var newval = $(this).val() * 1;
			$("#" + focusme).val( curval + newval);
			if(focusme=="new_cash_tendered_alt") {
				$("#new_cash_tendered").val(  $("#" + focusme).val()  * $("#currency").val()  );
			}			
		}
		$("#new_changedue").val($("#" + focusme).val());
		if($("#new_card_tendered").val() > 0) $("#carddetails").show();
		else $("#carddetails").hide();
		recompute();
	});
	$("#new_duration").click(function(){
		recompute();
	});
	$("#new_roomdeposit").change(function(){
		recompute();
	});

	$(".minibar").change(function(){
		$.post('minibar.php',{mb: $(this).attr('id'), qty: $(this).val(), rm: $("#newroomid").val() });
	});

	$("#cmdFingerScan").click(function(){
			$.post("oicfscan.php",{act:'scan', user: $("#new_adj_oic").val()});
			myinterval = setInterval(checkFScan, 3000);
		});

	$('#cmdApprove').approver({
		url: 'oiclist_json.php',
		approvaltype : 'Checkin Discount',
        approvalinfo : '',
		hidetarget: true,
		success: function() {
			$("#new_discount").val( $("#new_adjustment_amount").val() );
			recompute();
		}	
	});
/*
	$("#cmdApprove").click(function(){
			if($('#new_oic_pwd').val()=='') {
				//
				alert('Invalid input.');
			}else{
				$.post('oicvalidator.php',{usr: $("#new_adj_oic").val(), pwd:$('#new_adj_pwd').val()},
					function(resp) {
						if(resp==$("#new_adj_oic").val()) {
							$("#new_discount").val( $("#new_adjustment_amount").val() );
							recompute();
						}else{
							alert('Invalid input.');
						}
					}
				);
			}
			return false;
		});
*/
});

function checkFScan(){
	$.post("oicfscan.php",{act:'monitor', user: $("#new_adj_oic").val()},
	function(resp){
		if(resp.success==true) {
			$("#new_adj_pwd").val(resp.pass);
			//$("#cmdApprove").attr("enabled",true);
			$("#cmdApprove").trigger("click");
			clearInterval(myinterval);
		}
	},'json');
}
</script>
