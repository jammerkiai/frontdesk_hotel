<?php
/**
* extension.php
*/
session_start();
include_once("./config/config.inc.php");
include_once("./config/lobby.inc.php");
include_once("date.functions.php");
include_once("class.room.php");
include_once("reserve.functions.php");

if($_GET["act"]=="del") {
	$rid = $_GET["rid"];
	$date = $_GET["date"];
	$occ = $_GET["occ"];
	$sql = "update occupancy set expected_checkout='$date' where occupancy_id='$occ'";
	mysql_query($sql);
	$sql = "delete from room_sales where occupancy_id='$occ' and sales_date >= '$date' and item_id in ('15','17') ";
	mysql_query($sql);
}

function getCostByItemId($itemid) {
	$sql ="select sas_amount from sales_and_services where sas_id='$itemid'";
	$res = mysql_query($sql);
	$row = mysql_fetch_row($res);
	return $row[0];
}

function doExtras($startdate,$now,$ordercode) {
$occupancy = $_POST["hidden_occupancy"];
$user = $_SESSION["hotel"]["userid"];
$roomid=$_POST["room_id"];
	//discount
	if($_POST['discount']==17) {
		
		$cat = 3;
		$item = 17;
		$discamount = -1 * $_POST['newdiscount'];
		$sql  = " insert into room_sales( occupancy_id, sales_date, category_id, item_id ,  unit_cost, qty,update_date,update_by,room_id) 
				values ('$occupancy', '$startdate', $cat,$item,'$discamount',1,'$now','$user','$roomid') ";
				
		//echo $sql;
		mysql_query($sql) or die(mysql_error().$sql);
	}

	//xbed
	if($_POST['xbed']==11) {
		//echo "xbed";
		$cat = 1;
		$item = 11;
		$qty = $_POST['newxbed'];
		$amt = $_POST['hidden_bed_amount'];
		$sql  = " insert into room_sales( occupancy_id, sales_date, category_id, item_id ,  unit_cost, qty,update_date,order_code,status,update_by,room_id) 
				values ('$occupancy', '$now', $cat,$item,'$amt','$qty','$startdate','$ordercode','Printed','$user','$roomid') ";
		//echo $sql;
		mysql_query($sql) or die(mysql_error().$sql);
	}

	//xpax
	if($_POST['xpax']==23) {
		//echo "xpax";
		$cat = 1;
		$item = 23;
		$qty = $_POST['newxpax'];
		$amt = $_POST['hidden_pax_amount'];
		$sql  = " insert into room_sales( occupancy_id, sales_date, category_id, item_id ,  unit_cost, qty,update_date,order_code,status,update_by,room_id) 
				values ('$occupancy', '$now', $cat,$item,'$amt','$qty','$startdate','$ordercode','Printed','$user','$roomid') ";
		//echo $sql;
		mysql_query($sql) or die(mysql_error().$sql);
	}
}

if($_POST["cmdbtn"]=="Submit") {
$ordercode = mktime();
		if($_POST["choice"]==1) $duration=$_POST["new_extension"];
	if($_POST["choice"]==12) $duration_12=$_POST["new_12_extension"];
	if($_POST["choice"]==24) $duration_days=$_POST["new_day_extension"];
	$roomid=$_POST["room_id"];
	$occupancy = $_POST["hidden_occupancy"];
	$hidden_ot_amount = $_POST["hidden_ot_amount"];
	$hidden_ot_12_amount = $_POST["hidden_ot_12_amount"];
	$hidden_ot_day_amount = $_POST["hidden_ot_day_amount"];
	
	$rm = new room($roomid);
	$rm->getoccupancy();
	$indate = new DateTime($rm->expected_checkout);
	$startdate = $indate->format("Y-m-d H:i:s");
	
	$hrs12 = $duration_12 * 12;

	if(!$duration)$duration = 0;
	if(!$hrs12)$hrs12=0;
	if(!$duration_days)$duration_days=0;
	$newcheckout = date_add($indate, new DateInterval("PT".$duration."H") );
	$newcheckout = date_add($newcheckout, new DateInterval("PT".$hrs12."H"));
	$newcheckout = date_add($newcheckout, new DateInterval("P".$duration_days."D"));
	$newcheckout = $newcheckout->format("Y-m-d H:i:s");
//die($newcheckout);
	$now = date("Y-m-d H:i:s");
	$user = $_SESSION["hotel"]["userid"];
	//step 1. update occupancy_id expected checkout
	$sql = "update occupancy set expected_checkout='$newcheckout',wakeup='0',isalerted='No' where occupancy_id='$occupancy'";
	mysql_query($sql) or die(mysql_error().$sql);

	//step 2. insert into logs 
	$sql = " insert into occupancy_log(transaction_date, occupancy_id, update_by, transaction_type ) value ('$startdate', '$occupancy', '$user', 'Extension' ) ";
	mysql_query($sql) or die($sql);
	
	if($duration)
	{
	//step 3. insert into room_sales
		$sql  = " insert into room_sales( occupancy_id, sales_date, category_id, item_id ,  unit_cost, qty,update_date,update_by,room_id) 
				values ('$occupancy', '$startdate', 3,16,'$hidden_ot_amount','$duration','$now',$user,'$roomid') ";
		mysql_query($sql) or die(mysql_error().$sql);
		doExtras($startdate,$now,$ordercode);
	
	}

	if($duration_12)
	{
	//12 hours
		for($x=0; $x < $duration_12; $x++) {
			$hours = $x * 12;
			$newdate = date('Y-m-d H:i:s',strtotime("$startdate + $hours hours"));
			$sql  = " insert into room_sales( occupancy_id, sales_date, category_id, item_id ,  unit_cost, qty,update_date,remarks ,update_by,room_id) 
					values ('$occupancy', '$newdate', 3,15,'$hidden_ot_12_amount',1,'$now','Extension',$user,'$roomid') ";
			mysql_query($sql) or die(mysql_error().$sql);
			doExtras($newdate,$now,$ordercode);
		}
	}

	if($duration_days)
	{
	//Days
		for($x=0; $x < $duration_days; $x++) {
			$newdate = date('Y-m-d H:i:s',strtotime("$startdate + $x days"));
			$sql  = " insert into room_sales( occupancy_id, sales_date, category_id, item_id ,  unit_cost, qty,update_date,remarks,update_by,room_id) 
				values ('$occupancy', '$newdate', 3,15,'$hidden_ot_day_amount',1,'$now','Extension',$user,'$roomid') ";
			mysql_query($sql) or die(mysql_error().$sql);
			doExtras($newdate,$now,$ordercode);
		}
	}

	header("location: extension.php?roomid=$roomid&msg=1");
}



$roomid=$_GET["roomid"];
$rm = new room($roomid);
$rm->getoccupancy();

$ot_amount = $rm->getotamount();
$ot_amount12 = $rm->getExt12Amount();
$ot_amountday = $rm->getExtDayAmount();

$x_bed = getCostbyItemId(11);
$x_pax = getCostbyItemId(23);

$occupancy = $rm->occupancy_id;
if($_GET["msg"]==1) {
	$mesg = "<div style='color:#ff0000;'>Extension has been saved.</div>";
}

?>
<style>
body {font-size:.7em;}
legend.part{
	font-size:1.2em;
	font-weight:bold;
	border:1px solid #cccccc;
	background-color:#efefef;
	padding:2px;color:#9BD1E6;
}
.staytable {
	font-size:1em;
	border:1px solid #ffcccc;
	padding:4px;
}
div {
	margin:10px;
	border:1px solid #ffeeff;
}
#availability { width: 300px;}
	.availability {border: 1px solid #ccc; padding:4px; font-size: 11px; margin: 1px; float:left;}
	.reserved {background-color: #f33; color: #fff;cursor:pointer; float:left;}
	.occupied {background-color: green; color: #fff; float:left;}
</style>
<form method="post" action="extension.php" name="myform">
<fieldset id="availability">
<legend>Availability</legend>
<?php
	echo getRoomAvailabilityForeCast($roomid, 27);
?>
</fieldset>
<fieldset>
<legend class="part">Set Extension </legend>
<div style="float:left">
<?=$mesg?>
<div class='choicediv'>
<input type='radio' name='choice' value='1' class="choicebutton" id='rdb0' /><label for='rdb0'>Cost per Hr: <span><?=$ot_amount?></span></label><br />
<span id='new_extension_span' class='spanners'>Hours to Extend <input type="text" name="new_extension" id="new_extension" value="0" size="4" class="inputhours" /></span>
<? 
	$sql = "select * from rooms where site_id = '2' and room_id = '$roomid'";
	$res = mysql_query($sql);
	$num = mysql_num_rows($res);
	if($num > 0)
	{
?>
</div>
<div class='choicediv'>
<input type='radio' name='choice' value='12' class="choicebutton" id='rdb1' /><label for='rdb1'>Cost per 12 hours: <span><?=$ot_amount12?></span></label><br />
<span id='new_12_extension_span' class='spanners'>12hrs to Extend <input type="text" name="new_12_extension" id="new_12_extension" value="0" size="4" class="inputhours" /></span>
</div>

<div class='choicediv'>

<input type='radio' name='choice' value='24' class="choicebutton" id='rdb2' /><label for='rdb2'>Cost per Day: <span><?=$ot_amountday?></span></label><br />
<span id='new_day_extension_span' class='spanners'>Day to Extend <input type="text" name="new_day_extension" id="new_day_extension" value="0" size="4" class="inputhours" /></span>

</div>

<div class='choicediv'>
<input type='checkbox' name='discount' value='17' class="choicebutton" id='rdb3' /><label for='rdb3'>Discount: </label><br />
<span id='newdiscount_span'  class='spanners'>Amount <input type="text" name="newdiscount" id="newdiscount" value="0" size="4" class="inputhours" /></span>

</div>

<div class='choicediv'>
<input type='checkbox' name='xbed' value='11' class="choicebutton" id='rdb4' /><label for='rdb4'>Add Bed: <span><?=$x_bed?></span></label><br />
<span id='newxbed_span'  class='spanners'>No. of Beds <input type="text" name="newxbed" id="newxbed" value="0" size="4" class="inputhours" /></span>

</div>

<div class='choicediv'>
<input type='checkbox' name='xpax' value='23' class="choicebutton" id='rdb5' /><label for='rdb5'>Add PAX: <span><?=$x_pax?></span></label><br />
<span id='newxpax_span'  class='spanners'>No. of PAX <input type="text" name="newxpax" id="newxpax" value="0" size="4" class="inputhours" /></span>
<? }?>
</div>

<br />
<input type="submit" name="cmdbtn" value="Submit" style="width:60px;height:50px" 
onclick="return confirm('Submit Extension?  '+ myform.new_day_extension.value +' Days, '+ myform.new_12_extension.value +' 12hrs ' + myform.new_extension.value +' Hours');"/>

<input type="hidden" name="room_id" value="<?=$roomid?>" />
<input type="hidden" name="hidden_ot_amount" value="<?=$ot_amount?>" />
<input type="hidden" name="hidden_ot_12_amount" value="<?=$ot_amount12?>" />
<input type="hidden" name="hidden_ot_day_amount" value="<?=$ot_amountday?>" />
<input type="hidden" name="hidden_occupancy" value="<?=$occupancy?>" 
<input type="hidden" name="hidden_bed_amount" value="<?=$x_bed?>" />
<input type="hidden" name="hidden_pax_amount" value="<?=$x_pax?>" /></div>
<div style="float:left">
<?php echo $rm->getStaySummary(); ?>
</div>
<div style="float:left">
Guidelines / Notes:
<ul>
<li>Input one (1) extension type at a time.</li>
<li>When a combination of extension types is required, save the extensions in the following order:
	<ol>
	<li>Per Day (24-hrs)</li>
	<li>Per 12-hrs</li>
	<li>Per Hour</li>
	</ol>
</li>
</ul>
</div>

<div style="clear:both"></div>
</fieldset>
</form>

<script type='text/javascript' src='../js/jquery.js'></script>
<script type='text/javascript' src='../js/jquery.keypad.pack.js'></script>
 <link rel="stylesheet" type="text/css" href="../css/ajaxphp.css" />
 <script lang="javascript">
	$(document).ready(function(){
		$('.availability.reserved').click(function(){
			var mesg = 'Reserve Code: ' + $(this).attr('data-code');
			mesg += '\nGuest: ' + $(this).attr('data-guest');
			mesg += '\nPartner: ' + $(this).attr('data-partner');
			mesg += '\nReserve Fee: ' + $(this).attr('data-fee');
			alert(mesg);
		});
		$("#new_extension").keypad();
		$("#new_12_extension").keypad();
		$("#new_day_extension").keypad();
		$("#newdiscount").keypad();
		$("#newxbed").keypad();
		$("#newxpax").keypad();
		$('.spanners').hide();
		$('.choicebutton').click(function(){
			
			if($(this).val()=='1') {
				$('.spanners').hide();
				$('#new_extension_span').show();
			}else if($(this).val()=='12') {
				$('.spanners').hide();
				$('#new_12_extension_span').show();
			}else if($(this).val()=='24') {
				$('.spanners').hide();
				$('#new_day_extension_span').show();
			}else if($(this).attr('id')=='rdb3') {
				if($(this).attr('checked')) {
					$('#newdiscount_span').show();
				}else{
					$('#newdiscount_span').hide();
				}
			}else if($(this).attr('id')=='rdb4') {
				if($(this).attr('checked')) {
					$('#newxbed_span').show();
				}else{
					$('#newxbed_span').hide();
				}
			}else if($(this).attr('id')=='rdb5') {
				if($(this).attr('checked')) {
					$('#newxpax_span').show();
				}else{
					$('#newxpax_span').hide();
				}
			}
		});
	});
 </script>
