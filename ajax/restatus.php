<?php
/**
* restatus.php
*/
session_start();
include_once("config/config.inc.php");
include_once("./class.room.php");

/*if($_POST["act"]=="roomstatus" && $_POST["submit"]=="Update Status") {
	$r = new room($_POST["roomid"]) ;
	$r->updatestatus($_POST);
	//header("location: ../index.php");
	echo "<script lang='javascript'>parent.document.location.href='../index.php'</script>";
	exit; 
}*/


function getRoomMinibarQty($room,$mb)
{
	$sql = "select qty from room_minibar where room_id='$room' and minibar_id='$mb' ";
	$res = mysql_query($sql) or die($sql);

	$num = mysql_num_rows($res);
	if($num) {
		$row = mysql_fetch_row($res);
		return $row[0];
	}else{
		return 0;
	}
}

$now = date("Y-m-d");
$room =$_GET["room"] ;
$user = $_SESSION["hotel"]["userid"];
$reason = $_POST["newremarks"];
$rm = new room($room);
$f=$rm->floor_id - 1;
if($_POST["act"]=="oicappr"){
	$user = $_POST["usr"];
	$pwd = $_POST["pwd"];
	$sql = " select user_id from users where username='$user' 
			and userpass='$pwd'  ";
	$res = mysql_query($sql) or die($sql);
	if(mysql_num_rows($res)) {
		list($uid)=mysql_fetch_row($res);
		echo $uid ;
	}else{
		echo 0;
	}
	exit;
}
if($_POST["submit"]){	
	if(!isRoomOccupied($room))
	{
		if($_POST["submit"]=="Available"){
			updateRoomStatus($room,'1');
			updateRoomLog($room,'1',$reason);
		}elseif($_POST["submit"]=="Cleaning"){
			updateRoomStatus($room,'3');
			updateRoomLog($room,'3',$reason);
		}elseif($_POST["submit"]=="Maintenance"){
			updateRoomStatus($room,'4');
			updateRoomLog($room,'4',$reason);
		}else if($_POST["submit"]=="Undo Checkout"){			
			//get last occupancy id for the room
			$_sql = "select occupancy_id from occupancy where actual_checkout <> '0000-00-00 00:00:00' and room_id = '".$_POST["roomid"]."' order by actual_checkout desc limit 0,1";			
			$_res = mysql_query($_sql);			
			list($occupancy_id)=mysql_fetch_row($_res);
			
			//update actual_checkout
			$sql = "update occupancy set actual_checkout='0000-00-00 00:00:00', update_by='$user' where occupancy_id='$occupancy_id'";	
			mysql_query($sql) or die($sql . mysql_error());
	
			//add occupancy log
			$sql = " insert into occupancy_log(transaction_date, occupancy_id, update_by, remarks ) value ('$now', '$occupancy_id', '$user', 'Undo CheckOut') ";
			mysql_query($sql) or die($sql . mysql_error());
	
			//update room status			
			updateRoomStatus($_POST["roomid"],'2');
			updateRoomLog($_POST["roomid"],'2',$reason);
			echo "success";
			exit;
		}else if($_POST["submit"]=="Finish Makeup"){
			$sql = " update makeup set isFinished='Yes', finished_date='$now',updated_by='$user' where room_id='$room' ";
			mysql_query($sql) or die($sql . mysql_error());
			updateRoomStatus($room,'2');
			updateRoomLog($room,'2',$reason);
		}
	}
	
	echo "<script lang='javascript'>parent.document.location.href='../index.php?f=$f&room=$room'</script>";	
	exit; 
}


function updateRoomStatus($roomid,$statusid){
	$now = date("Y-m-d H:i:s");
	$user = $_SESSION["hotel"]["userid"];
	$sql = "update rooms set status='$statusid', last_update='$now', update_by='$user' where room_id='$roomid'";
	mysql_query($sql);
}

function updateRoomLog($roomid,$statusid,$reason){
	$now = date("Y-m-d H:i:s");
	$user = $_SESSION["hotel"]["userid"];
	$sql = "insert into room_log values (null, '$roomid','$status','$now','$user','$reason') ";
	mysql_query($sql);
}

function statusdropdown() {
	$thisarr = array('','Available','Occupied','Cleaning','Maintenance','Make Up');
	$retval = "<select name='status_id' class='inputfield'>";
	for($x=0;$x < count($thisarr);$x++) {
		$retval.="<option value='$x'>";
		$retval.= $thisarr[$x];
		$retval.="</option>";
	}
	$retval.="</select>";
	return $retval;
}

function getRoomLogs($roomid)
{
	$sql = "select remarks,update_date from room_log where remarks <> '' order by update_date desc limit 0,5";
	$res = mysql_query($sql);
	while(list($remarks,$update_date)=mysql_fetch_row($res))
	{
		$retval .= "$update_date - $remarks<br>";
	}
	return $retval;
}

function isRoomOccupied($roomid)
{
	$sql = "select status from rooms where room_id = '$roomid'";
	$res = mysql_query($sql);
	list($status)=mysql_fetch_row($res);
	if($status == '2')
	{
		return true;
	}
	return false;
}
?>
<form method="post" name="checkinform" id="checkinform">

<fieldset id="changestatus">
<legend>Change Room Status</legend>
<table class="formtable" cellpadding="3">
<!-- <tr>
<td>Select New Status: </td><td>
	<?=statusdropdown()?>
</td>
</tr> -->
<tr>
<td >Remarks:<br />
	<textarea cols="40" rows="3" name="newremarks"></textarea>
</td>
<td style="vertical-align:top"><br><? echo getRoomLogs($room); ?>
</td>
</tr>
<tr>
<td align="right">
<?php 

	$_sql = "select isFinished from makeup where room_id = '$room' order by makeup_date desc limit 0,1";
	$_res = mysql_query($_sql);
	list($isFinished)=mysql_fetch_row($_res);
	if($isFinished=='No'){
	?>
		<input type="submit" name="submit" value="Finish Makeup" onclick='return confirm("You choose Finish MakeUp status?");'/>		
	<?}
	else if($isFinished=='Yes' || $isFinished==''){
?>
	<input type="submit" name="submit" value="Available" onclick='return confirm("You choose Available status?");'/>
<?}?>
<input type="submit" name="submit" value="Cleaning" onclick='return confirm("You choose Cleaning status?");' />
<input type="submit" name="submit" value="Maintenance" onclick='return confirm("You choose Maintenance status?");'/>
<br>
</td>
<table class="formtable" cellpadding="3">
<td valign="top">Supervisor/OIC:</td>
<td>
Username:<br>
<?php
$sql = "select username, fullname from users where group_id=4";
$res = mysql_query($sql);
echo "<select name='new_adj_oic' id='new_adj_oic'>";
echo "<option ></option>";
while(list($usr,$full)=mysql_fetch_row($res))
{
	echo "<option value='$usr'>$full</option>";
}
echo "</select>";

?>
<input type="button" name="cmdFingerScan" id="cmdFingerScan" value="Finger Scan" /><br>
Password:<br>
<input type="password" name="new_adj_pwd" id="new_adj_pwd"  />
<input type="button" value="Undo Checkout" name="submit" id="cmdApprove" /> 
</td>
</tr>
</table>

</td>
</td>
&nbsp;
</td>
</tr>
</table>
</fieldset>

<fieldset id="minibar">
<legend>Minibar</legend>
<table  class="formtable">
<?php

$sql =" select a.minibar_id, a.fnb_id, a.amount,b.fnb_name from minibar a, fnb b where a.fnb_id=b.fnb_id ";
$res = mysql_query($sql);
while(list($mbid, $fnbid, $amount,$fnbname)=mysql_fetch_row($res)) {
	$barname = "bar_$mbid";
	$qty = getRoomMinibarQty($room, $mbid);
	
	echo "<tr>";
	echo "<td>$fnbname</td>";
	echo "<td><input type='text' name='$barname' id='$barname' alt='$amount' value='$qty' class='minibar numeric'></td>";
	echo "<td nowrap=true>
	<input type='button' value='-' class='incbutton' alt='#$barname' />
<input type='button' value='+' class='incbutton' alt='#$barname'  />
	</td>";
	echo "</tr>";
}
?>
</table>
</fieldset>
<input type="hidden" name="roomid" value="<?=$room?>" id='newroomid' />
<input type="hidden" name="act" value="roomstatus" />

</form>
 <link rel="stylesheet" type="text/css" href="../css/start/jquery-ui.css" />
 <link rel="stylesheet" type="text/css" href="../css/ajaxphp.css" />
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/jquery-ui.js"></script>
<script lang="javascript">
var myinterval='';
$(document).ready(function(){
	$(".minibar").change(function(){
		$.post('minibar.php',{mb: $(this).attr('id'), qty: $(this).val(), rm: $("#newroomid").val() });
	});
	$(".incbutton").click(function(){
		var target = $(this).attr("alt");
		var unitcost = $(target).attr("alt");

		if($(this).val()=="+") 
		{
			var newval = $(target).val()*1 + 1;
			$(target).val(  ( newval > 12 ) ? 12 : newval  );
		}
		else if($(this).val()=="-") 
		{
			var newval = $(target).val()*1 - 1;
			$(target).val(   (newval <= 0) ? 0 : newval );
		}
		
		$(target).trigger('change');
	});
$("#cmdApprove").click(function(){
			
			$.post("restatus.php",{act:'oicappr', usr: $("#new_adj_oic").val(), pwd:$("#new_adj_pwd").val() },function(resp){
					if(resp > 0) {
						//recompute();
						//$("#cmdAccept").show();
						if(confirm("You choose to Undo last Checkout?"))
						{
							$.post("restatus.php",{submit:'Undo Checkout',roomid:<?php echo $room ?>},
								function(resp){
									if(resp=='success') parent.document.location.href='../index.php?f=<?php echo $f?>&room=<?php echo $room ?>';
								});
						
						}
					}else{
						alert("Invalid OIC credentials.");
					}					
				});				
		});
		$("#cmdFingerScan").click(function(){
			$.post("oicfscan.php",{act:'scan', user: $("#new_adj_oic").val()});
			myinterval = setInterval(checkFScan, 3000);
		});
});

function checkFScan(){
	$.post("oicfscan.php",{act:'monitor', user: $("#new_adj_oic").val()},
	function(resp){
		if(resp.success==true) {
			$("#new_adj_pwd").val(resp.pass);
			$("#cmdApprove").trigger("click");
			clearInterval(myinterval);
		}
	},'json');
}
</script>