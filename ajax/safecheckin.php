<?php
/* getsafecheckindate()*/

function getSafeCheckInDate($now = null) {

    $now = new DateTime('2012-04-28 23:51:11');
    $time = $now->format('His');

    // 10pm transition
    if ($time > '220000' && $time < '221500') {
        $time = '221500';
    } else if ($time < '220000' && $time > '214500') {
        $time = '214500';

    // 6am transition
    } else if ($time > '060000' && $time < '061500') {
        $time = '061500';
    } else if ($time < '060000' && $time > '054500') {
        $time = '054500';

    // 2pm transition
    } else if ($time > '140000' && $time < '141500') {
        $time = '141500';
    } else if ($time < '140000' && $time > '134500') {
        $time = '134500';
    }

    $date = $now->format('Y-m-d');
    $safedate = new DateTime($date . ' ' . $time);

    return $safedate->format('Y-m-d H:i:s');

}
?>
