<?php
include_once('config/config.inc.php');
require_once('class.forecast.php');
require_once('date.functions.php');
session_start();

//ajax transaction
if (isset($_POST) && $_POST['ajax'] === '1') {
    $seldate = $_POST['seldate'];
    $shift = $_POST['shift'];
    $newval = $_POST['newval'];
    $sql = "update forecast 
            set forecast_value='$newval', adjusted_value='$newval'
            where forecast_date = '$seldate'
            and shiftnum = '$shift'
            ";
    mysql_query($sql) or die("{success: false, error: '$sql' }");
    die("{success: true}");
}


$month = isset($_POST['mo'])? $_POST['mo'] : date('m');
$year = isset($_POST['yr'])? $_POST['yr'] : date('Y');

$sql = "create temporary table temp_fc_monthly as
        select day(forecast_date) as day, 
        if (shiftnum=1, forecast_value, 0) as 'fc1',
        if (shiftnum=1, actual_value, 0) as 'act1',
        if (shiftnum=1, adjusted_value, 0) as 'adj1',
        if (shiftnum=2, forecast_value, 0) as 'fc2',
        if (shiftnum=2, actual_value, 0) as 'act2',
        if (shiftnum=2, adjusted_value, 0) as 'adj2',
        if (shiftnum=3, forecast_value, 0) as 'fc3',
        if (shiftnum=3, actual_value, 0) as 'act3',
        if (shiftnum=3, adjusted_value, 0) as 'adj3'
        from forecast
        where year(forecast_date)='$year'
        and month(forecast_date)='$month'
    ";
mysql_query($sql);
$sql = "select day, 
        sum(fc1) as fc1, sum(act1) as act1, sum(adj1) as adj1,
        sum(fc2) as fc2, sum(act2) as act2, sum(adj2) as adj2,
        sum(fc3) as fc3, sum(act3) as act3, sum(adj3) as adj3
        from temp_fc_monthly
        group by day
        ";
$res = mysql_query($sql) or die($sql);

$data = '';

while ($row = mysql_fetch_object($res)) {
    $day = str_pad($row->day, 2, "0", STR_PAD_LEFT);
    $mo = str_pad($month, 2, "0", STR_PAD_LEFT);
    $date = "{$year}-{$mo}-{$day}";
    $data .= "<tr>";
    $data .= "<td>" . $row->day . "</td>";
    $data .= "<td class='origfc' data-shift='1' data-date='$date'>" . $row->fc1 . "</td>";
    $data .= "<td class='adjfc'>" . $row->adj1 . "</td>";
    $data .= "<td>" . $row->act1 . "</td>";
    $data .= "<td class='origfc' data-shift='2' data-date='$date'>" . $row->fc2 . "</td>";
    $data .= "<td class='adjfc'>" . $row->adj2 . "</td>";
    $data .= "<td >" . $row->act2 . "</td>";
    $data .= "<td class='origfc' data-shift='3' data-date='$date'>" . $row->fc3 . "</td>";
    $data .= "<td  class='adjfc'>" . $row->adj3 . "</td>";
    $data .= "<td >" . $row->act3 . "</td>";
    $data .= "</tr>";
}

?>

<html>
<head>
<title></title>
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="../css/start/jquery-ui.css" />
<style>
body {font-family:helvetica;margin:0px;}
table {font-size:12px;font-family:helvetica;border:1px transparent #000;margin: 0 4px;}
th {width:auto; border:1px solid #cccccc;padding: 2px 6px}
.totals {color:#00F;font-weight:bold;}
td {border-bottom:1px solid #cccccc; text-align:center; padding: 2px; }
.amt {text-align:right}
h3 {font-size:12px;}
.menubar {background-color:#eeeeee;font-size:11px;padding:4px;border-bottom:1px solid #cccccc;}
.content {padding:10px; font-size:11px}
.message {display:inline; color:#ff0000;}
.targets {color: orange;}
.actuals {color: green;}
.menutitle {font-weight: bold;}
.origfc {cursor: pointer; background-color: #eee;}
</style>
</head>
<body>
<form method="post">
<div class="menubar">
<span class="menutitle">Shogun 2 Forecast for Month of </span>
<?php echo getMonthDropdown($month); ?>
<input type="text" name="yr" value="<?php echo $year ?>" id="yr" size="4" maxlength="4">
<input type="submit" name="submit" value="GO" />
<div class="message"><?php echo $result ?></div>
</div>
<table>
<thead>
<tr>
<th rowspan=2>Day</th>
<th colspan=3>Shift 1</th>
<th colspan=3>Shift 2</th>
<th colspan=3>Shift 3</th>
</tr>
<tr>
<th>Original</th>
<th>Adjusted</th>
<th>Actual</th>
<th>Original</th>
<th>Adjusted</th>
<th>Actual</th>
<th>Original</th>
<th>Adjusted</th>
<th>Actual</th>
</tr>
</thead>
<tbody>
<?php 
echo $data;
?>
</tbody>
</table>
</div>
</form>
<script>

$(document).ready(function(){
    $('#fcdate').datepicker({dateFormat:'yy-mm-dd'});
    $('#postaction').click(function(e) {
        return confirm('Are you sure you want to post this total?');
    });
    
    $('.origfc').click(function(e) {
        var shift = $(this).attr('data-shift'),
            seldate = $(this).attr('data-date'),
            currval = $(this).html(),
            newval = '';
        newval = prompt('Modify target forecast for ' + seldate + ' Shift ' + shift + '?', currval);
        if (newval !== currval && newval !== null) {
            if (confirm('Are you sure?')) {
                $.post(
                    'fc_monthly.php', 
                    {
                        ajax: 1,
                        seldate: seldate,
                        shift: shift,
                        newval: newval
                    }   
                );
                $(this).html(newval);
                $(this).next('.adjfc').html(newval);
            } else {
                $(this).html(currval);
            }
        }
    });
});
</script>
</body>
</html>
