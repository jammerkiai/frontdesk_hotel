<?php
/**
* fdadj.php
* front-desk adjustment script
*
*/
session_start();
if($_SESSION['hotel']['oic_ok']==false) {
	header("location: oic_login.php");
}

if(!isset($_GET['occ']) || $_GET['occ']=='') {
	header('location:fd_finder.php');
}
$occ = $_GET['occ'];

include_once("config/config.inc.php");
require_once('acctg/reportfns.php');

if($_POST['submit']=='Cancel') {
	$_SESSION['hotel']['oic_ok']=false;
	header("location: oic_login.php");
}elseif($_POST['submit']=='Save'){
	$date = $_POST["newdate"];
	$updateby='20';
	$remarks = $_POST['newremarks'];
	$amount = $_POST['newamount'];
	$occ = $_POST["occ"];
	if($_POST['roomsales']=='' && $_POST['foodsales']=='') {
		header("location: " . $_SERVER['SELF'] );
	}else{
		if($_POST['roomsales']=='') {
			$table='fnb_sales';
			list($item,$category)=explode('_',$_POST['foodsales']);
		}else{
			$table='room_sales';
			list($item,$category)=explode('_',$_POST['roomsales']);
		}
	}

	if($table!='' && $category!='' && $item!='' && $amount!='' && $remarks!='') {
		$sql = "insert into $table (sales_date,update_date,occupancy_id,category_id,item_id,unit_cost,qty,status,remarks,update_by) 
		values ('$date','$date',$occ,$category,$item,'$amount',1,'Paid','$remarks','$updateby') ";
		mysql_query($sql);
		//echo $sql;
		$_SESSION['hotel']['oic_ok']=false;
		header('location: oic_login.php');
	}else{
		$error = "All data is required.";
	}

}
?>
<html>
<head>
<script src="../js/jquery.js" type="text/javascript"></script>
<script src="../js/jquery-ui.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="../css/jquery.keypad.css" />
<script type="text/javascript" src="../js/jquery.keypad.pack.js"></script>
</head>
<body>
<form method='post' action=''>
<div class='toolbar'>
<span style='padding-right:4px;font-weight:bold;color:#996633;'>Apply Adjustment/Additional Transaction: </span>
<br />
<?php echo 'Room Sale ' . RoomSalesDropDown() ?>
<?php echo '&nbsp;&nbsp;&nbsp;Food or Beverage ' . FoodSalesDropDown()?>
<br />
<label for='adjvalue'>Transaction Date</label><input type='text' id='newdate' name='newdate' value='<?php echo $now ?>' />
<br />
<label for='adjvalue'>Amount</label> <input type='text' id='newamount' name='newamount' />
<br />
<label for='adjvalue'>Remarks</label> <input type='text' id='newremarks' name='newremarks' />
<input type='submit' name='submit' value='Save' />
<input type='submit' name='submit' value='Cancel' />
<input type='hidden' name='occ' id='occ' value='<?php echo $occ ?>' />
</div>
</form>
</body>
</html>