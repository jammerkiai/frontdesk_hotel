<?php
/**
* checkin.php
*/
session_start();
include_once("config/config.inc.php");
include_once("reporting.php");
include_once("currentcash.function.php");
include_once("../classes/class.creditlogger.php");
include_once("../classes/GLCodePairs.php");

$user = $_SESSION["hotel"]["userid"];

function getshift($date) {
	list($d, $t) = explode(" ", $date);
	list($h, $m, $s) = explode(":", $t);
	$sql = "select shift_id from shifts where $h between shift_start and shift_end";
	$res = mysql_query($sql) or die(mysql_error());
	list($shift)=mysql_fetch_row($res);

	if($h < 6 or $h >= 22)
	{
		$shift = 1;
	}
	elseif( $h >= 14 ){
		$shift = 3;
	}
	elseif($h >= 6)
	{
		$shift = 2;
	}

	return  $shift;
}


function getunitprice($item) {
	$sql = "select sas_amount from sales_and_services where sas_id='$item'";
	$res = mysql_query($sql) or die(mysql_error());
	$row = mysql_fetch_row($res);
	return $row[0];
}

function getSafeCheckInDate() {

    $now = new DateTime();
    $time = $now->format('His');

    // 10pm transition
    if ($time > '220000' && $time < '221500') {
        $time = '221500';
    } else if ($time < '220000' && $time > '214500') {
        $time = '214500';

    // 6am transition
    } else if ($time > '060000' && $time < '061500') {
        $time = '061500';
    } else if ($time < '060000' && $time > '054500') {
        $time = '054500';

    // 2pm transition
    } else if ($time > '140000' && $time < '141500') {
        $time = '141500';
    } else if ($time < '140000' && $time > '134500') {
        $time = '134500';
    }

    $date = $now->format('Y-m-d');
    $safedate = new DateTime($date . ' ' . $time);

    return $safedate->format('Y-m-d H:i:s');

}

if($_POST["act"]=="newcheckin")
{
	/* initialize direct variable access for posted data */
	reset($_POST);
	foreach($_POST as $key => $value)
	{
		${$key}=$value;
	}

    /* prepare common data */
	$user = $_SESSION["hotel"]["userid"];
	$now = getSafeCheckInDate();

    $shift = getshift($now);
	$new_checkin=$now;
	$CL = new CreditLine();
	$details = array(
	    'tdate' => $now
	);


	/* compute duration (length of stay) */
	$duration = $new_duration * 24 + $new_halfduration *12 + $new_extension + $hidden_duration;
	if($duration) {
		$indate = new DateTime($new_checkin);
		$newcheckout = date_add($indate, new DateInterval("PT".$duration."H") );
		$newcheckout = $indate->format("Y-m-d H:i:s");
	}

    /* cancel operation if room is not available */
	$roomIsOccupied = isRoomOccupied($newroomid);
	if ($roomIsOccupied) {
		die("This room is not available!");
	}

	/* proceed with transaction */
	if(!$roomIsOccupied)
	{

        $addtoforecast = ($register_flag) ? 1 : 0;

	    /* save occupancy */
	    $sql = "insert into occupancy(room_id, actual_checkin, expected_checkout, rate_id, update_by, shift_checkin,
        regflag)
		    values ('$newroomid','$new_checkin','$newcheckout', '$newrateid','$user', '$shift','$addtoforecast' ) ";
	    $res = mysql_query($sql) or die($sql);
	    $newoccupancy = mysql_insert_id();

	    $details['occupancy'] = $newoccupancy;

	    $sql = " insert into occupancy_log(transaction_date, occupancy_id, update_by, remarks, transaction_type )
	    value ('$now', '$newoccupancy', '$user', '', 'CheckIn' ) ";
	    mysql_query($sql) or die($sql);

	    /* update room status */
	    $sql = " update rooms set status=2, last_update='$now',update_by='$user' where room_id='$newroomid' ";
	    mysql_query($sql) or die($sql);
	    $sql = " insert into room_log (room_id,	status_id, update_date, update_by, remarks) values
			    ('$newroomid', '2', '$now', '$user', 'Standard CheckIn, $new_duration days, $new_extension extension')
	    ";
	    mysql_query($sql) or die($sql);


	    /* process guest details */
	    if($new_guestid) {
		    $sql = " insert into guest_history (guest_id, occupancy_id) values ($new_guestid, $newoccupancy) ";
		    mysql_query($sql);
	    } else {
		    if ($new_firstname != '' && $new_lastname != '') {
			    $sql = "select guest_id from guests where firstname='$new_firstname' and lastname='$new_lastname' ";
			    $res = mysql_query($sql);
			    $numrows = mysql_num_rows($res);
			    if ($numrows) {
				    list($new_guestid) = mysql_fetch_row($res);
			    } else {
				    $sql = "insert into guests (firstname, lastname) values ('$new_firstname','$new_lastname')";
				    $res = mysql_query($sql);
				    $new_guestid = mysql_insert_id();
			    }
			    $sql = " insert into guest_history (guest_id, occupancy_id) values ($new_guestid, $newoccupancy) ";
			    mysql_query($sql);
		    }
	    }

	    /*
	    *   insert  salestransaction details
	    *   room sales --> category 3
        *	standard room charge item_id=15
        */

	    $status = 'Paid';
	    $remarks = $cltype = (isset($_POST["management_endorsement"]) && $_POST["management_endorsement"]=='on') ? 'Management Endorsement' : 'Regular';


	    /* if there is a reservation claimed */
	    if($reservecode) {
		    /**
		    *  new_roomdeposit is actual claim, taken from reservedeposit
		    *  reservedeposit may include other deposits for other rooms
		    */
		    if (isset($new_roomdeposit) && $new_roomdeposit > 0) {
		        $cltype = 'Reservation';
		    } else {
		        $cltype = 'Online Partner Reservation';
		    }
		    $newreservefee = $reservedeposit - $new_roomdeposit;
		    $remarks.=" Reservation Code: $reservecode, Deposit Claimed: $new_roomdeposit";
		    $sql  ="insert into reservation_transactions(transaction_date,reservation_code, occupancy_id, amount_claimed,update_by )
			    values('$now', '$reservecode','$newoccupancy','$new_roomdeposit','$user');
		    " ;
		    mysql_query($sql);

		    /* save to salesreceipts as claimed deposit */
		    $sql = " insert into salesreceipts(receipt_date, occupancy_id, tendertype, amount, update_by)
				    values ('$now', '$newoccupancy', 'Reservation Fee', '$new_roomdeposit','$user')";
		    mysql_query($sql) or die(mysql_error());
		    $salesreceipt_id = mysql_insert_id();
		    $details['tdate'] = $now;
            $details['pdate'] = $now;
            $details['shift'] = $shift;
            $details['amount'] = $new_roomdeposit;
            $details['reftable'] = 'salesreceipts';
            $details['refnum'] = $salesreceipt_id;
            $details['remarks'] = $remarks;
		    $CL->save($GL_CODE_PAIR['NRCI_DEPCLAIM'], $details);//

		    $reservestatus = '';
		    if ($reserveclose) {
			    $reservestatus = " status='Claimed', ";
		    }

		    $sql= "update reservations set $reservestatus
			    reserve_fee = '$newreservefee',
			    date_updated='$now', updated_by='$user'
			    where reserve_code='$reservecode'";
		    mysql_query($sql) or die(mysql_error());

		    if ($reservedeposit >= $new_rate) {
			    //
		    }else{
			    $status = 'Draft';
		    }

		    $sql = "update reserve_rooms set status='Claimed' where rr_id='$reserveroomid' and reserve_code='$reservecode' ";
		    mysql_query($sql) or die(mysql_error());

		    if ($reserveclose == 1) {
			    $sql = "update reserve_rooms set status='Cancelled' where status='Pending' and reserve_code='$reservecode' ";
			    mysql_query($sql) or die(mysql_error());
		    }
	    }

	    /* first set up the initial GL log for the checkin */
	    if (isset($new_alreadypayed) && $new_alreadypayed == 1) {
	        $postAmount = $new_amountdue;
	    } else {
	        $postAmount = $new_cash_tendered;
	    }

	    $details['tdate'] = $now;
        $details['pdate'] = $now;
        $details['shift'] = $shift;
        $details['amount'] = $postAmount;
        $details['reftable'] = 'salesreceipts';
        $details['refnum'] = $salesreceipt_id;
        $details['remarks'] = $remarks;

	    if ($cltype === 'Regular' || $cltype === 'Reservation') {
	        $CL->save($GL_CODE_PAIR['CI_NORMAL'], $details);
	        $rechitCode = 'CO_CASH';
	    } elseif ($cltype === 'Online Partner Reservation') {
	        $CL->save($GL_CODE_PAIR['BANK_RSVN'], $details);
	        $CL->save($GL_CODE_PAIR['BRCI_CASH'], $details);
	        $rechitCode = 'BRCO_CASH';
	    }  elseif ($cltype === 'Management Endorsement') {
	        $CL->save($GL_CODE_PAIR['MECI'], $details);
	        $rechitCode = 'MECO_FREE';
	    }


	    /* insert first day room sale */
	    $sql  =" insert into room_sales( occupancy_id, sales_date,
				category_id, item_id , unit_cost, qty, update_date, remarks,update_by,room_id,status)
			    values ( '$newoccupancy', '$now', 3, 15, '$new_rate',1,'$now','$remarks','$user','$newroomid','$status') ";
	    mysql_query($sql);
	    $newrefnum = mysql_insert_id();

	    /* creditline logging */
	    /* first posting of roomsale, should reflect postdate which is 1 duration unit */
	    $firstDuration = 24;
	    if ($new_duration > 0) {
	        /* +24 hours */
	        $firstDuration = 24;
	    } else {
	        $firstDuration = 12;
	    }

	    /* determine date based on duration unit */
	    $indate2 = new DateTime($now);
		date_add($indate2, new DateInterval("PT".$firstDuration."H") );
		$postDate = $indate2->format("Y-m-d H:i:s");
        $postShift = getShift($postDate);

	    $details['reftable'] = 'room_sales';
        $details['pdate'] = $postDate;
        $details['shift'] = $postShift;

	    $discount = 0;
	    /**
	    * CRLN TRXN
	    *    save roomsale to credit line transactions to compute for future posting
	    */

	    /* discount */
	    if($new_discount)
	    {
	        $discount = $new_discount;
		    $new_discount = -1 * $new_discount;
		    $sql  =" insert into room_sales( occupancy_id, sales_date, category_id, item_id ,  unit_cost, qty,update_date,remarks,update_by,room_id)
			    values ('$newoccupancy', '$now', 3,17,'$new_discount','1','$now','$remarks','$user','$newroomid') ";
		    mysql_query($sql);

		    /* creditline logging */
	        $newdiscountid = mysql_insert_id();
            $details['refnum'] = $newdiscountid;
            $details['remarks'] = 'Regular Deposit: First Day';

            //$CL->save($GL_CODE_PAIR['NORMDISC_RS'], $details);
		    $sql = "insert into discount_log (occupancy_id, discount_given, oic, reason, update_date, remarks,update_by) values
				    ('$newoccupancy', '$new_discount', '$oic', '$new_reason', '$now','$remarks','$user');
			    ";
		    mysql_query($sql);

	    }
        $details['amount'] = $new_rate - $discount;
        $details['refnum'] = $newrefnum;
        $details['remarks'] = 'Room Sale: First Day';
	    $CL->save($GL_CODE_PAIR['CO_RSH'], $details);
        $CL->save($GL_CODE_PAIR[$rechitCode], $details);

	    /* extra bed */
	    if(isset($_POST['xtra_11']) && $_POST['xtra_11'] > 0) {
		    $newunitprice=getunitprice(11);
		    $val= $_POST['xtra_11'];
		    $itemid=11;
		    $sql = "insert into room_sales (sales_date, order_code, occupancy_id, category_id, item_id,  unit_cost, qty, update_date,update_by,room_id)
		    values ('$now', '0', '$newoccupancy', '1' ,'$itemid', '$newunitprice' ,'$val','$now','$user','$newroomid' )";
		    mysql_query($sql) or die($sql . mysql_error()) ;
		    /* creditline logging */
	        $newrefnum = mysql_insert_id();
	        $details['reftable'] = 'room_sales';
            $details['refnum'] = $newrefnum;
            $details['remarks'] = 'Additional Bed: First Day';
            $details['amount'] = $newunitprice * $val;
            $CL->save($GL_CODE_PAIR['CO_RSH'], $details);
		    $CL->save($GL_CODE_PAIR[$rechitCode], $details);
		    $print=true;
	    }

	    /* for length of stay greater than 1 unit */
	    if($new_duration > 0) {
		    for($x=1; $x <= $new_duration;$x++) {
		        $count = $x + 1;
			    $indate =  new DateTime($now);
			    $mynewcheckout = date_add($indate, new DateInterval("P".$x."D") );
			    $mynewcheckout = $indate->format("Y-m-d H:i:s");
			    $sql  =" insert into room_sales( occupancy_id, sales_date, category_id, item_id , unit_cost, qty, update_date, remarks,update_by,room_id)
			    values ( '$newoccupancy', '$mynewcheckout', 3, 15, '$new_rate',1,'$now','$remarks','$user','$newroomid') ";
			    mysql_query($sql) or die($sql);
			    $newrefnum = mysql_insert_id();

                /* creditline logging */
		        date_add($indate2, new DateInterval("PT". 24 ."H") );
		        $postDate = $indate2->format("Y-m-d H:i:s");
                $postShift = getShift($postDate);

	            $details['reftable'] = 'room_sales';
                $details['pdate'] = $postDate;
                $details['shift'] = $shift;
	            $discount = 0;
			    // discount
			    if($new_discount)
			    {
				    //$new_discount = -1 * $new_discount;
				    $discount = $new_discount;
				    $sql  =" insert into room_sales( occupancy_id, sales_date, category_id, item_id ,  unit_cost, qty,update_date,remarks,update_by,room_id)
					    values ('$newoccupancy', '$mynewcheckout', 3,17,'$new_discount','1','$now','$remarks','$user','$newroomid') ";
				    mysql_query($sql);
				    /* creditline logging */
	                $newrefnum = mysql_insert_id();
	                $details['reftable'] = 'room_sales';
                    $details['refnum'] = $newrefnum;
                    $details['remarks'] = "Regular Deposit: Day $count";
                    //$CL->save($GL_CODE_PAIR['NORMDISC_RS'], $details);
				    //discount_log
				    $sql = "insert into discount_log (occupancy_id, discount_given, oic, reason, update_date, remarks) values
						    ('$newoccupancy', '$new_discount', '$oic', '$new_reason', '$now','$remarks');
					    ";
				    mysql_query($sql);
			    }

                $details['refnum'] = $newrefnum;
                $details['remarks'] = "Room Sales: Day $count ";
                $details['amount'] = $new_rate + $discount;
                $CL->save($GL_CODE_PAIR['CO_RSH'], $details);
                $CL->save($GL_CODE_PAIR[$rechitCode], $details);


			    if(isset($_POST['xtra_11']) && $_POST['xtra_11'] > 0) {
				    $val= $_POST['xtra_11'];
				    $itemid=11;
				    $sql = "insert into room_sales (sales_date, order_code, occupancy_id, category_id, item_id,  unit_cost, qty, update_date,update_by,room_id)
				    values ('$mynewcheckout', '0', '$newoccupancy', '1' ,'$itemid', '$newunitprice' ,'$val','$now','$user','$newroomid' )";
				    mysql_query($sql) or die($sql . mysql_error()) ;
				    /* creditline logging */
	                $newrefnum = mysql_insert_id();
	                $details['reftable'] = 'room_sales';
                    $details['refnum'] = $newrefnum;
                    $details['remarks'] = 'Additional Bed';
                    $details['amount'] = $newunitprice * $val;
                    $CL->save($GL_CODE_PAIR['CO_RSH'], $details);
                    $CL->save($GL_CODE_PAIR[$rechitCode], $details);
				    $print=true;
				    /**
	                * CRLN TRXN for Xtra BEd
	                */
			    }

		    }
	    }

	    if($new_halfduration > 0) {
		    for($x=0; $x < $new_halfduration;$x++) {
		        $count = $x + 1;
			    if($new_duration > 0) {
				    $indate = new DateTime(date('Y-m-d H:i:s',strtotime($mynewcheckout."+$hidden_duration hours"))) ;
			    }else{
				    if($hidden_duration > 0) {
					    $indate = new DateTime(date('Y-m-d H:i:s',strtotime($now."+$hidden_duration hours")));
				    }else{
					    $indate = new DateTime($now);
				    }
			    }

			    $hrs = $x * 12;
			    $newcheckout2 = date_add($indate, new DateInterval("PT". $hrs ."H") );
			    $newcheckout2 = $indate->format("Y-m-d H:i:s");
			    $halfrate = $new_rate/2;
			    $halfrate = $new_12HRS;
			    $sql  =" insert into room_sales( occupancy_id, sales_date, category_id, item_id , unit_cost, qty, update_date, remarks,update_by,room_id)
			    values ( '$newoccupancy', '$newcheckout2', 3, 15, '$halfrate',1,'$now','$remarks','$user','$newroomid') ";
			    mysql_query($sql) or die($sql);
			    $newrefnum = mysql_insert_id();

                /* creditline logging */
                $postDate = date_add($indate2, new DateInterval("PT". 12 ."H") );
		        $postDate = $indate2->format("Y-m-d H:i:s");
                $postShift = getShift($postDate);

	            $details['reftable'] = 'room_sales';
                $details['refnum'] = $newrefnum;
                $details['remarks'] = "Room Sales: Half-Day $count ";
                $details['amount'] = $halfrate;
                $details['pdate'] = $postDate;
                $details['shift'] = $postShift;
                $CL->save($GL_CODE_PAIR['CO_RSH'], $details);
                $CL->save($GL_CODE_PAIR[$rechitCode], $details);
		    }
	    }

	    //  extension
	    if($new_extension)
	    {
		    if($newcheckout2) {
			    $addtothis = $newcheckout2;
			    $addhours =  12;
		    }elseif($mynewcheckout){
			    $addtothis = $mynewcheckout;
			    $addhours = 24;
		    }else{
			    $addhours = $hidden_duration;
			    $addtothis = $now;
		    }
		    $extendedcheckout  = date('Y-m-d H:i:s', strtotime("$addtothis +$addhours Hours"));
		    $sql  =" insert into room_sales( occupancy_id, sales_date, category_id, item_id , unit_cost, qty, update_date, remarks,update_by,room_id)
			    values ('$newoccupancy', '$extendedcheckout', 3,16,'$hidden_ot_amount','$new_extension','$now','$remarks','$user','$newroomid') ";
		    mysql_query($sql);
		    $details['refnum'] = $newrefnum;
		    /* creditline logging */
            $newrefnum = mysql_insert_id();
            $details['reftable'] = 'room_sales';

            $details['remarks'] = "Room Sales: Extension $new_extension ";
            $details['amount'] = $hidden_ot_amount * $new_extension;
            $details['pdate'] = $extendedcheckout;
            $CL->save($GL_CODE_PAIR['CO_RSH'], $details);
		    $CL->save($GL_CODE_PAIR[$rechitCode], $details);
		    $sql = " insert into occupancy_log(transaction_date, occupancy_id, update_by, transaction_type )
		    value ('$extendedcheckout', '$newoccupancy', '$user', 'Extension' ) ";
		    mysql_query($sql) or die($sql);
	    }

	    reset($_POST);
	    //$print=false;
	    foreach($_POST as $key=>$val)
	    {
		    if(substr($key,0,5)=="xtra_" && $val > 0)
		    {
			    list($tmp, $itemid)=explode("_", $key);
			    if($itemid!=11) {
				    $newunitprice=getunitprice($itemid);
				    $sql = "insert into room_sales (sales_date, order_code, occupancy_id, category_id, item_id,  unit_cost, qty, update_date, update_by,room_id)
				    values ('$now', '0', '$newoccupancy', '1' ,'$itemid', '$newunitprice' ,'$val','$now' ,'$user','$newroomid')";
				    mysql_query($sql) or die($sql . mysql_error()) ;
				    /* creditline logging */
                    $newrefnum = mysql_insert_id();
                    $details['reftable'] = 'room_sales';
                    $details['refnum'] = $newrefnum;
                    $details['remarks'] = "Room Sales: Extra Items - $itemid ";
                    $details['amount'] = $newunitprice * $val;
                    $details['pdate'] = $extendedcheckout;
                    $CL->save($GL_CODE_PAIR['CO_RSH'], $details);
                    $CL->save($GL_CODE_PAIR[$rechitCode], $details);
				    $print=true;
			    }
		    }
	    }



	    //payments

	    if( ($new_cash_tendered > 0)  &&  ($new_card_tendered > 0) )
	    {
		    $amt_tendered = $new_cash_tendered + $new_card_tendered;

	    }
	    elseif( ($new_cash_tendered > 0) &&  ($new_card_tendered <= 0) )
	    {

		    if($new_cash_tendered == $new_amountdue)
		    {
			    $sql = " insert into salesreceipts(receipt_date, occupancy_id, tendertype, amount, update_by)
				    values ('$now', '$newoccupancy', 'Cash', '$new_cash_tendered','$user')";
			    mysql_query($sql) or die(mysql_error());

			    setCurrentCash($new_cash_tendered,'in',$user);
			    /*
				    set all roomsales items to paid
			    */
			    $sql ="update room_sales set status='Paid',update_date = '$now' where occupancy_id='$newoccupancy'";
			    mysql_query($sql) or die(mysql_error());
		    }
		    elseif($new_cash_tendered > $new_amountdue)
		    {
			    /*
				    insert only enough $new_amountdue
			    */
			    $sql = " insert into salesreceipts(receipt_date, occupancy_id, tendertype, amount, update_by)
				    values ('$now', '$newoccupancy', 'Cash', '$new_amountdue','$user')";
			    mysql_query($sql) or die(mysql_error());

			    setCurrentCash($new_amountdue,'in',$user);
			    /*
				    set all roomsales items to paid
			    */
			    $sql =" update room_sales set status='Paid',update_by='$user', update_date = '$now' where occupancy_id='$newoccupancy'";
			    mysql_query($sql) or die(mysql_error());
			    /*
			    insert as deposit the balance from the account
			    */
			    $deposit = $new_cash_tendered - $new_amountdue;
			    if($new_alreadypayed != '1')
			    {
				    if($deposit >= 0) {
				    $sql = " insert into salesreceipts(receipt_date, occupancy_id, tendertype, amount,update_by)
					    values ('$now', '$newoccupancy', 'Deposit', '$deposit','$user')";
				    mysql_query($sql) or die(mysql_error());
				    setCurrentCash($deposit,'in',$user);
				    }
			    }
		    }
	    }
	    elseif( ($new_cash_tendered <= 0) &&  ($new_card_tendered > 0) )
	    {
			$tendertype = ($newisdebit == 1) ? 'Debit' : 'Credit';
		    if( $new_card_tendered >= $new_amountdue ) {

		        /* insert receipt details */
			    $sql = " insert into salesreceipts(receipt_date, occupancy_id, tendertype, amount,update_by)
				    values ('$now', '$newoccupancy', 'Card', '$new_card_tendered','$user')";
			    mysql_query($sql) or die(mysql_error());
			    $newsalesid = mysql_insert_id();
			    $sql ="update room_sales set status='Paid',update_by='$user',update_date = '$now',
					tendertype='$tendertype' where occupancy_id='$newoccupancy'";
			    mysql_query($sql) or die(mysql_error());


			    /* insert card details */
			$new_card_suffix = $_POST['new_card_suffix'];
			    $sql = " insert into card_payment_details (salesreceipt_id, card_type, approval_code, batch_number, is_debit, card_suffix)
					    values ('$newsalesid','{$_POST["new_card_type"]}','$new_approval_code','$new_batch_number', '$newisdebit', '$new_card_suffix')";
			    $fp = fopen("reports/sql.log", "a");
			    fwrite( $fp,$sql);
			    fclose($fp);
			    mysql_query($sql) or die(mysql_error());
		    }
	    }

        /* print checkin details */
	    if($print) {
		    $obj=new Reporting($newroomid);
		    $retval=$obj->printablemos();
		    $file = "mos{$room}.txt";
		    $fp = fopen("reports/" .$file, "w");
		    fwrite( $fp,$retval);
		    fclose($fp);
		    shell_exec('c:\pstools\psexec c:\xampp\htdocs\fds\ajax\p.bat ' . $file);
	    }
	}

	include_once("class.room.php");
	$rm = new room($newroomid);
	$f = $rm->floor_id-1;
	echo '<script lang="javascript">parent.document.location.href="../index.php?f='. $f .'&room='.$newroomid.'"</script>';
}

function isRoomOccupied($roomid)
{
	$sql = "select status from rooms where room_id = '$roomid'";
	$res = mysql_query($sql);
	list($status)=mysql_fetch_row($res);
	if($status == '2')
	{
		return true;
	}
	return false;
}


?>
