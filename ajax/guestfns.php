<?php
//guestfns.php

function getCurrentOccupancies() {
	$sql = "select occupancy_id from occupancy
		where actual_checkout='0000-00-00 00:00:00'";
	$res = mysql_query($sql);
	$retval = array();
	while ($row = mysql_fetch_row($res)) {
		$retval[] = $row[0];	
	}
	return $retval;
}

function getCheckedInGuests($key = '') {
    $sql = "select a.occupancy_id, b.guest_id, a.room_id,
        a.actual_checkin, a.expected_checkout,
        concat(b.firstname,' ', b.lastname) as guestname, d.door_name
        from occupancy a, guests b, guest_history c, rooms d
        where a.occupancy_id=c.occupancy_id
        and b.guest_id=c.guest_id
        and a.room_id=d.room_id
        and a.actual_checkout='0000-00-00 00:00:00' 
        order by guestname";
    $res = mysql_query($sql);
    $numrows = mysql_num_rows($res);
    $retval = "<div>$numrows record(s) found.</div>";
    $retval.= "<table class='guesttable'>";
    $retval.= "<tr>";
    $retval.= "<th>Guest Name</th>";
    $retval.= "<th>Room</th>";
    $retval.= "<th>Checkin</th>";        
    $retval.= "<th>Expected Checkout</th>";    
    $retval.= "<th>Actions</th>";    
    $retval.= "</tr>";
    while (list($occ, $gid, $rid, $in, $out, $name, $room) = mysql_fetch_row($res)) {
        $retval.="<tr>";
        $retval.="<td>$name</td>";
        $retval.="<td>$room</td>";
        $retval.="<td>$in</td>";
        $retval.="<td>$out</td>";
        $retval.="<td><input type='submit' name='gh_$gid' value='Guest History'>";
        $retval.="<input type='submit' name='occ_$occ' value='Get Details'></td>";        
        $retval.="</tr>";
    }
    $retval.= "</table>";
    return $retval;
}

function getGuestsByKeywordSearch($key = '') {
    $sql = "select guest_id, concat(firstname,' ', lastname) as guestname
        from guests 
        where  firstname like '%$key%' or lastname like '%$key%' 
        order by guestname";

    $res = mysql_query($sql);
    $numrows = mysql_num_rows($res);
    $retval = "<div>$numrows record(s) found.</div>";
    $retval.= "<table class='guesttable'>";
    $retval.= "<tr>";
    $retval.= "<th>Guest Name</th>";  
    $retval.= "<th>Actions</th>";    
    $retval.= "</tr>";
    while (list($gid, $name) = mysql_fetch_row($res)) {
        $name = ucwords(strtolower($name));
        $retval.="<tr>";
        $retval.="<td>$name</td>";
        $retval.="<td><input type='submit' name='gh_$gid' value='Guest History'>";
        $retval.=" <input type='submit' name='del_$gid' value='Delete' onclick='return confirm(\"Are you sure?\")'></td>";        
        $retval.="</tr>";
    }
    $retval.= "</table>";
    return $retval;
}

function getGuestHistory($gid = '') {
    $sql = "select a.occupancy_id, a.actual_checkin, a.actual_checkout, c.door_name
    		from occupancy a, guest_history b, rooms c
    		where a.room_id=c.room_id
    		and a.occupancy_id=b.occupancy_id
    		and b.guest_id='$gid'
    		order by a.occupancy_id desc";
    $res = mysql_query($sql);
    $numrows = mysql_num_rows($res);
    $retval = "<div>$numrows record(s) found.</div>";
    $retval.= "<table class='guesttable'>";
    $retval.= "<tr>";
    $retval.= "<th>Room #</th>";  
    $retval.= "<th>Check In</th>";
    $retval.= "<th>Check Out</th>"; 
    $retval.= "<th>Action</th>";
    $retval.= "</tr>";
    while (list($occ, $in, $out, $name) = mysql_fetch_row($res)) {
        $name = ucwords(strtolower($name));
        $retval.="<tr>";
        $retval.="<td>$name</td>";
        $retval.="<td>$in</td>";
        $retval.="<td>$out</td>";        
        $retval.="<td><input type='submit' name='occ_$occ' value='Get Details'></td>";
        $retval.="</tr>";
    }
    $retval.= "</table>";
    return $retval;
}

function deleteGuest($gid='') {
	if ($gid !== '') {
		$sql = "delete from guests where guest_id='$gid'";
		mysql_query($sql);
	}
}

function getSalesHistoryByOccupancy($occ) {
	ini_set('allow_url_fopen', 1);
	ini_set('allow_url_include', 1);
	$retval = include_once("occupancydetails.php?occ=$occ&viewonly=1");
	
	return $retval;
}

?>
