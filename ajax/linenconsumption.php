<?php
/**
file: linenconsumption.php
desc: workflow class to encapsulate linen consumption transactions
table: linen_consumption
*/

class linenconsumption 
{    
    public function __construct($occupancy = null, $fordate = null, $forshift = null) {
        $this->now = date('Y-m-d H:i:s');
        $this->user = $_SESSION['hotel']['userid'];
        $this->occupancy = $occupancy;
        $this->for_date = $fordate;
        $this->for_shift = $forshift;
    }
    
    public function saveByRoomtype($roomtype) {
        $kit = $this->getKitFromRoomType($roomtype);        
        $this->saveByKit($kit);
    }
    
    public function saveByKit($kit = null) {
        if (!is_null($kit)) {
            $sql = "select stockid, stock_item_qty
                    from kit_items 
                    where kit_id = '$kit'";
            $res = mysql_query($sql) or die($sql);
            
            $sql2 = "";
            while(list($stockid, $qty) = mysql_fetch_row($res)) {
                $this->saveStockitem($stockid, $qty);
            }
        }
    }

    public function getKitFromRoomType($roomtype) {
        $sql = "select kit_id from roomtype_linenkit where room_type_id='$roomtype'";
        $res = mysql_query($sql) or die($sql);
        if (mysql_num_rows($res)) {
            list($kid) = mysql_fetch_row($res);
            return $kid;
        }
        return null;
    }
    
    public function saveStockitem($stockid, $qty) {
        $sql2 = "insert into linen_consumption 
                    (date_logged, occupancy_id, stock_id, qty, user, for_date, for_shift)
                    values 
                    ('{$this->now}', '{$this->occupancy}', '$stockid', '$qty', '{$this->user}', 
                    '{$this->for_date}', '{$this->for_shift}');
                ";
        mysql_query($sql2);
    }
}
