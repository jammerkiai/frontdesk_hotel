<?php
/**
*	reusable date functions
*/

/**
* var date date_add
* params: 	$date 	- datestring with format "Y-m-d"
* 			$inc 	- integer to increment date with
*			$wc 	- indicates if increment is for Y(ear), M(onth), D(ay)
*/

function getdaterange($start, $daycount) {
	$arr = array();
	for($i=0; $i <=$daycount; $i++) {
		$arr[]=my_date_add($start, $i, 'D');
		//array_push($arr, my_date_add($start, $i, 'D');
	}
	return $arr;
}

function my_date_add($date, $inc, $wc){
	list($yr,$mo,$dy)=explode("-",$date);	
	if($wc=='D'){
		$dy=$dy+$inc;
	}elseif($wc=='M'){
		$mo=$mo+$inc;
	}elseif($wc=='Y'){
		$yr=$yr+$inc;
	}
	return date("Y-m-d", mktime(0,0,0,$mo,$dy,$yr));
}

function buildcalendaraslist($m,$y) {
	$mname = date("F", mktime(0,0,0,$m,1,$y));
	$retval = "<div class='calendar-hdr'><span class='prev'><a href='#p'>Prev</a></span> $mname $y <span class='next'><a href='#n'>Next</a></span></div>";
	$retval.="<ul class='calendar-list'>";
	$newm=$m;
	for($d=1; $d<32; $d++) {
		list($newm, $dname) = explode(" ", date("m l", mktime(0,0,0,$m,$d,$y)));
		if($newm!=$m) break;
		$retval .= "<li class='calendar-item $dname'>$dname <span style='float:right'>$d</span></li>";
	}
	$retval.="</ul>";
	return $retval;
}

function buildcalendar($m,$y) {
	list($mname,$dow) = explode(" ", date("F w", mktime(0,0,0,$m,1,$y)));
	$retval = "<div class='calendar-hdr'><span class='prev'><a href='#p'>Prev</a></span> $mname $y <span class='next'><a href='#n'>Next</a></span></div>";
	$retval.="<table id='reserve-calendar'>";
	$retval.="<thead>";
	$retval.="<tr><th>Su</th>";
	$retval.="<th>Mo</th>";
	$retval.="<th>Tu</th>";
	$retval.="<th>We</th>";
	$retval.="<th>Th</th>";
	$retval.="<th>Fr</th>";
	$retval.="<th>Sa</th>";
	$retval.="</tr>";
	$retval.="</thead>";
	$retval.="<tbody>";
	$newm=$m;
	$lastrow=false;
	$startrow = true;
	$daystart = 1 - $dow;
	$daylist = "";
	for($d=$daystart; $d<40; $d++) {
		list($newm, $dow, $day,$dname) = explode(" ", date("m w d l", mktime(0,0,0,$m,$d,$y)));
		if($newm > $m) $lastrow = true;
		if($dow==0) {
			if($lastrow) {
				$daylist.="</tr>";
				break;
			}else{
				$daylist.=(trim($daylist)=="") ? "<tr>" : "</tr><tr>";
			}
		}
		if($newm!=$m) {
			$class="notmonth";
		}else{
			$class="";
		}
		$tdid = "{$y}-{$newm}-{$day}";
		if($tdid == date("Y-m-d")) $class="today";
		$daylist .= "<td class='calendar-item $class' id='$tdid'><span style='float:right'>$day</span></td>";
		
	}
	$retval.=$daylist;
	$retval.="</tbody>";
	$retval.="</table>";
	return $retval;
}

function getMonthDropdown($selected = '')
{
	if (!$selected) $selected = date('m');
	$ret = "<select name='mo'>";
	for($i=1; $i <= 12; $i++) {
		$ret .= "<option value='$i'";
		if ($i == $selected ) $ret .=" selected ";
		$month = date('F', strtotime("2010/$i/01"));
		$ret .= ">$month</option>";
	}
	$ret.="</select>";
	return $ret;
}

function setStatus($selected)
{
	$arrStatus = array('Pending', 'Active', 'Cancalled');
	$ret = "<select name='selStatus'>";
	foreach($arrStatus as $strStatus) {
		$ret .= "<option value='$strStatus'";
		if ($strStatus == $selected ) $ret .=" selected ";
		$ret .= ">$strStatus</option>";
	}
	$ret.="</select>";
	return $ret;
}
?>