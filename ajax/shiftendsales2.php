<?php
session_start();
include_once('config/config.inc.php');
include_once("class.shiftendsales.php");

if (isset($_POST["rblshifts"])) {
	$shiftid = $_POST["rblshifts"];
	$sql = "select `shift-transaction_id`, datetime,user_id 
            from `shift-transactions` 
            where `shift-transaction_id` >= '$shiftid' 
            order by datetime asc limit 0, 2";
} else {
    $sql = "select `shift-transaction_id`, datetime,user_id 
            from `shift-transactions` 
            where shift = 'end' order by datetime desc limit 0, 2";
}

$res = mysql_query($sql);

$shiftid = '';

while (list ($id, $sdatetime, $suser_id) = mysql_fetch_row($res)) {
    if ($shiftid === '') {
        $shiftid = $id;
        $startshift = $sdatetime;
    } else {
        $end = $sdatetime;
    }
}

$ses = new shiftendsales($startshift, $end);

$ses->getOccupancies()->prepareData($startshift, $end);


$ses->resetData();
/*
$ses->getInTransitRoomSales();	
$ses->getCheckoutRoomSales();
$ses->getInTransitMiscSales();
$ses->getCheckoutMiscSales();
$ses->getInTransitFnbSales(1);
$ses->getCheckoutFnbSales(1);
$ses->getInTransitFnbSales(2);
$ses->getCheckoutFnbSales(2);
$ses->getInTransitFnbSales(3);
$ses->getCheckoutFnbSales(3);
*/
//room sales checkout
$ses->getCheckoutRoomSales();
//room sales 
$ses->getInTransitRoomSales();

//misc sales from intransit rooms
$ses->getInTransitMiscSales();
$ses->getCheckoutMiscSales();

$ses->getCheckoutFnbSales();
$ses->getInTransitFoodForRoomSales();

//sales for all types from special rooms
$ses->getSalesBySpecialOccupancy();
$ses->processTenderType();



$table = '<table>';
foreach ($ses->data as $index => $row) {
	$table.='<tr>';
	foreach ($row as $field) {
		$table.='<td>' . $field . '</td>';
	}
	$table.='</tr>';
}
$table.='</table>';


$totals = '<table>';
foreach ($ses->totals as $saletype => $row) {
	$totals.= '<tr>';
	$totals.= "<td>$saletype</td>";
	foreach ($row as $tender => $amount) {
		$totals.="<td>$tender</td><td>$amount</td>";
	}
	$totals.= '</tr>';
}
$totals.='</table>';


/*
echo "<hr>";
echo $ses->getInTransitFnbSales(3);
echo "<hr>";
echo $ses->getCheckoutFnbSales(3);
*/


//$occs = $ses->prepareData($startshift, $end)->listOccupancies();

?>
<html>
<head>
<title>Shiftend Summary for SL Posting</title>
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="../css/start/jquery-ui.css" />
<style>
body {font-family:helvetica;margin:0px;}
table {font-size:12px;font-family:helvetica;border-collapse: collapse;min-width: 500px;margin: 10px;}
th {width:auto; border-bottom:1px solid #cccccc;}
.totals {color:#00F;font-weight:bold;}
td {border-bottom:1px solid #cccccc; text-align:left; min-width: 100px; }
.amount {text-align:right}
h3 {font-size:12px;}
.menubar {background-color:#eeeeee;font-size:11px;padding:4px;border-bottom:1px solid #cccccc;}
.content {padding:10px; font-size:11px}
.message {display:inline; color:#ff0000;}
.targets {color: orange;}
.actuals {color: green;}
.details {display: none;}

</style>

</head>
<body>
<form method="post">
<div class="menubar">
    <div style="display:inline">
    Select Shift:  <? echo $ses->getLatestShifts($shiftid); ?>
    <input type="submit" name="submit" value="GO" />
    <input type="submit" name="submit" value="POST" />
    </div>
<div class="message"><?php echo $result ?></div>
</div>
<?php if (!isset($_POST["rblshifts"])) die(); ?>
<div class="content">

<?php 
/*
foreach ($ses->revenuesrc as $key => $value) {
	echo "<h3>$value</h3";
	echo $ses->displayTotals($value, 'Cash');
	echo $ses->displayTotals($value, 'Credit');
	echo $ses->displayTotals($value, 'Debit');
}
*/

echo $ses->displaySpecial();
?>

	<a href='#' id='detailtoggle'>Show</a>
	<div id="details" class="details">
	<?php echo $table ?>
	</div>
</div>

</form>
<script>

$(document).ready(function(){
    $('#fcdate').datepicker({dateFormat:'yy-mm-dd'});
	$('#detailtoggle').click(function(e){
		e.preventDefault();
		var val = $(this).html();
		if (val === 'Show') {
			$('#details').show();
			$(this).html('Hide');
		} else {
			$('#details').hide();
			$(this).html('Show');
		}
	}); 
});
</script>
<iframe src="" id="mf" title="forecastsummary" style="border: 0; width: 0; height: 0;" />
</body>
</html>

