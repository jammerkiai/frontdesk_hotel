<?php
session_start();
include_once("config/config.inc.php");
include_once("reporting.php");
$room = $_GET["roomid"];
$obj = new Reporting($room);
if($_GET["print"]==1) {
	$retval=chr(hexdec('1B')).chr(hexdec('4D')).chr(1);
	$retval.=$obj->printableros()."\n\n\n\n\n";
	$retval.=chr(hexdec('1D')).chr(hexdec('56')).chr(49);
	$file = "ros{$room}.txt";
	$fp = fopen("reports/" .$file, "w");
	fwrite( $fp,$retval);
	fclose($fp);
	shell_exec('c:\pstools\psexec c:\xampp\htdocs\fds\ajax\p.bat ' . $file);
	
}
if($obj->occupied == 0)
{
	die("Not Check In");
}
?>
<style>
h1 { text-align:center}
table {font-family:lucida,arial,helvetica}
table td {font-size:.6em} 
table th {font-size:.7em}
.x-footer{bottom:0;left:10;font-size:.6em}
</style>
<h1>Room Sales Summary</h1>
<br />
<table width='100%' >
<tr>
<td>Room</td><td><?php echo $obj->doorname ?></td>
<td>Check-in</td><td><?php echo date("Y-m-j h:i A",strtotime($obj->checkindate)) ?></td>
</tr>
<tr>
<td>Rate</td><td><?php echo $obj->ratename ?></td>
<td>Exp Checkout</td><td><?php echo date("Y-m-j h:i A",strtotime($obj->etd)) ?></td>
</tr>
<tr>

<td>Shift</td><td><?php echo $obj->shift ?></td>
<td>Wake Up</td><td><?php echo date("Y-m-j h:i A",strtotime($obj->wakeup)) ?></td>
</tr>

<tr>
<th>Room Sales:</th><th colspan=3>&nbsp;</th>
</tr>
</table>

<?php echo $obj->getRoomSales() ?>

<br />
<br />
<br />
<div class='x-footer'>
Last Update: <?php echo $obj->checkindate ?><br />
Updated by: <?php echo $obj->fullname ?><br />
</div>
