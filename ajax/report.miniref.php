<?php
session_start();
include_once("config/config.inc.php");
include_once("monthly.class.php");

$lsql = "select settings_value from settings where id = '3'";
$lres = mysql_query($lsql);
list($lobbyid)=mysql_fetch_row($lres);


function getMiniRefReport($month,$year)
{
	$num = cal_days_in_month(CAL_GREGORIAN, $month, $year) ;

	$sql = "select settings_value from settings where id = '1'";
	$res = mysql_query($sql);
	list($value) = mysql_fetch_row($res);

	$ret = "<div style='font-weight:bold'>".strtoupper($value)."<br>
	MINI REF SALES SUMMARY<br>
	FOR THE MONTH OF ".strtoupper(getMonthName($month))." ".$year."</div><br><br>";
	$ret .= "<table border=1 cellpadding=3 cellspacing=0>";
	$ret .= "<tr>";
	$ret .= "<th>&nbsp;</th>";
	$ret .= "<th>&nbsp;</th>";
	$ret .= "<th>1st shift</th>";
	$ret .= "<th>2nd shift</th>";
	$ret .= "<th>3rd shift</th>";
	$ret .= "<th>Total</th>";
	$ret .= "</tr>";	
	for($i = 1;  $i <= $num; $i++)
	{
		$start = date('Y-m-d H:i:s', strtotime('-1800 seconds',strtotime($year."-".$month."-".$i." 00:00:00")));
		$end = date('Y-m-d H:i:s', strtotime('+1800 seconds',strtotime($year."-".$month."-".$i." 23:59:00")));
		

		$sql = "select  `datetime` from `shift-transactions` where `datetime` between '$start' and '$end' 
		and shift = 'start' order by `datetime` asc limit 0,1";
		$res = mysql_query($sql);
		list($startdt1)=mysql_fetch_row($res);
		
		$sql = "select  `datetime` from `shift-transactions` where `datetime` between '$start' and '$end' 
		and shift = 'end' and `datetime` > '$startdt1' order by `datetime` asc limit 0,1";
		$res = mysql_query($sql);
		list($enddt1)=mysql_fetch_row($res);

		$sql = "select  `datetime` from `shift-transactions` where `datetime` between '$start' and '$end' 
		and shift = 'start' order by `datetime` asc limit 1,1";
		$res = mysql_query($sql);
		list($startdt2)=mysql_fetch_row($res);
		
		$sql = "select  `datetime` from `shift-transactions` where `datetime` between '$start' and '$end' 
		and shift = 'end' and `datetime` > '$startdt2' order by `datetime` asc limit 0,1";
		$res = mysql_query($sql);
		list($enddt2)=mysql_fetch_row($res);

		$sql = "select  `datetime` from `shift-transactions` where `datetime` between '$start' and '$end' 
		and shift = 'start' order by `datetime` asc limit 2,1";
		$res = mysql_query($sql);
		list($startdt3)=mysql_fetch_row($res);
		
		$sql = "select  `datetime` from `shift-transactions` where `datetime` between '$start' and '$end' 
		and shift = 'end' and `datetime` > '$startdt3' order by `datetime` asc limit 0,1";
		$res = mysql_query($sql);
		list($enddt3)=mysql_fetch_row($res);


		$shift1 =  new monthly('',$startdt1,$enddt1);
		$shift2 =  new monthly('',$startdt2,$enddt2);
		$shift3 =  new monthly('',$startdt3,$enddt3);

		$mini1 = $shift1->getMiniRefSales();
		$mini2 = $shift2->getMiniRefSales();
		$mini3 = $shift3->getMiniRefSales();
		$total = $mini1+$mini2+$mini3;
		$ret .= "<tr>";
		$ret .= "<td>$i</td>";
		$ret .= "<td>&nbsp;</td>";
		$ret .= "<td style='text-align:right'>".number_format($mini1)."</td>";
		$ret .= "<td style='text-align:right'>".number_format($mini2)."</td>";
		$ret .= "<td style='text-align:right'>".number_format($mini3)."</td>";
		$ret .= "<td style='text-align:right'>".number_format($total)."</td>";
		$ret .= "</tr>";

		$_mini1 += $mini1;
		$_mini2 += $mini2;
		$_mini3 += $mini3;
		$_total += $total;
	}
	$ret .= "<tr>";
	$ret .= "<td>&nbsp;</td>";
	$ret .= "<td>&nbsp;</td>";
	$ret .= "<th style='text-align:right'>".number_format($_mini1)."</th>";
	$ret .= "<th style='text-align:right'>".number_format($_mini2)."</th>";
	$ret .= "<th style='text-align:right'>".number_format($_mini3)."</th>";
	$ret .= "<th style='text-align:right'>".number_format($_total)."</th>";
	$ret .= "</tr>";
	$ret .= "</table>";
		
	

	
	return $ret;
}

function getMonthDropdown($name="month", $selected=null)
	{
			$dd = '<select name="'.$name.'" id="'.$name.'">';

			$months = array(
					1 => 'January',
					2 => 'February',
					3 => 'March',
					4 => 'April',
					5 => 'May',
					6 => 'June',
					7 => 'July',
					8 => 'August',
					9 => 'September',
					10 => 'October',
					11 => 'November',
					12 => 'December');
			/*** the current month ***/
			$selected = is_null($selected) ? date('n', time()) : $selected;

			for ($i = 1; $i <= 12; $i++)
			{
					$dd .= '<option value="'.$i.'"';
					if ($i == $selected)
					{
							$dd .= ' selected';
					}
					/*** get the month ***/
					$dd .= '>'.$months[$i].'</option>';
			}
			$dd .= '</select>';
			return $dd;
	}

	function getYearDropdown($name="year", $selected=null)
	{
			$dd = '<select name="'.$name.'" id="'.$name.'">';

			$months = array(
					1 => '2009',
					2 => '2010',
					3 => '2011',
					4 => '2012',
					5 => '2013',
					6 => '2014',
					7 => '2015',
					8 => '2016');
		   
			$selected = is_null($selected) ? date('Y', time()) : $selected;

			for ($i = 1; $i <= 8; $i++)
			{
					$dd .= '<option value="'.$months[$i].'"';
					if ($months[$i] == $selected)
					{
							$dd .= ' selected';
					}
					
					$dd .= '>'.$months[$i].'</option>';
			}
			$dd .= '</select>';
			return $dd;
	}

	function getMonthName($i)
	{
		$months = array(
					1 => 'January',
					2 => 'February',
					3 => 'March',
					4 => 'April',
					5 => 'May',
					6 => 'June',
					7 => 'July',
					8 => 'August',
					9 => 'September',
					10 => 'October',
					11 => 'November',
					12 => 'December');
		return $months[$i];
	}

?>
<style>
		.printable {
			font-family: sans-serif;
			font-size: 14px;
			font-weight: 550;
			}
		.report{
			font-family: sans-serif;
			font-size: 14px ;
			text-align:left;
			font-weight: 550;
				
		}
		.report td{
			padding-bottom:12px;
		}
		.summary{
			font-family: sans-serif;
			font-size: 14px;
			text-align:left;
			font-weight: 550;
		}
</style>
<script src="../js/jquery.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/jquery.print.js"></script>
<script type="text/javascript">
 
		// When the document is ready, initialize the link so
		// that when it is clicked, the printable area of the
		// page will print.
		$(document).ready(function(){		
				$("a").attr( "href", "javascript:void( 0 )" ).click(
						function(){
							// Print the DIV.
							$(".printable2").print();
							$(".printable").print(); 							
							// Cancel click event.
							return( false );
						});
 
			
		});
 
</script>
<form name=myform method=post>
<div>
Month: <? echo getMonthDropdown("ddlmonth",$_POST["ddlmonth"]); ?>
<br>
<br>
Year: <? echo getYearDropdown("ddlyear",$_POST["ddlyear"]); ?>
</div>
<br>
<input type='submit' value='Search' name='btnSearch' />
<br>
<br>
<a href="#">Print Report</a>
<br>
<br>
<div class='printable'>
<? if($_POST){ echo getMiniRefReport($_POST["ddlmonth"],$_POST["ddlyear"]);} ?>
</div><br />
<a href="#">Print Report</a>
</form>