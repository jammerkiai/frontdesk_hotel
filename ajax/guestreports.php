<?php
//guestreports.php
include_once("config/config.inc.php");
include_once("guestfns.php");


$cmd = isset($_POST) ? $_POST["cmd"] : "Checked In";
$data = '';
$viewonly = 1;
if ($cmd === 'Checked In') {
    $data = getCheckedInGuests();
} elseif ($cmd === 'Search') {
    $data = getGuestsByKeywordSearch($_POST['key']);
} else {
	foreach ($_POST as $key => $val) {
		if (substr($key, 0, 3) === 'gh_') {
			$gid = str_replace('gh_','', $key);
			$data = getGuestHistory($gid);
			break;
		} elseif (substr($key, 0, 4) === 'del_') {
			$gid = str_replace('del_','', $key);
			deleteGuest($gid);
			$data = getGuestsByKeywordSearch($_POST['key']);
			break;
		} elseif (substr($key, 0, 4) === 'occ_') {
			$occ = str_replace('occ_','', $key);
			$data = 'OCCDETAILS';
			break;
		}
	}
}

?>
<style>
html, body, table {
    font-family: arial,helvetica,sans-serif;
    font-size: 10px;
    margin: 0;
}

.toolbar {
    margin: 0;
    padding: 1px;
    font-size: 10px;
    border: 1px inset #ccc;
}

.workspace {
    padding: 10px;
}

.summary {
    
}

.guesttable {
    border: 1px solid #ddd;
    padding: 1px;
    font-family: arial,helvetica,sans-serif;
    font-size: 10px;
    width: 600px;
}

.guesttable th {
    background-color: #ccc;
}

.guesttable tr {
    border-bottom: 1px dotted #ccc;
}

</style>
<form method="post" name="guestform" action="guestreports.php">
<div class="toolbar">
<input type="submit" name="cmd" value="Checked In">
<input type="submit" name="cmd" value="Search">
<input type="text" name="key" value="<?php echo $_POST['key'] ?>"> 
</div>
<hr>
<div class="workspace">
<?php 
if ($data === 'OCCDETAILS' && isset($occ)) {
	include_once('occupancydetails.php');
} else {
	echo $data;
}
?>
</div>
</form>
