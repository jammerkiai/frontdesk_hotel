<?php
//clpostviewer.php
//will place selected items into temp table for viewing
require_once("../config/config.inc.php");
require_once('class.baseobject.php');
require_once('class.shift.php');
require_once('class.report.php');
require_once('reportfns.php');

$shift = isset($_GET['shift']) ? $_GET['shift'] : 0;
$pdate = isset($_GET['pdate']) ? $_GET['pdate'] : 0;


if (isset($_POST)) {
	foreach ($_POST as $key => $val) {
		${$key} = $val;
	}

	if ($ptask === 'temp') {
		$isql = "insert into creditline_temp_posts 
			select * from creditline_transactions 
			where post_date='$pdate' and post_shift='$shift'";
		mysql_query($isql);
	} elseif ($submit === 'Commit to GL') {
		$summarysql = "SELECT null, post_date, post_shift, glcode, account_name, sum(dr) as 'DR', sum(cr) as 'CR'
			from creditline_temp_posts a, 0_chart_master b
			where glcode=account_code
			group by glcode";
		$sql1 = "insert into creditline_post_summary $summarysql";
		mysql_query($sql1) or die(mysql_error() . $sql1) ;
		$sql2 = "insert into creditline_posts select * from creditline_temp_posts"; 
		mysql_query($sql2);
		$sql3 = "truncate table creditline_temp_posts"; 
		mysql_query($sql3);
		$sql4 = "delete from creditline_transactions where post_date='$pdate' and post_shift='$shift'"; 
		mysql_query($sql4);
		die('Successfully committed transactions.<a href="clposting.php" >Back to Main Posting</a>');
	} elseif ($submit === 'Cancel') {
		$sql3 = "truncate table creditline_temp_posts"; 
		mysql_query($sql3);
		die('Backing out previous transactions. <a href="clposting.php" >Back to Main Posting</a>');
	}
}

$summarysql = "SELECT post_date, post_shift, glcode, account_name, sum(dr) as 'DR', sum(cr) as 'CR'
	from creditline_temp_posts a, 0_chart_master b
	where glcode=account_code
	group by glcode";

$arrReport = array(
    'title'    => 'Summary of Transactions for Posting to GL',
    'subtitle' => "Date: $pdate Shift: $shift" ,
    'groupingField' => 'glcode',
    'sql'	   => $summarysql	
);

$report = new report($arrReport);
$report->buildReport();

?>
<style>
body, html {
	font-size: 10px;
	font-family: Arial, Helvetica, sans-serif;
}
</style>
<form method="post" action="clpostviewer.php">
	<input type="hidden" name="shift" value="<?php echo $shift ?>" />
	<input type="hidden" name="pdate" value="<?php echo $pdate ?>" />
	<input type="hidden" name="ptask" value="commit" />
	<input type="submit" name="submit" value="Cancel" />
	<input type="submit" name="submit" value="Commit to GL" />
</form>
<?php
$report->show();
?>