<?php
/**
* @file: clposting.php
* views/posts cl transactions
*/

require_once("../config/config.inc.php");
require_once('class.baseobject.php');
require_once('class.shift.php');
require_once('class.report.php');
require_once('reportfns.php');

$date = (isset($_GET['newdate'])) ? $_GET['newdate'] : date('Y-m-d');
$shift = (isset($_GET['shiftno'])) ? $_GET['shiftno'] : 1;
$occupancy = (isset($_GET['occupancy'])) ? $_GET['occupancy'] : '';

$ondateChecked = '';
$dateOperator = '<=';

if (isset($_GET['ondate'])) {
    $ondateChecked = 'checked';
    $dateOperator = '=';
}

$datefield = 'post_date';
$sql = "";

//for viewing all gl entries to post

$sql = "select occupancy_id as 'Occupancy',
            transaction_date as 'TrxnDate',
            post_date as 'Posting Date',
            post_shift as 'Post Shift',
            b.account_code as 'GL Code',
            b.account_name as 'GL Description',
            dr as 'DR',
            cr as 'CR',
            reftable as 'Reference',
            refnum as 'RefId',
            remarks as 'Remarks'
            from creditline_transactions a,
            0_chart_master b
            where $datefield $dateOperator '$date' 
            and a.glcode = b.account_code ";

//for viewing all checkins
$sql2 = "select occupancy_id as 'Occupancy', sum(dr) as 'DR'
    from creditline_transactions
    where post_date = '$date'
    and glcode = '3370'
";

//for viewing all rechits
$sql3 = "select occupancy_id as 'Occupancy', 
    glcode,
    account_name,
    sum(dr) as 'DR'
    from creditline_transactions, 0_chart_master
    where post_date = '$date'
    and glcode = account_code
    and glcode = '1020'
    ";
    


if ($occupancy) $sql .= "and occupancy_id='$occupancy' ";
        
if ($shift) {
    $sql .= " and post_shift='$shift' ";
    $sql2 .= " and post_shift='$shift' ";
    $sql3 .= " and post_shift='$shift' ";
}

$sql .= " order by occupancy_id, $datefield, post_shift ";
$sql2 .= 'group by occupancy_id, glcode';
$sql3 .= 'group by occupancy_id, glcode';

$arrReport = array(
    'title'    => 'Transactions for Posting',
    'subtitle' => '',
    'groupingField' => 'Occupancy',
    'sql'	   => $sql	
);

$report = new report($arrReport);
$report->buildReport();


?>
<html>
<head>
<link rel="stylesheet" type="text/css" href="./reports.css">
<link rel="stylesheet" type="text/css" href="../../css/start/jquery-ui.css">
<script type="text/javascript" src="../../js/jquery.js"></script>
<script type="text/javascript" src="../../js/jquery-ui.js"></script>

<script>
$(function(){

    $('#post2gl').click(function(e){
        e.preventDefault();
        $.post(
            'clpostviewer.php',
            {
                shift : $('#shiftno').val(),
                pdate : $('#newdate').val(),
                ptask : 'temp'
            },
            function() {
                document.location.href = 'clpostviewer.php?shift=' + $('#shiftno').val() + '&pdate=' + $('#newdate').val();
            }
        );
    });
})
</script>
</head>
<body>
<form>
<div>
Selected Date Only: <input type='checkbox' name='ondate' value='1' <?php echo $ondateChecked ?>>
Posting Date: <input type="text" name="newdate" size="10" id="newdate" value="<?php echo $date ?>" />
Post Shift: <?php echo makeShiftSelect($shift) ?>
Select Occupancy: <?php echo occupancyDropdown($occupancy) ?>
<input type="submit" name="submit" value="go" />
<input type="submit" name="submit" value="Post to GL" id="post2gl"/>
</div>
<?php 

$report->show(); 

$arrReport = array(
    'title'    => 'Checkins For ' . $date . (($shift) ? ' Shift ' . $shift : ''),
    'subtitle' => '',
    'aggregates' => array('DR'),
    'sql'      => $sql2  
);

$report2 = new report($arrReport);
$report2->buildReport();
$report2->show();

$arrReport = array(
    'title'    => 'Rechits For Selected Period',
    'subtitle' => '',
    'aggregates' => array('DR'),
    'sql'      => $sql3  
);


$report3 = new report($arrReport);
$report3->buildReport();
$report3->show();



?> 
</form>
<script>
$(document).ready(function(){
	$("#newdate").datepicker({dateFormat:'yy-mm-dd'});
});
</script>
</body>
</html>

<?php
function occupancyDropdown($selected = '') {
    $sql = "select distinct occupancy_id from creditline_transactions order by occupancy_id";
    $res = mysql_query($sql) or die(mysql_error());
    $ret = '<select name="occupancy">';
    while(list($occId) = mysql_fetch_row($res)) {
        $ret.= "<option value='$occId' ";
        $ret.= ($occId===$selected) ? ' selected ' : '';
        $ret.= ">$occId</option>";
    }
    $ret .= '</select>';
    return $ret;
}

?>
