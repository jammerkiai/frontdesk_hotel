<?php
require_once('../config/config.inc.php');
require_once('class.baseobject.php');
require_once('class.shift.php');
require_once('class.report.php');
require_once('reportfns.php');

/**
* variable setup stage
*/
$date = (isset($_GET["newdate"])) ? $_GET["newdate"] : date('Y-m-d');
list($year,$month,$day) = explode('-', $date); 
$shiftno = (isset($_GET["shiftno"])) ? $_GET["shiftno"] : 0;
$siteid = (isset($_GET["siteid"])) ? $_GET["siteid"] : 0;

/**
* build temporary data table
*/

$sql= "
	create temporary table guesttransactiondetails
		select  room_types.room_type_name, rooms.door_name, fnb_sales.sales_date,
		occupancy.actual_checkin, occupancy.actual_checkout,'FNB' as category_type,  food_categories.food_category_name as 'category', fnb.fnb_name as 'item', 
		fnb_sales.unit_cost, fnb_sales.qty, (fnb_sales.unit_cost * fnb_sales.qty) as 'total_cost',
		fnb_sales.status, fnb_sales.remarks,  timediff(fnb_sales.sales_date, 
		occupancy.actual_checkin) as 'LOS as of Trxn', occupancy.occupancy_id, occupancy.shift_checkin
		from fnb_sales, food_categories, fnb, occupancy, rooms, room_types
		where fnb_sales.category_id=food_categories.food_category_id
		and fnb_sales.item_id=fnb.fnb_id
		and fnb_sales.occupancy_id=occupancy.occupancy_id
		and occupancy.room_id=rooms.room_id
		and rooms.room_type_id=room_types.room_type_id
		and year(fnb_sales.sales_date) = '$year' 
		and month(fnb_sales.sales_date) = '$month'
		and day(fnb_sales.sales_date) = '$day'
	union
		select room_types.room_type_name, rooms.door_name, room_sales.sales_date,
		occupancy.actual_checkin, occupancy.actual_checkout,'RoomSales' as category_type, sas_category.sas_cat_name as 'category', sales_and_services.sas_description as 'item', 
		room_sales.unit_cost, room_sales.qty, (room_sales.unit_cost * room_sales.qty) as 'total_cost',
		room_sales.status, room_sales.remarks,  
		timediff(room_sales.sales_date, occupancy.actual_checkin) as 'LOS as of Trxn',
		occupancy.occupancy_id, occupancy.shift_checkin
		from room_sales, sas_category, sales_and_services, occupancy, rooms, room_types
		where room_sales.category_id=sas_category.sas_cat_id
		and room_sales.item_id=sales_and_services.sas_id
		and room_sales.occupancy_id=occupancy.occupancy_id
		and occupancy.room_id=rooms.room_id
		and rooms.room_type_id=room_types.room_type_id
		and year(room_sales.sales_date) = '$year' 
		and month(room_sales.sales_date) = '$month'
		and day(room_sales.sales_date) = '$day'
		";
mysql_query($sql) or die($sql);		

/**
* report setup stage
*/
$title = getHotel() . ' Guest Transaction Details';
$subtitle = $date;


$sql ="select * from guesttransactiondetails";	
$arrReport = array(
		'title'    => $title,
		'aggregates'=> array('total_amount'),
		'subtitle' => $subtitle,
		'sql'	   => $sql	
		);
$report = new report($arrReport);
$report->buildReport();


?>
<html>
<head>
<link rel="stylesheet" type="text/css" href="./reports.css">
<link rel="stylesheet" type="text/css" href="../../css/start/jquery-ui.css">
<script type="text/javascript" src="../../js/jquery.js"></script>
<script type="text/javascript" src="../../js/jquery-ui.js"></script>
</head>
<body>
<form>
<div class="menubar">
Select Date: <input type="text" name="newdate" id="newdate" value="<?php echo $date ?>" /> 
Select Shift: <?php echo makeShiftSelect($shiftno); ?> 
Select Classification: <?php echo makeSiteSelect($siteid); ?>
<input type="submit" name="submit" value="go" />
</div>
<div>
<?php
	$sql = "select occupancy_id, door_name, actual_checkin, actual_checkout,
			sales_date, category_type, category, item, total_cost
			from guesttransactiondetails
			order by occupancy_id, sales_date
			";
	$res = mysql_query($sql);
	$old_occ='';
	$table = '';
	$totalcost = 0;
	while(list($occ,$door, $in, $out, $sales, $category_type, $category, $item, $cost)=mysql_fetch_row($res)) {
		if($old_occ != $occ) {
			$table.="<h4>Room: $door</h4>
				<div class='date_item'>
				<h5><span class='dateleft'>Checkin: </span>$in</h5> 
				<h5><span class='dateleft'>Checkout: </span>$out</h5></div>";
		}
		$table.="<div class='line_item'><span class='dateleft'>$sales</span> $category <span class='right'>$cost</span></div>";
		if($old_occ != $occ && $totalcost > 0) {
			$table.="<div class='line_item'><span class='dateleft'>&nbsp;</span> Total: <span class='right'>$totalcost</span></div>";
			$totalcost=0;
			$old_occ = $occ;
		}
		$totalcost += $cost;
	}
	$table.="<div class='line_item'><span class='dateleft'>$sales</span> $category <span class='right'>$totalcost</span></div>";
	echo $table;
?>
<hr />
<?php $report->show();?> 
</div>
</form>
<script>
$(document).ready(function(){
	$("#newdate").datepicker({dateFormat:'yy-mm-dd'});
});
</script>
</body>
</html>