<?php
require_once('../config/config.inc.php');
require_once('class.baseobject.php');
require_once('class.shift.php');
require_once('class.report.php');


 


$shift = new shift(array('date'=>'2010-02-14','shiftno'=>3));
$shift->getShiftDuration();
$start = $shift->get('shiftStart');
$end = $shift->get('shiftEnd');
$sql= "
	select rooms.door_name, room_sales.sales_date,  sas_category.sas_cat_name, sales_and_services.sas_description, 
	room_sales.unit_cost, room_sales.qty, (room_sales.unit_cost * room_sales.qty) as 'total_cost',
	room_sales.status, room_sales.remarks,  datediff(room_sales.sales_date, occupancy.actual_checkin) as 'LOS as of Trxn'
	from room_sales, sas_category, sales_and_services, occupancy, rooms
	where room_sales.category_id=sas_category.sas_cat_id
	and room_sales.item_id=sales_and_services.sas_id
	and room_sales.occupancy_id=occupancy.occupancy_id
	and occupancy.room_id=rooms.room_id
	and room_sales.sales_date >= '$start' 
	and room_sales.sales_date <= '$end'
	order by rooms.door_name
	";
$arrReport = array(
		'title'    => 'Room Sales',
		'subtitle' => 'For Date: ' . date('l, F d, Y', strtotime($shift->get('date'))) . ' Shift No. ' . $shift->get('shiftno'),
		'sql'	   => $sql	
		);

$report = new report($arrReport);
$report->buildReport();
$report->show();


?>