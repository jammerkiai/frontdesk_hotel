<?php
/**
* @FILE:   cardsales.php
*/

if (isset($_POST['cmd']) && $_POST['cmd'] === 'view daily') header('location: cardsales.php');

require_once('../config/config.inc.php');
require_once('class.baseobject.php');
require_once('class.shift.php');
require_once('class.report.php');
require_once('reportfns.php');

$month = isset($_POST['month']) ? $_POST['month'] : date('m');
$year = isset($_POST['year']) ? $_POST['year'] : date('Y');


function getmonth($name, $selected) {
    $months = array(
        '00' => '',
        '01' => 'January',  
        '02' => 'Febuary',
        '03' => 'March',
        '04' => 'April',
        '05' => 'May',
        '06' => 'June',
        '07' => 'July',
        '08' => 'August',
        '09' => 'September',
        '10' => 'October',
        '11' => 'November',
        '12' => 'December'
    );
    $ret = "<select name='$name'>";
    foreach ($months as $key => $month) {
        if ($key !== '00') {
            $ret .= "<option value='$key'";
            if ($key === $selected) $ret .= " selected ";
            $ret .= ">$month</option>";
        }
    }
    $ret .= "</select>";
    return $ret;
}

function postdata($date, $site, $data) {

}

function getdata($month, $year) {
    $sql = "
            create temporary table temp_mcs as
            (
            select cardsale_date, 
            if (site_id=1, sum(amount), sum(0)) as 's1_amount',
            if (site_id=1, sum(commission), sum(0)) as 's1_commission',
            if (site_id=1, sum(tax), sum(0)) as 's1_tax',
            if (site_id=1, sum(net), sum(0)) as 's1_net',
            if (site_id=2, sum(amount), sum(0)) as 's2_amount',
            if (site_id=2, sum(commission), sum(0)) as 's2_commission',
            if (site_id=2, sum(tax), sum(0)) as 's2_tax',
            if (site_id=2, sum(net), sum(0)) as 's2_net'
            from monthlycardsales
            where year(cardsale_date)='$year' and month(cardsale_date)='$month'
            group by cardsale_date, site_id);";
     mysql_query($sql);
     $sql = "        
            select cardsale_date, 
            sum(s1_amount) as 's1_amount', 
            sum(s1_commission) as 's1_commission', 
            sum(s1_tax) as 's1_tax', 
            sum(s1_net) as 's1_net', 
            sum(s2_amount) as 's2_amount', 
            sum(s2_commission) as 's2_commission',
            sum(s2_tax) as 's2_tax', 
            sum(s2_net) as 's2_net'
            from temp_mcs group by cardsale_date;
    ";
    
    $res = mysql_query($sql);
    $ret = '';
    $totals = array(
        'receipt_date' => '',
        's1_amount' => 0,
        's1_commission' => 0,
        's1_tax' => 0,
        's1_net' => 0,
        's2_amount' => 0,
        's2_commission' => 0,
        's2_tax' => 0,
        's2_net' => 0,
    );
    while ($obj = mysql_fetch_object($res)) {
        $ret .= '<tr>';
        $ret .= '<td>' . $obj->cardsale_date . '</td>';
        $ret .= '<td>' . $obj->s1_amount . '</td>';
        $ret .= '<td>' . $obj->s1_commission . '</td>';
        $ret .= '<td>' . $obj->s1_tax . '</td>';
        $ret .= '<td>' . $obj->s1_net . '</td>';
        $ret .= '<td>' . $obj->s2_amount . '</td>';
        $ret .= '<td>' . $obj->s2_commission . '</td>';
        $ret .= '<td>' . $obj->s2_tax . '</td>';
        $ret .= '<td>' . $obj->s2_net . '</td>';
        //$ret .= '<td>' . round($obj->net, 2) . '</td>';
        $ret .= '</tr>';
        $totals['s1_amount'] += $obj->s1_amount;
        $totals['s1_commission'] += $obj->s1_commission;
        $totals['s1_tax'] += $obj->s1_tax;
        $totals['s1_net'] += $obj->s1_net;
        $totals['s2_amount'] += $obj->s2_amount;
        $totals['s2_commission'] += $obj->s2_commission;
        $totals['s2_tax'] += $obj->s2_tax;
        $totals['s2_net'] += $obj->s2_net;
    }
    $ret .= '<tr>';
    foreach ($totals as $total) {
        $ret .= '<th class="totals">' . (($total !== '') ? number_format($total, 2) : '') . '</th>';        
    }
    $ret .= '</tr>';

    return $ret;
}

function getHeaders() {
    $hdrs = array('Amount', 'Commission', 'Tax', 'Net Sales');
    
    $ret = "<tr><th rowspan='2'>Date</th>
                <th colspan='4'>Shogun 1</th>
                <th colspan='4'>Shogun 2</th>
            </tr>";
    
    $ret .= '<tr>';
    foreach ($hdrs as $hdr) {
        $ret .= "<th>$hdr</th>";
    }
    foreach ($hdrs as $hdr) {
        $ret .= "<th>$hdr</th>";
    }
    $ret .= '</tr>';
    return $ret;
}

?>
<html>
<head>
<title>Monthly Card Sales Summary</title>
<link rel="stylesheet" type="text/css" href="./reports.css">
<link rel="stylesheet" type="text/css" href="../../css/start/jquery-ui.css">
<script type="text/javascript" src="../../js/jquery.js"></script>
<script type="text/javascript" src="../../js/jquery-ui.js"></script>
<style>
table {
	border-collapse:collapse;
}

table th,td {
	padding:4px;
	text-align:center;
}

th.grand, td.grand {
	background-color:#eeffcc;
}

tr.weekend {
	background-color:#ffeecc;
}
#newdate {
    width: 120px;
}

.menubar {
    background-color: grey;
    color: white;
    font-weight: bold;
}

.totals {
    color: green;    
}

select, input[type=text] {
    font-weight: bold;
    font-size: 12px;
}

.year {width: 50px;}
</style>
</head>
<body>
<form method='post'>
<h5>Monthly Card Sales Summary</h5>
<div class="menubar">
<?php echo getmonth('month', $month) ?>
<input type='text' name='year' value='<?php echo $year ?>' class="year" maxlength=4/>
<input type='submit' name='cmd' value='go' />
<input type='submit' name='cmd' value='view daily'/>
</div>
<div id="workpanel">
<table border='1'>
<thead>
<?php echo getHeaders() ?>
</thead>
<tbody>
<?php echo getdata($month, $year) ?>
</tbody>
</table>
</div>
</form>
<script>
$(document).ready(function(){
	$("#newdate").datepicker({dateFormat:'yy-mm-dd'});
});
</script>
</body>
</html>
