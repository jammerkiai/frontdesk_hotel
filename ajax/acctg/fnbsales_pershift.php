<?php
require_once('../config/config.inc.php');
require_once('class.baseobject.php');
require_once('class.shift.php');
require_once('class.report.php');
require_once('reportfns.php');

$title = getHotel() . ' FNB Sales';

function getLastShift() {
	$time = date("h");
	if($time <= 6) {
		return 3;
	} elseif( $time >= 6 && $time < 14) {
		return 1;
	} elseif( $time >= 14 && $time < 22) {
		return 2;
	}
	return 1;
}


$shiftno = (isset($_GET["shiftno"])) ? $_GET["shiftno"] : getLastShift();
$date = (isset($_GET["newdate"])) ? $_GET["newdate"] : date('Y-m-d');


$shift = new shift(array('date'=>$date,'shiftno'=>$shiftno));
$shift->getShiftDuration();
$start = $shift->get('shiftStart');
$end = $shift->get('shiftEnd');
$sql= "
	select rooms.door_name, fnb_sales.sales_date, occupancy.actual_checkout, food_categories.food_category_name, fnb.fnb_name,
	fnb_sales.unit_cost, fnb_sales.qty, (fnb_sales.unit_cost * fnb_sales.qty) as 'total_cost',
	fnb_sales.status, fnb_sales.remarks,  timediff(fnb_sales.sales_date, occupancy.actual_checkin) as 'LOS as of Trxn'
	from fnb_sales, food_categories, fnb, occupancy, rooms
	where fnb_sales.category_id=food_categories.food_category_id
	and fnb_sales.item_id=fnb.fnb_id
	and fnb_sales.occupancy_id=occupancy.occupancy_id
	and occupancy.room_id=rooms.room_id
	and fnb_sales.sales_date >= '$start'
	and fnb_sales.sales_date <= '$end'
	order by rooms.door_name
	";

$arrReport = array(
		'title'    => $title,
		'aggregates'=> array('total_cost'),
		'subtitle' => 'For Date: ' . date('l, F d, Y', strtotime($shift->get('date'))) . ' Shift No. ' . $shift->get('shiftno'),
		'sql'	   => $sql
		);

$report = new report($arrReport);
$report->buildReport();
?>
<html>
<head>
<link rel="stylesheet" type="text/css" href="./reports.css">
<link rel="stylesheet" type="text/css" href="../../css/start/jquery-ui.css">
<script type="text/javascript" src="../../js/jquery.js"></script>
<script type="text/javascript" src="../../js/jquery-ui.js"></script>
</head>
<body>
<form>
<div>
Select Date: <input type="text" name="newdate" id="newdate" value="<?php echo $date ?>" />
Select Shift: <?php echo makeShiftSelect($shiftno); ?>
<input type="submit" name="submit" value="go" />
</div>
<?php $report->show(); ?>
</form>
<script>
$(document).ready(function(){
	$("#newdate").datepicker({dateFormat:'yy-mm-dd'});
});
</script>
</body>
</html>

