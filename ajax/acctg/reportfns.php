<?php

function getHotel() 
{
	return "Shogun 2";
}


function makeShiftSelect($selected=0){
	$retval="<select name='shiftno' id='shiftno'>";
	$retval.="<option value='0'>All</option>";
	for($x=1; $x <= 3; $x++) {
		 $retval.="<option value='$x' ";
		 $retval.=($x==$selected) ? " selected " : "";
		 $retval.=">$x</option>";
	}
	$retval.="</select>";
	return $retval;
}

function makeSiteSelect($selected=0){
	$arrSite = array('All','Walkup','Hotel');
	$retval="<select name='siteid'>";
	for($x=0; $x < count($arrSite); $x++) {
		 $retval.="<option value='$x' ";
		 $retval.=($x==$selected) ? " selected " : "";
		 $retval.=">$arrSite[$x]</option>";
	}
	$retval.="</select>";
	return $retval;
}

function getSpecialFloorId()
{
	$sql  ="select settings_value from settings where settings_name='SPECIALFLOORID'";
	$res = mysql_query($sql);
	$row = mysql_fetch_row($res);
	return $row[0];
}

function getSpecialRoomlist($floor)
{
	$sql  ="select room_id from rooms where floor_id='$floor'";
	$res = mysql_query($sql);
	$row = mysql_fetch_row($res);
	$arr = array();
	while($row = mysql_fetch_row($res)) {
		$arr[].=$row[0];
	}
	return implode(',',$arr);
}

function isSpecialFloor($occupancy)
{
	$exceptList  = explode(',',getSpecialRoomList(getSpecialFloorId()));
	$sql  = "select room_id from occupancy where occupancy_id='$occupancy'";
	$res = mysql_query($sql);
	$row = mysql_fetch_row($res);
	return in_array($exceptList,$row[0]) ;
}

function RoomSalesDropDown()
{
	$sql = " select a.sas_id, a.sas_cat_id, b.sas_cat_name, a.sas_description
			from sales_and_services a, sas_category b 
			where  a.sas_cat_id=b.sas_cat_id
			order by b.sas_cat_name, a.sas_description
			";
	$res = mysql_query($sql);
	$ret="<select name='roomsales' id='roomsales'>";
	$ret.="<option value=''></option>";
	while(list($sas,$cat,$name,$description)=mysql_fetch_row($res)) {
		$value = $sas.'_'.$cat;
		$ret.="<option value='$value'>$name - $description</option>";
	}
	$ret.="</select>";
	return $ret;
}

function FoodSalesDropDown()
{
	$sql = " select a.fnb_id, a.food_category_id, b.food_category_name, a.fnb_name
			from fnb a, food_categories b 
			where  a.food_category_id=b.food_category_id
			order by b.food_category_name, a.fnb_name
			";
	
	$res = mysql_query($sql);
	$ret="<select name='foodsales' id='foodsales'>";
	$ret.="<option value=''></option>";
	while(list($sas,$cat,$name,$description)=mysql_fetch_row($res)) {
		$strvalue = "$name - $description";
		$idvalue = $sas.'_'.$cat;
		$strvalue = (strlen($strvalue) > 50) ? substr($strvalue,0,50) : "$strvalue ...";
		$ret.="<option value='$idvalue'>$strvalue</option>";
	}
	$ret.="</select>";
	return $ret;
}