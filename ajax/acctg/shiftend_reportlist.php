<html>
<head>
<script type="text/javascript" src="../../js/jquery.js"></script>
<style>
body, html {
	width: 100%;
	height: 100%;
	margin: 0;
	font-family: arial, helvetica, sans-serif;
	font-size: 14px;
}
div {
	width: 20%;
	height: 100%;
	display: inline-block;
	overflow: auto;
}

div.reports {
	width: 39%;
}

div.listitem {
	text-align: center;
	width: 80%;
	height: 14px;
	display: inline-block;
	cursor: pointer;
	margin: 1px;
	background-color: #eee;
	padding: 2px 0 4px 5px;
}

div.listitem:hover{
	background-color: #fc9;
}

ul {list-style: none;}

iframe {
	width: 98%;
	height: 100%;
}
</style>


<?php
$reportdir = '../reports/';
$d = dir($reportdir);

echo "";
$data = array();
while (false !== ($entry = $d->read())) {
    if (!in_array($entry, array('.', '..')) &&  !is_dir($entry) ) {
    		$thisdateindex = '';
    	   if (stripos($entry, 'newcdailyreport') !== false) {
    	  		$link = $reportdir . $entry;
            $thisdate = str_replace('newcdailyreport', '', $entry);
            $thisdateindex = substr($thisdate, 0, 10);
            $data[$thisdateindex][1] = $link;
    	   } elseif(stripos($entry, 'cdailyreport') !== false) {     
            $link = $reportdir . $entry;
            $thisdate = str_replace('cdailyreport', '', $entry);
            $thisdateindex = substr($thisdate, 0, 10);
            $data[$thisdateindex][0] = $link;
         }
    }
}
$d->close();

krsort($data);

?>
<script type="text/javascript" >
	$(document).ready(function(){
		$('.listitem').click(function() {
			var reader = 'reader.php?f=';
				
			$('#old').attr('src', reader + $(this).attr('data-old'));			
			$('#new').attr('src', reader + $(this).attr('data-new'));
		});
	});
</script>
</head>
<body>
<div class="list">
<h3>Shift End Reports</h3>
<ul>
<?php 

foreach ($data as $key => $pair) {
	$yr = substr($key, 0, 4);
	$mo = substr($key, 4, 2);
	$dy = substr($key, 6, 2);
	$hr = substr($key, 8, 2);
	if (isset($pair[1])) {
		$old = $pair[0];
		$new = $pair[1];
		echo "<li><div class='listitem' data-old='$old' data-new='$new'>$yr-$mo-$dy {$hr}00H</div></li>";
	}	
}

?>
</ul>
</div>
<div  class="reports">
<h3>Old Report</h3>
<iframe id="old" src="http://www.google.com"></iframe>
</div>
<div class="reports">
<h3>New Version</h3>
<iframe id="new" src="http://www.yahoo.com"></iframe>
</div>
</body>
</html>