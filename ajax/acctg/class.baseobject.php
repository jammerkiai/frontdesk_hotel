<?php

class baseobject 
{
	public function init($params=array()) 
	{
		if(isset($params)) { 
			$this->initParams($params);
		}
	}
	
	public function initParams($params)
	{
		if (is_array($params)) {
			foreach($params as $key => $val) {
				$this->{$key} = $val;
			}
		}
	}
	
	public function get($var)
	{
		return $this->{$var};
	}

	public function set($name,$value)
	{
		$this->$name=$value;
	}
}
