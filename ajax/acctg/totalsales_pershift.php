<?php
require_once('../config/config.inc.php');
require_once('class.baseobject.php');
require_once('class.shift.php');
require_once('class.report.php');
require_once('reportfns.php');

function getLastShift() {
	$time = date("h");
	if($time <= 6) {
		return 3;
	} elseif( $time >= 6 && $time < 14) {
		return 1;
	} elseif( $time >= 14 && $time < 22) {
		return 2;
	}
	return 1;
}

$title = getHotel() . ' Total Sales';
$shiftno = (isset($_GET["shiftno"])) ? $_GET["shiftno"] : getLastShift();
$date = (isset($_GET["newdate"])) ? $_GET["newdate"] : date('Y-m-d');


$shift = new shift(array('date'=>$date,'shiftno'=>$shiftno));
$shift->getShiftDuration();
$start = $shift->get('shiftStart');
$end = $shift->get('shiftEnd');
$sql= "drop table if exists shiftsales";
mysql_query($sql);
$sql= "
	create table shiftsales
		select  room_types.room_type_name, rooms.door_name, fnb_sales.sales_date,occupancy.actual_checkout,'FNB' as category_type,  food_categories.food_category_name as 'category', fnb.fnb_name as 'item',
		fnb_sales.unit_cost, fnb_sales.qty, (fnb_sales.unit_cost * fnb_sales.qty) as 'total_cost',
		fnb_sales.status, fnb_sales.remarks,  timediff(fnb_sales.sales_date,
		occupancy.actual_checkin) as 'LOS as of Trxn', occupancy.occupancy_id
		from fnb_sales, food_categories, fnb, occupancy, rooms, room_types
		where fnb_sales.category_id=food_categories.food_category_id
		and fnb_sales.item_id=fnb.fnb_id
		and fnb_sales.occupancy_id=occupancy.occupancy_id
		and occupancy.room_id=rooms.room_id
		and rooms.room_type_id=room_types.room_type_id
		and fnb_sales.sales_date >= '$start'
		and fnb_sales.sales_date <= '$end'
	union
		select room_types.room_type_name, rooms.door_name, room_sales.sales_date, occupancy.actual_checkout,'RoomSales' as category_type, sas_category.sas_cat_name as 'category', sales_and_services.sas_description as 'item',
		room_sales.unit_cost, room_sales.qty, (room_sales.unit_cost * room_sales.qty) as 'total_cost',
		room_sales.status, room_sales.remarks,
		timediff(room_sales.sales_date, occupancy.actual_checkin) as 'LOS as of Trxn',
		occupancy.occupancy_id
		from room_sales, sas_category, sales_and_services, occupancy, rooms, room_types
		where room_sales.category_id=sas_category.sas_cat_id
		and room_sales.item_id=sales_and_services.sas_id
		and room_sales.occupancy_id=occupancy.occupancy_id
		and occupancy.room_id=rooms.room_id
		and rooms.room_type_id=room_types.room_type_id
		and room_sales.sales_date >= '$start'
		and room_sales.sales_date <= '$end'
		";
mysql_query($sql);

$sql = "select tendertype, sum(amount) as 'total_amount' from salesreceipts where receipt_date between '$shift->shiftStart' and '$shift->shiftEnd' group by tendertype ";

$arrReport = array(
		'title'    => 'Received per TenderType',
		'aggregates'=> array('total_amount'),
		'subtitle' => 'For Date: ' . date('l, F d, Y', strtotime($shift->get('date'))) . ' Shift No. ' . $shift->get('shiftno'),
		'sql'	   => $sql
		);

$report5 = new report($arrReport);
$report5->buildReport();

$sql = "select room_type_name,  door_name, category_type, category, item, sum(total_cost) as 'total_cost' from shiftsales
		group by occupancy_id, item order by category_type, category, item ";

$arrReport = array(
		'title'    => 'Summary of Sales per Guest Occupancy',
		'aggregates'=> array('total_cost'),
		'subtitle' => 'For Date: ' . date('l, F d, Y', strtotime($shift->get('date'))) . ' Shift No. ' . $shift->get('shiftno'),
		'sql'	   => $sql
		);

$report3 = new report($arrReport);
$report3->buildReport();

$sql = "select room_type_name,  door_name, category_type, category, item, sum(total_cost) as 'total_cost' from shiftsales
		where item like '%Discount%'
		group by occupancy_id order by category_type, category, item ";

$arrReport = array(
		'title'    => 'Summary of Discounts',
		'aggregates'=> array('total_cost'),
		'subtitle' => 'For Date: ' . date('l, F d, Y', strtotime($shift->get('date'))) . ' Shift No. ' . $shift->get('shiftno'),
		'sql'	   => $sql
		);

$report4 = new report($arrReport);
$report4->buildReport();

$sql = "select category_type, category, item, sum(total_cost) as 'total_cost' from shiftsales group by item order by category_type, category, item ";

$arrReport = array(
		'title'    => 'Summary of Sales by Item type',
		'aggregates'=> array('total_cost'),
		'subtitle' => 'For Date: ' . date('l, F d, Y', strtotime($shift->get('date'))) . ' Shift No. ' . $shift->get('shiftno'),
		'sql'	   => $sql
		);

$report = new report($arrReport);
$report->buildReport();

$sql = "select category_type, category,  sum(total_cost) as 'total_cost' from shiftsales group by category order by category_type, category, item ";

$arrReport = array(
		'title'    => 'Summary of Sales by Category',
		'aggregates'=> array('total_cost'),
		'subtitle' => 'For Date: ' . date('l, F d, Y', strtotime($shift->get('date'))) . ' Shift No. ' . $shift->get('shiftno'),
		'sql'	   => $sql
		);

$report2 = new report($arrReport);
$report2->buildReport();



$sql = "select * from shiftsales group by door_name, category, item, sales_date	";

$arrReport = array(
		'title'    => $title,
		'aggregates'=> array('total_cost'),
		'subtitle' => 'For Date: ' . date('l, F d, Y', strtotime($shift->get('date'))) . ' Shift No. ' . $shift->get('shiftno'),
		'sql'	   => $sql
		);

$report1 = new report($arrReport);
$report1->buildReport();


$sql = "";

?>
<html>
<head>
<link rel="stylesheet" type="text/css" href="./reports.css">
<link rel="stylesheet" type="text/css" href="../../css/start/jquery-ui.css">
<script type="text/javascript" src="../../js/jquery.js"></script>
<script type="text/javascript" src="../../js/jquery-ui.js"></script>
</head>
<body>
<form>
<div>
Select Date: <input type="text" name="newdate" id="newdate" value="<?php echo $date ?>" />
Select Shift: <?php echo makeShiftSelect($shiftno); ?>
<input type="submit" name="submit" value="go" />
</div>
<?php $report5->show();?>
<?php $report4->show();?>
<?php $report3->show();?>
<?php $report2->show();?>
<?php $report->show();?>
<?php $report1->show();?>
</form>
<script>
$(document).ready(function(){
	$("#newdate").datepicker({dateFormat:'yy-mm-dd'});
});
</script>
</body>
</html>
