<?php
require_once('../config/config.inc.php');
require_once('class.baseobject.php');
require_once('class.shift.php');
require_once('class.report.php');
require_once('reportfns.php');

$title = getHotel() . ' Rechit Sales';
$shiftno = ($_GET["shiftno"]) ?$_GET["shiftno"] : 1;
$date    = ($_GET["newdate"]) ?$_GET["newdate"] : date('Y-m-d');

$shift = new shift(array('date'=>$date,'shiftno'=>$shiftno));
$shift->getShiftDuration();
$start = $shift->get('shiftStart');
$end = $shift->get('shiftEnd');


$sql= "
	create temporary table current_checked_in_guests
		select  room_types.room_type_name, rooms.door_name, room_sales.sales_date,occupancy.actual_checkin, occupancy.actual_checkout,  sas_category.sas_cat_name, sales_and_services.sas_description, 
		(room_sales.unit_cost * room_sales.qty) as 'total_cost',
		room_sales.status, room_sales.remarks,  timediff(room_sales.sales_date, occupancy.actual_checkin) as 'LOS as of Trxn'
		from room_sales, sas_category, sales_and_services, occupancy, rooms, room_types
		where room_sales.category_id=sas_category.sas_cat_id
		and room_sales.item_id=sales_and_services.sas_id
		and room_sales.occupancy_id=occupancy.occupancy_id
		and occupancy.room_id=rooms.room_id
		and rooms.room_type_id=room_types.room_type_id
		and occupancy.actual_checkin <= '$start' 
		and occupancy.actual_checkout >= '$end'
	union
		select  room_types.room_type_name, rooms.door_name, room_sales.sales_date,occupancy.actual_checkin, occupancy.actual_checkout,  sas_category.sas_cat_name, sales_and_services.sas_description, 
		(room_sales.unit_cost * room_sales.qty) as 'total_cost',
		room_sales.status, room_sales.remarks,  timediff(room_sales.sales_date, occupancy.actual_checkin) as 'LOS as of Trxn'
		from room_sales, sas_category, sales_and_services, occupancy, rooms, room_types
		where room_sales.category_id=sas_category.sas_cat_id
		and room_sales.item_id=sales_and_services.sas_id
		and room_sales.occupancy_id=occupancy.occupancy_id
		and occupancy.room_id=rooms.room_id
		and rooms.room_type_id=room_types.room_type_id
		and occupancy.actual_checkin <= '$start' 
		and occupancy.actual_checkout = '0000-00-00 00:00:00'
	";
mysql_query($sql);


$sql="select * from current_checked_in_guests
	where sales_date >='$start' and sales_date <='$end'";
$arrReport = array(
		'title'    => $title,
		'aggregates'=> array('total_cost'),
		'subtitle' => 'For Date: ' . date('l, F d, Y', strtotime($shift->get('date'))) . ' Shift No. ' . $shift->get('shiftno'),
		'sql'	   => $sql	
		);

$report = new report($arrReport);
$report->buildReport();
?>

<html>
<head>
<link rel="stylesheet" type="text/css" href="./reports.css">
<link rel="stylesheet" type="text/css" href="../../css/start/jquery-ui.css">
<script type="text/javascript" src="../../js/jquery.js"></script>
<script type="text/javascript" src="../../js/jquery-ui.js"></script>
</head>
<body>
<form>
<div>
Select Date: <input type="text" name="newdate" id="newdate" value="<?php echo $date ?>" />
Select Shift: <?php echo makeShiftSelect($shiftno); ?>
<input type="submit" name="submit" value="go" />
</div>
<?php $report->show(); ?> 
</form>
<script>
$(document).ready(function(){
	$("#newdate").datepicker({dateFormat:'yy-mm-dd'});
});
</script>
</body>
</html>

