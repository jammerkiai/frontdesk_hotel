<?php
/*
* intransit sales
* - get unchecked out
* - get total of future dated sales for each occupancy
*/
require_once('../config/config.inc.php');

$now = date('Y-m-d H:i:s');

/*
* step 1. get unchecked out 
*/
$sql = " select a.occupancy_id, a.actual_checkin, a.room_id, b.door_name 
		from occupancy a, rooms b 
		where a.room_id=b.room_id 
		and a.room_id not in (146,147,148)
		and a.actual_checkout='0000-00-00 00:00:00' ";
$res = mysql_query($sql);
$grandtotal = 0;
$retval =  "<h2>INTRANSIT SALES AS OF $now</h2>";
while(list($occ, $checkin, $roomid, $door) = mysql_fetch_row($res)) {
	$retval .=  "<strong>OCCUPANCY: $occ CHECKIN: $checkin ROOMID: $roomid ROOM #: $door </strong><br />";
	
	$sql2 = " select roomsales_id, sales_date, item_id, (unit_cost*qty) as cost 
			from room_sales 
			where occupancy_id=$occ
			and sales_date > '$now' ";
	$res2 = mysql_query($sql2) or die(mysql_error());
	while( list( $rid, $sdate, $itd, $cost ) = mysql_fetch_row( $res2 )) {
		$retval .=  "DETAILS [$door]>> $rid, $sdate, $itd, $cost <br />";
	}
	
	$sql2 = " select sum(unit_cost*qty) as cost 
			from room_sales 
			where occupancy_id=$occ
			and sales_date > '$now' ";
	$res2 = mysql_query($sql2) or die(mysql_error());
	while( list( $intransit ) = mysql_fetch_row( $res2 )) {
		$grandtotal += $intransit;
		$retval .=  "TOTAL PER ROOM [$door] ==>> $intransit <br />";
	}
	$retval .=  "<hr />";
}
$grandtotal = number_format($grandtotal,2);
$retval .=  "<h3>GRAND TOTAL INTRANSIT: PHP $grandtotal</h3>";

echo $retval;
$now = preg_replace('/[\s:-]/', '', $now);
$newfilename = "../reports/intransit/intransit_{$now}.html";
$fp = fopen($newfilename,'w+');
fwrite($fp,$retval);
fclose($fp);
