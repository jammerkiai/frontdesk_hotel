<?php
require_once("reportfns.php");
?>
<html>
<head>
<style>
body {font-family:arial,helvetica,tahoma;font-size:1em;}
h1 {font-size:1.1em;} 
h2 {font-size:1.0em;} 
a {font-size:0.9em;}
</style>
</head>
<body>
<h1><?php echo getHotel() ?> Reports</h1>
<h2>Audit Tools</h2>
<ul>
<li><a href="../finder.php" target="_blank">Room Finder</a></li>
<li><a href="../report.checkout.php" target="_blank">Shift Checkout Reports</a></li>
<li><a href="../report.salesreceipts.php" target="_blank">Sales Receipts</a></li>
<li><a href="./monthlysalessummary.php" target="_blank">Monthly Sales Summary</a></li>
<li><a href="./clposting.php" target="_blank">GL Posting Summary</a></li>
<li><a href="./cardsales.php" target="_blank">Card Sales</a></li>
<li><a href="../fc_monthly.php" target="_blank">Forecast</a></li>
<li><a href="../fc_summary.php" target="_blank">OR Monthly Summary</a></li>
</ul>
<h2>Shift-end Reports</h2>
<ul>
<li><a href="roomsales_pershift.php" target="_blank">Room Sales</a></li>
<li><a href="fnbsales_pershift.php" target="_blank">Food & Beverage Sales</a></li>
<li><a href="totalsales_pershift.php" target="_blank">Total Sales</a></li>
<li><a href="intransitsales_pershift.php" target="_blank">In Transit (Current Guest Transactions)</a></li>
<li><a href="shiftend_reportlist.php" target="_blank">Shift End Report List</a></li>
</ul>
<h2>Monthly Reports</h2>
<ul>
<li><a href="../report.sales.hotel.php" target="_blank">Hotel Sales</a></li>
<li><a href="../report.coop.php" target="_blank">Coop Sales</a></li>
<li><a href="../report.miniref.php" target="_blank">Miniref Sales</a></li>
<li><a href="../report.fnb.php" target="_blank">Food & Beverage Sales</a></li>
<li><a href="../report.creditcard.php" target="_blank">Credit Card Sales</a></li>
<li><a href="../report.creditcard.php" target="_blank">Receivables</a></li>
<li><a href="../report.discount.php" target="_blank">Discounts</a></li>
<li><a href="../report.sales.walkup.php" target="_blank">Walkup Sales</a></li>
<li><a href="../report.sales.php" target="_blank">Sales</a></li>
</ul>
</body>
</html>
