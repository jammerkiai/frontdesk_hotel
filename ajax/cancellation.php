<?php
//transfer.php
session_start();
include_once("config/config.inc.php");
include_once("config/lobby.inc.php");
include_once("reporting.php");
include_once("class.room.php");

$p = $_POST;

/* if($p["act"]=='transfer') {
	$user = $_SESSION["hotel"]["userid"];
	$newroom  = $p["newroom"];
	$reason = $p["newreason"];
	$newadjust = $p["newadjust"];
	$currentroom = $p["currentroom"];
	//retrieve occupancy id
	$sql  = " select occupancy_id from occupancy where room_id='$currentroom' and actual_checkout='0000-00-00 00:00:00' limit 1";
	$res = mysql_query($sql);
	list($occupancy_id)=mysql_fetch_row($res);
	//update occupancy
	$now = date("Y-m-d H:i:s");
	$sql = " update occupancy set room_id='$newroom', update_by='$user' where occupancy_id='$occupancy_id'";
	mysql_query($sql);
	//new occupancy_log record
	$sql  = " insert into occupancy_log (transaction_date, occupancy_id, update_by, remarks) 
		values ('$now', '$occupancy_id', '$user', '$newreason')";
	mysql_query($sql);	
	//update rooms --> set  as make up
	$sql = " update rooms set status=5, last_update='$now', update_by='$user' where room_id='$currentroom' ";
	mysql_query($sql);
	//update new room - set status as occupied
	$sql = " update rooms set status=2, last_update='$now', update_by='$user' where room_id='$newroom' ";
	mysql_query($sql);
	
	if($newadjust) {
		$sql  =" insert into room_sales( occupancy_id, sales_date, category_id, item_id ,  unit_cost, qty) 
			values ('$occupancy_id', '$now', 3,18,'$newadjust','1') ";
		mysql_query($sql);
	}

} */

if($p["act"]=="Proceed with cancellation"){
	//check
	if($p["new_remark"]==""){
		echo "<script>alert('Please select a reason');</script>";	
	}else{
		$room =($_GET["roomid"]) ? $_GET["roomid"] : $_POST["roomid"];
		$rm = new room($room);
		$obj = new Reporting($room);
		$rm->getovertimehours();		
		if($rm->overtime_hours >= 1) {
		$add_cost = $rm->ot_amount * $rm->overtime_hours;		
		}
		
		$sql  = " select occupancy_id from occupancy where room_id='$room' and actual_checkout='0000-00-00 00:00:00' limit 1";
		$res = mysql_query($sql);
		list($occupancy_id)=mysql_fetch_row($res);


		$f=$rm->floor_id - 1;
		
		if($obj->totalbalance+$add_cost <= 0)
		{
			setCancellation($occupancy_id,$p["new_remark"],$room);
			echo "<script>alert('Successful cancellation');parent.document.location.href='../index.php?f=$f&room=$room'</script>";
			//echo "<script>alert('Please proceed to Checkout Panel');</script>";
		}
		else
		{
			echo "<script>alert('You still have Balances, please proceed to Checkout Panel');</script>";
		}
		
		exit;


		/*$croomid = $_GET["roomid"];
		$sql  = " select occupancy_id from occupancy where room_id='$current_room_id' and actual_checkout='0000-00-00 00:00:00' order by actual_checkin desc limit 0,1";
		$res = mysql_query($sql);
		list($occupancy_id)=mysql_fetch_row($res);
		//setCancellation($occupancy_id,$p["new_remark"]);
		//echo "<script>alert('Successful cancellation');parent.document.location.href='../index.php'</script>";	
		echo "<script>alert('Please proceed to Checkout Panel');</script>";
		exit;*/
	}
}

//set room status as make up
function setCancellation($occupancyid,$reason,$roomid){
	//update occupancy
	$now = date("Y-m-d H:i:s");
	$user = $_SESSION["hotel"]["userid"];
	$sql = " update occupancy set actual_checkout = '$now', room_id='$roomid', update_by='$user' where occupancy_id='$occupancyid'";
	mysql_query($sql);
	
	//cancel in occupancy log
	$sql = " insert into occupancy_log (transaction_date, occupancy_id, update_by, remarks, transaction_type) 
			values ('$now', '$occupancyid', '$user', '$reason', 'Cancel')";
	mysql_query($sql);

	//update rooms --> set  as make up
	$sql = " update rooms set status=5, last_update='$now', update_by='$user' where room_id='$roomid' ";
	mysql_query($sql);



}



function getFormDDLRemarks(){
	$ret = "<select name='new_remark' id='new_remark' width='100%'>";
	$ret .= "<option value=''></option>";
	$sql = "select remark_id,remark_text from remarks where remark_classification = '2'";
	$res = mysql_query($sql);
	while(list($remark_id,$remark_text) = mysql_fetch_row($res)){
		$ret .= "<option value='$remark_id'>$remark_text</option>";
	}
	$ret .= "</select>";
	return $ret;
}

?>
<link rel="stylesheet" type="text/css" href="../css/start/jquery-ui.css" />
<link rel="stylesheet" type="text/css" href="../css/ajaxphp.css" />
<link rel="stylesheet" type="text/css" href="../css/jquery.approver.css" />
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/jquery-ui.js"></script>
<script type="text/javascript" src="../js/jquery.approver.js"></script>
<form name="myform" id="myform" action="" method="post">
<fieldset id="changestatus">
<legend>Room Cancellation</legend>
<table class="formtable">
<!-- <tr>
<td>Select New Room: </td>
<td>
 get search filter functions from reservation to generate cascading select boxes
</td>
</tr> -->
<tr>
<td >Reasons:</td>
<td>
<? echo getFormDDLRemarks() ?>
</td>
</tr>
<tr>
<td colspan="2" align="right">
<input type="submit" name="act" id="act" value="Proceed with cancellation" class="approver" />
<!--
<input type="button" value="Proceed with Transfer" name="cmdApprove" id="cmdApprove" />
-->
<div id='diverror'></div>
</td>
</tr>
</table>
</td>
</tr>
</table>
</fieldset>
</form>
<script lang="javascript">
$(document).ready(function(){
    $('#act').approver({
        url : 'oiclist_json.php',
        approvaltype : 'Cancellation',
        approvalinfo : $('#new_remark').val()
    });
});
</script>
