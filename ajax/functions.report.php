<?php
/**
* checkout functions
*/

function getTotalCostByItemID() {

}

function getLobbyFoodSalesByItemNotIn($lobbyid, $start, $end, $arrItems=array()) {
	$strItems = implode(',',$arrItems);
	$_sql = "select sum(unit_cost*qty) from fnb_sales 
			where status in ('Paid')
			and category_id not in ($strItems)
			and occupancy_id = '$lobbyid'
			and update_date >= '$start' 
			and update_date <= '$end' ";
	$_res = mysql_query($_sql);
	list($value) = mysql_fetch_row($_res);
	return $value;
}

function getLobbyFoodSalesByItemIn($lobbyid, $start, $end, $arrItems=array()) {
	$strItems = implode(',',$arrItems);
	$_sql = "select sum(unit_cost*qty) from fnb_sales 
			where status in ('Paid')
			and category_id in ($strItems)
			and occupancy_id = '$lobbyid'
			and update_date >= '$start' 
			and update_date <= '$end' ";
	$_res = mysql_query($_sql);
	list($value) = mysql_fetch_row($_res);
	return $value;
}

function getLobbyMiscSalesByItemNotIn($lobbyid, $start, $end, $arrItems=array()) {
	$strItems = implode(',',$arrItems);
	$_sql = "select unit_cost*qty from room_sales 
		where status in ('Paid')
		and category_id not in ($strItems)
		and occupancy_id = '$lobbyid'
		and update_date >= '$start' 
		and update_date <= '$end' ";
	$_res = mysql_query($_sql);
	list($value) = mysql_fetch_row($_res);
	return $value;
}

function getLobbyMiscSalesByItemIn($lobbyid, $start, $end, $arrItems=array()) {
	$strItems = implode(',',$arrItems);
	$_sql = "select unit_cost*qty from room_sales 
		where status in ('Paid')
		and category_id in ($strItems)
		and occupancy_id = '$lobbyid'
		and update_date >= '$start' 
		and update_date <= '$end' ";
	$_res = mysql_query($_sql);
	list($value) = mysql_fetch_row($_res);
	return $value;
}

function getTotalRoomSalesByItemOccupancy($itemid,$occupancyid) {
	$sql = " select sum(unit_cost*qty) from room_sales 
			where status in ('Paid')
			and item_id = '$itemid'
			and occupancy_id = '$occupancyid' ";
	$res = mysql_query($sql);
	list($value) = mysql_fetch_row($res);
	return $value;
}

function getTotalFoodSalesByItemOccupancyNotIn($occupancy, $arrItems=array()) {
	$strItems = implode(',',$arrItems);
	$_sql = "select sum(unit_cost*qty) from fnb_sales 
			where status in ('Paid')
			and category_id not in ($strItems)
			and occupancy_id = '$occupancy'";
	$_res = mysql_query($_sql);
	list($value) = mysql_fetch_row($_res);
	return $value;
}

function getTotalFoodSalesByItemOccupancyIn($occupancy, $arrItems=array()) {
	$strItems = implode(',',$arrItems);
	$_sql = "select sum(unit_cost*qty) from fnb_sales 
			where status in ('Paid')
			and category_id in ($strItems)
			and occupancy_id = '$occupancy'";
	$_res = mysql_query($_sql);
	list($value) = mysql_fetch_row($_res);
	return $value;
}

function getTotalMiscSalesByItemOccupancyIn($occupancy,$arrItems=array()) {
	$strItems = implode(',',$arrItems);
	$sql = " select sum(unit_cost*qty) from room_sales 
			where status in ('Paid')
			and item_id in ($strItems)
			and occupancy_id = '$occupancy' ";
	$res = mysql_query($sql);
	list($value) = mysql_fetch_row($res);
	return $value;
}

function getTotalMiscSalesByCategoryOccupancyNotIn($occupancy,$arrItems=array()) {
	$strItems = implode(',',$arrItems);
	$sql = " select sum(unit_cost*qty) from room_sales 
			where status in ('Paid')
			and category_id not in ($strItems)
			and occupancy_id = '$occupancy' ";
	$res = mysql_query($sql);
	list($value) = mysql_fetch_row($res);
	return $value;
}


?>