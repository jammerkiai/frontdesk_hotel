<?php
session_start();
include_once("config/config.inc.php");

$act = $_POST["act"];
$user_id = $_SESSION["hotel"]["userid"];
$txtamount = $_POST["txtamount"];

$oic_id = $_POST["ddloic"];
$gsafekeep = "10000";

$datetime = date("Y-m-d H:i:s");
$shiftid = getShiftID($datetime);

if($act == "Continue to safekeep"){

	$cashonhand = getTotalCashonHand(getStartTime(), $datetime);
	if($cashonhand < $txtamount)
	{
		echo "<script>alert('You dont have enough cash on hand to safekeep.');</script>";
	}
	elseif($oic_id == "0")
	{
		echo "<script>alert('Please choose oic.');</script>";
	}
	elseif($txtamount < $gsafekeep)
	{
		echo "<script>alert('Inputted amount is lesser than safekeep.');</script>";
	}
	else
	{

		// ? get shift
		$_sql = "select fullname from users where user_id = '$user_id'";
		$_res = mysql_query($_sql);
		list($cusername)=mysql_fetch_row($_res);

		$_sql = "select fullname from users where user_id = '$oic_id'";
		$_res = mysql_query($_sql);
		list($ousername)=mysql_fetch_row($_res);

		$now = date("m/d/y g:i A", strtotime($datetime));

		$shift = getshift('');

		setSafekeep($txtamount,$user_id,$oic_id,getShiftID($datetime));
		$retval=chr(hexdec('1B')).chr(hexdec('4D')).chr(1);
		$retval.="SAFEKEEP REPORT\n\n
		SHIFT: $shift \n
		CASHIER: $cusername \n
		OIC: $ousername \n
		TIME: $now \n
		AMOUNT: ".number_format($txtamount,2)."\n\n\n\n\n";
		$retval.=chr(hexdec('1D')).chr(hexdec('56')).chr(49);

		$file = "safekeep.txt";
		$fp = fopen("reports/" .$file, "w");
		fwrite($fp,$retval);
		fclose($fp);
		shell_exec('c:\pstools\psexec c:\xampp\htdocs\fds\ajax\p.bat ' . $file);
		shell_exec('c:\pstools\psexec c:\xampp\htdocs\fds\ajax\p.bat ' . $file);

		echo "<script>alert('Successful safekeep');parent.document.location.href='../index.php'</script>";
	}
}

//methods
function getshift($date) {
	if(!$date)$date=date("Y-m-d H:i:s");
	list($d, $t) = explode(" ", $date);
	list($h, $m, $s) = explode(":", $t);
	$sql = "select shift_id from shifts where $h between shift_start and shift_end";
	$res = mysql_query($sql) or die(mysql_error() .$sql);
	list($shift)=mysql_fetch_row($res);

	if($h>=14&&$h<=21)
	{
		return $shift = "3rd";
	}
	elseif($h>=6&&$h<=13)
	{
		return $shift = "2nd";
	}
	return  "1st";
}

function setCurrentCash($amount,$transaction_type,$update_by)
{
	$now = date("Y-m-d H:i:s");
	$sql = "select current_amount from current_cash order by cc_date desc,cc_id desc  limit 0,1";
	$res = mysql_query($sql);

	list($current_amount)=mysql_fetch_row($res);
	if($transaction_type == 'in')
	{
		$current_amount = $amount + $current_amount;
	}
	else
	{
		$current_amount = $current_amount - $amount;
	}

	$sql = "insert into current_cash(transaction_amount,current_amount,transaction_type,update_by,cc_date) values('$amount','$current_amount','$transaction_type','$update_by','$now')";

	mysql_query($sql) or die(mysql_query($sql));
}

function getSalesByTimeframe($start, $end)
{
	$ret = 0;
	$sql = "select amount from salesreceipts where receipt_date >= '$start' and receipt_date <= '$end'";
	$res = mysql_query($sql);
	while(list($amount)=mysql_fetch_row($res))
	{
		$ret += $amount;
	}
	return $ret;
}

function getSafekeepAmountByTimeframe($start, $end)
{
	$ret = 0;
	$sql = "select amount from safekeep where safekeep_date >= '$start' and safekeep_date <= '$end'";
	$res = mysql_query($sql);
	while(list($amount)=mysql_fetch_row($res))
	{
		$ret += $amount;
	}
	return $ret;
}

function getTotalCashonHand($start, $end)
{
	$sql = "select current_amount from current_cash order by cc_date desc, cc_id desc limit 0,1";
	$res = mysql_query($sql);
	//while(list($amount)=mysql_fetch_row($res))
	//{
	//	$ret += $amount;
	//}
	list($ret)=mysql_fetch_row($res);
	return $ret;
}

function setSafekeep($amount,$cashier_id,$oic_id,$shift_id)
{


	$_date = date("Ymd");
	//shift_code;
	$shift_code = $_date."-".$shift_id;
	$total = 0;
	$sql = "select current_amount from safekeep order by safekeep_id desc limit 0,1";
	$res = mysql_query($sql) or die(mysql_error($sql));
	list($current_amount) = mysql_fetch_row($res);
	$total = $current_amount+$amount;
	$now = date("Y-m-d H:i:s");
	$sql = "insert into safekeep(safekeep_date,cashier_id,oic_id,amount,shift_code,current_amount) values('$now','$cashier_id','$oic_id','$amount','$shift_code','$total')";
	mysql_query($sql) or die(mysql_error($sql));

	setCurrentCash($amount,'Out',$cashier_id);

}

function getShiftID($datetime)
{
	list($date,$time)=explode(" ",$datetime);
	list($hour,$min,$sec)=explode(":",$time);
	$sql = "select * from shifts where shift_end > '$hour' and shift_start <='$hour'";
	$res = mysql_query($sql);
	list($shift_id) = mysql_fetch_row($res);
	return $shift_id;
}

function getStartTime()
{
	//workaround for getting starttime
	$date = date("Y-m-d ");
	$yesterday = date("Y-m-d", strtotime($date, '-1 DAY'));
	$h= date("H");
	if($h < 6) {
		$return = "$yesterday 22:00:01";
	}elseif($h < 14) {
		$return = "$date 06:00:01";
	}else {
		$return = "$date 14:00:01";
	}
	return $return;
	$sql = "select datetime from `shift-transactions` where shift = 'start' order by datetime desc";

	$res = mysql_query($sql);
	while(list($time) = mysql_fetch_row($res))
	{
		return $time;
	}

}

//forms
function getUserddl($group_id)
{
	$sql = "select user_id, fullname from users where group_id = '$group_id'";
	$res = mysql_query($sql);
	$opt = "<option value='0' ></option>";
	while(list($user_id, $fullname)=mysql_fetch_row($res)){
		$opt .= "<option value='$user_id'>$fullname</option>";
	}
	$ret = "<select id='ddloic' name='ddloic' class='keyselect'>";
	$ret .=	$opt;
	$ret .= "</select>";

	return $ret;
}


?>
<style>
.cmdbtn {
	border:1px solid  #ffffff;
	background-color: #9BD1E6;
	margin-right:2px;
	padding:2px;
	cursor:pointer;
}

.keypadfield{ border:1px solid #51B1D8; background-color:#E0EBEF; width:100%}
label{font-family:lucida,arial,helvetica;font-size:.7em}
.keyselect{font-family:lucida,arial,helvetica;font-size:.7em}
</style>
<form name='myform'  id="myform" method='post'>
<table>
<tr>
	<td><label>Amount:</label></td>
	<td><input type='text' id='txtamount' name='txtamount' value='<?=$gsafekeep?>' class='keypadfield'/>
	</td>
</tr>
<tr>
	<td><label>OIC</label></td>
	<td><? echo getUserddl("4") ?>
	</td>
</tr>
<tr>
	<td>&nbsp;</td>
	<td><input name="act" id="act" type="submit" value="Continue to safekeep" class="cmdbtn" /></td>
</tr>
</table>
</form>
<link rel="stylesheet" type="text/css" href="../css/jquery.keypad.css" />
<link rel="stylesheet" type="text/css" href="../css/start/jquery-ui.css" />
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/jquery.keypad.pack.js"></script>
<script type="text/javascript" src="../js/jquery-ui.js"></script>
<script lang="javascript">

$(document).ready(function(){
	$(".keypadfield").keypad({keypadOnly:true});
});
</script>
