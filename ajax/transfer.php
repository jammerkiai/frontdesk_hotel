<?php
//transfer.php
session_start();
include_once("config/config.inc.php");
include_once("config/lobby.inc.php");
include_once("class.room.php");

$p = $_POST;
$croomid = $_GET["roomid"];

if($p["cmd"]=='viewdifference'){
	
	echo getFormTransferDifference($p["croomid"],$p["nroomid"]);
	exit;
}elseif($p["cmd"]=='proceed'){	
	/*
	if(!checkIfUsernameIsOIC($p["new_user"],$p["new_pass"])){
		//error
		echo "{ error : \"<span style='color:red'>OIC password is incorrect.</span>\", message : '' }";
		exit;
	}
	else
	{
	*/
		setTransferRoom($p["croomid"],$p["nroomid"],$p["remark"],$p["new_adjust"]);
		$rm = new room($p["nroomid"]);
		$f=$rm->floor_id - 1;
		echo json_encode(array('success' => true, 'floor' => $f, 'room' => $p["nroomid"]));
		//echo "{ message : \"<script>alert('successful transfer');parent.document.location.href='../index.php?f=$f&room=".$p["nroomid"]."'</script>\", error : '' }";	
		exit;
	//}
}

function checkIfUsernameIsOIC($username,$password){
	$sql = "select user_id from users where username = '$username'
		and userpass = '$password'";
	
		//and group_id = '4'";			
	$res = mysql_query($sql);
	$num = mysql_num_rows($res); 	
	if($num > 0){
		return true;		
	}
	return false;
}

function setTransferRoom($current_room_id,$new_room_id,$newreason,$newadjust){

	$user = $_SESSION["hotel"]["userid"];
	
	//retrieve occupancy id
	$sql  = " select occupancy_id from occupancy where room_id='$current_room_id' and actual_checkout='0000-00-00 00:00:00' limit 1";
	$res = mysql_query($sql);
	list($occupancy_id)=mysql_fetch_row($res);
	
	//update occupancy
	$now = date("Y-m-d H:i:s");
	$sql = " update occupancy set room_id='$new_room_id', update_by='$user' where occupancy_id='$occupancy_id'";
	mysql_query($sql);
	
	//new occupancy_log record
	$sql  = " insert into occupancy_log (transaction_date, occupancy_id, update_by, remarks, transaction_type) 
		values ('$now', '$occupancy_id', '$user', '$newreason', 'Transfer')";
	mysql_query($sql);

	//update rooms --> set  as make up
	$sql = " update rooms set status=5, last_update='$now', update_by='$user' where room_id='$current_room_id' ";
	mysql_query($sql);
	
	//update new room - set status as occupied
	$sql = " update rooms set status=2, last_update='$now', update_by='$user' where room_id='$new_room_id' ";
	mysql_query($sql);	
	
	//update room_sales to set the new room for the remainder of stay
	$sql = " select roomsales_id, unit_cost from room_sales 
			where sales_date < '$now' 
			and occupancy_id=$occupancy_id
			and item_id=15
			order by roomsales_id desc
			limit 0,1";
	$res = mysql_query($sql);
	list($rid, $ucost)=mysql_fetch_row($res);

	$sql = " update room_sales set room_id='$new_room_id' 
		where occupancy_id='$occupancy_id' and category_id=3 and roomsales_id >= $rid ";
	mysql_query($sql);
	
	if($newadjust) {
		$sql = " update room_sales 
		set unit_cost=unit_cost+$newadjust 
		where occupancy_id='$occupancy_id' 
		and category_id=3 and item_id=15 and roomsales_id >= $rid ";
		mysql_query($sql);
		$sql3 = "update room_sales set unit_cost=unit_cost + $newadjust*(unit_cost/$ucost)
			where occupancy_id='$occupancy_id' 
			and category_id=3 and item_id=17 
			and roomsales_id >= '$rid' ";	

		mysql_query($sql3) or die($sql3);
	}
	
}

function getFormDDLRemarks(){
	
	$ret = "<select name='new_remark' id='new_remark' width='100%' style='font-size:15px'>";
	$ret .= "<option value=''></option>";
	$sql = "select remark_id,remark_text from remarks where remark_classification = '2'";
	$res = mysql_query($sql);
	while(list($remark_id,$remark_text) = mysql_fetch_row($res)){
		$ret .= "<option value='$remark_id'>$remark_text</option>";
	}
	$ret .= "</select>";
	return $ret;
}


function getFormDDLRoomTransfer($roomid){
	$sql = " select a.occupancy_id
			from occupancy a, rooms b, rates c, users d
			where a.room_id=b.room_id 
			and a.rate_id=c.rate_id 
			and a.update_by=d.user_id 
			and a.room_id='$roomid' 
			and actual_checkout='0000-00-00 00:00:00' ";
	$res = mysql_query($sql);
	
	$num = mysql_num_rows($res);
	if($num > 0) {
		list($occupancy)=mysql_fetch_row($res);
		unset($res);	
	}
	
	//get raw amount
	$room = new room($roomid);
	$sql = "select rate_id from occupancy where  occupancy_id = '$occupancy'";
	$res = mysql_query($sql);
	list($rate_id)=mysql_fetch_row($res);
	
	$sql = "select amount 
	        from room_type_rates 
	        where room_type_id = '".$room->room_type_id."' 
	        and 	rate_id = '".$rate_id."'";
	$res = mysql_query($sql);
	list($cost)=mysql_fetch_row($res);		
	
	$ret = "<select name='new_room' id='new_room' width='100%' style='font-size:25px'>";
	$ret .= "<option value=''></option>";
	$sql = "select room_id,door_name from 
	        (select room_type_id from room_type_rates 
	        where amount >= '$cost' 
	        and rate_id = '$rate_id') as table1, rooms 
	        where table1.room_type_id = rooms.room_type_id 
	        and rooms.status = '1'";

	$res = mysql_query($sql);
	$num = mysql_num_rows($res);
	if($num == 0){
		return $ret = "<font color='red'>No available room to transfer.</font>";
	}else{while(list($room_id,$door_name) = mysql_fetch_row($res)){
		
		$ret .= "<option value='$room_id'>$door_name</option>";
	}}
	$ret .= "</select>";
	return $ret;
}

function getFormTransferDifference($currentroomid,$newroomid){

	$croom = new room($currentroomid);
	$nroom = new room($newroomid);
	
	
	$sql = " select a.occupancy_id
			from occupancy a, rooms b, rates c, users d
			where a.room_id=b.room_id and a.rate_id=c.rate_id and a.update_by=d.user_id and a.room_id=$currentroomid and actual_checkout='0000-00-00 00:00:00' ";
	$res = mysql_query($sql);
	
	if(mysql_num_rows($res) > 0) {
		list($occupancy)=mysql_fetch_row($res);
		unset($res);	
	}
	
	/*$sql = " select a.roomsales_id, a.category_id, a.item_id, b.sas_description, a.unit_cost 
				from room_sales a, sales_and_services b	
				where a.item_id=b.sas_id and a.category_id=3 and a.occupancy_id='$occupancy'  ";
	
	$res = mysql_query($sql);
	$ccost = 0;
	while(list($rsid, $catid, $itemid, $itemdesc,$cost)=mysql_fetch_row($res)) {
		$ccost += $cost;
	}*/
	
	
	
	
	
	$sql = "select rate_id from occupancy where  occupancy_id = '$occupancy'";
	$res = mysql_query($sql);
	list($crate_id)=mysql_fetch_row($res);
	
	
	$sql = "select amount from room_type_rates where room_type_id = '".$croom->room_type_id."' and 	rate_id = '".$crate_id."'";
	$res = mysql_query($sql);
	list($ccost)=mysql_fetch_row($res);	
	
	$sql = "select amount from room_type_rates where room_type_id = '".$nroom->room_type_id."' and 	rate_id = '".$crate_id."'";
	$res = mysql_query($sql);
	list($ncost)=mysql_fetch_row($res);	
	
	
	$total = $ncost - $ccost;
	$ret = "<table border=1 cellpadding='1' cellspacing='1'>";
	$ret .= "<tr><td><b>New room:</b></td><td>".$nroom->door_name."</td><td>P ".$ncost."</td></tr>";
	$ret .= "<tr><td><b>Current room:</b></td><td>".$croom->door_name."</td><td>P ".$ccost."</td></tr>";	
	$ret .= "<tr><td valign='top'><b>Difference</b></td><td>&nbsp;</td><td align='right'>
		<input type='text' width='10' name='new_adjust' id='new_adjust' style='text-align:right' value='".$total."'/>
		<div id='usernameandpassword' align='left'>
		";
	//$ret .= getFormOIC();
	$ret .=	"
		</div>
		</td>
		</tr>";
	$ret .= "</table>";
	$ret .= "<input type='hidden' name='hid_adjust' id='hid_adjust' value='".$total."'/>";
	return $ret;	
}


function getFormOIC()
{
	//$ret = "Supervisor/OIC:<br>";
	$ret .= "Username:";

	$sql = "select username, fullname from users where group_id=4";
	$res = mysql_query($sql);
	$ret .= "<select name='new_user' id='new_user'>";
	$ret .= "<option ></option>";
	while(list($usr,$full)=mysql_fetch_row($res))
	{
		$ret .= "<option value='$usr'>$full</option>";
	}
	$ret .= "</select>";
	
	$ret .= "<br>Password:";
	$ret .= '<input type="password" name="new_pass" id="new_pass"  />';
	return $ret;
}


?>
<html>
<head>
<link rel="stylesheet" type="text/css" href="../css/start/jquery-ui.css" />
<link rel="stylesheet" type="text/css" href="../css/ajaxphp.css" />
<link rel="stylesheet" type="text/css" href="../css/jquery.approver.css" />
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/jquery-ui.js"></script>
<script type="text/javascript" src="../js/jquery.approver.js"></script>
</head>
<body>
<form>
<fieldset id="changestatus">
<legend>Room Transfer</legend>
<table class="formtable">
<tr>
<td>Select New Room: </td>
<td>
<? echo getFormDDLRoomTransfer($croomid)?>
</td>
</tr>
<tr>
<td>&nbsp;</td>
<td><div id="transferdiff">
</div>
</td>
</tr>
<tr>
<td >Reasons:</td>
<td>
<? echo getFormDDLRemarks() ?>
</td>
<tr>
<td>&nbsp;</td>
<td><div id="divmessage"></div></td>
</tr>
</tr>
<tr>
<td colspan="2" align="right"><input type="submit" name="act" id="act" value="Proceed with transfer" style="font-size:15px">
</td>
</tr>
</table>

</form>

<script lang="javascript">
$(document).ready(function(){	

    
	$("#new_room").change(function(){
		$.post("transfer.php",{ cmd:'viewdifference', nroomid: $(this).val(), croomid: <?=$croomid?> }, function(resp){
			$("#transferdiff").hide().html(resp).fadeIn('fast');		
		});
	});


	$("#act")
	    .click(function(e){
		    e.preventDefault();		
		    $.post("transfer.php",{ cmd:'proceed', croomid: <?=$croomid?>, nroomid: $("#new_room").val(),
				    hid_adjust:$("#hid_adjust").val(),new_adjust:$("#new_adjust").val(),
				    new_user:$("#new_user").val(),new_pass:$("#new_pass").val(),remark:$("#new_remark").val()}, 
					function(resp){
						if (resp.success == true) {
							alert('successful transfer');
							parent.document.location.href='../index.php?f=' + resp.floor + '&room=' + resp.room;
						}
						$("#diverror").html(resp.error);			
						$("#divmessage").html(resp.message);
						return false;		
					}, "json");
	    })
	    .approver({
			url: 'oiclist_json.php',
			approvaltype : 'Transfer',
			approvalinfo : $("#new_remark").val()
		});


});
</script>
</body>
</html>
