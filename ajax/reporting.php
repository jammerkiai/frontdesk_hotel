<?php


class Reporting
{

	public function __construct($room) 
	{
		$sql = " select a.occupancy_id, a.actual_checkin, b.door_name, c.rate_name, a.shift_checkin, 
		d.fullname,a.expected_checkout from occupancy a, rooms b, rates c, users d
		where a.room_id=b.room_id and a.rate_id=c.rate_id and a.update_by=d.user_id and a.room_id=$room and
		actual_checkout='0000-00-00 00:00:00' order by a.actual_checkin desc";
		$res = mysql_query($sql) or die($sql);
		$occupied = 0;
		if(mysql_num_rows($res) > 0) {
			list($occupancy, $checkindate,$doorname,$ratename,$shift,$fullname,$etd)=mysql_fetch_row($res);
			unset($res);
			$occupied = 1;
		}
		$this->occupied = $occupied;
		$this->occupancy = $occupancy;
		$this->checkindate = $checkindate;
		$this->doorname = $doorname;
		$this->ratename = $ratename;
		$this->shift = $shift;
		$this->fullname = $fullname; 
		$this->etd = $etd;
		$indate = new DateTime($etd);
		$newcheckout = date_sub($indate, new DateInterval("PT30M") );
		$this->wakeup = $indate->format("Y-m-d H:i:s");
		$this->totalbalance = $this->getTotalBalance();
		$this->totalbalance_display = $this->getTotalBalanceDisplay();
		$this->total = 0; 
    
	    $this->publishrate = $this->getPublishRate();
		$sql = "select settings_value from settings where id = '1'";
		$res = mysql_query($sql);
		list($val)=mysql_fetch_row($res);
		$this->hotel = $val;
	}
	
	public function getTotalBalance()
	{
		$now = date("Y-m-d H:i:s");
		$occupancy = $this->occupancy; 
		//retrieve room sales
		$sql = " select a.roomsales_id, a.category_id, a.item_id, b.sas_description, a.unit_cost * a.qty
				from room_sales a, sales_and_services b		
				where a.item_id=b.sas_id and a.category_id=3 and a.occupancy_id=$occupancy 
				and a.sales_date <= '$now' ";
		$res = mysql_query($sql) or die(mysql_error() . $sql);
		$rtotal = 0;

		while(list($rsid, $catid, $itemid, $itemdesc,$cost)=mysql_fetch_row($res)) {
		
			$rtotal += $cost;
		}

		
		//retrieve misc sales
		$mtotal = 0;
		$sql = " select a.roomsales_id, a.category_id, a.item_id, b.sas_description, a.unit_cost*a.qty 
			from room_sales a, sales_and_services b		
			where a.item_id=b.sas_id and a.category_id in (1, 2,4) and a.occupancy_id=$occupancy   and a.status not in ('Draft','Cancelled') ";
		$res = mysql_query($sql) or die(mysql_error() . $sql);
		
		while(list($rsid, $catid, $itemid, $itemdesc,$cost)=mysql_fetch_row($res)) {
			$mtotal += $cost;
		}

		
		//retrieve fnb sales
		$ftotal = 0;
		$sql = " select a.fnbsales_id, a.category_id,  a.item_id, c.food_category_name, b.fnb_code,b.fnb_name, a.unit_cost, a.qty, (a.unit_cost * a.qty) as totalcost
				from fnb_sales a, fnb b, food_categories c		
				where a.item_id=b.fnb_id and a.category_id=b.food_category_id and a.category_id=c.food_category_id and a.occupancy_id=$occupancy and a.status not in ('Draft','Cancelled') ";
		$res = mysql_query($sql) or die(mysql_error());

		while(list($sid, $catid, $itemid, $catname, $code, $itemdesc, $unitcost, $qty, $ftcost)=mysql_fetch_row($res)) {
			$ftotal += $ftcost;
		}
		
		//retrieve payments
		$ptotal = 0;
		$sql  = "select tendertype, amount from salesreceipts where occupancy_id='$occupancy' ";
		$res = mysql_query($sql) or die(mysql_error());
		$payments = "";
		while(list($type, $amount)=mysql_fetch_row($res)) {
			$ptotal += $amount;
		}

		$grandtotal = number_format($rtotal + $ftotal + $mtotal,2);
		$rtotal_f =number_format($rtotal,2);
		$mtotal_f =number_format($mtotal,2);
		$ftotal_f =number_format($ftotal,2);
		$payments_f = number_format($ptotal,2);
		$totalbalance = ($rtotal + $ftotal + $mtotal) - $ptotal;

		$this->rtotal = $rtotal;
		$this->ftotal = $ftotal;
		$this->mtotal = $mtotal;
		$this->total  = $rtotal + $ftotal + $mtotal;
		$this->totaldisplay =  number_format($this->total,2);
		return $totalbalance;
	}
	
	public function getTotalBalanceDisplay()
	{
		$change = 0;
		if($this->totalbalance < 0) $change = abs($this->totalbalance);
	
		$totalbalance_display = number_format($this->totalbalance,2);
		
		return $totalbalance_display;
	}
	
	
	public function getFnbSales()
	{
		$ftotal = 0;
		$sql = " select a.fnbsales_id, a.order_code, a.category_id,  a.item_id, c.food_category_name, b.fnb_code,b.fnb_name, 
		a.unit_cost, a.qty, (a.unit_cost * a.qty) as totalcost, a.status,a.sales_date
		from fnb_sales a, fnb b, food_categories c		
		where a.item_id=b.fnb_id and a.category_id=b.food_category_id and a.category_id=c.food_category_id and a.occupancy_id=$this->occupancy and a.status in ('Printed','Paid') ";
		$res = mysql_query($sql) or die(mysql_error());
		$fsales ="<table width='300' cellpadding=1 cellspacing=1>";
		$oldoc="";
		while(list($sid, $oc, $catid, $itemid, $catname, $code, $itemdesc, $unitcost, $qty, $totalcost,$status,$date)=mysql_fetch_row($res)) {
			if($oldoc!=$oc) {
				$date = date('m-d H:i',strtotime($date));
				$fsales .= "<tr><td colspan=4 style='border-bottom:1px dotted #cccccc;'><span style='font-style:italic'>$date: Order Slip#: $oc</span></td></tr>";
			}
			$fsales .= "<tr><td>&nbsp;</td>";
			$fsales.="<td>$itemdesc</td><td>$totalcost</td>";
			$fsales.="<td>$status</td>";
			$fsales .= "</tr>";
			$ftotal += $totalcost;
			$oldoc=$oc;
		}
		$fsales .= "<tr><td>&nbsp;</td><td>Total:</td><td style='text-align:right;border-top:1px solid #000000;border-bottom:2px solid #000000;'>".number_format($ftotal,2)."</td></tr>";
		$fsales .="</table>";
		
		return $fsales;
	}
	
	public function getMiscSales()
	{
		$mtotal = 0;
		$sql = " select a.roomsales_id, a.order_code, a.category_id, a.item_id, b.sas_description, a.unit_cost*a.qty,a.status,a.sales_date 
			from room_sales a, sales_and_services b		
			where a.item_id=b.sas_id and a.category_id in (1, 2, 4) and a.occupancy_id=$this->occupancy and a.status  in ('Printed','Paid')  ";
		$res = mysql_query($sql);
		$msales ="<table width='300' cellpadding=1 cellspacing=1>";
		$oldoc = "";
		while(list($rsid,$oc, $catid, $itemid, $itemdesc,$cost,$stat,$date)=mysql_fetch_row($res)) {
			if($oldoc!=$oc) {
				$date = date('m-d H:i',strtotime($date));
				$msales .= "<tr><td colspan=4 style='border-bottom:1px dotted #cccccc;'><span style='font-style:italic'>$date: Order Slip#: $oc</span></td></tr>";
			}
			$msales .= "<tr><td>&nbsp;</td>";
			$msales.="<td>$itemdesc</td><td style='text-align:right'>".number_format($cost,2)."</td>";
			$msales.="<td>$stat</td>";
			$msales .= "</tr>";
			$mtotal += $cost;
			$oldoc=$oc;
		}
		$msales .= "<tr><td>&nbsp;</td><td>Total:</td><td style='text-align:right;border-top:1px solid #000000;border-bottom:2px solid #000000;'>".number_format($mtotal,2)."</td></tr>";
		$msales .="</table>";
		
		return $msales;
	}
	
	
	public function getRoomCharges()
	{
		//retrieve room sales
		$now = date("Y-m-d H:i:s");
		$sql = " select a.roomsales_id, a.category_id, a.item_id, b.sas_description, a.unit_cost * a.qty,a.update_date
				from room_sales a, sales_and_services b	
				where a.item_id=b.sas_id and a.category_id=3 and a.occupancy_id=$this->occupancy and a.order_code=0 and a.sales_date <= '$now' ";
		$res = mysql_query($sql);
		$rtotal = 0;

		while(list($rsid, $catid, $itemid, $itemdesc,$cost,$update_date)=mysql_fetch_row($res)) {
			if($itemdesc == "Adjustment")
			{
				
			}
			$_sql = "select * from occupancy_log where transaction_type = 'Transfer' and occupancy_id = '$this->occupancy'";
			$_res = mysql_query($_sql);
			$_num = mysql_num_rows($_res);
			if($_num)
			{

			}
			$date = date('m-d H:i',strtotime($update_date));

			$rsales .= "<tr>";
			$rsales.="<td>&nbsp;</td><td width='150'>$date $itemdesc</td><td style='text-align:right'>".number_format($cost,2)."</td><td>&nbsp;</td>";
			$rsales .= "</tr>";
			$rtotal += $cost;
		}
		
		$rtotal_f =number_format($rtotal,2);
		
		$retval = "<table border=0 cellpadding=1 cellspacing=1 style='margin-left:10px;margin-top:10px' width='300' id='soatable'>
		<tr><th colspan=4>Room Charges</th></tr>
		<tr><th colspan=4>Room Rate: {$this->publishrate}</th></tr>
		$rsales
		<tr><td>&nbsp;</td><td  colspan=2>Sub Total:</td><td style='text-align:right'>$rtotal_f</td></tr></table>";
		
		return $retval;
		
		
	}
	
	public function getPayments()
	{
		$ptotal = 0;
		$sql  = "select tendertype, amount,receipt_date from salesreceipts where occupancy_id='$this->occupancy' ";
		$res = mysql_query($sql) or die(mysql_error());
		$payments = "";
		while(list($type, $amount,$date)=mysql_fetch_row($res)) {
		$date = date('m-d H:i',strtotime($date));
		$payments .= "<tr>";
		$payments.="<td>$date&nbsp;</td><td width='150'>$type</td><td style='text-align:right'>".number_format($amount,2)."</td><td>&nbsp;</td>";
		$payments .= "</tr>";
		$ptotal += $amount;
		}
		
		$payments_f = number_format($ptotal,2);
		
		$retval = "<table border=0 cellpadding=1 cellspacing=1 style='margin-left:10px;margin-top:10px' width='300' id='soatable'><tr>
		<th colspan=4>Deposit/Payments</th></tr>
		$payments
		<tr><td>&nbsp;</td><td colspan=2 style='font-weight:bold;font-size:.7em;'>Total of all payments:</td><td  style='text-align:right;font-weight:bold;font-size:.7em;'>$payments_f</td></tr>
		</table>";
		
		return $retval;
	}
	public function getRoomSales()
	{
		//retrieve room sales
		$now = date("Y-m-d H:i:s");
		$sql = " select a.roomsales_id, a.category_id, a.item_id, b.sas_description, a.unit_cost * a.qty, a.sales_date
				from room_sales a, sales_and_services b	
				where a.item_id=b.sas_id and a.category_id=3 and a.occupancy_id=$this->occupancy  and a.order_code=0 and a.sales_date <= '$now' ";
		$res = mysql_query($sql);
		$rtotal = 0;

		while(list($rsid, $catid, $itemid, $itemdesc,$cost,$sales_date)=mysql_fetch_row($res)) {
			$date = date('m-d H:i',strtotime($sales_date));
			$rsales .= "<tr>";
			$rsales.="<td>&nbsp;</td><td width='150'>$date: $itemdesc</td><td style='text-align:right'>".number_format($cost,2)."</td><td>&nbsp;</td>";
			$rsales .= "</tr>";
			$rtotal += $cost;
		}
		
		
		$ptotal = 0;
		$sql  = "select tendertype, amount,receipt_date from salesreceipts where occupancy_id='$this->occupancy'  and reference_id=0";
		$res = mysql_query($sql) or die(mysql_error());
		$payments = "";
		
		while(list($type, $amount,$date)=mysql_fetch_row($res)) {
			$date = date('m-d H:i',strtotime($date));
			$payments .= "<tr>";
			$payments.="<td>&nbsp;</td><td width='150'>$date: $type</td><td style='text-align:right'>".number_format($amount,2)."</td><td>&nbsp;</td>";
			$payments .= "</tr>";
			$ptotal += $amount;
		}
		

		$rtotal_f =number_format($rtotal,2);
		$payments_f = number_format($ptotal,2);
		$totalbalance = number_format($rtotal + $ftotal + $mtotal - $ptotal,2);
		
		$retval = "
			<table border=0 cellpadding=2 cellspacing=2 style='margin-left:10px;margin-top:10px'>
			<tr><th colspan=4>Room Charges</th></tr>
			<tr><th colspan=4>Room Rate: {$this->publishrate}</th></tr>
			$rsales
			<tr><td>&nbsp;</td><td  colspan=2>Sub Total:</td><td style='text-align:right'>$rtotal_f</td></tr>
			<tr><th colspan=4>Deposit/Payments</th></tr>
			$payments
			<tr><td>&nbsp;</td><td colspan=2>Sub Total:</td><td style='text-align:right'>$payments_f</td></tr>
			</table>
			";
			
	
		return $retval;  
	}
	
	public function getCheckoutFnbSales()
	{
		//retrieve fnb sales
		$ftotal = 0;
		$sql = " select a.fnbsales_id, a.category_id,  a.item_id, c.food_category_name, b.fnb_code,b.fnb_name, a.unit_cost, a.qty, (a.unit_cost * a.qty) as totalcost, a.update_date
				from fnb_sales a, fnb b, food_categories c		
				where a.item_id=b.fnb_id and a.category_id=b.food_category_id and a.category_id=c.food_category_id and a.occupancy_id=$this->occupancy and a.status  in ('Paid','Printed') ";
		$res = mysql_query($sql) or die(mysql_error());

		while(list($sid, $catid, $itemid, $catname, $code, $itemdesc, $unitcost, $qty, $ftcost,$date)=mysql_fetch_row($res)) {
			$date = date('m-d H:i',strtotime($date));
			$fsales .= "<tr>";
			$fsales.="<td>&nbsp;</td><td width='150'>$date $itemdesc</td><td style='text-align:right'>".number_format($ftcost,2)."</td><td>&nbsp;</td>";
			$fsales .= "</tr>";
			$ftotal += $ftcost;
		}
		$ftotal_f =number_format($ftotal,2);
		
		
		$retval = "<table border=0 cellpadding=1 cellspacing=1 style='margin-left:10px;margin-top:10px' width='300' id='soatable'><tr>
		<th colspan=4>Food and Beverage Sales</th></tr>
		$fsales
		<tr><td>&nbsp;</td><td  colspan=2>Sub Total:</td><td style='text-align:right'>$ftotal_f</td></tr></table>";
	
		
		return $retval;	
	}
	
	public function getCheckoutFnbBalance()
	{
		//retrieve fnb sales
		$ftotal = 0;
		$sql = " select a.fnbsales_id, a.category_id,  a.item_id, c.food_category_name, b.fnb_code,b.fnb_name, a.unit_cost, a.qty, (a.unit_cost * a.qty) as totalcost
				from fnb_sales a, fnb b, food_categories c		
				where a.item_id=b.fnb_id and a.category_id=b.food_category_id and a.category_id=c.food_category_id and a.occupancy_id=$this->occupancy and a.status  in ('Printed') ";
		$res = mysql_query($sql) or die(mysql_error());

		while(list($sid, $catid, $itemid, $catname, $code, $itemdesc, $unitcost, $qty, $ftcost)=mysql_fetch_row($res)) {
			$fsales .= "<tr>";
			$fsales.="<td width='10px'><input type='checkbox' name='fnb_cb[]' checked=true value='$sid' id='fnb_cb$sid'/>
						</td><td width='65%'><label for='fnb_cb$sid'>$itemdesc</label></td><td style='text-align:right' width='30%'>$ftcost</td>";
			$fsales .= "</tr>";
			$ftotal += $ftcost;
		}
		$ftotal_f =number_format($ftotal,2);
		
		
		$retval = "<table border=0 cellpadding=1 cellspacing=1 style='margin-left:10px;margin-top:10px' width='300' id='soatable'><tr>
		<th colspan=4>Food and Beverage Sales</th></tr>
		$fsales
		<tr><td>&nbsp;</td><td  colspan=2>Sub Total:</td><td style='text-align:right'>$ftotal_f</td></tr></table>";
	
		$retval = "<table style='font-size:11px;width:100%;' border=0>$fsales</table>";
		
		if($ftotal > 0) {
			return $retval;
			//return "<div>Food & Beverage Sales: <span style='float:right'>$ftotal_f</span></div>";	

		}
	}

	public function getCheckoutMosBalance()
	{
		//retrieve fnb sales
		$mtotal = 0;
		$sql = " select a.roomsales_id, a.category_id, a.item_id, b.sas_description, a.unit_cost * a.qty
			from room_sales a, sales_and_services b		
			where a.item_id=b.sas_id and a.category_id in (1, 2,4) and a.occupancy_id=$this->occupancy and a.status  in ('Printed') ";
		$res = mysql_query($sql) or die(mysql_error());

		while(list($rsid, $catid, $itemid, $itemdesc,$cost)=mysql_fetch_row($res)) {
			$msales.= "<tr>";
			//$msales.="<td>&nbsp;</td><td width='150'>$itemdesc</td><td style='text-align:right'>".number_format($cost,2)."</td><td>&nbsp;</td>";
			$msales.="<td width='10px'><input type='checkbox' name='misc_cb[]' checked=true value='$rsid' id='misc_cb$rsid'/>
						</td><td width='65%'><label for='misc_cb$rsid'>$itemdesc</label></td><td style='text-align:right' width='30%'>$cost</td>";
			
			$msales.= "</tr>";
			$mtotal += $cost;
			
		}
		$mtotal_f =number_format($mtotal,2);
		
		
		$retval = "<table border=0 cellpadding=1 cellspacing=1 style='margin-left:10px;margin-top:10px' width='300' id='soatable'><tr>
		<th colspan=4>Miscellaneous Sales</th></tr>$msales
		<tr><td>&nbsp;</td><td  colspan=2>Sub Total:</td><td style='text-align:right'>$mtotal_f</td></tr></table>";
		$retval = "<table style='font-size:11px;width:100%;' border=0>$msales</table>";
		
		if($mtotal > 0) {
			return $retval;
			//return "<div>Miscellaneous Sales: <span style='float:right'>$mtotal_f</span></div>";
		}
	}

	public function getCheckoutRoomBalance()
	{
		//retrieve fnb sales
		$now = date("Y-m-d H:i:s");
		$mtotal = 0;
		$sql = " select a.roomsales_id, a.category_id, a.item_id, b.sas_description, a.unit_cost,a.sales_date
			from room_sales a, sales_and_services b		
			where a.item_id=b.sas_id and a.category_id in (3) and a.occupancy_id=$this->occupancy and a.status  in ('Draft')  ";
		$res = mysql_query($sql) or die(mysql_error());

		while(list($rsid, $catid, $itemid, $itemdesc,$cost,$date)=mysql_fetch_row($res)) {
			$date = date('m-d H:i',strtotime($date));
			$msales.= "<tr>";
			//$msales.="<td>&nbsp;</td><td width='150'>$itemdesc</td><td style='text-align:right'>".number_format($cost,2)."</td><td>&nbsp;</td>";
			$msales.="<td width='10px'><input type='checkbox' name='room_cb[]' value='$rsid' checked=true id='room_cb$rsid'/>
						</td><td width='65%'><label for='room_cb$rsid'>$itemdesc - Adv. ($date)</label></td><td style='text-align:right' width='30%'>$cost</td>";
			
			$msales.= "</tr>";
			$mtotal += $cost;
			
		}
		$mtotal_f =number_format($mtotal,2);
		
		
		$retval = "<table border=0 cellpadding=1 cellspacing=1 style='margin-left:10px;margin-top:10px' width='300' id='soatable'><tr>
		<th colspan=4>Room Sales</th></tr>
		<tr><th colspan=4>Room Rate: {$this->publishrate}</th></tr>
		$msales
		<tr><td>&nbsp;</td><td  colspan=2>Sub Total:</td><td style='text-align:right'>$mtotal_f</td></tr></table>";
		$retval = "<table style='font-size:11px;width:100%;' border=0>$msales</table>";
		
		if($mtotal > 0) return $retval; 
			//return "<div>Room Sales: <span style='float:right'>$mtotal_f</span></div>";	

		
		
	}

	public function getCheckoutMosSales()
	{
		//retrieve fnb sales
		$mtotal = 0;
		$sql = " select a.roomsales_id, a.category_id, a.item_id, b.sas_description, a.unit_cost * a.qty, a.update_date
			from room_sales a, sales_and_services b		
			where a.item_id=b.sas_id and a.category_id in (1, 2,4) and a.occupancy_id=$this->occupancy and a.status  in ('Paid','Printed') ";
		$res = mysql_query($sql) or die(mysql_error());

		while(list($rsid, $catid, $itemid, $itemdesc,$cost,$date)=mysql_fetch_row($res)) {
			$date = date('m-d H:i',strtotime($date));
			$msales.= "<tr>";
			$msales.="<td>&nbsp;</td><td width='150'>$date $itemdesc</td><td style='text-align:right'>".number_format($cost,2)."</td><td>&nbsp;</td>";
			$msales.= "</tr>";
			$mtotal += $cost;
			
		}
		$mtotal_f =number_format($mtotal,2);
		
		
		$retval = "<table border=0 cellpadding=1 cellspacing=1 style='margin-left:10px;margin-top:10px' width='300' id='soatable'><tr>
		<th colspan=4>Miscellaneous Sales</th></tr>$msales
		<tr><td>&nbsp;</td><td  colspan=2>Sub Total:</td><td style='text-align:right'>$mtotal_f</td></tr></table>";
		return $retval;	
		
	}
	
	public function displayTotalBalance()
	{
		$retval = "<table border=0 cellpadding=1 cellspacing=1 style='margin-left:10px;margin-top:10px' width='300' id='soatable'>
		<tr><td colspan=3 style='font-weight:bold;font-size:.7em;color:#ff0000;'>Total Balance:</td><td style='text-align:right;font-weight:bold;font-size:.7em;color:#ff0000;'>$this->totalbalance_display</td></tr>
		</table>";
		
		return $retval; 
	}
	
	public function displayTotal()
	{
		$retval = "<table border=0 cellpadding=1 cellspacing=1 style='margin-left:10px;margin-top:10px' width='300' id='soatable'>
		<tr><td colspan=3 style='font-weight:bold;font-size:.7em;color:black;'>Total:</td><td style='text-align:right;font-weight:bold;font-size:.7em;color:black;'>$this->totaldisplay</td></tr>
		</table>";
		
		return $retval; 
	}
	public function getCheckoutSoa()
	{
		
	}

    public function getPublishRate()
	{
	    $sql = "select a.publishrate, a.amount 
	        from room_type_rates a, occupancy b, rooms c
	        where c.room_type_id = a.room_type_id
	        and b.room_id = c.room_id
	        and a.rate_id = b.rate_id
	        and b.occupancy_id = '{$this->occupancy}'
	        ";
	    $res = mysql_query($sql);
	    list($publishrate, $amount) = mysql_fetch_row($res);
	    return ($publishrate > 0) ? $publishrate : $amount;
	}

	/**
		taken from remarks table, $class is remarks_classification field
		$class: 1 = discounts, 2 = transfers , 3= adjustment, 4=checkout
	**/
	public function getdropdownremarks($class)
	{
		$sql = " select remark_text 
				from remarks 
				where remark_classification='$class'
				order by remark_text ";
		$res = mysql_query($sql);
		$retval="";
		if(mysql_num_rows($res)) {
			$retval="<select name='new_remark' id='new_remark_$class' class='adjustelement'>";
			$retval.="<option></option>";
			while(list($rem)=mysql_fetch_row($res)) {
				$retval.="<option value='$rem'>$rem</option>";
			}
			$retval.="</select>";
		}
		return $retval;
	}

	public function printreportfile($file) {
		shell_exec('c:\pstools\psexec c:\xampp\htdocs\fds\ajax\p.bat ' . $file);
	}

	public function printablesoa() 
	{
		$this->getTotalBalance();
		$retval=$this->printableheader("Statement of Account");	
		$retval.="\nRoom Sales: \t\t\t " . number_format($this->rtotal,2);
		$retval.="\nFood Sales: \t\t\t " .number_format($this->ftotal,2);
		$retval.="\nMiscellaneous Sales: \t\t\t " .number_format($this->mtotal,2);
		$paytotal = number_format($this->total,2);
		$retval.="
Please pay this Amount:\t\t $paytotal

	Thank you and come again.
	This is not a receipt.

Please ask your original receipt.

";
		return $retval;
	}

	public function printablefinalsoa() 
	{
		$this->getTotalBalance();
		$retval=$this->printableheader("Statement of Account");	
		$retval.="\nRoom Sales: \t\t\t " . number_format($this->rtotal,2);
		$retval.="\nFood Sales: \t\t\t " .number_format($this->ftotal,2);
		$retval.="\nMiscellaneous Sales: \t\t\t " .number_format($this->mtotal,2);
		$paytotal = number_format($this->total,2);
		$retval.="
Total:\t\t\t\t $paytotal

	Thank you and come again.
	This is not a receipt.

Please ask your original receipt.

";
		return $retval;
	}

	public function printableCheckoutsoa() 
	{
		$this->getTotalBalance();
		$retval=$this->printablecheckoutheader("Statement of Account");	
		$retval.="\nRoom Sales: \t\t\t " . number_format($this->rtotal,2);
		$retval.="\nFood Sales: \t\t\t " .number_format($this->ftotal,2);
		$retval.="\nMiscellaneous Sales: \t\t\t " .number_format($this->mtotal,2);
		$paytotal = number_format($this->total,2);
		$retval.="
Total:\t\t\t\t $paytotal

	Thank you and come again.
	This is not a receipt.

Please ask your original receipt.

";
		return $retval;
	}

	public function printableros($hdr=1) {
		$mtotal = 0;
		$sql = " select a.roomsales_id, a.category_id, a.item_id, b.sas_description, a.unit_cost * a.qty
			from room_sales a, sales_and_services b		
			where a.item_id=b.sas_id and a.category_id in (3) and a.occupancy_id=$this->occupancy and a.status  in ('Paid','Draft') ";
		$res = mysql_query($sql) or die(mysql_error());
		$now = date("Y-m-d H:i:s");
		if($hdr==1) $msales = $this->printableheader("Room Sales");
		
		while(list($rsid, $catid, $itemid, $itemdesc,$cost)=mysql_fetch_row($res)) {
			$msales.="$itemdesc:\t\t\t ".number_format($cost,2)."\n";
			$mtotal += $cost;
		}
		$mtotal_f =number_format($mtotal,2);
		$msales .= "Total:\t\t\t\t $mtotal_f \n";
		$retval = $msales;
		return $retval;	
	}

	public function printablefos($hdr=1,$ordercode="") 
	{
		//print miscellaneous sales
		if($hdr==1) $msales = $this->printableheader("Food Order Stub");
		$mtotal = 0;
		$ftotal = 0;

		$sql = " select a.fnbsales_id, a.category_id,  a.item_id, c.food_category_name, b.fnb_code,b.fnb_name, a.unit_cost, a.qty, (a.unit_cost * a.qty) as totalcost
				from fnb_sales a, fnb b, food_categories c		
				where a.item_id=b.fnb_id and a.category_id=b.food_category_id and a.category_id=c.food_category_id and a.occupancy_id=$this->occupancy and a.status  in ('Paid','Printed') ";
		if(trim($ordercode)!="") 
		{
			$sql .= " and a.order_code='$ordercode' ";

			$msales .="Order Slip# : $ordercode \n";
		}
		$res = mysql_query($sql) or die(mysql_error());
		
		$now = date("Y-m-d H:i:s");
		
		while(list($sid, $catid, $itemid, $catname, $code, $itemdesc, $unitcost, $qty, $ftcost)=mysql_fetch_row($res)) {
			$msales.="$itemdesc:\t $qty  $unitcost  ".number_format($ftcost,2)."\n";
			$mtotal += $ftcost;
		}
		$mtotal_f =number_format($mtotal,2);
		$msales .= "Total:\t\t\t\t $mtotal_f \n\n";
		$retval = $msales;
		return $retval;	
	}
	
	public function printablefosreplenish($hdr=1,$ordercode="") 
	{
		//print miscellaneous sales
		if($hdr==1) $msales = $this->printableheader("Food Order Stub - Mini Ref Replenish");
		$mtotal = 0;
		$ftotal = 0;

		$sql = " select a.fnbsales_id, a.category_id,  a.item_id, c.food_category_name, b.fnb_code,b.fnb_name, a.unit_cost, a.qty, (a.unit_cost * a.qty) as totalcost
				from fnb_sales a, fnb b, food_categories c		
				where a.item_id=b.fnb_id and a.category_id=b.food_category_id and a.category_id=c.food_category_id and a.occupancy_id=$this->occupancy and a.status  in ('Paid','Printed') ";
		if(trim($ordercode)!="") 
		{
			$sql .= " and a.order_code='$ordercode' ";

			$msales .="Order Slip# : $ordercode \n";
		}
		$res = mysql_query($sql) or die(mysql_error());
		
		$now = date("Y-m-d H:i:s");
		
		while(list($sid, $catid, $itemid, $catname, $code, $itemdesc, $unitcost, $qty, $ftcost)=mysql_fetch_row($res)) {
			$msales.="$itemdesc:\t $qty  $unitcost  ".number_format($ftcost,2)."\n";
			$mtotal += $ftcost;
		}
		$mtotal_f =number_format($mtotal,2);
		$msales .= "Total:\t\t\t\t $mtotal_f \n\n";
		$retval = $msales;
		return $retval;	
	}

	public function printablefoscancel($items="") 
	{
		//print Food sales cancellation
		if($items!='') {
			$msales = $this->printableheader("Food Order - Cancellation");
			$mtotal = 0;
			$sql = " select a.fnbsales_id, a.category_id,  a.item_id, c.food_category_name, b.fnb_code,b.fnb_name, a.unit_cost, a.qty, (a.unit_cost * a.qty) as totalcost, a.order_code
				from fnb_sales a, fnb b, food_categories c		
				where a.item_id=b.fnb_id and a.category_id=b.food_category_id and a.category_id=c.food_category_id and a.occupancy_id=$this->occupancy and a.fnbsales_id in ($items) and a.status  in ('Cancelled') ";
		
			$res = mysql_query($sql) or die(mysql_error());
			$now = date("Y-m-d H:i:s");
			$order_code = '';
			while(list($sid, $catid, $itemid, $catname, $code, $itemdesc, $unitcost, $qty, $ftcost,$ordercode)=mysql_fetch_row($res)) {
				$itemlist.="$itemdesc:\t $qty  $unitcost  ".number_format($ftcost,2)."\n";
				$mtotal += $ftcost;
				$order_code=$ordercode;
			}
			
			$msales .="Order Slip#: $order_code \n";
			$msales .= $itemlist;
			
			$mtotal_f =number_format($mtotal,2);
			$msales .= "Total:\t\t\t\t $mtotal_f \n\n";
			$retval = $msales;
			return $retval;	
		}
	}
	
	public function printablemoscancel($items="") 
	{
		//print miscellaneous sales
		if($items!='') {
			$msales = $this->printableheader("Miscellaneous Order - Cancellation");
			$mtotal = 0;
			$sql = " select a.roomsales_id, a.category_id, a.item_id, b.sas_description, a.unit_cost * a.qty, a.order_code
				from room_sales a, sales_and_services b		
				where a.item_id=b.sas_id and a.category_id in (1, 2,4, 7) and a.occupancy_id=$this->occupancy and a.roomsales_id in ($items) and a.status  in ('Cancelled') ";

			$res = mysql_query($sql) or die(mysql_error());
			$now = date("Y-m-d H:i:s");
			$order_code='';
			while(list($rsid, $catid, $itemid, $itemdesc,$cost,$ordercode)=mysql_fetch_row($res)) {
				$itemlist.="$itemdesc:\t\t\t ".number_format($cost,2)."\n";
				$mtotal += $cost;
				$order_code=$ordercode;
			}
			
			$msales .="Order Slip#: $order_code \n";
			$msales .= $itemlist;
			
			$mtotal_f =number_format($mtotal,2);
			$msales .= "Total:\t\t\t\t $mtotal_f \n\n";
			$retval = $msales;
			return $retval;	
		}
	}
	public function printablemos($hdr=1,$ordercode="") 
	{
		//print miscellaneous sales
		if($hdr==1) $msales = $this->printableheader("Miscellaneous Order Stub");
		$mtotal = 0;
		$sql = " select a.roomsales_id, a.category_id, a.item_id, b.sas_description, a.unit_cost * a.qty
			from room_sales a, sales_and_services b		
			where a.item_id=b.sas_id and a.category_id in (1, 2,4, 7) and a.occupancy_id=$this->occupancy and a.status  in ('Paid','Printed') ";
		if(trim($ordercode)!="") 
		{
			$sql .= " and a.order_code='$ordercode' ";
			$msales .="Order Slip#: $ordercode \n";
		}
		$res = mysql_query($sql) or die(mysql_error());
		$now = date("Y-m-d H:i:s");
		
		while(list($rsid, $catid, $itemid, $itemdesc,$cost)=mysql_fetch_row($res)) {
			$msales.="$itemdesc:\t\t\t ".number_format($cost,2)."\n";
			$mtotal += $cost;
		}
		$mtotal_f =number_format($mtotal,2);
		$msales .= "Total:\t\t\t\t $mtotal_f \n\n";
		$retval = $msales;
		return $retval;	
	}
	
	public function printableheader($title) {
		$now=date("Y-m-d H:i:s");
		$hdr  = "
	$title 

$this->hotel
Room No. {$this->doorname}
Date: $now
CheckIn: {$this->checkindate}
CheckOut: {$this->etd}
Shift: {$this->shift}

";
		return $hdr;
	}

	public function printablecheckoutheader($title) {
		$now=date("Y-m-d H:i:s");
		$hdr  = "
	$title 

$this->hotel
Room No. {$this->doorname}
CheckIn: {$this->checkindate}
CheckOut: {$now}
Shift: {$this->shift}

";
		return $hdr;
	}

}

?>
