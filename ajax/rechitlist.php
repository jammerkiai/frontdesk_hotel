<?php
//rechitlist.php
session_start();
include_once("config/config.inc.php");
$now = date("Y-m-d H:i:s");

function getShiftStartTime()
{
	$sql = "select datetime from `shift-transactions` where shift = 'start' order by datetime desc limit 0,1";
	$res = mysql_query($sql);
	list($time) = mysql_fetch_row($res);
	return $time;
}

function getSpecialFloorID()
	{
		$sql = " select settings_value from settings where settings_name = 'SPECIALFLOORID' ";
		$res = mysql_query($sql) or die($sql);

		if(mysql_num_rows($res)){
			$row = mysql_fetch_row($res);
			return $row[0];
		}
	}

 function getSpecialRoomIdList()
	{
		//echo "getSpecialRoomIdList<br>";
		$fid = getSpecialFloorId();
		$sql = " select room_id from rooms where floor_id=$fid";

		$res = mysql_query($sql) or die($sql);
		while(list($id)=mysql_fetch_row($res)) {
			$arrSpecialRooms[]=$id;
		}
		return implode(",",$arrSpecialRooms);
	}

function getPreviousShiftEndDate( $occupancy_id, $sales_date)
{
	$sql = " select sales_date from room_sales
			where item_id=15 and occupancy_id='$occupancy_id' and sales_date < '$sales_date'
			order by roomsales_id desc limit 0,1
	";
	$res = mysql_query($sql);
	list($sales_date2) = mysql_fetch_row($res);
	$sql = "select datetime from `shift-transactions`
		where shift = 'start' and datetime <='$sales_date2'
		order by datetime desc limit 0,1
			";
	$res = mysql_query($sql);
	if(mysql_num_rows($res)) {
		$row = mysql_fetch_row($res);
		return $row[0];
	}else{
		return 0;
	}
}

function getRechitList() {
	$exceptList = getSpecialRoomIdList();
	$start = getShiftStartTime();
	$end = date('Y-m-d H:i:s', strtotime($start . ' +8 hours'));
	$cutoff = date('Y-m-d H:i:s', strtotime($start . ' -12 hours'));
	$h = date("H");
	if($h<=6||$h<=6||$h>=22)
	{
		$shiftnum=1;
	}
	elseif($h>=13||$h>=14)
	{
		$shiftnum=3;
	}else{
		$shiftnum=2;
	}
	$sql = "
				select distinct e.sales_date, e.roomsales_id,b.door_name, a.occupancy_id,a.actual_checkin,a.actual_checkout,
				b.site_id,b.door_name,b.room_id,c.room_type_id, c.room_type_name,c.rank
				from occupancy a, rooms b, room_types c, rates d , room_sales e
				where
				(

				(e.sales_date >= '$start' and e.sales_date <= '$end' and a.actual_checkin <> e.sales_date and e.item_id=15)
				)
				and a.occupancy_id=e.occupancy_id
				and a.room_id = b.room_id
				and b.room_type_id=c.room_type_id
				and a.rate_id=d.rate_id
				and b.site_id=2
				and e.item_id in (15)
				and a.room_id not in ($exceptList)
				";





	$res = mysql_query($sql) or die(mysql_error());
	$ret = "<fieldset class='rechit'><legend>Rechit List</legend>
	<ul class='rechitlist'>";
	while(list($date, $roomsales_id, $door,$occupancy,)=mysql_fetch_row($res)) {
		$transaction_start=getPreviousShiftEndDate($occupancy,$date);
		$ret.="<li>$door - $date<ul>";
		$ret.=getFnbRechitSales($occupancy,$transaction_start);
		$ret.=getRechitMosSales($occupancy,$transaction_start);
		$ret.="</ul></li>";
	}
	$ret .= "</ul></fieldset>";
	return $ret;
}

function getRechitMosSales($occupancy,$start)
	{
		//retrieve fnb sales
		$mtotal = 0;
		$sql = " select a.roomsales_id, a.category_id, a.item_id, b.sas_description, a.unit_cost * a.qty, a.update_date,a.order_code
			from room_sales a, sales_and_services b
			where a.item_id=b.sas_id and
			a.category_id in (1, 2,4)
			and a.occupancy_id=$occupancy
			and a.update_date >='$start'
			and a.status  in ('Paid') ";
		$res = mysql_query($sql) or die(mysql_error());
		$ocode = '';
		while(list($rsid, $catid, $itemid, $itemdesc,$cos,$date,$order_code)=mysql_fetch_row($res)) {
			$date = date('m-d H:i',strtotime($date));
			if($ocode != $order_code) {
				$msales .= "<tr><td colspan=2>Order Code# $order_code</td><td colspan='3' align='right'>$date</td></tr>";
				$ocode=$order_code;
			}
			$msales.= "<tr>";
			$msales.="<td>&nbsp;</td><td width='150'>$itemdesc</td><td style='text-align:right'>".number_format($cost,2)."</td><td>&nbsp;</td>";
			$msales.= "</tr>";
			$mtotal += $cost;

		}
		$mtotal_f =number_format($mtotal,2);


		$retval = "<table border=0 cellpadding=1 cellspacing=1 class='rechittable'><tr>
		<th colspan=4>Miscellaneous Sales</th></tr>$msales
		<tr><td>&nbsp;</td><td  colspan=2>Sub Total:</td><td  colspan=2 style='border-top:1px solid #000;'>&nbsp;</td><td style='border-top:1px solid #000;text-align:right'>$mtotal_f</td></tr></table>";
		if($mtotal_f > 0) {
			return $retval;
		}
	}

function getFnbRechitSales($occupancy,$start)
	{
//retrieve fnb sales

		$ftotal = 0;
		$sql = " select a.fnbsales_id, a.category_id,  a.item_id, c.food_category_name,
				b.fnb_code,b.fnb_name, a.unit_cost, a.qty,
				(a.unit_cost * a.qty) as totalcost, a.update_date, a.order_code
				from fnb_sales a, fnb b, food_categories c
				where a.item_id=b.fnb_id and a.category_id=b.food_category_id
				and a.category_id=c.food_category_id
				and a.update_date >='$start'
				and a.occupancy_id=$occupancy and a.status  in ('Paid') ";
		$res = mysql_query($sql) or die(mysql_error());
		$ocode='';
		while(list($sid, $catid, $itemid, $catname, $code, $itemdesc, $unitcost, $qty, $ftcost,$date,$order_code)=mysql_fetch_row($res)) {
			$date = date('m-d H:i',strtotime($date));
			if($ocode != $order_code) {
				$fsales .= "<tr><td colspan=2>Order Code# $order_code</td><td colspan='3' align='right'>$date</td></tr>";
				$ocode=$order_code;
			}
			$fsales .= "<tr>";
			$fsales.="<td valign='top'>&nbsp;</td><td>$itemdesc</td><td style='text-align:right'>".number_format($ftcost,2)."</td><td>&nbsp;</td>";
			$fsales .= "</tr>";
			$ftotal += $ftcost;
		}
		$ftotal_f =number_format($ftotal,2);


		$retval = "<table border=0 cellpadding=1 cellspacing=1 class='rechittable'><tr>
		<th colspan=4 class='header'>Food and Beverage Sales</th></tr>
		$fsales
		<tr><td>&nbsp;</td><td>Sub Total:</td><td  colspan=2 style='border-top:1px solid #000;'>&nbsp;</td><td style='border-top:1px solid #000;text-align:right'>$ftotal_f</td></tr></table>";

		if($ftotal > 0) {
			return $retval;
		}
	}

?>
<html>
<script type="text/JavaScript">
function timedRefresh(timeoutPeriod) {
	setTimeout("location.reload(true);",timeoutPeriod);
}
</script>

<form name='myform' id='myform' method='post'>
<div id="roomlisting">
<? echo getRechitList() ?>
</div>
</form>

<style>
body{font-family:lucida,arial,helvetica;font-size:.7em;}
.rechit legend {color:#cc3322;font-weight:bold;}
.rechitlist {list-style:none;margin-left:-30px;margin-top:-2px;}
.rechitlist li{font-weight:bold;border:1px solid #ffcc99;padding:2px;margin:1px;}
.rechittable {width:400px}
.rechittable th,td {font-size:11px;}
.rechittable th.header {text-align:left}
table.menu a {
			width:125px;
			border:1px solid #333333;
			display: block;}

div.menu	a{
					color: #333333;
					background: #ffffff;
					text-decoration:none;
					font-size:11px;
					line-height:16px;
					padding: 2px 5px;
					font-family: Tahoma, verdana, sans-serif;}
div.menu (position:absolute;top:0; left:0;) /*fixes IE slowness?? */

div.menu	a:link {color: #333333; text-decoration: none; background: #cccc99;}
div.menu	a:active {color: #000000; text-decoration: none; background: #cccc99;}
div.menu	a:visited {color: #333333; text-decoration: none; background: #cccc99;}
div.menu	a:hover {color: #eeeeee; text-decoration: none; background: #333333;border:1px solid #000000;}


</style>
<script type="text/javascript" src="../js/jquery.js"></script>
<script lang="javascript">
$(document).ready(function(){
	$(".msgbox").hide();
	$(".btnAlert").mouseover(function(){
		$( $(this).attr("alt") ).show();
	}).mouseout(function(){
		$( $(this).attr("alt") ).hide();
	});
});
</script>
</body>
</html>
