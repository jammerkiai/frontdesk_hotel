<?php
include_once('config/config.inc.php');
require_once('class.forecast.php');
require_once('date.functions.php');
session_start();


$month = isset($_POST['mo'])? $_POST['mo'] : date('m');
$year = isset($_POST['yr'])? $_POST['yr'] : date('Y');

$fc = new forecast();

$sql1 = $fc->getsummaryquery($year, $month, 1);
$sql2 = $fc->getsummaryquery($year, $month, 2);

mysql_query("truncate table temp_fc_summary");
$sql = "insert into temp_fc_summary $sql1 ";
mysql_query($sql) or die(mysql_error());
$sql = "insert into temp_fc_summary $sql2 ";
mysql_query($sql) or die(mysql_error());
    
$sql = "select * from temp_fc_summary order by receipt_date, site_id, receipt_number";
$res = mysql_query($sql) or die(mysql_error());

$data = '';
$date = '';
$site = '';
$currentdate = '';
$currentsite = '';
$grand = $site = $day = array();
$firstrun = true;
while($row2 = mysql_fetch_object($res)) { 
	$total = 0;
	$total = $row2->room_sales + $row2->food_sales + $row2->misc_sales;
	$roomsales = $row2->room_sales / 1.12;
	$foodsales = $row2->misc_sales / 1.12;
	$miscsales = $row2->food_sales / 1.12;
	$subsales = $roomsales + $foodsales + $miscsales;
	$tax = $total / 1.12 * 0.12;
	$net = $total - $tax;
	
	if ($currentsite !== $row2->site_id) {
		//display site totals
		if ($firstrun === false) {
			$data .= '<tr>';
			$data .= '<th colspan=5>Sub-total</th>';
			$data .= "<td>" . number_format($site['room'], 2) . "</td>";
			$data .= "<td>" . number_format($site['food'], 2) . "</td>";
			$data .= "<td>" . number_format($site['misc'], 2) . "</td>";
			$data .= "<td>" . number_format($site['net'], 2) . "</td>";
			$data .= "<td>" . number_format($site['tax'], 2) . "</td>";
			$data .= "<td>" . number_format($site['total'], 2) . "</td>";
			$data .= '</tr>';
			unset($site);
			$site = array();
		}
	}
	
	if ($currentdate !== $row2->receipt_date) {
		//display day totals
		if ($firstrun === false) {
			$data .= '<tr>';
			$data .= '<th colspan=5>TOTAL</th>';
			$data .= "<td>" . number_format($day['room'], 2) . "</td>";
			$data .= "<td>" . number_format($day['food'], 2) . "</td>";
			$data .= "<td>" . number_format($day['misc'], 2) . "</td>";
			$data .= "<td>" . number_format($day['net'], 2) . "</td>";
			$data .= "<td>" . number_format($day['tax'], 2) . "</td>";
			$data .= "<td>" . number_format($day['total'], 2) . "</td>";
			$data .= '</tr>';
			unset($day);
			$day = array();
		}
	}
	
	
	$data .= '<tr>';
	$data .= "<th>" . (($currentdate === $row2->receipt_date) ? '' : $row2->receipt_date)  . "</th>";
	$data .= "<th>" . (($currentsite === $row2->site_id) ? '' : $row2->site_id) . "</th>";
	$data .= "<td>" . $row2->door_name . "</td>";
	$data .= "<td>" . $row2->guest_name . "</td>";
	$data .= "<td>" . $row2->receipt_number . "</td>";
	$data .= "<td>" . number_format($roomsales, 2) . "</td>";
	$data .= "<td>" . number_format($foodsales, 2) . "</td>";
	$data .= "<td>" . number_format($miscsales, 2) . "</td>";
	$data .= "<td>" . number_format($subsales, 2) . "</td>";
	$data .= "<td>" . number_format($tax, 2) . "</td>";
	$data .= "<td>" . number_format($total, 2) . "</td>";
	$data .= '</tr>';
	
	//totals
	$grand['room'] += $roomsales;
	$grand['food'] += $foodsales;
	$grand['misc'] += $miscsales;		
	$grand['net'] += $net;
	$grand['tax'] += $tax;
	$grand['total'] += $total;
	
	//site total
	$site['room'] += $roomsales;
	$site['food'] += $foodsales;
	$site['misc'] += $miscsales;		
	$site['net'] += $net;
	$site['tax'] += $tax;
	$site['total'] += $total;
	
	//day total
	$day['room'] += $roomsales;
	$day['food'] += $foodsales;
	$day['misc'] += $miscsales;		
	$day['net'] += $net;
	$day['tax'] += $tax;
	$day['total'] += $total;
	
	
	
	$currentdate = $row2->receipt_date;
	$currentsite = $row2->site_id;
	$firstrun = false;
}

$data .= '<tr>';
$data .= '<th colspan=5>Sub-total</th>';
$data .= "<td>" . number_format($site['room'], 2) . "</td>";
$data .= "<td>" . number_format($site['food'], 2) . "</td>";
$data .= "<td>" . number_format($site['misc'], 2) . "</td>";
$data .= "<td>" . number_format($site['net'], 2) . "</td>";
$data .= "<td>" . number_format($site['tax'], 2) . "</td>";
$data .= "<td>" . number_format($site['total'], 2) . "</td>";
$data .= '</tr>';
unset($site);
$site = array();
$data .= '<tr>';
$data .= '<th colspan=5>TOTAL</th>';
$data .= "<td>" . number_format($day['room'], 2) . "</td>";
$data .= "<td>" . number_format($day['food'], 2) . "</td>";
$data .= "<td>" . number_format($day['misc'], 2) . "</td>";
$data .= "<td>" . number_format($day['net'], 2) . "</td>";
$data .= "<td>" . number_format($day['tax'], 2) . "</td>";
$data .= "<td>" . number_format($day['total'], 2) . "</td>";
$data .= '</tr>';
unset($day);
$day = array();			
			
//grand total
$data .= '<tr>';
$data .= '<th colspan=5>GRAND TOTALS:</th>';
$data .= "<td>" . number_format($grand['room'], 2) . "</td>";
$data .= "<td>" . number_format($grand['food'], 2) . "</td>";
$data .= "<td>" . number_format($grand['misc'], 2) . "</td>";
$data .= "<td>" . number_format($grand['net'], 2) . "</td>";
$data .= "<td>" . number_format($grand['tax'], 2) . "</td>";
$data .= "<td>" . number_format($grand['total'], 2) . "</td>";
$data .= '</tr>';

$headers = array(
	'Day',
	'Shogun',
	'Room #',
	'Guest Name',
	'Receipt No.',
	'Room Sales',
	'Food Sales',
	'Misc Sales',
	'Sub-total',
	'Tax',
	'Total',
);

$titles = '<tr>';
foreach ($headers as $header) {
	$titles.= "<th>$header</th>";
}
$titles .= '</tr>';

if ($_POST['submit'] === 'print to excel') {
	$filename = "orsummary_$month_$year.xls";
	$output = "<table> $titles $data</table>";
	header('Content-type: application/ms-excel');
	header('Content-Disposition: attachment; filename='.$filename);
	echo $output;
	exit;
}

?>

<html>
<head>
<title></title>
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="../css/start/jquery-ui.css" />
<style>
body {font-family:helvetica;margin:0px;}
table {font-size:12px;font-family:helvetica;border-collapse: collapse;empty-cells:show;margin: 0 4px;}
th {width:auto; border:1px solid #cccccc;padding: 2px 6px}
.totals {color:#00F;font-weight:bold;}
td {border-bottom:1px solid #cccccc; text-align:center; padding: 2px; }
.amt {text-align:right}
h3 {font-size:12px;}
.menubar {background-color:#eeeeee;font-size:11px;padding:4px;border-bottom:1px solid #cccccc;}
.content {padding:10px; font-size:11px}
.message {display:inline; color:#ff0000;}
.targets {color: orange;}
.actuals {color: green;}
.menutitle {font-weight: bold;}
.origfc {cursor: pointer; background-color: #eee;}
</style>
</head>
<body>
<form method="post">
<div class="menubar">
<span class="menutitle">Shogun Hotel OR Summary </span>
<?php echo getMonthDropdown($month); ?>
<input type="text" name="yr" value="<?php echo $year ?>" id="yr" size="4" maxlength="4">
<input type="submit" name="submit" value="GO" />
<input type="submit" name="submit" value="print to excel" />
<div class="message"><?php echo $result ?></div>
</div>
<table>
<thead>
<tr>
<th>Day</th>
<th>Shogun</th>
<th>Room #</th>
<th>Guest Name</th>
<th>Receipt No.</th>
<th>Room Sales</th>
<th>Food Sales</th>
<th>Misc Sales</th>
<th>Sub-total</th>
<th>Tax</th>
<th>Total</th>
</tr>
</thead>
<tbody>
<?php 
echo $data;
?>
</tbody>
</table>
</div>
</form>
<script>

$(document).ready(function(){
    $('#fcdate').datepicker({dateFormat:'yy-mm-dd'});
    $('#postaction').click(function(e) {
        return confirm('Are you sure you want to post this total?');
    });
    
    $('.origfc').click(function(e) {
        var shift = $(this).attr('data-shift'),
            seldate = $(this).attr('data-date'),
            currval = $(this).html(),
            newval = '';
        newval = prompt('Modify target forecast for ' + seldate + ' Shift ' + shift + '?', currval);
        if (newval !== currval && newval !== null) {
            if (confirm('Are you sure?')) {
                $.post(
                    'fc_monthly.php', 
                    {
                        ajax: 1,
                        seldate: seldate,
                        shift: shift,
                        newval: newval
                    }   
                );
                $(this).html(newval);
                $(this).next('.adjfc').html(newval);
            } else {
                $(this).html(currval);
            }
        }
    });
});
</script>
</body>
</html>
