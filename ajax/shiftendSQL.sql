select rs.*, sr.tendertype
from room_sales rs left join salesreceipts sr 
on rs.occupancy_id = sr.occupancy_id and rs.update_date = sr.receipt_date
where sr.receipt_date <= '2012-09-29 16:00:52' and sr.receipt_date >= '2012-09-29 08:09:52'
and rs.sales_date <= '2012-09-29 16:00:52' and rs.sales_date >= '2012-09-29 08:09:52'
and rs.status = 'Paid'


(
SELECT 'RoomSales', d.update_date, d.occupancy_id, sum( unit_cost * qty ), sr.tendertype
FROM room_sales d, temp_occupancy_sales a, salesreceipts sr
WHERE d.occupancy_id = a.occupancy_id
AND d.status = 'Paid'
AND category_id =3
AND sr.receipt_date = d.update_date
AND sr.occupancy_id = d.occupancy_id
AND d.update_date >= '2012-09-29 08:09:52'
AND d.update_date <= '2012-09-29 16:00:52'
GROUP BY d.update_date, d.occupancy_id
)



UNION (

SELECT 'MiscSales', e.update_date, sum( unit_cost * qty )
FROM room_sales e, temp_occupancy_sales z
WHERE e.occupancy_id = z.occupancy_id
AND e.status = 'Paid'
AND category_id <>3
AND e.update_date >= '2012-09-29 08:09:52'
AND e.update_date <= '2012-09-29 16:00:52'
GROUP BY e.update_date, e.occupancy_id
)
UNION (

SELECT 'FnbSales', f.update_date, sum( unit_cost * qty )
FROM fnb_sales f, temp_occupancy_sales y
WHERE f.occupancy_id = y.occupancy_id
AND f.status = 'Paid'
AND f.update_date >= '2012-09-29 08:09:52'
AND f.update_date <= '2012-09-29 16:00:52'
GROUP BY f.update_date, f.occupancy_id
)


-- Card transactions
insert into temp_shiftend_sales_summary
SELECT rs.update_date, rs.occupancy_id, sum( rs.unit_cost * rs.qty ) AS computed_amount, sr.amount as salesreceipt_amount,
    sr.tendertype, cpd.card_type, cpd.is_debit
FROM room_sales rs
LEFT JOIN salesreceipts sr ON rs.update_date = sr.receipt_date
    AND rs.occupancy_id = sr.occupancy_id
LEFT JOIN card_payment_details cpd ON sr.salesreceipt_id = cpd.salesreceipt_id
where rs.status = 'Paid'
AND rs.update_date >= '2012-09-29 08:09:52'
AND rs.update_date <= '2012-09-29 16:00:52'
group by rs.update_date

UNION

SELECT fs.update_date, fs.occupancy_id, sum( fs.unit_cost * fs.qty ) AS amount, sr.amount,
    sr.tendertype, cpd.card_type, cpd.is_debit
FROM fnb_sales fs
LEFT JOIN salesreceipts sr ON fs.update_date = sr.receipt_date
    AND fs.occupancy_id = sr.occupancy_id
LEFT JOIN card_payment_details cpd ON sr.salesreceipt_id = cpd.salesreceipt_id
where fs.status = 'Paid'
AND fs.update_date >= '2012-09-29 08:09:52'
AND fs.update_date <= '2012-09-29 16:00:52'
group by fs.update_date



