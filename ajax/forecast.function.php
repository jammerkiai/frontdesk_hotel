<?php
/*
forecast.function.php
*/
session_start();
include_once("config/config.inc.php");
include_once("acctg/class.baseobject.php");
include_once("acctg/class.shift.php");

function insertfc($occupancy, $now, $receiptflag) {
	$shift = new shift(array());
	$thisshift = $shift->shiftno;
	$lastShiftend = getPreviousShiftEnd($occupancy, $now);
	$maxRange = date('Y-m-d H:i:s', strtotime($lastShiftend. ' -24 hours'));
	$room = getRoomsales($occupancy, $maxRange);
	$food = getFoodsales($occupancy, $lastShiftend);
	$misc = getMiscsales($occupancy, $lastShiftend);
	$amount = $room + $food + $misc;
	$door = getDoorname($occupancy);
	if ($receiptflag == 1) {
		$sql = "insert into forecast_collection 
			(fc_date, shiftnum, occupancy_id, door_name, food_sales, room_sales, misc_sales, amount) 
			values ('$now', '$thisshift', '$occupancy','$door', '$food', '$room', '$misc', '$amount') ";
	} else {
		$sql = "insert into tempfc 
			(fc_date, shiftnum, occupancy_id, door_name, food_sales, room_sales, misc_sales, amount) 
			values ('$now', '$thisshift', '$occupancy','$door', '$food', '$room', '$misc', '$amount') ";
	}
	mysql_query($sql) or die(mysql_error());
}

function getComputedAmount($occupancy) {
	$roomsales = getRoomsales($occupancy);
	$fnbsales = getFoodsales($occupancy);
	$miscsales = getMiscsales($occupancy);
	return $roomsales + $fnbsales + $miscsales;
}

function getDoorname($occupancy) {
	$sql = "select a.door_name from rooms a, occupancy b 
		where a.room_id=b.room_id and b.occupancy_id='$occupancy' ";
	$res = mysql_query($sql);
	list($door)= mysql_fetch_row($res);
	return $door;
}

function getFoodsales($occupancy, $now) {
	$sql = "select sum(unit_cost*qty) from fnb_sales 
			where occupancy_id='$occupancy' and update_date >='$now' and status='Paid' ";
	$res = mysql_query($sql);
	list($fnbsales)=mysql_fetch_row($res);
	return $fnbsales;
}

function getRoomsales($occupancy, $now) {
	$sql = "select sum(unit_cost*qty) from room_sales 
			where occupancy_id='$occupancy' and status='Paid' and sales_date >='$now' and category_id = 3";
	$res = mysql_query($sql);
	list($roomsales)=mysql_fetch_row($res);
	return $roomsales;
}

function getMiscsales($occupancy) {
	$sql = "select sum(unit_cost*qty) from room_sales 
			where occupancy_id='$occupancy' and status='Paid' and update_date >='$now' and category_id <> 3";
	$res = mysql_query($sql);
	list($roomsales)=mysql_fetch_row($res);
	return $roomsales;
}


function runfc($date='', $shift='') {
	if($date=='' && $shift=='') {
		$shift = new shift(array());
		$thisshift = $shift->shiftno;
		$now = date('Y-m-d');
	} else {
		$now = $date;
		$thisshift = $shift;
	}
	$limit = getLimit($now,$thisshift);
	$runningtotal = getRunningFC($now, $thisshift);
	while($limit > $runningtotal) {
		if(randomSelect($now, $thisshift) === false) break;
		$runningtotal = getRunningFC($now, $thisshift);
		if ($limit < $runningtotal) break; 
	}
}

function randomSelect($date, $shift) {
	//$sql = "select * from tempfc where shiftnum='$shift' and date(fc_date)='$date' order by rand() limit 1";
	$sql = "select * from tempfc where date(fc_date)='$date' order by rand() limit 1";
	$res = mysql_query($sql);
	$num = mysql_num_rows($res);
	if ($num) {
		list($id, $fd,$fs,$occ,$door,$food,$room, $misc, $fv)=mysql_fetch_row($res);
		$sql2 = "insert into forecast_collection 
			(fc_date, shiftnum, occupancy_id, door_name, food_sales, room_sales, misc_sales, amount) 
			values ('$fd','$fs','$occ','$door', '$food', '$room', '$misc', '$fv')";
		mysql_query($sql2) or die(mysql_error() . $sql2);
		$sql3 = "delete from tempfc where fc_date='$fd' and shiftnum='$fs' and occupancy_id='$occ' and amount='$fv'";
		mysql_query($sql3) or die(mysql_error() . $sql3);
		return true;
	}
	return false;
}

function getLimit($date, $shift) {
	$sql = "select forecast_value from forecast where forecast_date='$date' and shiftnum='$shift'";
	$res = mysql_query($sql);
	list($limit) = mysql_fetch_row($res);
	return $limit;
}

function getRunningFC($now,$thisshift) {
	$sql  = "select sum(amount) from forecast_collection
			where date(fc_date)='$now' and shiftnum='$thisshift' ";
	$res = mysql_query($sql);
	list($amount)=mysql_fetch_row($res) or die($sql);
	return $amount;
}

function getForecastList($date, $shift, $print='') {
	$sql = "select * from forecast_collection 
			where fc_date='$date' and shiftnum='$shift' ";
	$res = mysql_query($sql);
	$num = mysql_num_rows($res);
	if($num) {
$out =" 
RmNo\t | RS\t | FS\t | MS\t | NET\t | VAT\t | TOT\t
---------------------------------------------------\n";

//$out="\n\n";

		while(list($id, $fd, $fs, $occ,$dr, $food, $room, $misc,  $amt)=mysql_fetch_row($res)) {
			$tax = $amt * 0.12;
			$vatable = $amt - $tax;
			$out.="$dr\t | $room\t| $food\t| $misc\t| $vatable\t| $tax\t| $amt \n";
			$totalamt += $amt;
		}
		echo "<pre>$out</pre>";
		if ($print==1) {
			$pout = chr(hexdec('1B')).chr(hexdec('4D')).chr(1);
			$pout .= "$out\n\n\n";
			$pout .= chr(hexdec('1D')).chr(hexdec('56')).chr(49);
			$fp = fopen("reports/fc.txt", "w");
			fwrite( $fp,$pout);
			fclose($fp);
			shell_exec('c:\pstools\psexec c:\xampp\htdocs\fds\ajax\p.bat ' . $file);
		}
	} else {
		$retval = "no data found";
	}
	return $retval;
}


function getForecastManager($date, $shift) {
	$sql = "select * from forecast_collection 
			where fc_date='$date' and shiftnum='$shift' ";
	$res = mysql_query($sql);
	$num = mysql_num_rows($res);
	$target = getLimit($date, $shift);
	$target = number_format($target,2);
	if($num) {
		//$retval="$num records found.<br>";
		$retval.="<table>";
		$retval.="<tr>";
		$retval.="<th><input type='checkbox' name='cball' id='cball' class='cball' /></th>";
		$retval.="<th>Occupancy</th>";
		$retval.="<th>Room No.</th>";
		$retval.="<th>Room Sales</th>";
		$retval.="<th>Food Sales</th>";
		$retval.="<th>Misc Sales</th>";
		$retval.="<th>Net of VAT</th>";
		$retval.="<th>12%</th>";
		$retval.="<th>Amount</th>";
		$retval.="</tr>";
		
		while(list($id, $fd, $fs, $occ,$dr, $food, $room, $misc,  $amt)=mysql_fetch_row($res)) {
			$tax = $amt * 0.12;
			$vatable = $amt - $tax;
			$tax = number_format($tax,2);
			$vatable = number_format($vatable,2);
			$retval.="<tr>";
			$retval.="<th><input type='checkbox' name='cball' id='cball' class='cball' /></th>";	
			$retval.="<td>$occ</td>";
			$retval.="<td>$dr</td>";
			$retval.="<td>$room</td>";
			$retval.="<td>$food</td>";
			$retval.="<td>$misc</td>";
			$retval.="<td>$vatable</td>";
			$retval.="<td>$tax</td>";
			$retval.="<td class='amt'>$amt</td>";
			$retval.="</tr>";
			$totalamt += $amt;
		}
		$totalamt = number_format($totalamt, 2);
		$retval.="<tr><th colspan='2' class='amt'>Total:</th><th class='amt'>$totalamt</th></tr>";
		$retval.="<tr><th colspan='2' class='amt'>Target:</th><th class='amt'>$target</th></tr>";
		$retval.="</table>";
	} else {
		$retval = "no data found";
	}
	return $retval;
}


function getTempForecastManager($date, $shift) {
	$sql = "select * from tempfc 
			where date(fc_date)='$date' and shiftnum='$shift' ";
	$res = mysql_query($sql);
	$num = mysql_num_rows($res);
	if($num) {
		//$retval="$num records found.<br>";
		$retval.="<table>";
		$retval.="<tr>";
			$retval.="<th>Date</th>";
			$retval.="<th>Occupancy</th>";
			$retval.="<th>Room No.</th>";
			$retval.="<th>Room Sales</th>";
			$retval.="<th>Food Sales</th>";
			$retval.="<th>Misc Sales</th>";
			$retval.="<th>Net of VAT</th>";
			$retval.="<th>12%</th>";
			$retval.="<th>Amount</th>";
			$retval.="</tr>";
		while(list($id, $fd, $fs, $occ, $dr, $food, $room, $misc,  $amt)=mysql_fetch_row($res)) {
			$tax = $amt * 0.12;
			$vatable = $amt - $tax;
			$tax = number_format($tax,2);
			$vatable = number_format($vatable,2);
			$retval.="<tr>";
			$retval.="<td>$fd</td>";
			$retval.="<td>$occ</td>";
			$retval.="<td>$dr</td>";
			$retval.="<td>$room</td>";
			$retval.="<td>$food</td>";
			$retval.="<td>$misc</td>";
			$retval.="<td>$vatable</td>";
			$retval.="<td>$tax</td>";
			$retval.="<td class='amt'>$amt</td>";
			$retval.="</tr>";
			$totalamt += $amt;
		}
		$totalamt = number_format($totalamt, 2);
		$retval.="<tr><th colspan='2' class='amt'>Total:</th><th class='amt'>$totalamt</th></tr>";
		$retval.="</table>";
	} else {
		$retval = "no data found";
	}
	return $retval;
}

function getPreviousShiftEnd($occupancy, $now)
	{
		$sql = "select a.datetime 
				from `shift-transactions` a, room_sales b
				where a.shift = 'end' 
				and a.datetime >= b.sales_date 
				and b.occupancy_id=$occupancy
				and b.sales_date <='$now'
				and b.category_id=3
				and b.item_id in (15,16)
				order by datetime asc limit 0,1
			";
		$res = mysql_query($sql);
		if(mysql_num_rows($res)) {
			$row = mysql_fetch_row($res);
			return $row[0];
		}else{
			return 0;
		}
	}
?>