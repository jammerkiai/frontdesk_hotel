<?php
/**
* fc_registry functions
*/

class forecast {

    public function __construct($date = null, $shift = null) {
        $this->date = is_null($date) ? date('Y-m-d') : $date;
        $this->shift = is_null($shift) ? 1 : $shift;
        $this->user = $_SESSION['hotel']['userid'];
        $this->username = $_SESSION['hotel']['fullname'];
        $this->getforecast($this->date, $this->shift);
    }

    public function getforecast($date, $shift = 1) {
	    $sql = "select forecast_value, adjusted_value from forecast
			    where date(forecast_date)='$date'
			    and shiftnum = '$shift'";
	    $res = mysql_query($sql);
	    $row = mysql_fetch_row($res);
	    $this->forecast = $row[0];
	    $this->adjusted = $row[1];
    }

    public function getlastendshifttime() {
        $sql = "select `datetime` from `shift-transactions`
            where shift='end' order by `shift-transaction_id` desc";
        $res = mysql_query($sql) or die($sql);
        $row = mysql_fetch_row($res);
        return $row[0];
    }

    public static function getshift($date='') {
	    if(!$date)$date=date("Y-m-d H:i:s");
	    list($d, $t) = explode(" ", $date);
	    list($h, $m, $s) = explode(":", $t);
	    $sql = "select shift_id from shifts where $h between shift_start and shift_end";
	    $res = mysql_query($sql) or die(mysql_error() .$sql);
	    list($shift)=mysql_fetch_row($res);

	    if($h>=14&&$h<=21)
	    {
		    return $shift = "3";
	    }
	    elseif($h>=6&&$h<=13)
	    {
		    return $shift = "2";
	    }
	    return  "1";
    }

    public static function getlastreceiptnumber() {
        $sql = "select max(receipt_number) from occupancy_receipts";
        $res = mysql_query($sql) or die($sql);
        $row = mysql_fetch_row($res);
        return $row[0] + 1;
    }


    public function getsummaryquery($year, $month, $site=2) {
        $db = 'shogun' . $site . 'fds';
        $db = 'shogunfds' . $site . '';
        mysql_query("truncate table $db.temp_occupancy");
        $sql = "insert into $db.temp_occupancy
                select *
                from $db.occupancy
                where occupancy_id in (
                    select occupancy_id from $db.room_sales
                    where year(regdate) = '$year'
                    and month(regdate) = '$month'
                    union
                    select occupancy_id from $db.fnb_sales
                    where year(regdate) = '$year'
                    and month(regdate) = '$month'
                    );
            ";

        $res = mysql_query($sql) or die($sql);

        $sql2 = "
            select c.receipt_date, $site, b.door_name,
            concat_ws(' ', g.firstname, g.lastname) as guest_name, c.receipt_number,
            (select sum(unit_cost * qty) from $db.room_sales d
                where d.occupancy_id = a.occupancy_id
                and d.status='Paid'
                and category_id = 3
                and c.receipt_number = d.regflag
                and year(d.regdate) = '$year'
                and month(d.regdate) = '$month'
                group by d.occupancy_id, d.regflag)
                as room_sales,
            (select sum(unit_cost * qty) from $db.room_sales e
                where e.occupancy_id = a.occupancy_id
                and e.status='Paid'
                and category_id <> 3
                and c.receipt_number = e.regflag
                and year(e.regdate) = '$year'
                and month(e.regdate) = '$month'
                group by e.occupancy_id, e.regflag)
                as misc_sales,
            (select sum(unit_cost * qty) from $db.fnb_sales f
                where f.occupancy_id = a.occupancy_id
                and f.status='Paid'
                and c.receipt_number = f.regflag
                and year(f.regdate) = '$year'
                and month(f.regdate) = '$month'
                group by f.occupancy_id, f.regflag)
                as food_sales
            from $db.temp_occupancy a
            join $db.rooms b on a.room_id=b.room_id
            join $db.occupancy_receipts c on a.occupancy_id=c.occupancy_id
            left join $db.guest_history gh on a.occupancy_id = gh.occupancy_id
            left join $db.guests g on gh.guest_id = g.guest_id
            where a.room_id = b.room_id
            and a.occupancy_id =  c.occupancy_id
            order by c.receipt_date, c.receipt_number
        ";
       return $sql2;
    }

    private function _getregisteredquery() {
        // setup temp occupancy table for checked out guests
       $start = $this->getlastendshifttime();
       mysql_query('truncate table temp_occupancy');
       $sql = "insert into temp_occupancy
                select *
                from occupancy
                where occupancy_id in (
                    select occupancy_id from room_sales
                    where regdate = '{$this->date}'
                    and regshift = '{$this->shift}'
                    union
                    select occupancy_id from fnb_sales
                    where regdate = '{$this->date}'
                    and regshift = '{$this->shift}'
                    );
            ";

        $res = mysql_query($sql) or die($sql);

        $sql2 = "
            select a.occupancy_id, b.door_name, a.actual_checkout, c.receipt_number,
            (select sum(unit_cost * qty) from room_sales d
                where d.occupancy_id = a.occupancy_id
                and d.status='Paid'
                and category_id = 3
                and c.receipt_number = d.regflag
                and d.regdate = '{$this->date}'
                and d.regshift = '{$this->shift}'
                group by d.occupancy_id, d.regflag)
                as room_sales,
            (select sum(unit_cost * qty) from room_sales e
                where e.occupancy_id = a.occupancy_id
                and e.status='Paid'
                and category_id <> 3
                and c.receipt_number = e.regflag
                and e.regdate = '{$this->date}'
                and e.regshift = '{$this->shift}'
                group by e.occupancy_id, e.regflag)
                as misc_sales,
            (select sum(unit_cost * qty) from fnb_sales f
                where f.occupancy_id = a.occupancy_id
                and f.status='Paid'
                and c.receipt_number = f.regflag
                and f.regdate = '{$this->date}'
                and f.regshift = '{$this->shift}'
                group by f.occupancy_id, f.regflag)
                as food_sales
            from temp_occupancy a, rooms b, occupancy_receipts c
            where a.room_id = b.room_id
            and a.occupancy_id =  c.occupancy_id
            order by c.receipt_number
        ";
       return $sql2;
    }

    public function getregistered() {
        $sql2 = $this->_getregisteredquery();
        $res3 = mysql_query($sql2) or die($sql2);
        $data = "";
        $totals = array('room_sales' => 0, 'food_sales'=> 0, 'misc_sales'=> 0, 'total'=>0 , 'tax' => 0, 'subsales' => 0);
        $num = mysql_num_rows($res3);
        echo "<h3>Number of records: $num</h3>";

        while($row2 = mysql_fetch_object($res3)) {

            $total = 0;
            $checked = ($regflag === 1) ? ' checked ' : '' ;
            $total = $row2->room_sales + $row2->food_sales + $row2->misc_sales;
            $roomsales = $row2->room_sales / 1.12;
            $foodsales = $row2->food_sales / 1.12;
            $miscsales = $row2->misc_sales / 1.12;
            $subsales = $roomsales + $foodsales + $miscsales;
            $tax = $total / 1.12 * 0.12;
            $net = $total - $tax;

            $data .= "<tr>";
            $data .= "<td><input type=checkbox name='occsel[]' value='" . $row2->occupancy_id . "' $checked></td>";
            $data .= "<td>" . $row2->door_name . "</td>";
            $data .= "<td>" . $row2->actual_checkout . "</td>";
            $data .= "<td>" . number_format($roomsales, 2) . "</td>";
            $data .= "<td>" . number_format($foodsales, 2) . "</td>";
            $data .= "<td>" . number_format($miscsales, 2) . "</td>";
            $data .= "<td>" . number_format($subsales, 2) . "</td>";
            $data .= "<td>" . number_format($tax, 2) . "</td>";
            $data .= "<td>" . number_format($total, 2) . "</td>";
            $data .= "<td>" . $row2->receipt_number . "</td>";
            $data .= "</tr>";
            $totals['room_sales'] += $roomsales;
            $totals['food_sales'] += $foodsales;
            $totals['misc_sales'] += $miscsales;
            $totals['subsales'] += $subsales;
            $totals['total'] += $total;
            $totals['tax'] += $tax;
            $totals['net'] += $net;
        }
        $data .= "<tr class='totals'><td colspan='3'>Totals</td>";
        $data .= "<td>" . number_format($totals['room_sales'], 2) . "</td>";
        $data .= "<td>" . number_format($totals['food_sales'], 2) . "</td>";
        $data .= "<td>" . number_format($totals['misc_sales'], 2) . "</td>";
        $data .= "<td>" . number_format($totals['subsales'], 2) . "</td>";
        $data .= "<td>" . number_format($totals['tax'], 2) . "</td>";
        $data .= "<td>" . number_format($totals['total'], 2) . "</td>";

        //$data .= "<td>" . $totals['net'] . "</td>";
        $data .= "</tr>";
        $this->postamount = $totals['total'];
        return $data;
    }

    private function _buildprintablefile() {
        $sql2 = $this->_getregisteredquery();

        $res2 = mysql_query($sql2) or die($sql2);
        $data = "";

        $data .= "\nDate: ". $this->date;
        $data .= "\nShift: ". $this->shift;
        $data .= "\nPrinted By: ". $this->username;
        $data .= "\nPrint #: ". $this->printcount;

        $totals = array('room_sales' => 0, 'food_sales'=> 0, 'misc_sales'=> 0, 'total'=>0 , 'tax' => 0, 'subsales' => 0);

        $padcount = 10;
        $data .= "\n\n\n";
        $data .= str_pad('Room', 4) ." | ";
        $data .= str_pad('Checkout', 19) ." | ";
        $data .= str_pad('Room Sales', $padcount) ." | ";
        $data .= str_pad('Food Sales', $padcount) ." | ";
        $data .= str_pad('Misc Sales', $padcount) ." | ";
        $data .= str_pad('SubTotal', $padcount) ." | ";
        $data .= str_pad('Tax', $padcount) ." | ";
        $data .= str_pad('Total', $padcount) ." | ";
        $data .= str_pad('RCPT #', $padcount);
        $data .= "\n";
        $data .= str_pad('', 120, '=');
        while($row2 = mysql_fetch_object($res2)) {
            $total = 0;
            $checked = ($regflag === 1) ? ' checked ' : '' ;
            $total = $row2->room_sales + $row2->food_sales + $row2->misc_sales;
            $roomsales = $row2->room_sales / 1.12;
            $foodsales = $row2->food_sales / 1.12;
            $miscsales = $row2->misc_sales / 1.12;
            $subsales = $roomsales + $foodsales + $miscsales;
            $tax = $total / 1.12 * 0.12;
            $net = $total - $tax;

            $data .= "\n";
            $data .= str_pad($row2->door_name, 4) . " | ";
            $data .= str_pad($row2->actual_checkout, $padcount) . " | ";
            $data .= str_pad(number_format($roomsales, 2), $padcount) . " | ";
            $data .= str_pad(number_format($foodsales, 2), $padcount) . " | ";
            $data .= str_pad(number_format($miscsales, 2), $padcount) . " | ";
            $data .= str_pad(number_format($subsales, 2), $padcount) . " | ";
            $data .= str_pad(number_format($tax, 2), $padcount) . " | ";
            $data .= str_pad(number_format($total, 2), $padcount) . " | ";
            $data .= str_pad($row2->receipt_number, $padcount);
            $data .= " ";
            $totals['room_sales'] += $roomsales;
            $totals['food_sales'] += $foodsales;
            $totals['misc_sales'] += $miscsales;
            $totals['subsales'] += $subsales;
            $totals['total'] += $total;
            $totals['tax'] += $tax;
            $totals['net'] += $net;
        }
        /*
        $data .= "<tr class='totals'><td colspan='3'>Totals</td>";
        $data .= "<td>" . $totals['room_sales'] . "</td>";
        $data .= "<td>" . $totals['food_sales'] . "</td>";
        $data .= "<td>" . $totals['misc_sales'] . "</td>";
        $data .= "<td>" . $totals['tax'] . "</td>";
        $data .= "<td>" . $totals['total'] . "</td>";

        //$data .= "<td>" . $totals['net'] . "</td>";
        $data .= "</tr>";
        $this->postamount = $totals['total'];
        */
        /*
        $content = chr(hexdec('1B')).chr(hexdec('4D')).chr(1);
		$content.= " $data \n\n\n\n\n";
		$content.= chr(hexdec('1D')).chr(hexdec('56')).chr(49);
        */

        $content = "<html><title>ForecastSummary</title>
                    <script>
                    function printDoc() {
                        document.title = 'fcreport.txt';
                        window.print();
                    }
                    </script>
                    <body onLoad=''><pre>";
        $content.= " $data ";
        $content.= "</pre></body></html>";

		$file = "fcsummary.html";
		$fp = fopen("reports/" .$file, "w");
		fwrite( $fp,$content);
		fclose($fp);

		//shell_exec('c:\pstools\psexec c:\xampp\htdocs\fds\ajax\p.bat ' . $file);
    }

    public function getcollectiondata($regflag = 0) {
       // setup temp occupancy table for checked out guests
       $start = $this->getlastendshifttime();
       mysql_query('truncate table temp_occupancy');
       $sql = "insert into temp_occupancy
                select *
                from occupancy
                where actual_checkout >= '$start';
            ";
        $res = mysql_query($sql) or die($sql);

        $sql2 = "
            select distinct a.occupancy_id, b.door_name, a.actual_checkout,
            (select group_concat(distinct c.tendertype) from salesreceipts c
				where a.occupancy_id=c.occupancy_id) as tendertype,
            (select sum(unit_cost * qty) from room_sales d
                where d.occupancy_id=a.occupancy_id
                and d.status='Paid'
                and category_id = 3
                and d.regflag = 0
                group by d.occupancy_id)
                as room_sales,
            (select sum(unit_cost * qty) from room_sales e
                where e.occupancy_id=a.occupancy_id
                and e.status='Paid'
                and category_id <> 3
                and e.regflag = 0
                group by e.occupancy_id)
                as misc_sales,
            (select sum(unit_cost * qty) from fnb_sales f
                where f.occupancy_id=a.occupancy_id
                and f.status='Paid'
                and f.regflag = 0
                group by f.occupancy_id)
                as food_sales
            from temp_occupancy a, rooms b
            where a.room_id=b.room_id
            order by a.actual_checkout
        ";

        $res2 = mysql_query($sql2) or die($sql2);
        $data = "";
        $totals = array('room_sales' => 0, 'food_sales'=> 0, 'misc_sales'=> 0, 'total'=>0 , 'tax' => 0, 'subsales' => 0);
        while($row2 = mysql_fetch_object($res2)) {
            $total = 0;
            $checked = ($regflag === 1) ? ' checked ' : '' ;
            $total = $row2->room_sales + $row2->food_sales + $row2->misc_sales;
            $roomsales = $row2->room_sales / 1.12;
            $foodsales = $row2->food_sales / 1.12;
            $miscsales = $row2->misc_sales / 1.12;
            $subsales = $roomsales + $foodsales + $miscsales;
            $tax = $total / 1.12 * 0.12;
            $net = $total - $tax;

            if ($total > 0) {
                $data .= "<tr>";
                $data .= "<td><input type=checkbox class='ocitem' name='occsel[]' value='" . $row2->occupancy_id . "' $checked></td>";
                $data .= "<td>" . $row2->door_name . "</td>";
                $data .= "<td>" . $row2->actual_checkout . "</td>";
                $data .= "<td>" . number_format($roomsales, 2) . "</td>";
                $data .= "<td>" . number_format($foodsales, 2) . "</td>";
                $data .= "<td>" . number_format($miscsales, 2) . "</td>";
                $data .= "<td>" . number_format($subsales, 2) . "</td>";
                $data .= "<td>" . number_format($tax, 2) . "</td>";
                $data .= "<td>" . number_format($total, 2) . "</td>";
                $data .= "<td>" . $row2->tendertype . "</td>";
                $data .= "</tr>";
                $totals['room_sales'] += $roomsales;
                $totals['food_sales'] += $foodsales;
                $totals['misc_sales'] += $miscsales;
                $totals['subsales'] += $subsales;
                $totals['total'] += $total;
                $totals['tax'] += $tax;
                $totals['net'] += $net;
            }
        }
        $data .= "<tr class='totals'><td colspan='3'>Totals</td>";
        $data .= "<td>" . number_format($totals['room_sales'], 2) . "</td>";
        $data .= "<td>" . number_format($totals['food_sales'], 2) . "</td>";
        $data .= "<td>" . number_format($totals['misc_sales'], 2) . "</td>";
        $data .= "<td>" . number_format($totals['subsales'], 2) . "</td>";
        $data .= "<td>" . number_format($totals['tax'], 2) . "</td>";
        $data .= "<td>" . number_format($totals['total'], 2) . "</td>";

        $data .= "</tr>";
        return $data;
    }


    public function registeritems($data) {
        /**
            1. get next receipt number
            2. insert into occupancy_receipts
            3. update room_sales based on occupancy_id and regflag
            4. update fnb_sales based on occupancy_id and regflag
        */
        $regdate = $data['fcdate'];
        $regshift = $data['fcshift'];
        $series = $data['receipt_start'];
        foreach ($data['occsel'] as $occ) {
            $this->_addreceipt2occupancy($occ, $series);
            $this->_updatereceiptforroomsales($occ, $series);
            $this->_updatereceiptforfnbsales($occ, $series);
            $series++;
        }


    }

    protected function _addreceipt2occupancy($occ, $series) {
        $sql = "insert into occupancy_receipts
            (occupancy_id, receipt_date, receipt_shift, receipt_number, issued_by)
            values
            ('$occ', '{$this->date}', '{$this->shift}', '$series', '{$this->user}')";

        mysql_query($sql) or die($sql);
    }

    protected function _updatereceiptforroomsales($occ, $series) {
        $sql = "update room_sales
                set regflag='$series',
                regdate = '{$this->date}',
                regshift = '{$this->shift}'
                where occupancy_id='$occ'
                and regflag = 0";

        mysql_query($sql) or die($sql);
    }

    protected function _updatereceiptforfnbsales($occ, $series) {
        $sql = "update fnb_sales set regflag='$series',
                regdate = '{$this->date}',
                regshift = '{$this->shift}'
                where occupancy_id='$occ'
                and regflag = 0";

        mysql_query($sql) or die($sql);
    }

    public function post($data) {
        $actual = $data['postamount'];
        $target = $data['targetamount'];
        $diff = $target - $actual;
        echo "$this->forecast, $target, $actual, $diff";
        $sql = "update forecast set
                actual_value = '$actual'
                where forecast_date = '{$this->date}'
                and shiftnum = '{$this->shift}'
                ";

        mysql_query($sql) or die($sql);
        //adjust next day's date
        $nextshift = $this->_getnextfcshift();
        $nextdate = $this->_getnextfcdate();
        $sql = "update forecast set
                adjusted_value = forecast_value + $diff
                where forecast_date = '$nextdate'
                and shiftnum = '$nextshift'
                ";

        mysql_query($sql) or die($sql);
    }

    protected function _getnextfcshift() {
        if ($this->shift == 3) {
            return 1;
        }
        return $this->shift + 1;
    }

    protected function _getnextfcdate() {
        if ($this->shift == 3) {
            $nextdate = strtotime(date("Y-m-d", strtotime($this->date)) . " +1 day");
            return date('Y-m-d', $nextdate);
        }
        return $this->date;
    }

    public function printregistered() {
        $this->_addprintcount();
        $this->_getprintcount();
        $this->_buildprintablefile();
    }

    private function _addprintcount() {
        $now = date('Y-m-d H:i:s');
        $sql = "insert into fcprintlog (print_date, regdate, regshift, printed_by)
            values('$now', '{$this->date}', '{$this->shift}', '{$this->user}') ";
        mysql_query($sql);
    }

    private function _getprintcount() {
        $sql = "select count(*) from fcprintlog
                where regdate='{$this->date}'
                and regshift='{$this->shift}' ";
        $res = mysql_query($sql);
        $row = mysql_fetch_row($res);
        $this->printcount = $row[0];
    }
}


