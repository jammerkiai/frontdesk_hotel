<?php
/**
* lobby config
*/

if(ifLobby())
{
	
	if($_SERVER['PHP_SELF'] != "/fds/ajax/order.php ")
	{
		die("You cannot acces this page.");
	}
}

function ifLobby()
{
	$_sql = "select settings_value from settings where settings_name = 'SPECIALFLOORID'";
	$_res = mysql_query($_sql);
	list($val)=mysql_fetch_row($_res);
	
	$sql = "select room_id from rooms where floor_id=$val";
	$res = mysql_query($sql);
	$arrRooms=array();
	while(list($rid)=mysql_fetch_row($res)) {
		$arrRooms[]=$rid;
	}
	$room =($_GET["roomid"]) ? $_GET["roomid"] : $_POST["roomid"];

	if(in_array($room,$arrRooms))
	{
		return true;
	}

	return false;
}
?>