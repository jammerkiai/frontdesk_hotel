-- phpMyAdmin SQL Dump
-- version 3.2.0.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 11, 2009 at 03:00 PM
-- Server version: 5.1.37
-- PHP Version: 5.3.0

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `hfe`
--

-- --------------------------------------------------------

--
-- Table structure for table `rates_dow`
--

CREATE TABLE IF NOT EXISTS `rates_dow` (
  `rate_id` int(11) NOT NULL AUTO_INCREMENT,
  `rate_name` varchar(20) NOT NULL,
  `hour_start` int(11) NOT NULL,
  `hour_end` int(11) NOT NULL,
  `dow` varchar(256) DEFAULT NULL,
  `display` varchar(3) NOT NULL DEFAULT 'Yes',
  PRIMARY KEY (`rate_id`),
  UNIQUE KEY `rate_name` (`rate_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=40 ;

--
-- Dumping data for table `rates_dow`
--

INSERT INTO `rates_dow` (`rate_id`, `rate_name`, `hour_start`, `hour_end`, `dow`, `display`) VALUES
(1, '3 HRS', 24, 18, 'Tuesday|Sunday', 'Yes'),
(2, '12 HRS', 24, 1, 'Monday|Tuesday|Wednesday|Thursday|Friday|Saturday|Sunday', 'Yes'),
(3, '24 HRS', 24, 23, 'Sunday', 'No'),
(4, 'Early Bird', 4, 8, NULL, 'Yes'),
(33, 'ASD', 24, 1, 'Thursday', 'Yes'),
(34, 'sdfsdfs', 24, 1, 'Wednesday', 'Yes'),
(35, 'sdfsdf', 5, 2, 'Thursday', 'Yes'),
(36, 'ghjghj', 3, 11, 'Friday', 'Yes'),
(37, 'dfgdcxvbxcv', 24, 2, 'Saturday', 'Yes'),
(38, 'xxcv', 4, 4, 'Saturday', 'Yes');
