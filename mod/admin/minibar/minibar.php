<?php
require "../../../ajax/config/config.inc.php";

if ( isset ( $_POST['cmdAdd']) && $_POST['cmdAdd'] == 'Add New' ) {
	header('Location: minibar_add.php') ;
} 
else if ( isset ( $_POST['cmdDelete']) && $_POST['cmdDelete'] != '' ) { //save operation
	for ( $cnt = 0; $cnt < sizeof($_POST["rec_id"]) ; $cnt ++ ) {
		if (isset($_POST["rec_id"][$cnt]) && $_POST["rec_id"][$cnt] != '' && $_POST["rec_id"][$cnt] != 'on') {
			$ids .= $_POST["rec_id"][$cnt];
			if ($cnt < sizeof($_POST["rec_id"]) - 1) {
				$ids .= ',';
			}
		}
	}
	if (is_array($_POST["rec_id"])){
		header('Location: minibar_delete.php?id='.$ids) ;
	}
}
else if (isset ( $_POST['cmdSearch']) && $_POST['cmdSearch'] != '' && isset($_POST['search'])) {
	$param_search = " AND room_type_id IN (SELECT room_type_id FROM room_types WHERE room_type_name LIKE '".addslashes($_POST['search'])."%' OR room_type_name LIKE '%".addslashes($_POST['search'])."%') ";
}

$maxRows_rsMinibar = 10;
$pageNum_rsMinibar = 0;
if (isset($_GET['pageNum_rsMinibar'])) {
  $pageNum_rsMinibar = $_GET['pageNum_rsMinibar'];
}
$startRow_rsMinibar = $pageNum_rsMinibar * $maxRows_rsMinibar;
$param_rsMinibar = " WHERE 1=1 ".$param_search;

//$query_rsMinibar = sprintf("select * from rooms %s ORDER BY door_name %s", $param_rsMinibar, $sortDate);
$query_rsMinibar = sprintf("select * from minibar %s ", $param_rsMinibar);
$query_limit_rsMinibar = sprintf("%s LIMIT %d, %d", $query_rsMinibar, $startRow_rsMinibar, $maxRows_rsMinibar);
$rsMinibar = mysql_query($query_limit_rsMinibar) or die(mysql_error());
$row_rsMinibar = mysql_fetch_assoc($rsMinibar);

if (isset($_GET['totalRows_rsMinibar'])) {
  $totalRows_rsMinibar = $_GET['totalRows_rsMinibar'];
} else {
  $all_rsMinibar = mysql_query($query_rsMinibar);
  $totalRows_rsMinibar = mysql_num_rows($all_rsMinibar);
}
$totalPages_rsMinibar = ceil($totalRows_rsMinibar/$maxRows_rsMinibar)-1;

$queryString_rsMinibar = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_rsMinibar") == false && 
        stristr($param, "totalRows_rsMinibar") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_rsMinibar = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_rsMinibar = sprintf("&totalRows_rsMinibar=%d%s", $totalRows_rsMinibar, $queryString_rsMinibar);

$TFM_LimitLinksEndCount = 9;
$TFM_temp = $pageNum_rsMinibar + 1;
$TFM_startLink = max(1,$TFM_temp - intval($TFM_LimitLinksEndCount/2));
$TFM_temp = $TFM_startLink + $TFM_LimitLinksEndCount - 1;
$TFM_endLink = min($TFM_temp, $totalPages_rsMinibar + 1);
if($TFM_endLink != $TFM_temp) $TFM_startLink = max(1,$TFM_endLink - $TFM_LimitLinksEndCount + 1);

$roomType = array();
$rsRoomType = mysql_query("SELECT * FROM room_types ORDER BY room_type_name");
while($row_rsRoomType = mysql_fetch_assoc($rsRoomType)) {
	$roomType[$row_rsRoomType['room_type_id']] = $row_rsRoomType['room_type_name'];
}

$fnb = array();
$rsFNB = mysql_query("SELECT * FROM fnb ORDER BY fnb_name");
while($row_rsFNB = mysql_fetch_assoc($rsFNB)) {
	$fnb[$row_rsFNB['fnb_id']] = $row_rsFNB['fnb_name'];
}

?>
<html>
<head>
<title>Minibar</title>
<script type="text/javascript" src="../../../js/custom.js"></script>
<link href="../../../css/admin.css" rel="stylesheet" type="text/css">
</head>
<body>
<form name="form1" method="post" action="">
<table width="80%" border="0" align="center" cellpadding="3" cellspacing="2">
            <?php if (isset($_REQUEST['strMsg']) && $_REQUEST['strMsg'] != '') { ?>
            <tr>
              <td valign="middle"><div class="prompt" style="color:#009900; font-size:12px;"><?php echo $_REQUEST['strMsg'] ; ?></div></td>
            </tr>
            <?php } ?>
            <tr>
              <td valign="top" ><div style="float:left; width:30%"><p><font style="font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; color:#678197;">MINIBAR</font></p>
              </div><div style="float:left; width:70%;"><p align="right"><font style="font:13px Arial,Verdana,  Helvetica, sans-serif; color:#666666">Room Type:</font>&nbsp;<input type"text" name="search" id="search" class="textbox-search-style">&nbsp;<input name="cmdSearch" type="submit" id="cmdSearch" value="Search" class="buttons" style="width:70px;" />&nbsp;<input name="cmdAdd" type="submit" id="cmdAdd" value="Add New" class="buttons" style="width:70px;" />&nbsp;<input name="cmdDelete" type="submit" id="cmdDelete" onClick="GP_popupConfirmMsg('You are about to delete a record(s). Are you sure you want to continue?');return document.MM_returnValue" value="Delete Checked Items" class="buttons" /></p></div></td>
			</td>
            </tr>
            <tr>
              <td><table width="100%" border="0" cellspacing="0" cellpadding="4" class="tablesorter">
				<thead>
                <tr class="bgHeader">
                  <td align="center" valign="top" width="5%"><input name="rec_id[]" type="checkbox" class="options" id="rec_id[]" onClick="toggleChecked(this)"></td>
				  <td align="center" width="35%"><strong style="color:#678197;">Room Type</strong></td>
                  <td align="center" valign="middle" width="30%"><strong style="color:#678197;">FNB</strong></td>
				  <td align="center" valign="middle" width="10%"><strong style="color:#678197;">Amount</strong></td>
				  <td align="center" valign="middle" width="20%"><strong style="color:#678197;">Action</strong></td>
                </tr></thead>
                <?php if ($totalRows_rsMinibar > 0) { $bgNumber = 1; // Show if recordset not empty ?>
                <?php do { 
					$bgNumber++; 
					$bgResult = $bgNumber % 2;
				?>
                <tr <?php if ($bgResult > 0) echo 'bgcolor="#FAFAFA"';
					else echo "bgcolor=#F8FFFF"?>>
                  <td align="center" valign="top" class="divider"><input name="rec_id[]" type="checkbox" class="options" id="rec_id[]" onClick="toggleController(this)" value="<?php echo $row_rsMinibar['minibar_id']; ?>"></td>
				  <td align="center"><?php
						foreach($roomType as $key_roomType  => $value_roomType) {
							if ($row_rsMinibar['room_type_id'] == $key_roomType) echo $value_roomType;
						}
					?></td>
                  <td align="center" valign="top">
				  	<?php
						foreach($fnb as $key_fnb  => $value_fnb) {
							if ($row_rsMinibar['fnb_id'] == $key_fnb) echo $value_fnb;
						}
					?>
				  </td>
				  <td align="center" valign="top"><?=$row_rsMinibar['amount']?></td>
				  <td align="center"><p><img src="../../../img/application_edit.png" align="bottom">&nbsp;<a href="../minibar/minibar_edit.php?id=<?php echo $row_rsMinibar['minibar_id']; ?>">Edit</a></p></td>
                </tr>
                <?php } while ($row_rsMinibar = mysql_fetch_assoc($rsMinibar)); ?>
                <?php } // Show if recordset not empty ?>
              </table></td>
            </tr>
			<tr><td colspan="4">&nbsp;</td></tr>
            <?php if ($totalRows_rsMinibar > 0) { // Show if recordset not empty ?>
            <tr>
              <td align="right" valign="middle"><?php if ($pageNum_rsMinibar > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_rsMinibar=%d%s", $currentPage, 0, $queryString_rsMinibar); ?>"><strong><<</strong> First</a> <a href="<?php printf("%s?pageNum_rsMinibar=%d%s", $currentPage, max(0, $pageNum_rsMinibar - 1), $queryString_rsMinibar); ?>"><strong><</strong> Prev</a>
                <?php } // Show if not first page ?>
                <?php
for ($i = $TFM_startLink; $i <= $TFM_endLink; $i++) {
  $TFM_LimitPageEndCount = $i -1;
  if($TFM_LimitPageEndCount != $pageNum_rsMinibar) {
    printf('<a href="'."%s?pageNum_rsMinibar=%d%s", $currentPage, $TFM_LimitPageEndCount, $queryString_rsMinibar.'">');
    echo "$i</a>";
  }else{
    echo "[<b>$i</b>]";
  }
if($i != $TFM_endLink) echo("&nbsp;");}
?>
                <?php if ($pageNum_rsMinibar < $totalPages_rsMinibar) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_rsMinibar=%d%s", $currentPage, min($totalPages_rsMinibar, $pageNum_rsMinibar + 1), $queryString_rsMinibar); ?>">Next <strong>></strong></a> <a href="<?php printf("%s?pageNum_rsMinibar=%d%s", $currentPage, $totalPages_rsMinibar, $queryString_rsMinibar); ?>">Last <strong>>></strong></a>
                <?php } // Show if not last page ?> </td>
            </tr>
            <?php } // Show if recordset not empty ?>
            <?php if ($totalRows_rsMinibar == 0) { // Show if recordset empty ?>
            <tr>
                <td height="55" align="center" valign="middle"><strong style="color:#FF0000;">No record found. </strong></td>
            </tr>
            <?php } // Show if recordset empty ?>
          </table>
</form>
</body>
</head>
</html>