<?php
require "../../../ajax/config/config.inc.php";

function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = addslashes($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if (isset($_GET['id']) && $_GET['id'] != "") {
	$minibarId = trim($_GET['id']);
	$rsMinibar = mysql_query("SELECT * FROM minibar WHERE minibar_id = ".$minibarId);
	$row_rsMinibar = mysql_fetch_assoc($rsMinibar);
	
	$rsRoomType = mysql_query("SELECT * FROM room_types ORDER BY room_type_name");
	$rownum_rsRoomType = mysql_num_rows($rsRoomType);
	$rsFNB = mysql_query("SELECT * FROM fnb ORDER BY fnb_name");
	$rownum_rsFNB = mysql_num_rows($rsFNB);
}
else {
	header("Location:minibar.php");
}

if ((isset($_POST["save"])) && ($_POST["save"] == "Save")) {
	$rsMinibarExist = mysql_query("SELECT room_type_id, fnb_id FROM minibar WHERE room_type_id = ".trim($_POST['room_type'])." AND fnb_id = ".trim($_POST['fnb'])." AND room_type_id != ".trim($_POST['minibar_room_type_id'])." AND fnb_id != ".trim($_POST['minibar_fnb_id']));
	$rownum_rsMinibarExists = mysql_num_rows($rsMinibarExist);
	if ($rownum_rsMinibarExists == 0) {
 		$updateSQL = sprintf("UPDATE minibar SET room_type_id=%s, fnb_id=%s, amount=%s WHERE minibar_id=%s",
                       GetSQLValueString($_POST['room_type'], "text"),
					   GetSQLValueString($_POST['fnb'], "text"),
					   GetSQLValueString($_POST['amount'], "text"),
					   GetSQLValueString($_POST['minibar_id'], "int"));

  $Result1 = mysql_query($updateSQL) or die(mysql_error());

  $insertGoTo = "minibar.php?strMsg=Record has been successfully updated.";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
 }
 else echo "<script>alert('* Minibar entry already exists.')</script>";
} 
elseif ((isset($_POST["back"])) && ($_POST["back"] == "Back")) {
	header("Location:minibar.php");
}# end update validate

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Edit Minibar</title>
<script type="text/javascript" src="../../../js/custom.js"></script>
<link href="../../../css/admin.css" rel="stylesheet" type="text/css" />
</head>

<body>
<form method="post" name="minibar_edit" >
<table width="80%" border="0" align="center" cellpadding="3" cellspacing="2">
	<tr>
		<td valign="top">
			<table border="0" align="left" cellpadding="2" cellspacing="2" class="tablesorter"> 
				<tr align="left" valign="top"><td colspan="2" align="center" class="bgHeader"><strong>EDIT MINIBAR</strong></td></tr>
				
				<tr align="left" valign="top">
                  <td align="left" width="15%">Room Type </td>
                  <td><select name="room_type" id="room_type" style="width:200px;" class="textbox-style">
				  		<?php
							if ($rownum_rsRoomType > 0) { 
							while($row_rsRoomType = mysql_fetch_assoc($rsRoomType)) {
								if ($row_rsMinibar['room_type_id'] == $row_rsRoomType['room_type_id']) $selected_roomType = "selected";
								else $selected_roomType = "";
						?>
				  		<option value="<?=$row_rsRoomType['room_type_id']?>" <?=$selected_roomType?>><?=$row_rsRoomType['room_type_name']?></option>
						<?php } }?>
						</select>
					</td>
				</tr>
				<tr align="left" valign="top">
                  <td align="left">FNB</td>
                  <td><select name="fnb" id="fnb" style="width:200px;" class="textbox-style">
				  		<?php
							if ($rownum_rsFNB > 0) { 
							while($row_rsFNB = mysql_fetch_assoc($rsFNB)) {
								if ($row_rsMinibar['fnb_id'] == $row_rsFNB['fnb_id']) $selected_fnb = "selected";
								else $selected_fnb = "";
						?>
				  		<option value="<?=$row_rsFNB['fnb_id']?>" <?=$selected_fnb?>><?=$row_rsFNB['fnb_name']?></option>
						<?php } }?>
						</select>				 
					</td>
                </tr>
				<tr align="left" valign="top">
                  <td align="left">Amount</td>
                  <td><input name="amount" type="text" id="amount" style="width:300px;" class="textbox-style" value="<?=$row_rsMinibar['amount']?>" /></td>
                </tr>
				 
                <tr align="left" valign="top">
                  <td>&nbsp;<input type="hidden" name="minibar_id" value="<?=trim($_GET['id'])?>"/><input type="hidden" name="minibar_room_type_id" value="<?=$row_rsMinibar['room_type_id']?>" /><input type="hidden" name="minibar_fnb_id" value="<?=$row_rsMinibar['fnb_id']?>" /></td>
                  <td align="right"><input name="save" type="submit" class="buttons" id="Save" onClick="YY_checkform('minibar_edit','amount','#1_99999','1','Field Aamount is required and must be a number.');return document.MM_returnValue" value="Save" />
                  &nbsp;<input name="back" type="submit" id="back" value="Back" class="buttons" />				                </td>
                </tr>
          </table>
		</td>
	</tr>
</table>
</form>
</body>
</html>
