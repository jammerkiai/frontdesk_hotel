<?php
require "../../../ajax/config/config.inc.php";

function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = addslashes($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

$error = "";
$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["save"])) && ($_POST["save"] == "Save")) {
	$rsMinibarExist = mysql_query("SELECT room_type_id, fnb_id FROM minibar WHERE room_type_id = ".trim($_POST['room_type'])." AND fnb_id = ".trim($_POST['fnb']));
	$rownum_rsMinibarExists = mysql_num_rows($rsMinibarExist);
	if ($rownum_rsMinibarExists == 0) {
	  $insertSQL = sprintf("INSERT INTO minibar (room_type_id, fnb_id, amount) VALUES (%s, %s, %s)",
                       GetSQLValueString($_POST['room_type'], "int"),
					   GetSQLValueString($_POST['fnb'], "int"),
					   GetSQLValueString($_POST['amount'], "int"));

 	 $Result1 = mysql_query($insertSQL) or die(mysql_error());

 	 $insertGoTo = "minibar_add.php?strMsg=Record has been successfully added.";
	  if (isset($_SERVER['QUERY_STRING'])) {
 	   $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
 	   $insertGoTo .= $_SERVER['QUERY_STRING'];
 	 }
 	 header(sprintf("Location: %s", $insertGoTo));
  }
  else echo "<script>alert('* Minibar entry already Exists.')</script>";
}
elseif ((isset($_POST["back"])) && ($_POST["back"] == "Back")) {
	header("Location:minibar.php");
} # end insert validate

$rsRoomType = mysql_query("SELECT * FROM room_types ORDER BY room_type_name");
$rownum_rsRoomType = mysql_num_rows($rsRoomType);
$rsFNB = mysql_query("SELECT * FROM fnb ORDER BY fnb_name");
$rownum_rsFNB = mysql_num_rows($rsFNB);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Add Minibar</title>
<script type="text/javascript" src="../../../js/custom.js"></script>
<link href="../../../css/admin.css" rel="stylesheet" type="text/css" />
</head>

<body>
<form method="post" name="room_add" id="room_add" >
<table width="80%" border="0" align="center" cellpadding="3" cellspacing="2">
    <?php if (isset($_REQUEST['strMsg']) && $_REQUEST['strMsg'] != '') { ?>
    <tr>
    	<td valign="middle"><div class="prompt" style="color:#009900; font-size:12px;"><?php echo $_REQUEST['strMsg'] ; ?></div></td>
    </tr>
    <?php } ?>
	<tr>
		<td valign="top">
			<table border="0" align="left" cellpadding="2" cellspacing="2" class="tablesorter"> 
				<tr align="left" valign="top"><td colspan="2" align="center" class="bgHeader"><strong>ADD MINIBAR</strong></td></tr>      
                <tr align="left" valign="top">
                  <td align="left" width="15%">Room Type</td>
                  <td><select name="room_type" id="room_type" style="width:200px;" class="textbox-style">
				  		<option value="">[Choose]</option>
				  		<?php 
							if ($rownum_rsRoomType > 0) {
							while($row_rsRoomType = mysql_fetch_assoc($rsRoomType)) {
						?>
				  		<option value="<?=$row_rsRoomType['room_type_id']?>"><?=$row_rsRoomType['room_type_name']?></option>
						<?php } }?>
						</select>				  
				</td>
                </tr>
				<tr align="left" valign="top">
                  <td align="left">FNB</td>
                  <td><select name="fnb" id="fnb" style="width:200px;" class="textbox-style">
				  		<option value="">[Choose]</option>
				  		<?php 
							if ($rownum_rsFNB > 0) {
							while($row_rsFNB = mysql_fetch_assoc($rsFNB)) {
						?>
				  		<option value="<?=$row_rsFNB['fnb_id']?>"><?=$row_rsFNB['fnb_name']?></option>
						<?php } }?>
						</select>				  </td>
                </tr>
				<tr align="left" valign="top">
                  <td align="left" width="15%">Amount</td>
                  <td><input name="amount" type="text" class="textbox-style" id="amount" style="width:300px;" /></td>
                </tr>
 
                <tr align="left" valign="top">
                  <td>&nbsp;</td>
                  <td align="right"><input name="save" type="submit" class="buttons" id="Save" onclick="YY_checkform('room_add','amount','#1_99999','1','Field Amount is required and must be a number.','room_type','#q','1','Field Room Type is required.','fnb','#q','1','Field FNB is required.');return document.MM_returnValue" value="Save" />
                  &nbsp;<input name="back" type="submit" id="Back" value="Back" class="buttons" />				                </td>
                </tr>
          </table>
		</td>
	</tr>
</table>
</form>
</body>
</html>
