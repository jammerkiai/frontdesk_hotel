<?php
require "../../../ajax/config/config.inc.php";

if ( isset ( $_POST['cmdAdd']) && $_POST['cmdAdd'] != '' ) {
	header('Location: room_add.php') ;
} 
else if ( isset ( $_POST['cmdDelete']) && $_POST['cmdDelete'] != '' ) { //save operation
	for ( $cnt = 0; $cnt < sizeof($_POST["rec_id"]) ; $cnt ++ ) {
		if (isset($_POST["rec_id"][$cnt]) && $_POST["rec_id"][$cnt] != '' && $_POST["rec_id"][$cnt] != 'on') {
			$ids .= $_POST["rec_id"][$cnt];
			if ($cnt < sizeof($_POST["rec_id"]) - 1) {
				$ids .= ',';
			}
		}
	}
	if (is_array($_POST["rec_id"])){
		header('Location: room_delete.php?id='.$ids) ;
	}
}
else if (isset ( $_POST['cmdSearch']) && $_POST['cmdSearch'] != '' && isset($_POST['search'])) {
	$param_search = " AND (door_name LIKE '".addslashes($_POST['search'])."%' OR door_name LIKE '%".addslashes($_POST['search'])."%') ";
}
else if (isset($_POST['cmdAssign']) && $_POST['cmdAssign'] === 'Bulk Room Type Assignment' ) {
	header("location: ../rates/bulkroomtypes.php");
}

$maxRows_rsRooms = 10;
$pageNum_rsRooms = 0;
if (isset($_GET['pageNum_rsRooms'])) {
  $pageNum_rsRooms = $_GET['pageNum_rsRooms'];
}
$startRow_rsRooms = $pageNum_rsRooms * $maxRows_rsRooms;
$param_rsRooms = " WHERE 1=1 ".$param_search;

//$query_rsRooms = sprintf("select * from rooms %s ORDER BY door_name %s", $param_rsRooms, $sortDate);
$query_rsRooms = sprintf("select * from rooms %s ", $param_rsRooms);
$query_limit_rsRooms = sprintf("%s LIMIT %d, %d", $query_rsRooms, $startRow_rsRooms, $maxRows_rsRooms);
$rsRooms = mysql_query($query_limit_rsRooms) or die(mysql_error());
$row_rsRooms = mysql_fetch_assoc($rsRooms);

if (isset($_GET['totalRows_rsRooms'])) {
  $totalRows_rsRooms = $_GET['totalRows_rsRooms'];
} else {
  $all_rsRooms = mysql_query($query_rsRooms);
  $totalRows_rsRooms = mysql_num_rows($all_rsRooms);
}
$totalPages_rsRooms = ceil($totalRows_rsRooms/$maxRows_rsRooms)-1;

$queryString_rsRooms = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_rsRooms") == false && 
        stristr($param, "totalRows_rsRooms") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_rsRooms = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_rsRooms = sprintf("&totalRows_rsRooms=%d%s", $totalRows_rsRooms, $queryString_rsRooms);

$TFM_LimitLinksEndCount = 9;
$TFM_temp = $pageNum_rsRooms + 1;
$TFM_startLink = max(1,$TFM_temp - intval($TFM_LimitLinksEndCount/2));
$TFM_temp = $TFM_startLink + $TFM_LimitLinksEndCount - 1;
$TFM_endLink = min($TFM_temp, $totalPages_rsRooms + 1);
if($TFM_endLink != $TFM_temp) $TFM_startLink = max(1,$TFM_endLink - $TFM_LimitLinksEndCount + 1);


$roomType = array();
$rsRoomType = mysql_query("SELECT * FROM room_types");
while($row_rsRoomType = mysql_fetch_assoc($rsRoomType)) {
	$roomType[$row_rsRoomType['room_type_id']] = $row_rsRoomType['room_type_name'];
}

$status = array();
$rsStatus = mysql_query("SELECT * FROM statuses");
while($row_rsStatus = mysql_fetch_assoc($rsStatus)) {
	$status[$row_rsStatus['status_id']] = $row_rsStatus['status_name'];
}
?>
<html>
<head>
<title>Rooms</title>
<script type="text/javascript" src="../../../js/custom.js"></script>
<link href="../../../css/admin.css" rel="stylesheet" type="text/css">
</head>
<body>
<form name="form1" method="post" action="">
<table width="80%" border="0" align="center" cellpadding="3" cellspacing="2">
            <?php if (isset($_REQUEST['strMsg']) && $_REQUEST['strMsg'] != '') { ?>
            <tr>
              <td valign="middle"><div class="prompt" style="color:#009900; font-size:12px;"><?php echo $_REQUEST['strMsg'] ; ?></div></td>
            </tr>
            <?php } ?>
            <tr>
              <td valign="top" ><div style="float:left; width:30%"><p><font style="font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; color:#678197;">ROOM</font></p></div><div style="float:left; width:70%;"><p align="right"><font style="font:13px Arial,Verdana,  Helvetica, sans-serif; color:#666666">Room:</font>&nbsp;<input type"text" name="search" id="search" class="textbox-search-style">&nbsp;<input name="cmdSearch" type="submit" id="cmdSearch" value="Search" class="buttons" style="width:70px;" />&nbsp;<input name="cmdAdd" type="submit" id="cmdAdd" value="Add New" class="buttons" style="width:70px;" />&nbsp;<input name="cmdDelete" type="submit" id="cmdDelete" onClick="GP_popupConfirmMsg('You are about to delete a record(s). Are you sure you want to continue?');return document.MM_returnValue" value="Delete Checked Items" class="buttons" />
			  <input type="submit" name="cmdAssign" value="Bulk Room Type Assignment" class="buttons" />
			  </p></div></td>
			</td>
            </tr>
            <tr>
              <td><table width="100%" border="0" cellspacing="0" cellpadding="4" class="tablesorter">
				<thead>
                <tr class="bgHeader">
                  <td align="center" valign="top" width="5%"><input name="rec_id[]" type="checkbox" class="options" id="rec_id[]" onClick="toggleChecked(this)"></td>
                  <td align="left" valign="middle" width="35%"><strong style="color:#678197;">Door Room</strong></td>
				  <td align="center" width="20%"><strong style="color:#678197;">Room Type</strong></td>
                  <td align="center" valign="middle" width="20%"><strong style="color:#678197;">Status</strong></td>
				  <td align="center" valign="middle" width="20%"><strong style="color:#678197;">Action</strong></td>
                </tr></thead>
                <?php if ($totalRows_rsRooms > 0) { $bgNumber = 1; // Show if recordset not empty ?>
                <?php do { 
					$bgNumber++; 
					$bgResult = $bgNumber % 2;
				?>
                <tr <?php if ($bgResult > 0) echo 'bgcolor="#FAFAFA"';
					else echo "bgcolor=#F8FFFF"?>>
                  <td align="center" valign="top" class="divider"><input name="rec_id[]" type="checkbox" class="options" id="rec_id[]" onClick="toggleController(this)" value="<?php echo $row_rsRooms['room_id']; ?>"></td>
                  <td align="left"><a href="rooms_view.php?id=<?php echo $row_rsRooms['room_id']; ?>"><?php echo $row_rsRooms['door_name']; ?></a></td>
				  <td align="center">
				  	<?php
						foreach($roomType as $keyRoomType => $valueRoomType) {
							if ($row_rsRooms['room_type_id'] == $keyRoomType) echo $valueRoomType;
						}
					?>
				  </td>
                  <td align="center" valign="top">
				  	<?php
						foreach($status as $keyStatus => $valueStatus) {
							if ($row_rsRooms['status'] == $keyStatus) echo $valueStatus;
						}
					?>
				  </td>
				  <td align="center"><p><img src="../../../img/application.png" align="bottom">&nbsp;<a href="room_view.php?id=<?php echo $row_rsRooms['room_id']; ?>">View</a>&nbsp;&nbsp;&nbsp;&nbsp;<img src="../../../img/application_edit.png" align="bottom">&nbsp;<a href="room_edit.php?id=<?php echo $row_rsRooms['room_id']; ?>">Edit</a></p></td>
                </tr>
                <?php } while ($row_rsRooms = mysql_fetch_assoc($rsRooms)); ?>
                <?php } // Show if recordset not empty ?>
              </table></td>
            </tr>
			<tr><td colspan="4">&nbsp;</td></tr>
            <?php if ($totalRows_rsRooms > 0) { // Show if recordset not empty ?>
            <tr>
              <td align="right" valign="middle"><?php if ($pageNum_rsRooms > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_rsRooms=%d%s", $currentPage, 0, $queryString_rsRooms); ?>"><strong><<</strong> First</a> <a href="<?php printf("%s?pageNum_rsRooms=%d%s", $currentPage, max(0, $pageNum_rsRooms - 1), $queryString_rsRooms); ?>"><strong><</strong> Prev</a>
                <?php } // Show if not first page ?>
                <?php
for ($i = $TFM_startLink; $i <= $TFM_endLink; $i++) {
  $TFM_LimitPageEndCount = $i -1;
  if($TFM_LimitPageEndCount != $pageNum_rsRooms) {
    printf('<a href="'."%s?pageNum_rsRooms=%d%s", $currentPage, $TFM_LimitPageEndCount, $queryString_rsRooms.'">');
    echo "$i</a>";
  }else{
    echo "[<b>$i</b>]";
  }
if($i != $TFM_endLink) echo("&nbsp;");}
?>
                <?php if ($pageNum_rsRooms < $totalPages_rsRooms) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_rsRooms=%d%s", $currentPage, min($totalPages_rsRooms, $pageNum_rsRooms + 1), $queryString_rsRooms); ?>">Next <strong>></strong></a> <a href="<?php printf("%s?pageNum_rsRooms=%d%s", $currentPage, $totalPages_rsRooms, $queryString_rsRooms); ?>">Last <strong>>></strong></a>
                <?php } // Show if not last page ?> </td>
            </tr>
            <?php } // Show if recordset not empty ?>
            <?php if ($totalRows_rsRooms == 0) { // Show if recordset empty ?>
            <tr>
                <td height="55" align="center" valign="middle"><strong style="color:#FF0000;">No record found. </strong></td>
            </tr>
            <?php } // Show if recordset empty ?>
          </table>
</form>
</body>
</head>
</html>