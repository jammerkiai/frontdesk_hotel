<?php
require "../../../ajax/config/config.inc.php";

function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = addslashes($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

if ( isset ( $_POST['cmdAdd']) && $_POST['cmdAdd'] != '' ) {
	$rsAmenityExist = mysql_query("SELECT * FROM amenities WHERE amenity_description = '".addslashes(trim($_POST['amenity']))."'");
	$rownum_rsAmenityExists = mysql_num_rows($rsAmenityExist);
	if ($rownum_rsAmenityExists == 0) {
		$insertSQL = sprintf("INSERT INTO amenities (amenity_description) VALUES (%s)",GetSQLValueString($_POST['amenity'], "text"));
		$Result1 = mysql_query($insertSQL) or die(mysql_error());
	} 
	else {
		echo "<script>alert('Amenity already Exists.')</script>";
	}
}
if ( isset ( $_POST['cmdUpdate']) && $_POST['cmdUpdate'] != '' ) {
	$amenity_array = array();
	$amenity_array = explode("|",$_POST['amenity_array']);
	foreach($amenity_array as $key => $value) {
		if (isset($_POST['amenity_'.$value]) && $_POST['amenity_'.$value] != "") { 
			$updateSQL = sprintf("UPDATE amenities SET amenity_description=%s WHERE amenity_id=%s",
				GetSQLValueString($_POST['amenity_'.$value], "text"), GetSQLValueString($value, "int"));
  			$Result2 = mysql_query($updateSQL) or die(mysql_error());
		}
	}
} 
else if ( isset ( $_POST['cmdDelete']) && $_POST['cmdDelete'] != '' ) { //save operation
	for ( $cnt = 0; $cnt < sizeof($_POST["rec_id"]) ; $cnt ++ ) {
		if (isset($_POST["rec_id"][$cnt]) && $_POST["rec_id"][$cnt] != '' && $_POST["rec_id"][$cnt] != 'on') {
			$ids .= $_POST["rec_id"][$cnt];
			if ($cnt < sizeof($_POST["rec_id"]) - 1) {
				$ids .= ',';
			}
		}
	}
	if (is_array($_POST["rec_id"])){
		header('Location: amenity_delete.php?id='.$ids) ;
	}
}
else if (isset ( $_POST['cmdSearch']) && $_POST['cmdSearch'] != '' && isset($_POST['search'])) {
	$param_search = " AND (amenity_description LIKE '".addslashes($_POST['search'])."%' OR amenity_description LIKE '%".addslashes($_POST['search'])."%') ";
}

$maxRows_rsAmenity = 10;
$pageNum_rsAmenity = 0;
if (isset($_GET['pageNum_rsAmenity'])) {
  $pageNum_rsAmenity = $_GET['pageNum_rsAmenity'];
}
$startRow_rsAmenity = $pageNum_rsAmenity * $maxRows_rsAmenity;
$param_rsAmenity = " WHERE 1=1 ".$param_search;

//$query_rsAmenity = sprintf("select * from rooms %s ORDER BY door_name %s", $param_rsAmenity, $sortDate);
$query_rsAmenity = sprintf("select * from amenities %s ", $param_rsAmenity);
$query_limit_rsAmenity = sprintf("%s LIMIT %d, %d", $query_rsAmenity, $startRow_rsAmenity, $maxRows_rsAmenity);
$rsAmenity = mysql_query($query_limit_rsAmenity) or die(mysql_error());
$row_rsAmenity = mysql_fetch_assoc($rsAmenity);

if (isset($_GET['totalRows_rsAmenity'])) {
  $totalRows_rsAmenity = $_GET['totalRows_rsAmenity'];
} else {
  $all_rsAmenity = mysql_query($query_rsAmenity);
  $totalRows_rsAmenity = mysql_num_rows($all_rsAmenity);
}
$totalPages_rsAmenity = ceil($totalRows_rsAmenity/$maxRows_rsAmenity)-1;

$queryString_rsAmenity = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_rsAmenity") == false && 
        stristr($param, "totalRows_rsAmenity") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_rsAmenity = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_rsAmenity = sprintf("&totalRows_rsAmenity=%d%s", $totalRows_rsAmenity, $queryString_rsAmenity);

$TFM_LimitLinksEndCount = 9;
$TFM_temp = $pageNum_rsAmenity + 1;
$TFM_startLink = max(1,$TFM_temp - intval($TFM_LimitLinksEndCount/2));
$TFM_temp = $TFM_startLink + $TFM_LimitLinksEndCount - 1;
$TFM_endLink = min($TFM_temp, $totalPages_rsAmenity + 1);
if($TFM_endLink != $TFM_temp) $TFM_startLink = max(1,$TFM_endLink - $TFM_LimitLinksEndCount + 1);

?>
<html>
<head>
<title>Amenity</title>
<script type="text/javascript" src="../../../js/custom.js"></script>

<link href="../../../css/admin.css" rel="stylesheet" type="text/css">
</head>
<body>

<form name="form1" method="post" action="amenity.php">
<table width="80%" border="0" align="center" cellpadding="3" cellspacing="2">
            <?php if (isset($_REQUEST['strMsg']) && $_REQUEST['strMsg'] != '') { ?>
            <tr>
              <td valign="middle"><div class="prompt" style="color:#009900; font-size:12px;"><?php echo $_REQUEST['strMsg'] ; ?></div></td>
            </tr>
            <?php } ?>
            <tr>
              <td valign="top" ><div style="float:left; width:30%"><p><font style="font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; color:#678197;">AMENITIES</font></p>
              </div><div style="float:left; width:70%;"><p align="right"><font style="font:13px Arial,Verdana,  Helvetica, sans-serif; color:#666666">Amenity:</font>&nbsp;<input type"text" name="search" id="search" class="textbox-search-style">&nbsp;<input name="cmdSearch" type="submit" id="cmdSearch" value="Search" class="buttons" style="width:70px;" />&nbsp;<input name="cmdAdd" type="submit" class="buttons" id="cmdAdd" style="width:70px;" onClick="YY_checkform('form1','amenity','#q','0','Field Amenity is required.');return document.MM_returnValue" value="Add New" />&nbsp;<input name="cmdUpdate" type="submit" id="cmdUpdate" value="Update" class="buttons" style="width:70px;" />&nbsp;<input name="cmdDelete" type="submit" id="cmdDelete" onClick="GP_popupConfirmMsg('You are about to delete a record(s). Are you sure you want to continue?');return document.MM_returnValue" value="Delete Checked Items" class="buttons" /></p></div></td>
			</td>
            </tr>
            <tr>
              <td><table width="100%" border="0" cellspacing="0" cellpadding="4" class="tablesorter">
				<thead>
                <tr class="bgHeader">
                  <td align="center" valign="top" width="5%"><input name="rec_id[]" type="checkbox" class="options" id="rec_id[]" onClick="toggleChecked(this)"></td>
                  <td align="left" valign="middle" width="3%"><strong style="color:#678197;">ID</strong></td>
				  <td align="left" width="90%"><strong style="color:#678197;">Amenity</strong></td>
                </tr></thead>
                <?php
					$amenityArray = ""; 
					if ($totalRows_rsAmenity > 0) { $bgNumber = 1; // Show if recordset not empty ?>
                <?php do { 
					$bgNumber++; 
					$bgResult = $bgNumber % 2;
					$amenityArray .= $row_rsAmenity['amenity_id']."|";
				?>
                <tr <?php if ($bgResult > 0) echo 'bgcolor="#FAFAFA"';
					else echo "bgcolor=#F8FFFF"?>>
                  <td align="center" valign="top" class="divider"><input name="rec_id[]" type="checkbox" class="options" id="rec_id[]" onClick="toggleController(this)" value="<?php echo $row_rsAmenity['amenity_id']; ?>"></td>
                  <td align="left"><?php echo $row_rsAmenity['amenity_id']; ?></td>
				  <td align="left"><input type="text" name="amenity_<?=$row_rsAmenity['amenity_id']?>" value="<?php echo $row_rsAmenity['amenity_description']; ?>" class="textbox-style"></td>
                </tr>
                <?php } while ($row_rsAmenity = mysql_fetch_assoc($rsAmenity)); ?>
                <?php } // Show if recordset not empty ?>
				<tr><td>&nbsp;</td><td><input type="hidden" name="amenity_array" value="<?=substr($amenityArray,0,-1)?>">&nbsp;</td><td><input name="amenity" id="amenity" type="text" class="textbox-style" ><script>document.getElementById('amenity').focus()</script></td></tr>
              </table></td>
            </tr>
			<tr><td colspan="4">&nbsp;</td></tr>
            <?php if ($totalRows_rsAmenity > 0) { // Show if recordset not empty ?>
            <tr>
              <td align="right" valign="middle"><?php if ($pageNum_rsAmenity > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_rsAmenity=%d%s", $currentPage, 0, $queryString_rsAmenity); ?>"><strong><<</strong> First</a> <a href="<?php printf("%s?pageNum_rsAmenity=%d%s", $currentPage, max(0, $pageNum_rsAmenity - 1), $queryString_rsAmenity); ?>"><strong><</strong> Prev</a>
                <?php } // Show if not first page ?>
                <?php
for ($i = $TFM_startLink; $i <= $TFM_endLink; $i++) {
  $TFM_LimitPageEndCount = $i -1;
  if($TFM_LimitPageEndCount != $pageNum_rsAmenity) {
    printf('<a href="'."%s?pageNum_rsAmenity=%d%s", $currentPage, $TFM_LimitPageEndCount, $queryString_rsAmenity.'">');
    echo "$i</a>";
  }else{
    echo "[<b>$i</b>]";
  }
if($i != $TFM_endLink) echo("&nbsp;");}
?>
                <?php if ($pageNum_rsAmenity < $totalPages_rsAmenity) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_rsAmenity=%d%s", $currentPage, min($totalPages_rsAmenity, $pageNum_rsAmenity + 1), $queryString_rsAmenity); ?>">Next <strong>></strong></a> <a href="<?php printf("%s?pageNum_rsAmenity=%d%s", $currentPage, $totalPages_rsAmenity, $queryString_rsAmenity); ?>">Last <strong>>></strong></a>
                <?php } // Show if not last page ?> </td>
            </tr>
            <?php } // Show if recordset not empty ?>
            <?php if ($totalRows_rsAmenity == 0) { // Show if recordset empty ?>
            <tr>
                <td height="55" align="center" valign="middle"><strong style="color:#FF0000;">No record found. </strong></td>
            </tr>
            <?php } // Show if recordset empty ?>
          </table>
</form>
</body>
</head>
</html>