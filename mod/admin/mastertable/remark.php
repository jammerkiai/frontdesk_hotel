<?php
require "../../../ajax/config/config.inc.php";

function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = addslashes($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

if ( isset ( $_POST['cmdAdd']) && $_POST['cmdAdd'] != '' ) {
	$rsRemarkExist = mysql_query("SELECT * FROM remarks WHERE remark_text = '".addslashes(trim($_POST['remark']))."'");
	$rownum_rsRemarkExists = mysql_num_rows($rsRemarkExist);
	if ($rownum_rsRemarkExists == 0) {
		$insertSQL = sprintf("INSERT INTO remarks (remark_text,remark_classification) VALUES (%s,%s)",GetSQLValueString($_POST['remark_text'], "text"),GetSQLValueString($_POST['remark_classification'], "text"));
		$Result1 = mysql_query($insertSQL) or die(mysql_error());
	} 
	else {
		echo "<script>alert('Remark Text already Exists.')</script>";
	}
}
if ( isset ( $_POST['cmdUpdate']) && $_POST['cmdUpdate'] != '' ) {
	$remark_array = array();
	$remark_array = explode("|",$_POST['remark_array']);
	foreach($remark_array as $key => $value) {
		if (isset($_POST['remark_text_'.$value]) && $_POST['remark_text_'.$value] != "" && isset($_POST['remark_classification_'.$value]) && $_POST['remark_classification_'.$value] != "") { 
			$updateSQL = sprintf("UPDATE remarks SET remark_text=%s, remark_classification=%s WHERE remark_id=%s",
				GetSQLValueString($_POST['remark_text_'.$value], "text"), GetSQLValueString($_POST['remark_classification_'.$value], "text"), GetSQLValueString($value, "int"));
  			$Result2 = mysql_query($updateSQL) or die(mysql_error());
		}
	}
} 
else if ( isset ( $_POST['cmdDelete']) && $_POST['cmdDelete'] != '' ) { //save operation
	for ( $cnt = 0; $cnt < sizeof($_POST["rec_id"]) ; $cnt ++ ) {
		if (isset($_POST["rec_id"][$cnt]) && $_POST["rec_id"][$cnt] != '' && $_POST["rec_id"][$cnt] != 'on') {
			$ids .= $_POST["rec_id"][$cnt];
			if ($cnt < sizeof($_POST["rec_id"]) - 1) {
				$ids .= ',';
			}
		}
	}
	if (is_array($_POST["rec_id"])){
		header('Location: remark_delete.php?id='.$ids) ;
	}
}
else if (isset ( $_POST['cmdSearch']) && $_POST['cmdSearch'] != '' && isset($_POST['search'])) {
	$param_search = " AND (remark_text LIKE '".addslashes($_POST['search'])."%' OR remark_text LIKE '%".addslashes($_POST['search'])."%') ";
}

$maxRows_rsRemark = 10;
$pageNum_rsRemark = 0;
if (isset($_GET['pageNum_rsRemark'])) {
  $pageNum_rsRemark = $_GET['pageNum_rsRemark'];
}
$startRow_rsRemark = $pageNum_rsRemark * $maxRows_rsRemark;
$param_rsRemark = " WHERE 1=1 ".$param_search;

//$query_rsRemark = sprintf("select * from rooms %s ORDER BY door_name %s", $param_rsRemark, $sortDate);
$query_rsRemark = sprintf("select * from remarks %s ", $param_rsRemark);
$query_limit_rsRemark = sprintf("%s LIMIT %d, %d", $query_rsRemark, $startRow_rsRemark, $maxRows_rsRemark);
$rsRemark = mysql_query($query_limit_rsRemark) or die(mysql_error());
$row_rsRemark = mysql_fetch_assoc($rsRemark);

if (isset($_GET['totalRows_rsRemark'])) {
  $totalRows_rsRemark = $_GET['totalRows_rsRemark'];
} else {
  $all_rsRemark = mysql_query($query_rsRemark);
  $totalRows_rsRemark = mysql_num_rows($all_rsRemark);
}
$totalPages_rsRemark = ceil($totalRows_rsRemark/$maxRows_rsRemark)-1;

$queryString_rsRemark = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_rsRemark") == false && 
        stristr($param, "totalRows_rsRemark") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_rsRemark = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_rsRemark = sprintf("&totalRows_rsRemark=%d%s", $totalRows_rsRemark, $queryString_rsRemark);

$TFM_LimitLinksEndCount = 9;
$TFM_temp = $pageNum_rsRemark + 1;
$TFM_startLink = max(1,$TFM_temp - intval($TFM_LimitLinksEndCount/2));
$TFM_temp = $TFM_startLink + $TFM_LimitLinksEndCount - 1;
$TFM_endLink = min($TFM_temp, $totalPages_rsRemark + 1);
if($TFM_endLink != $TFM_temp) $TFM_startLink = max(1,$TFM_endLink - $TFM_LimitLinksEndCount + 1);

?>
<html>
<head>
<title>Remark</title>
<script type="text/javascript" src="../../../js/custom.js"></script>

<link href="../../../css/admin.css" rel="stylesheet" type="text/css">
</head>
<body>

<form name="form1" method="post" action="remark.php">
<table width="80%" border="0" align="center" cellpadding="3" cellspacing="2">
            <?php if (isset($_REQUEST['strMsg']) && $_REQUEST['strMsg'] != '') { ?>
            <tr>
              <td valign="middle"><div class="prompt" style="color:#009900; font-size:12px;"><?php echo $_REQUEST['strMsg'] ; ?></div></td>
            </tr>
            <?php } ?>
            <tr>
              <td valign="top" ><div style="float:left; width:30%"><p><font style="font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; color:#678197;">REMARK</font></p>
              </div><div style="float:left; width:70%;"><p align="right"><font style="font:13px Arial,Verdana,  Helvetica, sans-serif; color:#666666">Remark:</font>&nbsp;<input type"text" name="search" id="search" class="textbox-search-style">&nbsp;<input name="cmdSearch" type="submit" id="cmdSearch" value="Search" class="buttons" style="width:70px;" />&nbsp;<input name="cmdAdd" type="submit" class="buttons" id="cmdAdd" style="width:70px;" onClick="YY_checkform('form1','remark_text','#q','0','Field Remark Text is required.','remark_classification','#1_9','1','Field Remark Classification is required and must be a number.');return document.MM_returnValue" value="Add New" />&nbsp;<input name="cmdUpdate" type="submit" id="cmdUpdate" value="Update" class="buttons" style="width:70px;" />&nbsp;<input name="cmdDelete" type="submit" id="cmdDelete" onClick="GP_popupConfirmMsg('You are about to delete a record(s). Are you sure you want to continue?');return document.MM_returnValue" value="Delete Checked Items" class="buttons" /></p></div></td>
			</td>
            </tr>
            <tr>
              <td><table width="100%" border="0" cellspacing="0" cellpadding="4" class="tablesorter">
				<thead>
                <tr class="bgHeader">
                  <td align="center" valign="top" width="5%"><input name="rec_id[]" type="checkbox" class="options" id="rec_id[]" onClick="toggleChecked(this)"></td>
                  <td align="left" valign="middle" width="3%"><strong style="color:#678197;">ID</strong></td>
				  <td align="left" width="40%"><strong style="color:#678197;">Remark Text</strong></td>
				  <td align="left" width="57%"><strong style="color:#678197;">Remark Classification</strong></td>
                </tr></thead>
                <?php
					$remarkArray = ""; 
					if ($totalRows_rsRemark > 0) { $bgNumber = 1; // Show if recordset not empty ?>
                <?php do { 
					$bgNumber++; 
					$bgResult = $bgNumber % 2;
					$remarkArray .= $row_rsRemark['remark_id']."|";
				?>
                <tr <?php if ($bgResult > 0) echo 'bgcolor="#FAFAFA"';
					else echo "bgcolor=#F8FFFF"?>>
                  <td align="center" valign="top" class="divider"><input name="rec_id[]" type="checkbox" class="options" id="rec_id[]" onClick="toggleController(this)" value="<?php echo $row_rsRemark['remark_id']; ?>"></td>
                  <td align="left"><?php echo $row_rsRemark['remark_id']; ?></td>
				  <td align="left"><input type="text" name="remark_text_<?=$row_rsRemark['remark_id']?>" value="<?php echo $row_rsRemark['remark_text']; ?>" class="textbox-style"></td>
				  <td align="left"><input type="text" name="remark_classification_<?=$row_rsRemark['remark_id']?>" value="<?php echo ($row_rsRemark['remark_classification'] ? $row_rsRemark['remark_classification'] : "none"); ?>" class="textbox-style"></td>
                </tr>
                <?php } while ($row_rsRemark = mysql_fetch_assoc($rsRemark)); ?>
                <?php } // Show if recordset not empty ?>
				<tr><td>&nbsp;</td><td><input type="hidden" name="remark_array" value="<?=substr($remarkArray,0,-1)?>">&nbsp;</td><td><input name="remark_text" id="remark_text" type="text" class="textbox-style" ><script>document.getElementById('remark_text').focus()</script></td><td><input name="remark_classification" id="remark_classification" type="text" class="textbox-style" ></td></tr>
              </table></td>
            </tr>
			<tr><td colspan="4">&nbsp;</td></tr>
            <?php if ($totalRows_rsRemark > 0) { // Show if recordset not empty ?>
            <tr>
              <td align="right" valign="middle"><?php if ($pageNum_rsRemark > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_rsRemark=%d%s", $currentPage, 0, $queryString_rsRemark); ?>"><strong><<</strong> First</a> <a href="<?php printf("%s?pageNum_rsRemark=%d%s", $currentPage, max(0, $pageNum_rsRemark - 1), $queryString_rsRemark); ?>"><strong><</strong> Prev</a>
                <?php } // Show if not first page ?>
                <?php
for ($i = $TFM_startLink; $i <= $TFM_endLink; $i++) {
  $TFM_LimitPageEndCount = $i -1;
  if($TFM_LimitPageEndCount != $pageNum_rsRemark) {
    printf('<a href="'."%s?pageNum_rsRemark=%d%s", $currentPage, $TFM_LimitPageEndCount, $queryString_rsRemark.'">');
    echo "$i</a>";
  }else{
    echo "[<b>$i</b>]";
  }
if($i != $TFM_endLink) echo("&nbsp;");}
?>
                <?php if ($pageNum_rsRemark < $totalPages_rsRemark) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_rsRemark=%d%s", $currentPage, min($totalPages_rsRemark, $pageNum_rsRemark + 1), $queryString_rsRemark); ?>">Next <strong>></strong></a> <a href="<?php printf("%s?pageNum_rsRemark=%d%s", $currentPage, $totalPages_rsRemark, $queryString_rsRemark); ?>">Last <strong>>></strong></a>
                <?php } // Show if not last page ?> </td>
            </tr>
            <?php } // Show if recordset not empty ?>
            <?php if ($totalRows_rsRemark == 0) { // Show if recordset empty ?>
            <tr>
                <td height="55" align="center" valign="middle"><strong style="color:#FF0000;">No record found. </strong></td>
            </tr>
            <?php } // Show if recordset empty ?>
          </table>
</form>
</body>
</head>
</html>