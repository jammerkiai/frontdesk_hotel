<?php
require "../../../ajax/config/config.inc.php";

function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = addslashes($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

if ( isset ( $_POST['cmdAdd']) && $_POST['cmdAdd'] != '' ) {
	$rsFoodCategoryExist = mysql_query("SELECT * FROM food_categories WHERE food_category_name = '".addslashes(trim($_POST['amenity']))."'");
	$rownum_rsFoodCategoryExists = mysql_num_rows($rsFoodCategoryExist);
	if ($rownum_rsFoodCategoryExists == 0) {
		$insertSQL = sprintf("INSERT INTO food_categories (food_category_name) VALUES (%s)",GetSQLValueString($_POST['food_category_name'], "text"));
		$Result1 = mysql_query($insertSQL) or die(mysql_error());
	} 
	else {
		echo "<script>alert('Food Category Name already Exists.')</script>";
	}
}
if ( isset ( $_POST['cmdUpdate']) && $_POST['cmdUpdate'] != '' ) {
	$food_category_array = array();
	$food_category_array = explode("|",$_POST['food_category_array']);
	foreach($food_category_array as $key => $value) {
		if (isset($_POST['food_category_name_'.$value]) && $_POST['food_category_name_'.$value] != "") { 
			$updateSQL = sprintf("UPDATE food_categories SET food_category_name=%s WHERE food_category_id=%s",
				GetSQLValueString($_POST['food_category_name_'.$value], "text"), GetSQLValueString($value, "int"));
  			$Result2 = mysql_query($updateSQL) or die(mysql_error());
		}
	}
} 
else if ( isset ( $_POST['cmdDelete']) && $_POST['cmdDelete'] != '' ) { //save operation
	for ( $cnt = 0; $cnt < sizeof($_POST["rec_id"]) ; $cnt ++ ) {
		if (isset($_POST["rec_id"][$cnt]) && $_POST["rec_id"][$cnt] != '' && $_POST["rec_id"][$cnt] != 'on') {
			$ids .= $_POST["rec_id"][$cnt];
			if ($cnt < sizeof($_POST["rec_id"]) - 1) {
				$ids .= ',';
			}
		}
	}
	if (is_array($_POST["rec_id"])){
		header('Location: food_category_delete.php?id='.$ids) ;
	}
}
else if (isset ( $_POST['cmdSearch']) && $_POST['cmdSearch'] != '' && isset($_POST['search'])) {
	$param_search = " AND (food_category_name LIKE '".addslashes($_POST['search'])."%' OR food_category_name LIKE '%".addslashes($_POST['search'])."%') ";
}

$maxRows_rsFoodCategory = 10;
$pageNum_rsFoodCategory = 0;
if (isset($_GET['pageNum_rsFoodCategory'])) {
  $pageNum_rsFoodCategory = $_GET['pageNum_rsFoodCategory'];
}
$startRow_rsFoodCategory = $pageNum_rsFoodCategory * $maxRows_rsFoodCategory;
$param_rsFoodCategory = " WHERE 1=1 ".$param_search;

//$query_rsFoodCategory = sprintf("select * from rooms %s ORDER BY door_name %s", $param_rsFoodCategory, $sortDate);
$query_rsFoodCategory = sprintf("select * from food_categories %s ", $param_rsFoodCategory);
$query_limit_rsFoodCategory = sprintf("%s LIMIT %d, %d", $query_rsFoodCategory, $startRow_rsFoodCategory, $maxRows_rsFoodCategory);
$rsFoodCategory = mysql_query($query_limit_rsFoodCategory) or die(mysql_error());
$row_rsFoodCategory = mysql_fetch_assoc($rsFoodCategory);

if (isset($_GET['totalRows_rsFoodCategory'])) {
  $totalRows_rsFoodCategory = $_GET['totalRows_rsFoodCategory'];
} else {
  $all_rsFoodCategory = mysql_query($query_rsFoodCategory);
  $totalRows_rsFoodCategory = mysql_num_rows($all_rsFoodCategory);
}
$totalPages_rsFoodCategory = ceil($totalRows_rsFoodCategory/$maxRows_rsFoodCategory)-1;

$queryString_rsFoodCategory = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_rsFoodCategory") == false && 
        stristr($param, "totalRows_rsFoodCategory") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_rsFoodCategory = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_rsFoodCategory = sprintf("&totalRows_rsFoodCategory=%d%s", $totalRows_rsFoodCategory, $queryString_rsFoodCategory);

$TFM_LimitLinksEndCount = 9;
$TFM_temp = $pageNum_rsFoodCategory + 1;
$TFM_startLink = max(1,$TFM_temp - intval($TFM_LimitLinksEndCount/2));
$TFM_temp = $TFM_startLink + $TFM_LimitLinksEndCount - 1;
$TFM_endLink = min($TFM_temp, $totalPages_rsFoodCategory + 1);
if($TFM_endLink != $TFM_temp) $TFM_startLink = max(1,$TFM_endLink - $TFM_LimitLinksEndCount + 1);

?>
<html>
<head>
<title>Food Category</title>
<script type="text/javascript" src="../../../js/custom.js"></script>

<link href="../../../css/admin.css" rel="stylesheet" type="text/css">
</head>
<body>

<form name="form1" method="post" action="food_category.php">
<table width="80%" border="0" align="center" cellpadding="3" cellspacing="2">
            <?php if (isset($_REQUEST['strMsg']) && $_REQUEST['strMsg'] != '') { ?>
            <tr>
              <td valign="middle"><div class="prompt" style="color:#009900; font-size:12px;"><?php echo $_REQUEST['strMsg'] ; ?></div></td>
            </tr>
            <?php } ?>
            <tr>
              <td valign="top" ><div style="float:left; width:30%"><p><font style="font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; color:#678197;">FOOD CATEGORY</font></p>
              </div><div style="float:left; width:70%;"><p align="right"><font style="font:13px Arial,Verdana,  Helvetica, sans-serif; color:#666666">Food Category:</font>&nbsp;<input type"text" name="search" id="search" class="textbox-search-style">&nbsp;<input name="cmdSearch" type="submit" id="cmdSearch" value="Search" class="buttons" style="width:70px;" />&nbsp;<input name="cmdAdd" type="submit" class="buttons" id="cmdAdd" style="width:70px;" onClick="YY_checkform('form1','food_category_name','#q','0','Field Food Category Name is required.');return document.MM_returnValue" value="Add New" />&nbsp;<input name="cmdUpdate" type="submit" id="cmdUpdate" value="Update" class="buttons" style="width:70px;" />&nbsp;<input name="cmdDelete" type="submit" id="cmdDelete" onClick="GP_popupConfirmMsg('You are about to delete a record(s). Are you sure you want to continue?');return document.MM_returnValue" value="Delete Checked Items" class="buttons" /></p></div></td>
			</td>
            </tr>
            <tr>
              <td><table width="100%" border="0" cellspacing="0" cellpadding="4" class="tablesorter">
				<thead>
                <tr class="bgHeader">
                  <td align="center" valign="top" width="5%"><input name="rec_id[]" type="checkbox" class="options" id="rec_id[]" onClick="toggleChecked(this)"></td>
                  <td align="left" valign="middle" width="3%"><strong style="color:#678197;">ID</strong></td>
				  <td align="left" width="90%"><strong style="color:#678197;">Amenity</strong></td>
                </tr></thead>
                <?php
					$FoodCatArray = ""; 
					if ($totalRows_rsFoodCategory > 0) { $bgNumber = 1; // Show if recordset not empty ?>
                <?php do { 
					$bgNumber++; 
					$bgResult = $bgNumber % 2;
					$FoodCatArray .= $row_rsFoodCategory['food_category_id']."|";
				?>
                <tr <?php if ($bgResult > 0) echo 'bgcolor="#FAFAFA"';
					else echo "bgcolor=#F8FFFF"?>>
                  <td align="center" valign="top" class="divider"><input name="rec_id[]" type="checkbox" class="options" id="rec_id[]" onClick="toggleController(this)" value="<?php echo $row_rsFoodCategory['food_category_id']; ?>"></td>
                  <td align="left"><?php echo $row_rsFoodCategory['food_category_id']; ?></td>
				  <td align="left"><input type="text" name="food_category_name_<?=$row_rsFoodCategory['food_category_id']?>" value="<?php echo $row_rsFoodCategory['food_category_name']; ?>" class="textbox-style"></td>
                </tr>
                <?php } while ($row_rsFoodCategory = mysql_fetch_assoc($rsFoodCategory)); ?>
                <?php } // Show if recordset not empty ?>
				<tr><td>&nbsp;</td><td><input type="hidden" name="food_category_array" value="<?=substr($FoodCatArray,0,-1)?>">&nbsp;</td><td><input name="food_category_name" id="food_category_name" type="text" class="textbox-style" ><script>document.getElementById('food_category_name').focus()</script></td></tr>
              </table></td>
            </tr>
			<tr><td colspan="4">&nbsp;</td></tr>
            <?php if ($totalRows_rsFoodCategory > 0) { // Show if recordset not empty ?>
            <tr>
              <td align="right" valign="middle"><?php if ($pageNum_rsFoodCategory > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_rsFoodCategory=%d%s", $currentPage, 0, $queryString_rsFoodCategory); ?>"><strong><<</strong> First</a> <a href="<?php printf("%s?pageNum_rsFoodCategory=%d%s", $currentPage, max(0, $pageNum_rsFoodCategory - 1), $queryString_rsFoodCategory); ?>"><strong><</strong> Prev</a>
                <?php } // Show if not first page ?>
                <?php
for ($i = $TFM_startLink; $i <= $TFM_endLink; $i++) {
  $TFM_LimitPageEndCount = $i -1;
  if($TFM_LimitPageEndCount != $pageNum_rsFoodCategory) {
    printf('<a href="'."%s?pageNum_rsFoodCategory=%d%s", $currentPage, $TFM_LimitPageEndCount, $queryString_rsFoodCategory.'">');
    echo "$i</a>";
  }else{
    echo "[<b>$i</b>]";
  }
if($i != $TFM_endLink) echo("&nbsp;");}
?>
                <?php if ($pageNum_rsFoodCategory < $totalPages_rsFoodCategory) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_rsFoodCategory=%d%s", $currentPage, min($totalPages_rsFoodCategory, $pageNum_rsFoodCategory + 1), $queryString_rsFoodCategory); ?>">Next <strong>></strong></a> <a href="<?php printf("%s?pageNum_rsFoodCategory=%d%s", $currentPage, $totalPages_rsFoodCategory, $queryString_rsFoodCategory); ?>">Last <strong>>></strong></a>
                <?php } // Show if not last page ?> </td>
            </tr>
            <?php } // Show if recordset not empty ?>
            <?php if ($totalRows_rsFoodCategory == 0) { // Show if recordset empty ?>
            <tr>
                <td height="55" align="center" valign="middle"><strong style="color:#FF0000;">No record found. </strong></td>
            </tr>
            <?php } // Show if recordset empty ?>
          </table>
</form>
</body>
</head>
</html>