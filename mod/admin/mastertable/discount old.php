<?php
require "../../../ajax/config/config.inc.php";

function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = addslashes($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

if ( isset ( $_POST['cmdAdd']) && $_POST['cmdAdd'] != '' ) {
	$rsDiscountExist = mysql_query("SELECT * FROM discounts WHERE discount_label = '".addslashes(trim($_POST['discount_label']))."'");
	$rownum_rsDiscountExists = mysql_num_rows($rsDiscountExist);
	if ($rownum_rsDiscountExists == 0) {
		$insertSQL = sprintf("INSERT INTO discounts (discount_label, discount_type, discount_percent) VALUES (%s,%s,%s)",GetSQLValueString($_POST['discount_label'], "text"),GetSQLValueString($_POST['discount_type'], "text"),GetSQLValueString($_POST['discount_percent'], "double"));
		$Result1 = mysql_query($insertSQL) or die(mysql_error());
	} 
	else {
		echo "<script>alert('Discount Label/Name already Exists.')</script>";
	}
}
if ( isset ( $_POST['cmdUpdate']) && $_POST['cmdUpdate'] != '' ) {
	$discount_array = array();
	$discount_array = explode("|",$_POST['discount_array']);
	foreach($discount_array as $key => $value) {
		if ((isset($_POST['discount_label_'.$value]) && $_POST['discount_label_'.$value] != "") && (isset($_POST['discount_percent_'.$value]) && $_POST['discount_percent_'.$value] != "")) {
			$updateSQL = sprintf("UPDATE discounts SET discount_label=%s, discount_type=%s, discount_percent=%s WHERE discount_id=%s",
				GetSQLValueString($_POST['discount_label_'.$value], "text"), 
				GetSQLValueString($_POST['discount_type_'.$value], "text"),
				GetSQLValueString($_POST['discount_percent_'.$value], "double"), 
				GetSQLValueString($value, "int"));
  			$Result2 = mysql_query($updateSQL) or die(mysql_error());
		}
	}
} 
else if ( isset ( $_POST['cmdDelete']) && $_POST['cmdDelete'] != '' ) { //save operation
	for ( $cnt = 0; $cnt < sizeof($_POST["rec_id"]) ; $cnt ++ ) {
		if (isset($_POST["rec_id"][$cnt]) && $_POST["rec_id"][$cnt] != '' && $_POST["rec_id"][$cnt] != 'on') {
			$ids .= $_POST["rec_id"][$cnt];
			if ($cnt < sizeof($_POST["rec_id"]) - 1) {
				$ids .= ',';
			}
		}
	}
	if (is_array($_POST["rec_id"])){
		header('Location: discount_delete.php?id='.$ids) ;
	}
}
else if (isset ( $_POST['cmdSearch']) && $_POST['cmdSearch'] != '' && isset($_POST['search'])) {
	$param_search = " AND (discount_label LIKE '".addslashes($_POST['search'])."%' OR discount_label LIKE '%".addslashes($_POST['search'])."%') ";
}

$maxRows_rsDiscount = 10;
$pageNum_rsDiscount = 0;
if (isset($_GET['pageNum_rsDiscount'])) {
  $pageNum_rsDiscount = $_GET['pageNum_rsDiscount'];
}
$startRow_rsDiscount = $pageNum_rsDiscount * $maxRows_rsDiscount;
$param_rsDiscount = " WHERE 1=1 ".$param_search;

//$query_rsDiscount = sprintf("select * from rooms %s ORDER BY door_name %s", $param_rsDiscount, $sortDate);
$query_rsDiscount = sprintf("select * from discounts %s ", $param_rsDiscount);
$query_limit_rsDiscount = sprintf("%s LIMIT %d, %d", $query_rsDiscount, $startRow_rsDiscount, $maxRows_rsDiscount);
$rsDiscount = mysql_query($query_limit_rsDiscount) or die(mysql_error());
$row_rsDiscount = mysql_fetch_assoc($rsDiscount);

if (isset($_GET['totalRows_rsDiscount'])) {
  $totalRows_rsDiscount = $_GET['totalRows_rsDiscount'];
} else {
  $all_rsDiscount = mysql_query($query_rsDiscount);
  $totalRows_rsDiscount = mysql_num_rows($all_rsDiscount);
}
$totalPages_rsDiscount = ceil($totalRows_rsDiscount/$maxRows_rsDiscount)-1;

$queryString_rsDiscount = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_rsDiscount") == false && 
        stristr($param, "totalRows_rsDiscount") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_rsDiscount = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_rsDiscount = sprintf("&totalRows_rsDiscount=%d%s", $totalRows_rsDiscount, $queryString_rsDiscount);

$TFM_LimitLinksEndCount = 9;
$TFM_temp = $pageNum_rsDiscount + 1;
$TFM_startLink = max(1,$TFM_temp - intval($TFM_LimitLinksEndCount/2));
$TFM_temp = $TFM_startLink + $TFM_LimitLinksEndCount - 1;
$TFM_endLink = min($TFM_temp, $totalPages_rsDiscount + 1);
if($TFM_endLink != $TFM_temp) $TFM_startLink = max(1,$TFM_endLink - $TFM_LimitLinksEndCount + 1);

$discount_type = array("Standard","Special");
?>
<html>
<head>
<title>Discount</title>
<script type="text/javascript" src="../../../js/custom.js"></script>

<link href="../../../css/admin.css" rel="stylesheet" type="text/css">
</head>
<body>

<form name="form1" method="post" action="discount.php">
<table width="80%" border="0" align="center" cellpadding="3" cellspacing="2">
            <?php if (isset($_REQUEST['strMsg']) && $_REQUEST['strMsg'] != '') { ?>
            <tr>
              <td valign="middle"><div class="prompt" style="color:#009900; font-size:12px;"><?php echo $_REQUEST['strMsg'] ; ?></div></td>
            </tr>
            <?php } ?>
            <tr>
              <td valign="top" ><div style="float:left; width:30%"><p><font style="font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; color:#678197;">DISCOUNT</font></p>
              </div><div style="float:left; width:70%;"><p align="right"><font style="font:13px Arial,Verdana,  Helvetica, sans-serif; color:#666666">Discount:</font>&nbsp;<input type"text" name="search" id="search" class="textbox-search-style">&nbsp;<input name="cmdSearch" type="submit" id="cmdSearch" value="Search" class="buttons" style="width:70px;" />&nbsp;<input name="cmdAdd" type="submit" class="buttons" id="cmdAdd" style="width:70px;" onClick="YY_checkform('form1','discount_label','#q','0','Field Discount Name is required.','discount_percent','#.001_100','1','Field Discount Percent\' is required and must be a number.');return document.MM_returnValue" value="Add New" />&nbsp;<input name="cmdUpdate" type="submit" id="cmdUpdate" value="Update" class="buttons" style="width:70px;" />&nbsp;<input name="cmdDelete" type="submit" id="cmdDelete" onClick="GP_popupConfirmMsg('You are about to delete a record(s). Are you sure you want to continue?');return document.MM_returnValue" value="Delete Checked Items" class="buttons" /></p></div></td>
			</td>
            </tr>
            <tr>
              <td><table width="100%" border="0" cellspacing="0" cellpadding="4" class="tablesorter">
				<thead>
                <tr class="bgHeader">
                  <td align="center" valign="top" width="4%"><input name="rec_id[]" type="checkbox" class="options" id="rec_id[]" onClick="toggleChecked(this)"></td>
                  <td align="left" valign="middle" width="3%"><strong style="color:#678197;">ID</strong></td>
				  <td align="left" width="31%"><strong style="color:#678197;">Discount</strong></td>
				  <td align="left" width="31%"><strong style="color:#678197;">Discount Type</strong></td>
				   <td align="left" width="31%"><strong style="color:#678197;">Percent</strong>&nbsp;<font style="font-size:10px;">(Decimal Format)</font></td>
                </tr></thead>
                <?php
					$amenityArray = ""; 
					if ($totalRows_rsDiscount > 0) { $bgNumber = 1; // Show if recordset not empty ?>
                <?php do { 
					$bgNumber++; 
					$bgResult = $bgNumber % 2;
					$discountArray .= $row_rsDiscount['discount_id']."|";
				?>
                <tr <?php if ($bgResult > 0) echo 'bgcolor="#FAFAFA"';
					else echo "bgcolor=#F8FFFF"?>>
                  <td align="center" valign="top" class="divider"><input name="rec_id[]" type="checkbox" class="options" id="rec_id[]" onClick="toggleController(this)" value="<?php echo $row_rsDiscount['discount_id']; ?>"></td>
                  <td align="left"><?php echo $row_rsDiscount['discount_id']; ?></td>
				  <td align="left"><input type="text" name="discount_label_<?=$row_rsDiscount['discount_id']?>" value="<?php echo $row_rsDiscount['discount_label']; ?>" class="textbox-style-2"></td>
				  <td align="left"><select name="discount_type_<?=$row_rsDiscount['discount_id']?>" class="textbox-style-2">
				  	<?php
						foreach($discount_type as $keyDT => $valueDT) {
							if ($row_rsDiscount['discount_type'] == $valueDT) echo '<option value="'.$valueDT.'" selected>'.$valueDT.'</option>';
							else echo '<option value="'.$valueDT.'">'.$valueDT.'</option>';
						}
					?>
				  </select></td>
				  <td align="left"><input type="text" name="discount_percent_<?=$row_rsDiscount['discount_id']?>" value="<?php echo $row_rsDiscount['discount_percent']; ?>" class="textbox-style-2" ></td>
                </tr>
                <?php } while ($row_rsDiscount = mysql_fetch_assoc($rsDiscount)); ?>
                <?php } // Show if recordset not empty ?>
				<tr><td>&nbsp;</td><td><input type="hidden" name="discount_array" value="<?=substr($discountArray,0,-1)?>">&nbsp;</td><td><input name="discount_label" id="discount_label" type="text" class="textbox-style-2" ><script>document.getElementById('discount_label').focus()</script></td><td><select name="discount_type" id="discount_type" class="textbox-style-2" >
				<option value="Standard">Standard</option><option value="Special">Special</option></select></td>
				<td><input name="discount_percent" id="discount_percent" type="text" class="textbox-style-2" ></td>
				</tr>
              </table></td>
            </tr>
			<tr><td colspan="4">&nbsp;</td></tr>
            <?php if ($totalRows_rsDiscount > 0) { // Show if recordset not empty ?>
            <tr>
              <td align="right" valign="middle"><?php if ($pageNum_rsDiscount > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_rsDiscount=%d%s", $currentPage, 0, $queryString_rsDiscount); ?>"><strong><<</strong> First</a> <a href="<?php printf("%s?pageNum_rsDiscount=%d%s", $currentPage, max(0, $pageNum_rsDiscount - 1), $queryString_rsDiscount); ?>"><strong><</strong> Prev</a>
                <?php } // Show if not first page ?>
                <?php
for ($i = $TFM_startLink; $i <= $TFM_endLink; $i++) {
  $TFM_LimitPageEndCount = $i -1;
  if($TFM_LimitPageEndCount != $pageNum_rsDiscount) {
    printf('<a href="'."%s?pageNum_rsDiscount=%d%s", $currentPage, $TFM_LimitPageEndCount, $queryString_rsDiscount.'">');
    echo "$i</a>";
  }else{
    echo "[<b>$i</b>]";
  }
if($i != $TFM_endLink) echo("&nbsp;");}
?>
                <?php if ($pageNum_rsDiscount < $totalPages_rsDiscount) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_rsDiscount=%d%s", $currentPage, min($totalPages_rsDiscount, $pageNum_rsDiscount + 1), $queryString_rsDiscount); ?>">Next <strong>></strong></a> <a href="<?php printf("%s?pageNum_rsDiscount=%d%s", $currentPage, $totalPages_rsDiscount, $queryString_rsDiscount); ?>">Last <strong>>></strong></a>
                <?php } // Show if not last page ?> </td>
            </tr>
            <?php } // Show if recordset not empty ?>
            <?php if ($totalRows_rsDiscount == 0) { // Show if recordset empty ?>
            <tr>
                <td height="55" align="center" valign="middle"><strong style="color:#FF0000;">No record found. </strong></td>
            </tr>
            <?php } // Show if recordset empty ?>
          </table>
</form>
</body>
</head>
</html>