<?php
require "../../../ajax/config/config.inc.php";

function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = addslashes($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if (isset($_GET['id']) && $_GET['id'] != "") {
	$rateId = trim($_GET['id']);
	$rsRate = mysql_query("SELECT * FROM rates_dow WHERE rate_id = ".$rateId);
	$row_rsRate = mysql_fetch_assoc($rsRate);
	if (isset($row_rsRate['dow'])) {
		$dow = explode("|",$row_rsRate['dow']);
	}
	else {
		$dow = array("","","","","","","");
	}
}
else {
	header("Location:rates.php");
}

if ((isset($_POST["save"])) && ($_POST["save"] == "Save")) {
	$rsRateExist = mysql_query("SELECT rate_name FROM rates_dow WHERE rate_name = '".trim($_POST['rate_name'])."' AND rate_id != ".trim($_POST['rate_id']));
	$rownum_rsRateExists = mysql_num_rows($rsRateExist);
	if ($rownum_rsRateExists == 0) {
	 	$dow="";
		if (isset($_POST['dow_monday'])) $dow.=$_POST['dow_monday']."|"; 
		if (isset($_POST['dow_tuesday'])) $dow.=$_POST['dow_tuesday']."|"; 
		if (isset($_POST['dow_wednesday'])) $dow.=$_POST['dow_wednesday']."|"; 
		if (isset($_POST['dow_thursday'])) $dow.=$_POST['dow_thursday']."|";
		if (isset($_POST['dow_friday'])) $dow.=$_POST['dow_friday']."|"; 
		if (isset($_POST['dow_saturday'])) $dow.=$_POST['dow_saturday']."|";
		if (isset($_POST['dow_sunday'])) $dow.=$_POST['dow_sunday']."|";
		$dow=substr($dow,0,-1);
  $updateSQL = sprintf("UPDATE rates_dow SET rate_name=%s, hour_start=%s, hour_end=%s, dow=%s, duration=%s, display=%s WHERE rate_id=%s",
                       GetSQLValueString($_POST['rate_name'], "text"),
					   GetSQLValueString($_POST['hour_start'], "int"),
					   GetSQLValueString($_POST['hour_end'], "int"),
					   GetSQLValueString($dow, "text"),
					   GetSQLValueString($_POST['duration'], "int"),
					   GetSQLValueString($_POST['display'], "text"),
					   GetSQLValueString($_POST['rate_id'], "int"));

  $Result1 = mysql_query($updateSQL) or die(mysql_error());

  $insertGoTo = "rates.php?strMsg=Record has been successfully updated.";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
 }
 else echo "<script>alert('* Rate Name already Exists.')</script>";
} 
elseif ((isset($_POST["back"])) && ($_POST["back"] == "Back")) {
	header("Location:rates.php");
}# end update validate

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Edit Rate</title>
<script type="text/javascript" src="../../../js/custom.js"></script>
<link href="../../../css/admin.css" rel="stylesheet" type="text/css" />
</head>

<body>
<form method="post" name="rates_edit" id="rates_edit" >
<table width="80%" border="0" align="center" cellpadding="3" cellspacing="2">
	<tr>
	  <td valign="top"><table border="0" align="left" cellpadding="2" cellspacing="2" class="tablesorter"> 
				<tr align="left" valign="top">
				  <td colspan="2" align="center" class="bgHeader"><strong>EDIT RATES</strong></td>
				</tr>      
                <tr align="left" valign="top">
                  <td align="left" width="15%">Rate Name  </td>
                  <td><input name="rate_name" type="text" class="textbox-style" id="rate_name" style="width:300px;" value="<?=$row_rsRate['rate_name']?>" /></td>
                </tr>
				<tr align="left" valign="top">
                  <td align="left">Hour Start</td>
                  <td><select name='hour_start' id='hour_start' style="width:200px;" class="textbox-style">
					<option value='0'></option>
					<option value='24' <?=($row_rsRate['hour_start'] == 24 ? "selected": "")?>>12AM</option>
					<option value='1' <?=($row_rsRate['hour_start'] == 1 ? "selected": "")?>>1AM</option>
					<option value='2' <?=($row_rsRate['hour_start'] == 2 ? "selected": "")?>>2AM</option>
					<option value='3' <?=($row_rsRate['hour_start'] == 3 ? "selected": "")?>>3AM</option>
					<option value='4' <?=($row_rsRate['hour_start'] == 4 ? "selected": "")?>>4AM</option>
					<option value='5' <?=($row_rsRate['hour_start'] == 5 ? "selected": "")?>>5AM</option>
					<option value='6' <?=($row_rsRate['hour_start'] == 6 ? "selected": "")?>>6AM</option>
					<option value='7' <?=($row_rsRate['hour_start'] == 7 ? "selected": "")?> >7AM</option>
					<option value='8' <?=($row_rsRate['hour_start'] == 8 ? "selected": "")?>>8AM</option>
					<option value='9' <?=($row_rsRate['hour_start'] == 9 ? "selected": "")?>>9AM</option>
					<option value='10' <?=($row_rsRate['hour_start'] == 10 ? "selected": "")?>>10AM</option>
					<option value='11' <?=($row_rsRate['hour_start'] == 11 ? "selected": "")?>>11AM</option>
					<option value='12' <?=($row_rsRate['hour_start'] == 12 ? "selected": "")?>>12PM</option>
					<option value='13' <?=($row_rsRate['hour_start'] == 13 ? "selected": "")?>>1PM</option>
					<option value='14' <?=($row_rsRate['hour_start'] == 14 ? "selected": "")?>>2PM</option>
					<option value='15' <?=($row_rsRate['hour_start'] == 15 ? "selected": "")?>>3PM</option>
					<option value='16' <?=($row_rsRate['hour_start'] == 16 ? "selected": "")?>>4PM</option>
					<option value='17' <?=($row_rsRate['hour_start'] == 17 ? "selected": "")?>>5PM</option>
					<option value='18' <?=($row_rsRate['hour_start'] == 18 ? "selected": "")?>>6PM</option>
					<option value='19' <?=($row_rsRate['hour_start'] == 19 ? "selected": "")?>>7PM</option>
					<option value='20' <?=($row_rsRate['hour_start'] == 20 ? "selected": "")?>>8PM</option>
					<option value='21' <?=($row_rsRate['hour_start'] == 21 ? "selected": "")?>>9PM</option>
					<option value='22' <?=($row_rsRate['hour_start'] == 22 ? "selected": "")?>>10PM</option>
					<option value='23' <?=($row_rsRate['hour_start'] == 23 ? "selected": "")?>>11PM</option>
					</select>
</td>
                </tr>
								<tr align="left" valign="top">
                  <td align="left">Hour Start</td>
                  <td><select name='hour_end' id='hour_end' style="width:200px;" class="textbox-style" >
					<option value='0'></option>
					<option value='24' <?=($row_rsRate['hour_end'] == 24 ? "selected": "")?>>12AM</option>
					<option value='1' <?=($row_rsRate['hour_end'] == 1 ? "selected": "")?>>1AM</option>
					<option value='2' <?=($row_rsRate['hour_end'] == 2 ? "selected": "")?>>2AM</option>
					<option value='3' <?=($row_rsRate['hour_end'] == 3 ? "selected": "")?>>3AM</option>
					<option value='4' <?=($row_rsRate['hour_end'] == 4 ? "selected": "")?>>4AM</option>
					<option value='5' <?=($row_rsRate['hour_end'] == 5 ? "selected": "")?>>5AM</option>
					<option value='6' <?=($row_rsRate['hour_end'] == 6 ? "selected": "")?>>6AM</option>
					<option value='7' <?=($row_rsRate['hour_end'] == 7 ? "selected": "")?> >7AM</option>
					<option value='8' <?=($row_rsRate['hour_end'] == 8 ? "selected": "")?>>8AM</option>
					<option value='9' <?=($row_rsRate['hour_end'] == 9 ? "selected": "")?>>9AM</option>
					<option value='10' <?=($row_rsRate['hour_end'] == 10 ? "selected": "")?>>10AM</option>
					<option value='11' <?=($row_rsRate['hour_end'] == 11 ? "selected": "")?>>11AM</option>
					<option value='12' <?=($row_rsRate['hour_end'] == 12 ? "selected": "")?>>12PM</option>
					<option value='13' <?=($row_rsRate['hour_end'] == 13 ? "selected": "")?>>1PM</option>
					<option value='14' <?=($row_rsRate['hour_end'] == 14 ? "selected": "")?>>2PM</option>
					<option value='15' <?=($row_rsRate['hour_end'] == 15 ? "selected": "")?>>3PM</option>
					<option value='16' <?=($row_rsRate['hour_end'] == 16 ? "selected": "")?>>4PM</option>
					<option value='17' <?=($row_rsRate['hour_end'] == 17 ? "selected": "")?>>5PM</option>
					<option value='18' <?=($row_rsRate['hour_end'] == 18 ? "selected": "")?>>6PM</option>
					<option value='19' <?=($row_rsRate['hour_end'] == 19 ? "selected": "")?>>7PM</option>
					<option value='20' <?=($row_rsRate['hour_end'] == 20 ? "selected": "")?>>8PM</option>
					<option value='21' <?=($row_rsRate['hour_end'] == 21 ? "selected": "")?>>9PM</option>
					<option value='22' <?=($row_rsRate['hour_end'] == 22 ? "selected": "")?>>10PM</option>
					<option value='23' <?=($row_rsRate['hour_end'] == 23 ? "selected": "")?>>11PM</option>
					</select>
</td>
                </tr>
				<tr align="left" valign="top">
                  <td align="left">Days of Week</td>
                  <td>&nbsp;<input type='checkbox' name='dow_monday' id='dow_monday' <?=(in_array("Monday", $dow) ? "checked" : "")?>  value='Monday'>Monday<br />&nbsp;<input type='checkbox' name='dow_tuesday' id='dow_tuesday' <?=(in_array("Tuesday", $dow) ? "checked" : "")?>  value='Tuesday'>Tuesday<br />&nbsp;<input type='checkbox' name='dow_wednesday' id='dow_wednesday' <?=(in_array("Wednesday", $dow) ? "checked" : "")?>  value='Wednesday'>Wednesday<br />&nbsp;<input type='checkbox' name='dow_thursday' id='dow_thursday' <?=(in_array("Thursday", $dow) ? "checked" : "")?>  value='Thursday'>Thursday<br />&nbsp;<input type='checkbox' name='dow_friday' id='dow_friday' <?=(in_array("Friday", $dow) ? "checked" : "")?>  value='Friday'>Friday<br />&nbsp;<input type='checkbox' name='dow_saturday' id='dow_saturday' <?=(in_array("Saturday", $dow) ? "checked" : "")?>  value='Saturday'>Saturday<br />&nbsp;<input type='checkbox' name='dow_sunday' id='dow_sunday' <?=(in_array("Sunday", $dow) ? "checked" : "")?>  value='Sunday'>Sunday<br /></td>
                </tr>
				<tr align="left" valign="top">
                  <td align="left">Duration</td>
                  <td><input type='text'  name='duration' id='duration' value="<?=$row_rsRate['duration']?>"></td>
				<tr align="left" valign="top">
                  <td align="left" width="15%">Display</td>
                  <td><input type='radio' name='display' id='display' <?=($row_rsRate['display']=="Yes" ? "checked" : "")?> value='Yes'>Yes&nbsp;<input type='radio' name='display' id='display' <?=($row_rsRate['display']=="No" ? "checked" : "")?>  value='No'>No</td>
                </tr>
 
                <tr align="left" valign="top">
                  <td><input type="hidden" name="rate_id" value="<?=$rateId?>"/></td>
                  <td align="right"><input name="save" type="submit" class="buttons" id="Save" value="Save" onclick="YY_checkform('rates_edit','display[0]','#q','2','Field Display is required.','display[1]','#q','2','Field Display is required.','rate_name','#q','0','Field Rate Name is required.','duration','#1_24','1','Field Duration is required and must be a number.','hour_start','#q','1','Field Hour Start is required.','hour_end','#q','1','Field Hour End is required.');return document.MM_returnValue" />
                  &nbsp;<input name="back" type="submit" id="Back" value="Back" class="buttons" />				                </td>
                </tr>
          </table></td>
	</tr>
</table>
</form>
</body>
</html>
