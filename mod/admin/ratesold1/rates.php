<?php
require "../../../ajax/config/config.inc.php";
require_once('ratefns.php');

if ($_POST['cmd'] == 'save') {
	$fields = '';
	foreach($_POST as $name => $value) {
		if( strpos($name, 'new_') !== false ) {
			$fieldName = str_replace('new_', '', $name);
			$fields .= ($fields=='') ? '' : ',';
			$fields .= $fieldName;
			$values .= ($values == '') ? '' : ',';
			$values .= "'$value'";
		}
	}
	$sql = "insert into rates ($fields) values ($values)";
	if (mysql_query($sql)) {
		header('location: rates.php');
	} else {
		$mesg = "Error encountered: " . mysql_error();
		$mesg.= "<br><a href='rates.php'>Back</a>";
		die($mesg);
	}
} elseif($_POST['cmd'] == 'delete') {
	if (isset($_POST['rateid'])) {
		if( is_array($_POST['rateid'])) {
			$rateid = implode(',',$_POST['rateid']); 
		} else {
			$rateid = $_POST['rateid'];
		}
		$sql = "delete from rates where rate_id in ($rateid)";
		if (!mysql_query($sql)) {
			$mesg = "Error encountered: " . mysql_error();
			$mesg.= "<br><a href='rates.php'>Back</a>";
			die($mesg);
		}
		header('location: rates.php');
	} else {
		$mesg = 'Please select at least one item.';
		$mesg.= "<br><a href='rates.php'>Back</a>";
		die($mesg);
	}
} elseif ($_POST['cmd'] == 'edit') {
	if(!isset($_POST['rateid'])) {
		$mesg = 'Please select one item.';
		$mesg.= "<br><a href='rates.php'>Back</a>";
		die($mesg);
	}
} elseif ($_POST['cmd'] == 'update') {
	$fields = '';
	foreach($_POST as $name => $value) {
		if( strpos($name, 'new_') !== false ) {
			$fieldName = str_replace('new_', '', $name);
			$fields .= ($fields=='') ? '' : ',';
			$fields .= " $fieldName='$value' ";
		}
	}
	$rate_id= $_POST['new_rate_id'];
	$sql = "update rates set $fields where rate_id='$rate_id'";
	mysql_query($sql);
	header('location: rates.php');
} elseif ($_POST['cmd'] == 'set rates') {
	if(isset($_POST['rateid'])) {
		$rateid=$_POST['rateid'];
		if(is_array($rateid)) {
			$rateid=$rateid[0];
		}
		header('location: roomrates.php?id=' . $rateid);
	} else {
		$mesg = 'Please select one item.';
		$mesg.= "<br><a href='rates.php'>Back</a>";
		die($mesg);
	}
}

?>
<script src="jquery.js"></script>
<link  type="text/css" rel="stylesheet" href="style.css" />
<form method="post" action="rates.php">

<?php
	echo "<div class='error'>$mesg</div>";
	if ( in_array($_POST['cmd'], array('new')) ) {
		echo buildButtons(array('save', 'cancel'));
		echo "<h2>New Rate Definition</h2>";
		echo formEntry();
	} elseif (in_array($_POST['cmd'], array('edit'))) {
		echo buildButtons(array('update', 'cancel'));
		echo "<h2>Edit Rate Definition</h2>";
		echo formEntry(getRecord($_POST['rateid']));
	} else {
		echo buildButtons(array('new', 'edit', 'delete', 'set rates'));
		echo "<h2>Rate Definitions</h2>";
		echo rateTable();
	}
?>
</form>
<!--//
javascript section
 //-->
<script>
<script>
$(document).ready(function(){
	$('#cbmaster').click(function(){
		$('.cbitem').attr('checked', $(this).attr('checked'));
	});
});
</script>