<?php
require "../../../ajax/config/config.inc.php";

function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = addslashes($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

$error = "";
$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["save"])) && ($_POST["save"] == "Save")) {
	$rsUserExist = mysql_query("SELECT username FROM users WHERE username = '".trim($_POST['username'])."'");
	$rownum_rsUserExists = mysql_num_rows($rsUserExist);
	if ($rownum_rsUserExists == 0) {
	  $insertSQL = sprintf("INSERT INTO users (username, userpass, fullname, group_id) VALUES (%s, %s, %s, %s)",
                       GetSQLValueString($_POST['username'], "text"),
					   GetSQLValueString($_POST['password'], "text"),
					   GetSQLValueString($_POST['fullname'], "text"),
					   GetSQLValueString($_POST['group'], "int"));

 	 $Result1 = mysql_query($insertSQL) or die(mysql_error());

 	 $insertGoTo = "user_add.php?strMsg=Record has been successfully added.";
	  if (isset($_SERVER['QUERY_STRING'])) {
 	   $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
 	   $insertGoTo .= $_SERVER['QUERY_STRING'];
 	 }
 	 header(sprintf("Location: %s", $insertGoTo));
  }
  else echo "<script>alert('* User Name already Exists.')</script>";
}
elseif ((isset($_POST["back"])) && ($_POST["back"] == "Back")) {
	header("Location:users.php");
} # end insert validate

$rsGroup = mysql_query("SELECT group_id, group_name FROM groups ORDER BY group_name");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Add User</title>
<script type="text/javascript" src="../../../js/custom.js"></script>
<link href="../../../css/admin.css" rel="stylesheet" type="text/css" />
</head>

<body>
<form method="post" name="room_add" id="room_add" >
<table width="80%" border="0" align="center" cellpadding="3" cellspacing="2">
    <?php if (isset($_REQUEST['strMsg']) && $_REQUEST['strMsg'] != '') { ?>
    <tr>
    	<td valign="middle"><div class="prompt" style="color:#009900; font-size:12px;"><?php echo $_REQUEST['strMsg'] ; ?></div></td>
    </tr>
    <?php } ?>
	<tr>
		<td valign="top">
			<table border="0" align="left" cellpadding="2" cellspacing="2" class="tablesorter"> 
				<tr align="left" valign="top"><td colspan="2" align="center" class="bgHeader"><strong>ADD USER</strong></td></tr>      
                <tr align="left" valign="top">
                  <td align="left" width="15%">Username</td>
                  <td><input name="username" type="text" class="textbox-style" id="username" style="width:300px;" /></td>
                </tr>
                <tr align="left" valign="top">
                  <td align="left" width="15%">Password</td>
                  <td><input name="password" type="password" class="textbox-style" id="password" style="width:300px;" /></td>
                </tr>
                <tr align="left" valign="top">
                  <td align="left" width="15%">Fullname</td>
                  <td><input name="fullname" type="text" class="textbox-style" id="fullname" style="width:300px;" /></td>
                </tr>
				<tr align="left" valign="top">
                  <td align="left">Group</td>
                  <td><select name="group" id="group" style="width:200px;" class="textbox-style">
				  		<option value="">[Choose]</option>
				  		<?php 
							while($row_rsGroup = mysql_fetch_assoc($rsGroup)) {
						?>
				  		<option value="<?=$row_rsGroup['group_id']?>"><?=$row_rsGroup['group_name']?></option>
						<?php }?>
						</select>				  </td>
                </tr>
 
                <tr align="left" valign="top">
                  <td>&nbsp;</td>
                  <td align="right"><input name="save" type="submit" id="Save" value="Save" class="buttons" onClick="YY_checkform('room_add','username','#q','0','Field Username is required.','fullname','#q','0','Field Fullname is required.','group','#q','1','Field Group is required.','password','#q','0','Field Password is required.');return document.MM_returnValue" />
                  &nbsp;<input name="back" type="submit" id="Back" value="Back" class="buttons" />				                </td>
                </tr>
          </table>
		</td>
	</tr>
</table>
</form>
</body>
</html>
