<?php
require "../../../ajax/config/config.inc.php";

function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = addslashes($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if (isset($_GET['id']) && $_GET['id'] != "") {
	$userId = trim($_GET['id']);
	$rsUser = mysql_query("SELECT * FROM users WHERE user_id = ".$userId);
	$row_rsUser = mysql_fetch_assoc($rsUser);
	$rsGroup = mysql_query("SELECT * FROM groups ORDER BY group_name");
}
else {
	header("Location:users.php");
}

if ((isset($_POST["save"])) && ($_POST["save"] == "Save")) {
	$rsUserExist = mysql_query("SELECT username FROM users WHERE username = '".trim($_POST['username'])."' AND user_id != ".trim($_POST['user_id']));
	$rownum_rsUserExists = mysql_num_rows($rsUserExist);
	if ($rownum_rsUserExists == 0) {
  $updateSQL = sprintf("UPDATE users SET username=%s, userpass=%s, fullname=%s, group_id=%s WHERE user_id=%s",
                       GetSQLValueString($_POST['username'], "text"),
					   GetSQLValueString($_POST['password'], "text"),
					   GetSQLValueString($_POST['fullname'], "text"),
					   GetSQLValueString($_POST['group_id'], "int"),
					   GetSQLValueString($_POST['user_id'], "int"));

  $Result1 = mysql_query($updateSQL) or die(mysql_error());

  $insertGoTo = "users.php?strMsg=Record has been successfully updated.";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
 }
 else echo "<script>alert('* Username already exists.')</script>";
} 
elseif ((isset($_POST["back"])) && ($_POST["back"] == "Back")) {
	header("Location:users.php");
}# end update validate

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Edit User</title>
<script type="text/javascript" src="../../../js/custom.js"></script>
<link href="../../../css/admin.css" rel="stylesheet" type="text/css" />
</head>

<body>
<form method="post" name="user_edit" >
<table width="80%" border="0" align="center" cellpadding="3" cellspacing="2">
	<tr>
		<td valign="top">
			<table border="0" align="left" cellpadding="2" cellspacing="2" class="tablesorter"> 
				<tr align="left" valign="top"><td colspan="2" align="center" class="bgHeader"><strong>EDIT USER</strong></td></tr>      
                <tr align="left" valign="top">
                  <td align="left" width="15%">UserName  </td>
                  <td><input name="username" type="text" class="textbox-style" id="username" style="width:300px;" value="<?=$row_rsUser['username']?>" /></td>
                </tr>
				<tr align="left" valign="top">
                  <td align="left" width="15%">Password</td>
                  <td><input name="password" type="password" class="textbox-style" id="password" style="width:300px;" value="<?=$row_rsUser['userpass']?>" /></td>
				</tr>
				<tr align="left" valign="top">
                  <td align="left">Fullname</td>
                  <td><input name="fullname" type="text" id="fullname" style="width:300px;" class="textbox-style" value="<?=$row_rsUser['fullname']?>" /></td>
                </tr>
				<tr align="left" valign="top">
                  <td align="left">Group</td>
                  <td><select name="group_id" id="group_id" style="width:200px;" class="textbox-style">
				  		<?php 
							while($row_rsGroup = mysql_fetch_assoc($rsGroup)) {
								if ($row_rsUser['group_id'] == $row_rsGroup['group_id']) $selected_user = "selected";
								else $selected_user = "";
						?>
				  		<option value="<?=$row_rsGroup['group_id']?>" <?=$selected_user?>><?=$row_rsGroup['group_name']?></option>
						<?php }?>
						</select>				  </td>
                </tr>
				 
                <tr align="left" valign="top">
                  <td>&nbsp;<input type="hidden" name="user_id" value="<?=trim($_GET['id'])?>"/></td>
                  <td align="right"><input name="save" type="submit" id="Save" value="Save" class="buttons" onClick="YY_checkform('user_edit','username','#q','0','Field Username is required.','fullname','#q','0','Field Fullname is required.','password','#q','0','Field Password is required.');return document.MM_returnValue" />
                  &nbsp;<input name="back" type="submit" id="back" value="Back" class="buttons" />				                </td>
                </tr>
          </table>
		</td>
	</tr>
</table>
</form>
</body>
</html>
