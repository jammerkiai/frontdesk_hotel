<?php
require "../../../ajax/config/config.inc.php";

if ( isset ( $_POST['cmdAdd']) && $_POST['cmdAdd'] == 'Add New' ) {
	header('Location: miscsales_add.php') ;
} 
else if ( isset ( $_POST['cmdDelete']) && $_POST['cmdDelete'] != '' ) { //save operation
	for ( $cnt = 0; $cnt < sizeof($_POST["rec_id"]) ; $cnt ++ ) {
		if (isset($_POST["rec_id"][$cnt]) && $_POST["rec_id"][$cnt] != '' && $_POST["rec_id"][$cnt] != 'on') {
			$ids .= $_POST["rec_id"][$cnt];
			if ($cnt < sizeof($_POST["rec_id"]) - 1) {
				$ids .= ',';
			}
		}
	}
	if (is_array($_POST["rec_id"])){
		header('Location: miscsales_delete.php?id='.$ids) ;
	}
}
else if (isset ( $_POST['cmdSearch']) && $_POST['cmdSearch'] != '' && isset($_POST['search'])) {
	$param_search = " AND (sas_description LIKE '".addslashes($_POST['search'])."%' OR sas_description LIKE '%".addslashes($_POST['search'])."%') ";
}

$maxRows_rsSas = 10;
$pageNum_rsSas = 0;
if (isset($_GET['pageNum_rsSas'])) {
  $pageNum_rsSas = $_GET['pageNum_rsSas'];
}
$startRow_rsSas = $pageNum_rsSas * $maxRows_rsSas;
$param_rsSas = " WHERE 1=1 ".$param_search;

//$query_rsSas = sprintf("select * from rooms %s ORDER BY door_name %s", $param_rsFnb, $sortDate);
$query_rsSas = sprintf("select * from sales_and_services %s ", $param_rsSas);
$query_limit_rsSas = sprintf("%s LIMIT %s, %s", $query_rsSas, $startRow_rsSas, $maxRows_rsSas);
$rsSas = mysql_query($query_limit_rsSas) or die(mysql_error() . $query_rsSas);

$row_rsSas = mysql_fetch_assoc($rsSas);

if (isset($_GET['totalRows_rsSas'])) {
  $totalRows_rsSas = $_GET['totalRows_rsSas'];
} else {
  $all_rsSas = mysql_query($query_rsSas);
  $totalRows_rsSas = mysql_num_rows($all_rsSas);
}
$totalPages_rsSas = ceil($totalRows_rsSas/$maxRows_rsSas)-1;

$queryString_rsSas = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_rsSas") == false && 
        stristr($param, "totalRows_rsSas") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_rsSas = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_rsSas = sprintf("&totalRows_rsSas=%d%s", $totalRows_rsSas, $queryString_rsSas);

$TFM_LimitLinksEndCount = 9;
$TFM_temp = $pageNum_rsSas + 1;
$TFM_startLink = max(1,$TFM_temp - intval($TFM_LimitLinksEndCount/2));
$TFM_temp = $TFM_startLink + $TFM_LimitLinksEndCount - 1;
$TFM_endLink = min($TFM_temp, $totalPages_rsSas + 1);
if($TFM_endLink != $TFM_temp) $TFM_startLink = max(1,$TFM_endLink - $TFM_LimitLinksEndCount + 1);


$SasCat = array();
$rsSasCat = mysql_query("SELECT * FROM sas_category ORDER BY sas_cat_name");
$rownum_rsSasCat = mysql_num_rows($rsSasCat);
if ($rownum_rsSasCat >0) {
	while($row_rsSasCat = mysql_fetch_assoc($rsSasCat)) {
		$SasCat[$row_rsSasCat['sas_cat_id']] = $row_rsSasCat['sas_cat_name'];
	}
}
?>
<html>
<head>
<title>SAS </title>
<script type="text/javascript" src="../../../js/custom.js"></script>
<link href="../../../css/admin.css" rel="stylesheet" type="text/css">
</head>
<body>
<form name="form1" method="post" action="">
<table width="80%" border="0" align="center" cellpadding="3" cellspacing="2">
            <?php if (isset($_REQUEST['strMsg']) && $_REQUEST['strMsg'] != '') { ?>
            <tr>
              <td valign="middle"><div class="prompt" style="color:#009900; font-size:12px;"><?php echo $_REQUEST['strMsg'] ; ?></div></td>
            </tr>
            <?php } ?>
            <tr>
              <td valign="top" ><div style="float:left; width:30%"><p><font style="font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; color:#678197;">SAS</font></p>
              </div><div style="float:left; width:70%;">
                <p align="right"><font style="font:13px Arial,Verdana,  Helvetica, sans-serif; color:#666666">SAS Description:</font>&nbsp;<input type"text" name="search" id="search" class="textbox-search-style">&nbsp;<input name="cmdSearch" type="submit" id="cmdSearch" value="Search" class="buttons" style="width:70px;" />&nbsp;<input name="cmdAdd" type="submit" id="cmdAdd" value="Add New" class="buttons" style="width:70px;" />&nbsp;<input name="cmdDelete" type="submit" id="cmdDelete" onClick="GP_popupConfirmMsg('You are about to delete a record(s). Are you sure you want to continue?');return document.MM_returnValue" value="Delete Checked Items" class="buttons" /></p></div></td>
			</td>
            </tr>
            <tr>
              <td><table width="100%" border="0" cellspacing="0" cellpadding="4" class="tablesorter">
				<thead>
                <tr class="bgHeader">
                  <td align="center" valign="top" width="5%"><input name="rec_id[]" type="checkbox" class="options" id="rec_id[]" onClick="toggleChecked(this)"></td>
				  <td align="left" width="35%"><strong style="color:#678197;">SAS Description</strong></td>
				  <td align="center" valign="middle" width="25%"><strong style="color:#678197;">SAS Category</strong></td>
				  <td align="center" valign="middle" width="12%"><strong style="color:#678197;">Price</strong></td>
				  <td align="center" valign="middle" width="20%"><strong style="color:#678197;">Action</strong></td>
                </tr></thead>
                <?php if ($totalRows_rsSas > 0) { $bgNumber = 1; // Show if recordset not empty ?>
                <?php do { 
					$bgNumber++; 
					$bgResult = $bgNumber % 2;
				?>
                <tr <?php if ($bgResult > 0) echo 'bgcolor="#FAFAFA"';
					else echo "bgcolor=#F8FFFF"?>>
                  <td align="center" valign="top" class="divider"><input name="rec_id[]" type="checkbox" class="options" id="rec_id[]" onClick="toggleController(this)" value="<?php echo $row_rsSas['sas_id']; ?>"></td>
				  <td align="left"><?php echo $row_rsSas['sas_description'];?></td>               
				  </td>
				  <td align="center" valign="top"><?php
				  	foreach($SasCat as $key => $value) {
						if ($key == $row_rsSas['sas_cat_id']) echo $value;
					}
				  ?></td>
				  <td align="center"><?=$row_rsSas['sas_amount']?></td>
				  <td align="center"><p><img src="../../../img/application_edit.png" align="bottom">&nbsp;<a href="../misc/miscsales_edit.php?id=<?php echo $row_rsSas['sas_id']; ?>">Edit</a></p></td>
                </tr>
                <?php } while ($row_rsSas = mysql_fetch_assoc($rsSas)); ?>
                <?php } // Show if recordset not empty ?>
              </table></td>
            </tr>
			<tr><td colspan="4">&nbsp;</td></tr>
            <?php if ($totalRows_rsSas > 0) { // Show if recordset not empty ?>
            <tr>
              <td align="right" valign="middle"><?php if ($pageNum_rsFnb > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_rsSas=%d%s", $currentPage, 0, $queryString_rsSas); ?>"><strong><<</strong> First</a> <a href="<?php printf("%s?pageNum_rsSas=%d%s", $currentPage, max(0, $pageNum_rsSas - 1), $queryString_rsSas); ?>"><strong><</strong> Prev</a>
                <?php } // Show if not first page ?>
                <?php
for ($i = $TFM_startLink; $i <= $TFM_endLink; $i++) {
  $TFM_LimitPageEndCount = $i -1;
  if($TFM_LimitPageEndCount != $pageNum_rsSas) {
    printf('<a href="'."%s?pageNum_rsSas=%d%s", $currentPage, $TFM_LimitPageEndCount, $queryString_rsSas.'">');
    echo "$i</a>";
  }else{
    echo "[<b>$i</b>]";
  }
if($i != $TFM_endLink) echo("&nbsp;");}
?>
                <?php if ($pageNum_rsFSas < $totalPages_rsSas) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_rsSas=%d%s", $currentPage, min($totalPages_rsSas, $pageNum_rsSas + 1), $queryString_rsSas); ?>">Next <strong>></strong></a> <a href="<?php printf("%s?pageNum_rsSas=%d%s", $currentPage, $totalPages_rsSas, $queryString_rsSas); ?>">Last <strong>>></strong></a>
                <?php } // Show if not last page ?> </td>
            </tr>
            <?php } // Show if recordset not empty ?>
            <?php if ($totalRows_rsSas == 0) { // Show if recordset empty ?>
            <tr>
                <td height="55" align="center" valign="middle"><strong style="color:#FF0000;">No record found. </strong></td>
            </tr>
            <?php } // Show if recordset empty ?>
          </table>
</form>
</body>
</head>
</html>
