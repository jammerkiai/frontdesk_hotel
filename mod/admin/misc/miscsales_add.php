<?php
require "../../../ajax/config/config.inc.php";

function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = addslashes($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

$error = "";
$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["save"])) && ($_POST["save"] == "Save")) {
	$rsSasExist = mysql_query("SELECT sas_id FROM sales_and_services WHERE sas_description = ".trim($_POST['sas_description'])."'");
	$rownum_rsSasExist = mysql_num_rows($rsSasExist);
	if ($rownum_rsSasExist == 0) {
	  $insertSQL = sprintf("INSERT INTO sales_and_services (sas_description,  sas_cat_id, sas_amount) VALUES (%s, %s, %s)",
                       GetSQLValueString($_POST['sas_description'], "text"),				
					   GetSQLValueString($_POST['sas_category'], "int"),
					   GetSQLValueString($_POST['price'], "double"));

 	 $Result1 = mysql_query($insertSQL) or die(mysql_error());

 	 $insertGoTo = "miscsales_add.php?strMsg=Record has been successfully added.";
	  if (isset($_SERVER['QUERY_STRING'])) {
 	   $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
 	   $insertGoTo .= $_SERVER['QUERY_STRING'];
 	 }
 	 header(sprintf("Location: %s", $insertGoTo));
  }
  else echo "<script>alert('* SAS description/code entry already Exists.')</script>";
}
elseif ((isset($_POST["back"])) && ($_POST["back"] == "Back")) {
	header("Location:miscsales.php");
} # end insert validate

$rsSasCat = mysql_query("SELECT * FROM sas_category ORDER BY sas_cat_name");
$rownum_rsSasCat = mysql_num_rows($rsSasCat);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Add SAS</title>
<script type="text/javascript" src="../../../js/custom.js"></script>
<link href="../../../css/admin.css" rel="stylesheet" type="text/css" />
</head>

<body>
<form method="post" name="sas_add" id="sas_add" >
<table width="80%" border="0" align="center" cellpadding="3" cellspacing="2">
    <?php if (isset($_REQUEST['strMsg']) && $_REQUEST['strMsg'] != '') { ?>
    <tr>
    	<td valign="middle"><div class="prompt" style="color:#009900; font-size:12px;"><?php echo $_REQUEST['strMsg'] ; ?></div></td>
    </tr>
    <?php } ?>
	<tr>
	  <td valign="top">
			<table border="0" align="left" cellpadding="2" cellspacing="2" class="tablesorter"> 
				<tr align="left" valign="top"><td colspan="2" align="center" class="bgHeader"><strong>ADD SAS</strong></td></tr>      
                <tr align="left" valign="top">
                  <td align="left" width="15%">SAS Description</td>
                  <td><input name="sas_description" type="text" id="sas_description" class="textbox-style" /></td>
                </tr>
				
				<tr align="left" valign="top">
                  <td align="left">SAS Category</td>
                  <td><select name="sas_category" id="sas_category" style="width:200px;" class="textbox-style">
				  		<option value="">[Choose]</option>
				  		<?php 
							if ($rownum_rsSasCat > 0) {
							while($row_rsSasCat = mysql_fetch_assoc($rsSasCat)) {
						?>
				  		<option value="<?=$row_rsSasCat['sas_cat_id']?>"><?=$row_rsSasCat['sas_cat_name']?></option>
						<?php } }?>
						</select>				  </td>
                </tr>
				<tr align="left" valign="top">
                  <td align="left" width="15%">Price</td>
                  <td><input name="price" type="text" class="textbox-style" id="price" style="width:300px;" /></td>
                </tr>
 
                <tr align="left" valign="top">
                  <td>&nbsp;</td>
                  <td align="right"><input name="save" type="submit" class="buttons" id="Save" onclick="YY_checkform('sas_add','sas_description','#q','0','Field SAS description is required.','sas_category','#q','0','Field SAS Category is required.','price','#1_99999','1','Field Price is required and must be a number.');return document.MM_returnValue" value="Save" />&nbsp;<input name="back" type="submit" id="Back" value="Back" class="buttons" />				                </td>
                </tr>
          </table>
		</td>
	</tr>
</table>
</form>
</body>
</html>
