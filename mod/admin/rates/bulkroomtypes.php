<?php
require "../../../ajax/config/config.inc.php";
require_once('ratefns.php');

$rtid = $_GET['rtid'];
$cmd = $_GET['cmd'];

if ($cmd == 'rates') {
	header('location: rates.php');
} elseif ($cmd == 'discounts') {
	header('location: ../mastertable/discount.php');
} elseif ($cmd == 'room types') {
	header('location: ../mastertable/room_type.php');
} elseif ($cmd =='save') {
	$rtid = $_GET['new_room_type_id'];
	if (isset($_GET['roomid']) && is_array($_GET['roomid'])) {
		$rooms = implode(',' , $_GET['roomid']);
		$sql = "update rooms set room_type_id=$rtid where room_id in ($rooms)";
		mysql_query($sql);
		header('location: bulkroomtypes.php?rtid=' . $_GET['new_room_type_id']);
	} else {
		$mesg = 'Please select at least one item.';
		$mesg.= "<br><a href='bulkroomtypes.php?rtid=".$_GET['new_room_type_id']."'>Back</a>";
		die($mesg);
	}
}



?>
<script src="jquery.js"></script>
<link  type="text/css" rel="stylesheet" href="style.css" />
<form method="get" action="bulkroomtypes.php">
<?php 
if ($cmd == 'new') {
	echo buildButtons(array('save', 'cancel'));
	echo "<h2>New Discount per Room Type</h2>";
	echo 'Selected Room Type: ' . ddRoomtypes($_GET['new_room_type_id']);
 	echo '<br>Select Rate: ' . ddRates() ;
	echo '<br>Select Discount: ' . ddDiscounts();
} elseif ($cmd == 'edit') {
	echo buildButtons(array('update', 'cancel'));
	echo "<h2>Edit Rate per Room Type</h2>";
	if( is_array($_GET['rtr_id'])) {
		$rtrid = implode(',',$_GET['rtr_id']); 
	} else {
		$rtrid = $_GET['rtr_id'][0];
	}
	echo frmRoomTypeRates(getRtrRecord($rtrid));
} else {
	echo buildButtons(array('save', 'rates', 'discounts', 'room types'));
	echo "<h2>Rooms per Room Type</h2>";
	echo "Select Room Type: ". ddRoomtypes($rtid);
	if ($rtid != '') {
		echo listRooms($rtid);
	}
}
?>
</form>
<!--//
javascript section
 //-->
<script>
$(document).ready(function(){
	$('#cbmaster').click(function(){
		$('.cbitem').attr('checked', $(this).attr('checked'));
	});
	$('#new_room_type_id').change(function(){
		document.location.href = 'bulkroomtypes.php?rtid=' + $(this).val();
	});
	$('#delete').click(function(){
		return confirm('Delete selected item(s)?');
	});
});
</script>