<?php
require "../../../ajax/config/config.inc.php";
require_once('ratefns.php');

$rtid = $_GET['rtid'];
$cmd = $_GET['cmd'];

if ($cmd == 'rates') {
	header('location: rates.php');
} elseif ($cmd == 'discounts') {
	header('location: ../mastertable/discount.php');
} elseif ($cmd == 'room types') {
	header('location: ../mastertable/room_type.php');
} elseif ($cmd =='save') {
	$rtid = $_GET['new_room_type_id'];
	$did = $_GET['new_discount_id'];
	$rid = $_GET['new_rate_id'];
	$sql = "insert into room_type_discounts (room_type_id, rate_id, discount_id) 
		values ('$rtid', '$rid', '$did' ) ";
	mysql_query($sql);
	header('location: roomdiscounts.php?rtid=' . $_GET['new_room_type_id']);
}elseif ($cmd =='remove') {
	if (isset($_GET['rate_disc'])) {
		if( is_array($_GET['rate_disc'])) {
			$ratedisc = $_GET['rate_disc'][0]; 
		} else {
			$rateid = $_GET['rate_disc'];
		}
		
		list($rate, $disc)=explode("_", $rateid);
		
		$rtid = $_GET['new_room_type_id'];
		$sql = "delete from room_type_discounts 
			where room_type_id=$rtid and rate_id=$rate and discount_id=$disc";
		mysql_query($sql);
		header('location: roomdiscounts.php?rtid=' . $_GET['new_room_type_id']);
	} else {
		$mesg = 'Please select at least one item.';
		$mesg.= "<br><a href='roomrates.php?id=".$_GET['new_rate_id']."'>Back</a>";
		die($mesg);
	}
}



?>
<script src="jquery.js"></script>
<link  type="text/css" rel="stylesheet" href="style.css" />
<form method="get" action="roomdiscounts.php">
<?php 
if ($cmd == 'new') {
	echo buildButtons(array('save', 'cancel'));
	echo "<h2>New Discount per Room Type</h2>";
	echo 'Selected Room Type: ' . ddRoomtypes($_GET['new_room_type_id']);
 	echo '<br>Select Rate: ' . ddRates() ;
	echo '<br>Select Discount: ' . ddDiscounts();
} elseif ($cmd == 'edit') {
	echo buildButtons(array('update', 'cancel'));
	echo "<h2>Edit Rate per Room Type</h2>";
	if( is_array($_GET['rtr_id'])) {
		$rtrid = implode(',',$_GET['rtr_id']); 
	} else {
		$rtrid = $_GET['rtr_id'][0];
	}
	echo frmRoomTypeRates(getRtrRecord($rtrid));
} else {
	echo buildButtons(array('new', 'remove', 'rates', 'discounts', 'room types'));
	echo "<h2>Discounts per Room Type</h2>";
	echo "Selected Room Type: ". ddRoomtypes($rtid);
	if ($rtid != '') {
		echo listRoomTypeDiscounts($rtid);
	}
}
?>
</form>
<!--//
javascript section
 //-->
<script>
$(document).ready(function(){
	$('#cbmaster').click(function(){
		$('.cbitem').attr('checked', $(this).attr('checked'));
	});
	$('#new_room_type_id').change(function(){
		document.location.href = 'roomdiscounts.php?rtid=' + $(this).val();
	});
	$('#delete').click(function(){
		return confirm('Delete selected item(s)?');
	});
});
</script>