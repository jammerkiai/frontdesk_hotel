#clear db for new installation
truncate table access_logs;
truncate table card_payment_details;
truncate table discount_log;
truncate table fnb_sales;
truncate table guest_history;
truncate table guest_requests;
truncate table occupancy;
truncate table occupancy_log;
truncate table occupancy_guests;
truncate table room_log;
truncate table room_sales;
truncate table salesreceipts;
truncate table security_receivables;
truncate table shift_transactions;
update rooms set status=1;